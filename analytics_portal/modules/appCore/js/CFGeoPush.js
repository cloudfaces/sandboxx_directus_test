/*v1.0.0*/
$.extend(CloudFaces.GeoPush, {
    init: true,

    loadFences: function() {
        $.ajax({
            url: CloudFaces.GeoPush.Config.fencesUrl + CloudFaces.Config.app + '/',
            type: 'GET',
            dataType: 'json',
            success: function(fences) {
                if (fences && fences.Status === 'Success' && fences.Data) {
                    CloudFaces.GeoPush.addRemoveFences(fences.Data);
                }
            }
        });
    },

    addRemoveFences: function(fences) {
        if (typeof fences !== 'undefined' && $.isArray(fences) && fences.length > 0) {
            CFLocation.getFences(function(items) {
                $.each(items, function(i, item) {
                    CFLocation.removeFence(item.id);
                });

                $.each(fences, function(i, item) {
                    var fence = {
                        lat: item.fence_lat,
                        long: item.fence_long,
                        id: item.id,
                        radius: item.fence_radius
                    };

                    CFLocation.addFence(fence);
                });

                var appId = CloudFaces.Config.app;
                var ain = CloudFaces.DeviceCode.getDeviceAin();
                var os = isMobile.Android() ? 'android' : (isMobile.iOS() ? 'ios' : 'web');

                CFLocation.setFencingUrl(CloudFaces.GeoPush.Config.fenceLogUrl(appId, ain, os));
                CFLocation.getCurrentPosition(function(pos){
                    // $('body .content').append('<br />position: ' + JSON.stringify(pos));
                });
            });
        }
    },

    events: function() {
        if (CloudFaces.GeoPush.init) {
            CloudFaces.GeoPush.init = false;
        }
    }
});
$(document).on('cf-initialized', function() {
    CloudFaces.GeoPush.events();
    CloudFaces.GeoPush.loadFences();
});
