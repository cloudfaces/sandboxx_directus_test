CloudFaces.Caching = function(list_id, itemId, inx, time, alive, userToken) {
	return new function() {
		// PARAMETERS
		var getUserToken =
			CloudFaces.Helpers.getUserToken() != ''
				? CloudFaces.Helpers.getUserToken()
				: '';

		var parent = this;
		this.list_id = list_id || 0;
		this.inx = inx || '_trash_';
		this.time = time || 0;
		this.itemId = itemId || '';
		this.userToken = userToken || getUserToken;
		if (alive) {
			this.storage = CFPersistentStorage;
		} else {
			this.storage = CFVariableStorage;
		}

		// METHODS
		this.getCMS = function(caller) {
			var token = parent.userToken;
			if (token == undefined) {
				token = '';
			}
			CloudFaces.API.list.getItems(
				CloudFaces.Config.app,
				parent.list_id,
				token,
				function(items) {
					caller(items);
				}
			);
		};

		this.getElementCMS = function(caller) {
			var itemId = parent.itemId;
			if (itemId == undefined) {
				itemId = '';
			}
			var token = parent.userToken;
			if (token == undefined) {
				token = '';
			}

			CloudFaces.API.list.getItem(
				CloudFaces.Config.app,
				parent.list_id,
				itemId,
				token,
				function(items) {
					caller(items);
				}
			);
		};

		this.needUpdate = function() {
			if (parent.time == 0) return 'cms';
			return true;
		};

		this.getFirst = function(callback) {
			if (
				!parent.needUpdate() &&
				parent.storage.valueExists(parent.inx) &&
				parent.storage.readValue(parent.inx)
			) {
				var data = JSON.parse(parent.storage.readValue(parent.inx));
				if (data.length > 0) {
					parent.callback(data[0]);
				} else {
					parent.callback([]);
				}
			} else {
				parent.getCMS(function(items) {
					if (
						typeof items != 'undefined' &&
						!items.hasOwnProperty('message_long')
					) {
						if (items.code != 'no_internet') {
							if (typeof items != 'undefined' && items.length > 0) {
								var item = items[0];
								parent.storage.writeValue(parent.inx, JSON.stringify(items));
								CloudFaces.Offline.set(parent.inx, JSON.stringify(items));
								callback(item);
							} else {
								callback([]);
							}
						} else {
							callback(CloudFaces.Offline.get(parent.inx));
						}
					} else {
						callback([]);
						CloudFaces.Offline.showPopupNoInternet(
							CloudFaces.Language.translate('need_login'),
							CloudFaces.Language.translate('no_internet_understand')
						);
					}
				});
			}
		};
		this.getLast = function(callback) {
			if (
				!parent.needUpdate() &&
				parent.storage.valueExists(parent.inx) &&
				parent.storage.readValue(parent.inx)
			) {
				var data = JSON.parse(parent.storage.readValue(parent.inx));
				if (data.length > 0) {
					parent.callback(data[0]);
				} else {
					parent.callback([]);
				}
			} else {
				parent.getCMS(function(items) {
					if (
						typeof items != 'undefined' &&
						!items.hasOwnProperty('message_long')
					) {
						if (items.code != 'no_internet') {
							if (typeof items != 'undefined' && items.length > 0) {
								var lastEl = items.length - 1;
								var item = items[lastEl];
								parent.storage.writeValue(parent.inx, JSON.stringify(items));
								CloudFaces.Offline.set(parent.inx, JSON.stringify(items));
								callback(item);
							} else {
								callback([]);
							}
						} else {
							callback(CloudFaces.Offline.get(parent.inx));
						}
					} else {
						callback([]);
						CloudFaces.Offline.showPopupNoInternet(
							CloudFaces.Language.translate('need_login'),
							CloudFaces.Language.translate('no_internet_understand')
						);
					}
				});
			}
		};

		this.getAll = function(callback) {
			if (
				!parent.needUpdate() &&
				parent.storage.valueExists(parent.inx) &&
				parent.storage.readValue(parent.inx)
			) {
				var data = JSON.parse(parent.storage.readValue(parent.inx));
				if (data.length > 0) {
					callback(data);
				} else {
					callback([]);
				}
			} else {
				parent.getCMS(function(items) {
					if (
						typeof items != 'undefined' &&
						!items.hasOwnProperty('message_long')
					) {
						if (items.code != 'no_internet') {
							if (typeof items != 'undefined' && items.length > 0) {
								parent.storage.writeValue(parent.inx, JSON.stringify(items));
								CloudFaces.Offline.set(parent.inx, JSON.stringify(items));
								callback(items);
							} else {
								callback([]);
							}
						} else {
							callback(CloudFaces.Offline.get(parent.inx));
						}
					} else {
						callback([]);
						CloudFaces.Offline.showPopupNoInternet(
							CloudFaces.Language.translate('need_login'),
							CloudFaces.Language.translate('no_internet_understand')
						);
					}
				});
			}
		};

		this.getElement = function(callback) {
			if (
				!parent.needUpdate() &&
				parent.storage.valueExists(parent.inx) &&
				parent.storage.readValue(parent.inx)
			) {
				var data = JSON.parse(parent.storage.readValue(parent.inx));
				if (data.length > 0) {
					callback(data);
				} else {
					callback([]);
				}
            } else {
                parent.getElementCMS(function(items) {

                    if (typeof items != 'undefined' && !items.hasOwnProperty('message_long')) {

                        if (items.code != 'no_internet') {
                                parent.storage.writeValue(parent.inx, JSON.stringify(items));
                                CloudFaces.Offline.set(parent.inx, JSON.stringify(items));
                                callback(items);
                            } else {
                            callback(CloudFaces.Offline.get(parent.inx));
                        }
                    } else {
                        callback([]);
                        CloudFaces.Offline.showPopupNoInternet(CloudFaces.Language.translate('need_login'), CloudFaces.Language.translate('no_internet_understand'))
                    }
                });
            }
		};
	}();
};

CloudFaces.Offline = {
	addPopup: false,

	get: function(paramName) {
		if (CFPersistentStorage.valueExists(paramName)) {
			CloudFaces.Offline.showPopupNoInternet(
				CloudFaces.Language.translate('check_internet'),
				CloudFaces.Language.translate('no_internet_understand')
			);
			return JSON.parse(CFPersistentStorage.readValue(paramName));
		}
	},

	set: function(paramName, params) {
		CFPersistentStorage.writeValue(paramName, params);
	},

	showPopupNoInternet: function(infotext, textButtn) {
		if (!CloudFaces.Offline.addPopup) {
			var dirPrefix = '/modules/';
			if (location.pathname.indexOf('modules') >= 0) {
				dirPrefix = '../';
			}
			$('body').append(
				'<div class="no-internet-popup">' +
					'<div class="no-internet-inner">' +
					'<div class="no-internet-icon">' +
					'<img src="' +
					dirPrefix +
					'appCore/images/warning.png" alt="" />' +
					'</div>' +
					'<div class="no-internet-warning-text">' +
					infotext +
					'</div>' +
					'<div class="understand">' +
					textButtn +
					'</div>' +
					'</div>' +
					'</div>'
			);
			CloudFaces.Offline.addPopup = true;
			$(document).on('click', '.understand', function() {
				$('.no-internet-popup').remove();
				CloudFaces.Offline.addPopup = false;
			});
		}
	}
};
