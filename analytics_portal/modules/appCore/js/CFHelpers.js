CloudFaces.Helpers = {
    clearStorage: function(key) {
        if (key) {
            CFPersistentStorage.writeValue(key, '');
        } else {
            CFPersistentStorage.writeValue('checkVersion', '');
            CFPersistentStorage.writeValue('promotions', '');
            CFPersistentStorage.writeValue('welcomeMessage', '');
            CFPersistentStorage.writeValue('getAIN', '');
            CFPersistentStorage.writeValue('device_code', '');
            CFPersistentStorage.writeValue('device_ain', '');
            CFPersistentStorage.writeValue('checkVersion', '');
            CFPersistentStorage.writeValue('checkedNotificationsfromPopup', '');
            CFPersistentStorage.writeValue(CloudFaces.Config.app + '_checkVersion', '');
            CFPersistentStorage.writeValue(CloudFaces.Config.app + '_promotions', '');
            CFPersistentStorage.writeValue(CloudFaces.Config.app + '_welcomeMessage', '');
            CFPersistentStorage.writeValue(CloudFaces.Config.app + '_getAIN', '');
            CFPersistentStorage.writeValue(CloudFaces.Config.app + '_device_code', '');
            CFPersistentStorage.writeValue(CloudFaces.Config.app + '_device_ain', '');
            CFPersistentStorage.writeValue(CloudFaces.Config.app + '_checkVersion', '');
            CFPersistentStorage.writeValue(CloudFaces.Config.app + '_checkedNotificationsfromPopup', '');
        }
    },

    dateDBFormat: function(date) {
        var parts = (date).split(/[- :]/);

        for (var i in parts) parts[i] = parseInt(parts[i]);
        var new_date = new Date(parts[0], parts[1] - 1, parts[2], parts[3], parts[4]);

        return new_date;
    },

    twoDigits: function(d) {
        if (0 <= d && d < 10) return "0" + d.toString();
        if (-10 < d && d < 0) return "-0" + (-1 * d).toString();
        return d.toString();
    },

    toMysqlFormat: function(date) {
        return date.getFullYear() + "-" + CloudFaces.Helpers.twoDigits(1 + date.getMonth()) + "-" + CloudFaces.Helpers.twoDigits(date.getDate()) + " " + CloudFaces.Helpers.twoDigits(date.getHours()) + ":" + CloudFaces.Helpers.twoDigits(date.getMinutes()) + ":" + CloudFaces.Helpers.twoDigits(date.getSeconds());
    },

    getMonthByInx: function(inx) {
        window.months = window.months || [];
        if (window.months.length == 0) {
            window.months = [
                CloudFaces.Language.translate('january'),
                CloudFaces.Language.translate('february'),
                CloudFaces.Language.translate('march'),
                CloudFaces.Language.translate('april'),
                CloudFaces.Language.translate('may'),
                CloudFaces.Language.translate('june'),
                CloudFaces.Language.translate('july'),
                CloudFaces.Language.translate('august'),
                CloudFaces.Language.translate('september'),
                CloudFaces.Language.translate('october'),
                CloudFaces.Language.translate('november'),
                CloudFaces.Language.translate('december')
            ]
        }

        return typeof window.months[inx] != 'undefined' ? window.months[inx] : '';
    },

    createThumb: function(image, width, height) {
        if (image) {
            var thumb = CloudFaces.Config.timThumbUrl + image;
            if (typeof width != 'undefined' && width) {
                thumb += '&w=' + width;
            }
            if (typeof height != 'undefined' && height) {
                thumb += '&h=' + height;
            }

            return thumb
        } else {
            return '<img src="images/missing_image.png" alt="' + CloudFaces.Language.translate('missing_image') + '" />'
        }
    },

    getHours: function(date) {
        return (date.getHours() < 10 ? '0' : '') + date.getHours();
    },

    getDate: function(date) {
        return (date.getDate() < 10 ? '0' : '') + date.getDate();
    },

    getMinutes: function(date) {
        return (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
    },

    getMonth: function(date) {
        return ((date.getMonth() + 1) < 10 ? '0' : '') + (date.getMonth() + 1);
    },

    showLoader: function() {
        $('#loader').show();
    },

    hideLoader: function() {
        $('#loader').hide();
    },

    cutText: function(text, bufferLen, allowedLen) {
        var buffer = bufferLen || 20,
            allowed_length = allowedLen || 50;

        if (typeof text != 'undefinde' && text) {
            if (text.length <= (allowed_length + buffer)) {
                return text;
            } else {
                var left = text.substr(0, allowed_length);
                var right = text.substr(allowed_length, text.length).split(/\s+/g)[0];
                return left + (typeof right != 'undefined' && right ? right : '') + '...';
            }
        }

        return '';
    },

    navigateTo: function(coords) {
        var navigateUrl = 'http://maps.google.com/maps?daddr=' + coords;
        if ($('body').hasClass('ios')) {
            navigateUrl = 'maps://maps.google.com/maps?daddr=' + coords;
        }

        location.href = navigateUrl;
    },
    phoneCall: function(phone) {
        CloudFaces.Helpers.showLoader();
        if (phone != undefined) {
            CloudFaces.Helpers.hideLoader();
            window.location = 'tel: ' + phone;
        } else {
            CloudFaces.API.option.get(CloudFaces.Config.app, CloudFaces.Config.options.contactCallPhone, function(phone) {
                var NativePhone = phone;
                CloudFaces.Helpers.hideLoader();
                window.location = 'tel: ' + NativePhone;
            });
        }
    },

    getUserData: function(callBack) {
        if (CFPersistentStorage.valueExists(CloudFaces.Config.app + '_userData')) {
            if (CFPersistentStorage.readValue(CloudFaces.Config.app + '_userData') != null) {
                if (CFPersistentStorage.readValue(CloudFaces.Config.app + '_userData') != '') {
                    return JSON.parse(CFPersistentStorage.readValue(CloudFaces.Config.app + '_userData'))
                } else {
                    return '';

                }
            } else {
                return '';
            }
        } else {
            return '';
        }
    },

    getUserToken: function(callBack) {
        if (CFPersistentStorage.valueExists(CloudFaces.Config.app + '_userData')) {
            if (CFPersistentStorage.readValue(CloudFaces.Config.app + '_userData') != null) {
                if (CFPersistentStorage.readValue(CloudFaces.Config.app + '_userData') != '') {
                    var userData = JSON.parse(CFPersistentStorage.readValue(CloudFaces.Config.app + '_userData'));
                    return userData.token;
                } else {
                    return '';

                }
            } else {
                return '';
            }
        } else {
            return '';
        }
    },

    checkLogin: function(callBack) {
        var refreshIntervalId = setInterval(function() {
            if (CloudFaces.Helpers.getUserData() != '') {
                clearInterval(refreshIntervalId);
                callBack(true);
            }
            callBack(false);
        }, 1000);
    },

    checkWebSite: function(callBack) {
        if (!isMobile.Android() && !isMobile.iOS()) {
            return true;
        } else {
            return false;
        }
    }



};
