CloudFaces.Transition = function( options ) {

	this.$container = null;
	this.transitions = {};
	this.animation_duration = '0';
	this.duration_modifier = 1;
	var that = this;
	
	// private variables and validation
	if ( options.container === undefined || ! options.container ) {
		return null;
	} else {
		this.$container = options.container;
	}
	
	if ( options.animation_duration === undefined || ! options.animation_duration ) {
		this.animation_duration = '500';
	} else {
		this.animation_duration = options.animation_duration;
	}
	
	if ( options.duration_modifier === undefined || options.duration_modifier < 0 ) {
		this.duration_modifier = 1;
	} else {
		this.duration_modifier = options.duration_modifier;
	}
	
	var available_directions = [ 'X', 'Y', 'Z' ];
	var user_directions = {};
	
	
	if ( typeof this.$container.css( 'transform' ) != 'undefined' ) {
		this.user_reset_directions = this.$container.css( 'transform' );
	} else if ( typeof this.$container[0].style[ transform ] != 'undefined' ) {
		this.user_reset_directions = this.$container[0].style[ transform ];
	} else {
		$.each( available_directions, function( inx, value ) {
			this.user_reset_directions[ inx.toUpperCase() ] = value;
		});
	}
	
	if ( typeof options.transitions == 'object' && options.transitions ) {
		$.each( options.transitions, function( inx, value ) {
			if ( available_directions.indexOf( inx ) > -1 ) {
				inx = inx + '';
				user_directions[ inx.toUpperCase() ] = value;
			}
		});
	} else {
		return null;
	}
	
	this.user_directions = user_directions;
	
	// Private methods
	if (typeof this.getBrowserEnginePrefix != 'function' ) {
		CloudFaces.Transition.getBrowserEnginePrefix = function () {
			var transition, vendor, i;
			transition = "Transition";
			vendor = ["Moz", "Webkit", "Khtml", "O", "ms"];
			i = 0;
			while (i < vendor.length) {
				if (typeof document.body.style[vendor[i] + transition] === "string") {
					return { css: vendor[i] };
				}
				i++;
			}
			return false;
		}
	}
	
	if (typeof this.transition != 'function' ) {
		CloudFaces.Transition.prototype.transition = function ( callback, operaton ) {
			var prefix, prefix_name = '', transform, duration, timing, animation_timing_function = "linear", transform_methods = '';
			
			// Select the css code based on browser engine
			prefix = CloudFaces.Transition.getBrowserEnginePrefix();
			if ( typeof prefix != 'undefined' && typeof prefix.css != 'undefined' ) {
				prefix_name = prefix.css;
			}
			transform = prefix_name + "Transform";
			duration = prefix_name + "TransitionDuration";
			timing = prefix_name + "TransitionTimingFunction";

			// Set style to activate the slide transition
			that.$container[0].style[duration] = ( that.animation_duration * that.duration_modifier ) + 'ms';
			that.$container[0].style[timing] = animation_timing_function;
			
			$.each( that.user_directions, function( inx, value ) {
				transform_methods += 'translate' + inx + '(' + value + ')' + ' ';
			});
			
			if ( operaton == 'start' ) {
				that.$container[0].style[transform] = transform_methods;
			} else {
				that.$container[0].style[transform] = that.user_reset_directions;
			}
			
			that.$container.on("transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd", function () {
				if ( typeof callback === 'function' && callback ) {
					callback();
				}
				
				that.$container.unbind("transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd");
			});
		}
	}	
	
	// Public methods
	return new (function() {
		this.start = function( callback ) {
			that.transition( callback, 'start' );
		}
		
		this.reset = function( callback ) {
			that.transition( callback, 'reset' );
		}
	});
}