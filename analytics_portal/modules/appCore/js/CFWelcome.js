$.extend(CloudFaces.Welcome, {
    init: true,

    container: '',

    showWelcomeMessage: function(container) {
        /*Open Welcome Popup*/
        var $welcome = $('<div class="welcome"><div class="welcome_box"></div></div>');
        var $welcomeBox = $welcome.find('.welcome_box');
        $(container).each(function() {
            $(this).append($welcome);

            new CloudFaces.Caching(CloudFaces.Welcome.Config.tables.welcome, 'list_welcome', 0, false).getAll(function(welcome) {
                if (typeof welcome != 'undefined' && welcome.length > 0) {
                    $.each(welcome, function(i, item) {
                        function ShowMesegeWelcome(welcome_message) {
                            if (item.visible == '1' && welcome_message != CloudFaces.Config.app + '_success') {

                                var size = ($(document).width() / 2);
                                var image = CloudFaces.API.url(item.logo);
                                var thumb = item.logo ? CloudFaces.Helpers.createThumb(image, size) : '';
                                var urlLink = '';

                                if (CloudFaces.LayoutPromotions && CloudFaces.LayoutPromotions.checkPromotions()) {
                                    urlLink = '<div class="close_pop_up buttonOk backgroundTemplate textColorButton">' +
                                        CloudFaces.Language.translate('promotions_ok') +
                                        '</div>';
                                }

                                var $element = $(
                                    '<div data-id="' + item.id + '"  class="closeWelcome">' +
                                    '<i class="fa fa-times" aria-hidden="true"></i>' +
                                    '</div>' +
                                    '<div id=""  class="logo_welcome" ><img src="' + thumb + '"></div>' +
                                    '<div class="tittle colorTemplate">' + item["title"] + '</div>' +
                                    '<div class="description ">' +
                                    item['descriptions'] +
                                    '</div>' +
                                    urlLink
                                );

                                /***
                                 * add Promotions to Notifications Begin
                                 ***/
                                if (CloudFaces.LayoutPromotions && CloudFaces.LayoutPromotions.hasOwnProperty('addPromotions')) {
                                    CloudFaces.LayoutPromotions.addPromotions($welcomeBox, 2);
                                }

                                $welcomeBox.prepend($element);
                                $welcome.show();
                                $welcome.css('top', '0%');
                            }
                        }
                        setTimeout(function() {
                            var welcome_message = CFPersistentStorage.readValue(CloudFaces.Config.app + '_welcomeMessage');
                            ShowMesegeWelcome(welcome_message);

                        }, 10);
                    });
                }
            });
        });
    },
    events: function() {
        if (CloudFaces.Welcome.init) {
            $(document).on('click', '.close_pop_up, .welcome_box .closeWelcome', function() {
                $('.welcome_box').animate({
                    top: '100%'
                }, 300, function() {
                    $('.welcome').hide();
                });
                CFPersistentStorage.writeValue(CloudFaces.Config.app + '_welcomeMessage', CloudFaces.Config.app + '_success');
            });

            CloudFaces.Welcome.init = false;
        }
    },

});

$(document).on('cf-initialized', function() {
    CloudFaces.Welcome.events();
    CloudFaces.Welcome.showWelcomeMessage('body');
    CloudFaces.Notifications.showPastNotifications('body');


});
