CloudFaces.DeviceCode = {
    api_url: CloudFaces.Config.ApiURL + 'get_device_id/',
    project_id: CloudFaces.Config.app,
    project_hash: CloudFaces.Config.AppHash,
    device_code: '',
    device_ain: '',
    device_token: '',
    os: '',
    getAIN: '',


    init: function() {
        var device_code = CloudFaces.DeviceCode.getDeviceCode();
        var device_ain = CloudFaces.DeviceCode.getDeviceAin();

        if (!device_code || !device_ain) {
            CloudFaces.DeviceCode.set();

            CloudFaces.DeviceCode.application_infos();
        } else {
            CloudFaces.DeviceCode.device_code = device_code;
            CloudFaces.DeviceCode.device_ain = device_ain;

            CloudFaces.DeviceCode.application_infos();
        }
    },

    set: function() {
        var debug = CloudFaces.hasOwnProperty('debug') ? CloudFaces.debug : (window.location.href.indexOf('http') >= 0);

        $.ajax({
            url: this.api_url,
            type: 'GET',
            data: {
                os: CloudFaces.DeviceCode.phoneType(),
                project_id: CloudFaces.Config.app, //this.project_id,
                project_hash: CloudFaces.Config.AppHash, //this.project_hash
                debug: debug
            },
            dataType: 'json',
            success: function(result) {
                if (typeof result == 'object') {
                    //it is object...
                } else {
                    result = JSON.parse(result);
                }
                if (result && result['Status'] == 'Success') {
                    var device_data = result['Data'];
                    if (typeof device_data == 'object') {
                        //it is object...
                    } else {
                        device_data = JSON.parse(device_data);
                    }
                    if (typeof device_data != 'undefined' && device_data) {
                        if (typeof device_data.error != 'undefined' && device_data.error == 0 && typeof device_data.code) {
                            CloudFaces.DeviceCode.device_code = device_data.code;
                            CloudFaces.DeviceCode.device_ain = device_data.ain;
                            CFPersistentStorage.writeValue(CloudFaces.Config.app + '_device_code', device_data.code);
                            CFPersistentStorage.writeValue(CloudFaces.Config.app + '_device_ain', device_data.ain);
                            CFPersistentStorage.writeValue('app_id',  CloudFaces.Config.app);
                            CFPersistentStorage.writeValue('app_hash_url', CloudFaces.Config.ApiURL + 'project/version/project_hash/' + CloudFaces.Config.AppHash +'/');
                            $('.device_ain1').html('AIN : ' + device_data.ain);
                            //CloudFaces.DeviceCode.registerDeviceTokenAin(device_data.ain);
                        }
                    }
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {}
        });
    },

    getDeviceCode: function() {
        var device_code = CFPersistentStorage.valueExists(CloudFaces.Config.app + '_device_code') ? CFPersistentStorage.readValue(CloudFaces.Config.app + '_device_code') : '';

        if (typeof device_code != 'undefined' && device_code != 'undefined' && device_code) {
            CloudFaces.DeviceCode.device_code = device_code;

            return device_code;
        }

        return 0;
    },

    getDeviceAin: function() {
        var device_ain = CFPersistentStorage.valueExists(CloudFaces.Config.app + '_device_ain') ? CFPersistentStorage.readValue(CloudFaces.Config.app + '_device_ain') : '';

        if (typeof device_ain != 'undefined' && device_ain != 'undefined' && device_ain) {
            CloudFaces.DeviceCode.device_ain = device_ain;

            return device_ain;
        }

        return 0;
    },

    getDeviceToken: function() {
        var device_token = CFPersistentStorage.valueExists(CloudFaces.Config.app + '_device_token') ? CFPersistentStorage.readValue(CloudFaces.Config.app + '_device_token') : '';

        if (typeof device_token != 'undefined' && device_token != 'undefined' && device_token) {
            CloudFaces.DeviceCode.device_token = device_token;

            return device_token;
        }

    },

    phoneType: function() {
        if (navigator.userAgent.match(/Android/i)) {
            return 'android';
        } else if (navigator.userAgent.match(/iPhone|iPad|iPod/i)) {
            return 'ios';
        } else {
            return 'web';
        }    },

    application_infos: function() {
        /***
         * ADD VERSION NUMBER AND AIN BEGIN
         ***/
        var ain = CloudFaces.DeviceCode.getDeviceAin();

        var $element = $(
            '<div class="device_info">' +
            '<div>' +
            '<span class="device_version">' +
            'v. : ' +
            (CloudFaces.Version ? CloudFaces.Version.Config.currentVersion : '') +
            '</span>' +
            '      ' +
            '<span class="device_ain1">' +
            'AIN : ' +
            (ain == 0 ? '' : ain) +
            '</span>' +
            '</div>' +
            '</div>'
        );

        $('.ip-header').append($element);
    },

    registerDeviceTokenAin: function(ain) {
        if (CloudFaces.debug) {
            var token = 'APA91bHQFZxOwrh3PUNHhoOnRGLsVmYFa6OCxrZ_vEmtF6reM2v4EHmtTZnbHxjuh8N0Tiss4alXydXxBuJqzNnDeiz8daoBuK7UhsadZVMfubfogCSaM45GUsjA0hUjktoPooMJaEGY';
            //Lyubo: APA91bHVOv2fszXMuU6Kx4g7syb9D5mfDd2LsEkGIl2L9CydjCBhbKE2pn1y7inj47hSRgEzzmuacLJF3BXddZNOq__EKP8qnCwG2Gf7BlyyB36MMtIdO3ojnE7F44RW7WbcJxCmJF61
            //Alex: APA91bHQFZxOwrh3PUNHhoOnRGLsVmYFa6OCxrZ_vEmtF6reM2v4EHmtTZnbHxjuh8N0Tiss4alXydXxBuJqzNnDeiz8daoBuK7UhsadZVMfubfogCSaM45GUsjA0hUjktoPooMJaEGY

            if (isMobile.iOS()) {
                token = '660a1ac471ed0ee29e7c566a7031d0d8df5683bcd675447ac6d127b25d65c596';
                //Ned: 1eb1c183ab516b630d0cc8dbd9bfa4e4f88260c46e0440678ebefdf1c6996a7b
                //Lyubo: 660a1ac471ed0ee29e7c566a7031d0d8df5683bcd675447ac6d127b25d65c596
            }

            if (token && ain) {
                CloudFaces.API.registerAppWithToken(token.replace(/\s/g, ''), ain, function(a) {});
            }
        } else {
            token = CFPushNotifications.getToken();

            if (token != '' && ain != '' && ain != 0 && token != ain) {
                CloudFaces.API.registerAppWithToken(token.replace(/\s/g, ''), ain, function(a) {});
                CFPersistentStorage.writeValue(CloudFaces.Config.app + '_device_token', token.replace(/\s/g, ''));
            }
        }
    }
}

$(document).on('cf-ready', function() {
    var getAINKey = CloudFaces.Config.app + '_getAIN';

    CloudFaces.DeviceCode.application_infos();
    CloudFaces.DeviceCode.getAIN = CFPersistentStorage.valueExists(getAINKey) && CFPersistentStorage.readValue(getAINKey) !== '' ? CFPersistentStorage.readValue(getAINKey) : '';
    if (CloudFaces.DeviceCode.getAIN !== 'yes') {
        CFPersistentStorage.writeValue(getAINKey, 'yes');
        CloudFaces.DeviceCode.init();
    }


    var ain = CloudFaces.DeviceCode.getDeviceAin();
    if (typeof ain !== 'undefined' && ain !== '') {
        CloudFaces.DeviceCode.registerDeviceTokenAin(ain);
    }
});