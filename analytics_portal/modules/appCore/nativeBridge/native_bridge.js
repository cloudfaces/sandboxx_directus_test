(function() {
	document.addEventListener('DOMContentLoaded', function(event) {
		var src, cssClass;
		var scr = document.createElement('script');
		console.log('DOM fully loaded and parsed');
		var dirPrefix = '';
		// var dirPrefix = '../';
		if (location.pathname.indexOf('modules') >= 0) {
			dirPrefix = '../../';
		}

		src = dirPrefix + 'modules/appCore/nativeBridge/native_bridge_web.js';
		cssClass = 'web';

		scr.setAttribute('src', src);
		document.body.className = (document.body.className + ' ' + cssClass).trim();
		document.head.appendChild(scr);
	});
})();