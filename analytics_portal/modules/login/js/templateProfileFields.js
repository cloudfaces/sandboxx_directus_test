$.extend(CloudFaces.Login, {
    Type: {
        radioBttns: function(userData, key, templateData, callback) {
            // ******************  THIS IS HACK FOR UNSUPPORTED MULTILINGUAL FILDS IN PROFILE TEMPLATES  ****************** //
            var Fakefields = templateData.text_in_fields.split(',')
            var allLang = CloudFaces.Config.languages;

            for (var curent in allLang) {
                var p = allLang.indexOf(CloudFaces.Language.get());

                if (allLang[curent] == CloudFaces.Language.get()) {
                    for (var l in Fakefields) {
                        var fields = Fakefields[p].split('| ');
                        if (l == p) {
                            console.log('l: ', l)
                            console.log('filds: ', fields)
                            var userSelect = userData[key] ? userData[key].split('| ') : '';
                            var a = '<div class="box-element radio">'
                            a += '<div class="' + key + '">' + CloudFaces.Language.translate(key) + ':</div>';

                            for (var i = 0; i < fields.length; i++) {
                                var checkedEl = '';
                                userSelect.indexOf(fields[i]) >= 0 ? checkedEl = 'checked' : '';

                                a += '<input type="radio" ' + checkedEl + ' id="' + fields[i] + '" name="' + key + '" value="' + fields[i] + '">';
                                a += '<label for="' + fields[i] + '">' + fields[i] + '</label>';
                                if (i == fields.length - 1) {
                                    a += '</div>'
                                    callback(a)
                                }
                            }
                        }
                    }
                }
            }
        },
        checkBoxBttns: function(userData, key, templateData, callback) {
            var fields = templateData.text_in_fields.split('| ')
            var userSelect = userData[key] ? userData[key].split('| ') : ''
            var a = '<div class="box-element">';
            a += '<div class="' + key + '">' + CloudFaces.Language.translate(key) + ':</div>';

            for (var i = 0; i < fields.length; i++) {
                var checkedEl = '';
                userSelect.indexOf(fields[i]) >= 0 ? checkedEl = 'checked' : '';
                a += '<input type="checkbox" ' + checkedEl + ' id="' + fields[i] + '" name="' + key + '" value="' + fields[i] + '">';
                a += '<label class="multipleChoice" for="' + fields[i] + '">' + fields[i] + '</label>';
                if (i == fields.length - 1) {
                    a += '</div>'
                    callback(a)
                }
            }
        },
        imageBttns: function(userData, key, templateData, formCamera, isLocal, base64, callback) {
            var checkData = userData[key] != null && userData[key] != '';
            if (formCamera) {
                checkData = true
            }
            $('.box-element-profile').remove();
            var a = '<div class="box-element-profile">';
            if (checkData) {
                var profileImage = '',
                deleteUrl = '',
                thumb,
                img = '';
                if (formCamera) {
                    if (isLocal) {
                        thumb = userData
                        img = userData
                    } else {
                        img = userData
                        thumb = 'data:image/jpeg;base64,' + base64;
                    }
                    deleteUrl = userData;
                } else {
                    profileImage = userData[key];
                    var width = $(document).width() * 2,
                        image = CloudFaces.API.url(profileImage);
                        thumb = CloudFaces.Helpers.createThumb(image, width);
                        img = thumb;
                }

                a += '<div class="profile-iamge" style="background:url(' + thumb + ')">';
                a += '<img src="' + img + '" alt="">'
                a += '</div>';
                a += '<div class="profile-iamge-inner" data-url="' + deleteUrl + '" data-name="' + key + '" style="background:url(' + thumb + ')">';
                a += '</div>';
                a += '<div class="image-bttns">';
                a += '<div class="change-iamge"   data-name="' + key + '">' + CloudFaces.Language.translate('change_image') + '</div>';
                a += '<div class="remove-image" data-url="' + deleteUrl + '">' + CloudFaces.Language.translate('remove_image') + '</div>';
                a += '</div>';
                a += '</div>';
                callback(a)
            } else {
                a += '<div class="profile-iamge" style="background:url(images/profile.png)">';
                a += '</div>';
                a += '<div class="profile-iamge-inner" style="background:url(images/profile.png)">';
                a += '</div>';
                a += '<div class="image-bttns">'
                a += '<div class="addImage"  data-name="' + key + '"  >' + CloudFaces.Language.translate('add_image') + '</div>';
                a += '</div>';
                a += '</div>';
                callback(a)
            }
        }
    }
})
