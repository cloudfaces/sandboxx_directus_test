/*v1.0.4*/
$.extend(CloudFaces.Login, {
	init: true,
	newArrayForSaveprofile: [],
	loginPage: {
		loadContent: function(container) {
			$('#ip-container').each(function() {
				if (
					typeof CFNavigation.getNavigationContext() != 'undefined' &&
					CFNavigation.getNavigationContext()
				) {
					CloudFaces.Helpers.showLoader();
					var header = $.parseJSON(CFNavigation.getNavigationContext());
					if (typeof header['hide'] != 'undefined' && header['hide']) {
						$('.ip-header')
							.hide()
							.remove();
						CloudFaces.Helpers.hideLoader();
					}
				}

				
			});

			var $container = $(container);
			var element = $(
				//'<form-signin>' + CloudFaces.Language.translate('login_welcomeMsg') + '</h1>' +
				'<h1><img src="images/logo.png" alt=""></h1>' +
					'<label>' +
					CloudFaces.Language.translate('email') +
					':' +
					'<input class="email-input" type="text"/>' +
					'<span class="err-taken-mail">' +
					CloudFaces.Language.translate('not_valide_email') +
					'</span>' +
					'</label>' +
					'<label>' +
					CloudFaces.Language.translate('password') +
					':' +
					'<input type="password"/>' +
					'<span class="err-taken-password">' +
					CloudFaces.Language.translate('add_password') +
					'</span>' +
					'</label>' +
					'<button class="login-btn">' +
					CloudFaces.Language.translate('analytics_download') +
					'</button>' 
			);

			$container.append(element);

			$('.fb-login-btn').on('click', function() {
				var perms = 'email';
				CFFacebook.login(perms, function(data) {
					if (data.status == 'success') {
						var fbToken = data.token;
						var sendData = {};
						sendData.fb_token = data.token;
						$.ajax({
							url: CloudFaces.Login.Config.graphApiUrl + data.token,
							type: 'GET',
							dataType: 'json',
							success: function(callbackEmail) {
								var items = {};
								items.email = callbackEmail.email;
								items.user_name = callbackEmail.name;
								items.fb_id = callbackEmail.id;
								items.fb_token = fbToken;

								CloudFaces.Login.registerSaveData(items);
							}
						});
					} else {
						if (data.status == 'error') {
							CFDialogAlert('Error', data.message);
						}
						if (data.status == 'cancel') {
							return;
						}
					}
				});
			});
		}
	},

	registerTermsPage: {
		loadContent: function(container) {
			var $container = $(container);

			element = $(
				'<h1>' +
					CloudFaces.Language.translate('register_terms') +
					'</h1>' +
					'<p>' +
					CloudFaces.Language.translate('register_terms_text') +
					'</p>' +
					'<button onclick="CFNavigation.navigateBack(\'\');">' +
					CloudFaces.Language.translate('back') +
					'</button>'
			);

			$container.append(element);
			new CloudFaces.Analytics('registerTerms', 'RegisterTerms').set(function(
				result
			) {});
		}
	},

	registerPage: {
		loadContent: function(container) {
			var $container = $(container);

			element = $(
				'<label>' +
					CloudFaces.Language.translate('email') +
					':' +
					'<input class="email-input" type="text"/>' +
					'<span class="err-taken-mail">' +
					CloudFaces.Language.translate('not_valide_email') +
					'</span>' +
					'</label>' +
					'<label>' +
					CloudFaces.Language.translate('password') +
					':' +
					'<input class="password-input" type="password"/>' +
					'<span class="err-taken-password">' +
					CloudFaces.Language.translate('add_password') +
					'</span>' +
					'</label>' +
					'<div class="checbox-wrapper">' +
					'<input type="checkbox" id="terms-cbx" name="terms-cbx" value="accepted" />' +
					'<label class="terms-cbx-label" for="terms-cbx"><div>' +
					CloudFaces.Language.translate('register_accept_terms') +
					'<br>' +
					'<span onclick="CFNavigation.navigate(\'' +
					CloudFaces.Login.Config.pages.registerTerms +
					"', '')\">" +
					CloudFaces.Language.translate('register_terms') +
					'</span>' +
					'</div></label>' +
					'</div>' +
					'<button class="register-btn">' +
					CloudFaces.Language.translate('register') +
					'</button>'
			);

			$container.append(element);
			CloudFaces.Login.registerPage.events();
			new CloudFaces.Analytics('register', 'Register').set(function(result) {});
		},
		events: function() {
			$('.register-btn').on('click', function() {
				if ($('#terms-cbx').is(':checked')) {
					$('.terms-cbx-label').removeClass('required');
					if (CloudFaces.Login.checkPassword($('.password-input').val())) {
						var sendData = {
							email: $('.email-input').val(),
							password: $('.password-input').val()
						};

						if (typeof items != 'undefined' && items.length > 0) {
							$('.email-input').addClass('taken-mail');
						} else {
							$('.email-input').removeClass('taken-mail');
							$('.err-taken-password').hide();
							CloudFaces.Login.registerSaveData(sendData);
						}
					}
				} else {
					$('.terms-cbx-label').addClass('required');
				}
			});
		}
	},
	checkPassword: function(password) {
		if (password != '') {
			if (password.length < 3) {
				$('.err-taken-password').show();
				$('.err-taken-password').text(
					CloudFaces.Language.translate('small_password')
				);
			} else {
				$('.err-taken-password').hide();
				return true;
			}
		} else {
			$('.err-taken-password').show();
		}
	},

	registerSaveData: function(sendData) {
		sendData.device_indentifier = CloudFaces.DeviceCode.getDeviceToken();

		CloudFaces.API.User.register(sendData, function(userData) {
			if (userData.Status == 'Error') {
				$('.err-taken-mail').show();
				$('.err-taken-mail').text(
					CloudFaces.Language.translate(userData.message_long)
				);
				$('.register-btn').effect('shake');
			} else {
				$('.err-taken-mail').hide();
				CloudFaces.Login.startLogin(sendData);
			}
		});
	},

	isValidEmail: function(email) {
		return /\S+@\S+\.\S+/.test(email);
	},

	startLogin: function(sendData) {
		sendData.app_token = CFPersistentStorage.valueExists(
			CloudFaces.Config.app + '_device_token'
		)
			? CFPersistentStorage.readValue(CloudFaces.Config.app + '_device_token')
			: '';
		CloudFaces.API.User.Login(sendData, function(userData) {
			if (userData.Status == 'Error') {
				$('.wrong_email').remove();
				$('.err-taken-mail').show();
				$('.err-taken-mail').text(
					CloudFaces.Language.translate(userData.message_long)
				);
				// $('.register-btn').effect("shake")
			} else if (!userData || (userData.category_id != '2' && userData.category_id != '11')) {
				$('.err-taken-mail')
					.show()
					.css('text-align', 'center');
				$('.err-taken-mail').text(
					CloudFaces.Language.translate('no_permissions')
				);
				$('.register-btn').effect('shake');
			} else {
				CFPersistentStorage.writeValue(
					CloudFaces.Config.app + '_userData',
					JSON.stringify(userData)
				);
				location.href = CloudFaces.Login.Config.navigatePageAfterLogin + '?token=' + userData.token;
			}
		});
	},

	ressetPassword: function(content) {
		var $container = $(content);

		$container.append(
			'<div class="form-for-key">' +
				'<label for="">' +
				CloudFaces.Language.translate('email') +
				'<input type="text" class="email-for-key" name="email"/>' +
				'<span class="err-taken-mail"></span>' +
				'</label>' +
				'<button class="get-reset-key">' +
				CloudFaces.Language.translate('changePassword') +
				'</button>' +
				'</div>'
		);

		var element = $(
			'<div class="positionDiv ressetForm">' +
				'<div class="left-login">' +
				'<div class="forgotten_pass">' +
				CloudFaces.Language.translate('tokenInfo') +
				'<form class="forgottenPassForm">' +
				'<label for="">' +
				CloudFaces.Language.translate('resetPasswordKey') +
				'<input type="text" name="resetPasswordKey" class="resetKey"/>' +
				'<span class="err-taken-mail"></span>' +
				'</label>' +
				'<label for="">' +
				CloudFaces.Language.translate('newPassword') +
				'<input type="password" name="password"/>' +
				'</label>' +
				'<label for="">' +
				CloudFaces.Language.translate('repeatNewPassword') +
				'<input type="password" name="repeatNewPass"/>' +
				'</label>' +
				'<button class="restPass">' +
				CloudFaces.Language.translate('changePassword') +
				'</button>' +
				'</form>' +
				'</div>' +
				'</div>' +
				'</div>'
		);
		$container.append(element);
	},

	profile: function(conteiner) {
		var $conteiner = $(conteiner);
		var userCache = CFPersistentStorage.readValue(
			CloudFaces.Config.app + '_userData'
		);
		if (userCache) {
			userCache = JSON.parse(userCache);
		} else {
			CFMenuNavigation.navigate(CloudFaces.Login.Config.pages.login, '');
			return;
		}

		CloudFaces.API.User.getUserData(userCache.token, function(userData) {
			if (userData && Array.isArray(userData) && userData[0]) {
				userData = userData[0];
			}

			var $templatePage = $('<form class="top-section profile"></form>');
			$conteiner.append($templatePage);
			var $buttnSave = $(
				'<div class="saveprofile">' +
					CloudFaces.Language.translate('save_profil') +
					'</div>'
			);
			// $buttnSave.data('context', JSON.stringify(userData));
			$templatePage.after($buttnSave);
			$('.top-section').outerHeight(
				$(window).height() -
					$('.saveprofile').outerHeight(true) -
					$('.header_page').outerHeight(true)
			);

			if (userData && userData.message_long != 'error_token') {
				CloudFaces.API.User.profilTemplate(function(profileTemplate) {
					profileTemplate.sort(function(p1, p2) {
						return p1.item_order > p2.item_order;
					});

					profileTemplate.forEach(function(profileField) {
						var findEl = true;
						var key = profileField.name_of_field;
						if (profileField.type != 'text') {
							if (profileField.type == 'checkbox') {
								CloudFaces.Login.Type.checkBoxBttns(
									userData,
									key,
									profileField,
									function(callback) {
										$('.top-section').append(callback);
									}
								);
							} else if (profileField.type == 'radio') {
								CloudFaces.Login.Type.radioBttns(
									userData,
									key,
									profileField,
									function(callback) {
										$('.top-section').append(callback);
									}
								);
							} else if (profileField.type == 'image') {
								var base64 = false,
									isLocal = false,
									getDatafromCamera = false;
								CloudFaces.Login.Type.imageBttns(
									userData,
									key,
									profileField,
									getDatafromCamera,
									isLocal,
									base64,
									function(callback) {
										$('.top-section').prepend(callback);
										// ios fix 270* rotation
										if ($('body').hasClass('ios')) {
											$('.profile-iamge-inner').addClass('ios-rotate-270');
										}
									}
								);
							}

							findEl = false;
							return findEl;
						}

						if (findEl) {
							var text = userData[key];
							if (text == null) {
								text = '';
							}
							if (
								key != 'item_token' &&
								key != 'item_order' &&
								key != 'item_parent'
							) {
								$('.top-section').append(
									'<label class="' +
										key +
										'">' +
										CloudFaces.Language.translate(profileField.text_in_fields) +
										':' +
										'<input class="email-input" name="' +
										key +
										'" type="text" value="' +
										text +
										'"/>' +
										'<span class="err-taken-mail">' +
										CloudFaces.Language.translate('not_valide_email') +
										'</span>' +
										'</label>'
								);
							}
						}
					});
				});
			} else {
				CFPersistentStorage.writeValue(CloudFaces.Config.app + '_userData', '');
				CFMenuNavigation.navigate(CloudFaces.Login.Config.pages.login, '');
			}
		});
	},
	popupStatus: function(text, status) {
		var img = '<i class="fa fa-times-circle" aria-hidden="true"></i>';
		status
			? (img = '<i class="fa fa-check-circle" aria-hidden="true"></i>')
			: '';
		$('.popup-feedback').remove();
		$('body').append(
			'<div class="popup-feedback">' +
				'<div class="popup-feedback-inner">' +
				img +
				CloudFaces.Language.translate(text) +
				'</div>' +
				'</div>'
		);
		$('.popup-feedback').animate(
			{
				opacity: 0.25
			},
			4000,
			function() {
				// Animation complete.
				$(this).remove();
			}
		);
	},

	saveImageprofile: function(imgUrl, key, id, token) {
		var imageSendRequest = new CFHttpRequest();
		imageSendRequest.Method = 'POST';
		imageSendRequest.Url =
			CloudFaces.Config.ApiURL +
			'project/id/' +
			CloudFaces.Config.app +
			'/lists/' +
			CloudFaces.Login.Config.tables.users +
			'/item/' +
			id +
			'/columnname/' +
			key +
			'/';
		imageSendRequest.BinaryFile = imgUrl;
		imageSendRequest.sendBinary(function(result) {});
	},

	getPermission: function(permissionType, callback) {
		if ($('body').hasClass('ios')) {
			callback(true);
		} else {
			CFPrivacy.checkPermission(permissionType, function(enabled) {
				if (!enabled) {
					CFPrivacy.requestPermission(permissionType, function(granted) {
						callback(true);
						// return granted;
					});
				} else {
					// permission granted , this block will execute always once permission is granted
					// do not forget to include the logic here also
					callback(true);
				}
			});
		}
	},

	events: function() {
		if (CloudFaces.Login.init) {
			$(document).on('click', '.login-btn', function() {
				if (CloudFaces.Login.checkPassword($('input[type="password"]').val())) {
					var userData = {
						email: $('.email-input').val(),
						password: $('input[type="password"]').val()
					};

					if (!CloudFaces.Login.isValidEmail(userData.email)) {
						$('.email-input').addClass('not-valid');
						$('.err-taken-mail').show();
						// $('.login-btn').effect("shake")
						$('.wrong_email').remove();
						return;
					} else {
						$('.err-taken-mail').hide();
						$('.email-input').removeClass('not-valid');
					}

					CloudFaces.Login.startLogin(userData);
					// CloudFaces.Slideout.Config.addLogOutButtn();
				}
			});

			$(document).on('click', '.feedback-popup', function() {
				$('.feedback-popup').remove();
			});

			$(document).on('click', '.restPass', function(e) {
				e.preventDefault();
				if ($('.resetKey').val() != 0) {
					var forgottenPassForm = $('.forgottenPassForm').serializeArray();
					var newPass = '';
					var repeatNewPass = '';
					$.each(forgottenPassForm, function(inx, obj) {
						if (obj.name == 'password') {
							newPass = obj.value;
						} else if (obj.name == 'repeatNewPass') {
							repeatNewPass = obj.value;
						}
					});

					var invalidPass = false;
					if (newPass != repeatNewPass) {
						invalidPass = true;
						CloudFaces.Login.popupStatus('password_not_same', false);
					}

					if (newPass.length < 3) {
						invalidPass = true;
						CloudFaces.Login.popupStatus('small_password', false);
					}

					if (newPass.length > 20) {
						invalidPass = true;
						CloudFaces.Login.popupStatus('long_password', false);
					}

					if (!invalidPass) {
						CloudFaces.API.User.resetPasswaord(
							$('.resetKey').val(),
							forgottenPassForm,
							function(userData) {
								if (userData.message_long) {
									$('.positionDiv.ressetForm .err-taken-mail')
										.show()
										.text(
											CloudFaces.Language.translate('reset_not_valide_password')
										);
								} else {
									CloudFaces.Login.popupStatus('success_save_profil', true);
									CFMenuNavigation.navigate(
										CloudFaces.Login.Config.pages.login,
										''
									);
								}
							}
						);
					}
				} else {
					$('.positionDiv.ressetForm .err-taken-mail')
						.show()
						.text(CloudFaces.Language.translate('no_add_key'));
				}
			});

			$(document).on('click', '.get-reset-key', function() {
				if (CloudFaces.Login.isValidEmail($('.form-for-key input').val())) {
					var data = {
						email: $('.form-for-key input').val(),
						email_sender: CloudFaces.Language.translate('sender_email_reset'),
						email_body: CloudFaces.Language.translate(
							'text_email_reset_password'
						),
						email_subject: CloudFaces.Language.translate('email_subject')
					};

					CloudFaces.API.User.getKey(data, function(userData) {
						if (userData.message_long) {
							$('.form-for-key .err-taken-mail')
								.show()
								.text(CloudFaces.Language.translate(userData.message_long));
						} else {
							$('.form-for-key').hide();
							$('.ressetForm').show();
						}
					});
				} else {
					$('.form-for-key .err-taken-mail')
						.show()
						.text(CloudFaces.Language.translate('not_valid_email'));
				}
			});

			$(document).on('click', '.saveprofile', function() {
				var userprofile = $('.profile').serializeArray();
				var userCache = CFPersistentStorage.readValue(
					CloudFaces.Config.app + '_userData'
				);
				if (userCache) {
					userCache = JSON.parse(userCache);
				} else {
					CFMenuNavigation.navigate(CloudFaces.Login.Config.pages.login, '');
					return;
				}

				CloudFaces.API.User.getUserData(userCache.token, function(userData) {
					if (userData && Array.isArray(userData) && userData[0]) {
						userData = userData[0];
					}

					var newArry = [];
					for (var y = 0; y < userprofile.length; y++) {
						var addNew = true;
						for (var i = 0; i < newArry.length; i++) {
							if (newArry[i].name == userprofile[y].name) {
								newArry[i].value += '| ' + userprofile[y].value;
								addNew = false;
							}
						}

						if (addNew) {
							newArry.push(userprofile[y]);
						}

						if (y == userprofile.length - 1) {
							var addToCFPersistentStorage = userData;

							newArry.forEach(function(ind, val) {
								addToCFPersistentStorage[val.name] = val.value;
							});

							newArry.push({
								name: 'id',
								value: userData.id
							});

							var valueImg = $('.profile-iamge-inner').data('name');
							var imgSrc = $('.profile-iamge-inner').data('url');
							if (imgSrc) {
								CloudFaces.Login.saveImageprofile(
									imgSrc,
									valueImg,
									userData.id,
									userData.token
								);
							}

							CFPersistentStorage.writeValue(
								CloudFaces.Config.app + '_userData',
								JSON.stringify(addToCFPersistentStorage)
							);
							CloudFaces.API.User.update(
								newArry,
								userData.id,
								userData.token,
								function(addToCFPersistentStorage) {
									CloudFaces.Login.popupStatus('success_save_profil', true);
								}
							);
						}
					}
				});
			});

			//add profile image
			$(document).on('click', '.change-iamge, .addImage', function() {
				var key = $(this).data('name');
				CloudFaces.Login.getPermission('camera', function(callbackCamera) {
					CloudFaces.Login.getPermission('storage', function(callbackStorage) {
						if (callbackCamera && callbackStorage) {
							CloudFaces.Helpers.showLoader();
							var isLocal = location.href.charAt(0) == 'f';

							// var defaultResult = '';

							// if (isLocal) {
							// 	defaultResult = 'URL';
							// } else {
							// 	defaultResult = 'DATA';
							// }

							CFPictureChooser.capturePicture('PHOTO', 'URL', function(
								callback
							) {
								if (typeof callback != 'undefined' && callback != null) {
									var imageUri = callback;
									var getDatafromCamera = true;
									var base64 = false;
									if (isLocal) {
										CloudFaces.Login.Type.imageBttns(
											imageUri,
											key,
											'',
											getDatafromCamera,
											isLocal,
											base64,
											function(callback) {
												$('.top-section').prepend(callback);
											}
										);
									} else {
										CFPictureChooser.getPictureData(imageUri, function(data) {
											base64 = data;
											CloudFaces.Login.Type.imageBttns(
												imageUri,
												key,
												'',
												getDatafromCamera,
												isLocal,
												base64,
												function(callback) {
													$('.top-section').prepend(callback);
												}
											);
										});
									}

									$('.addImage').waitForImages(function() {
										$('.addImage').outerHeight($('.addImage').outerWidth());
										$('.claims_photo').outerHeight($('.addImage').outerWidth());
										CloudFaces.Helpers.hideLoader();
									});
								} else {
									CloudFaces.Helpers.hideLoader();
								}
							});
						}
					});
				});
			});

			$(document).on('click', '.remove-image', function() {
				var thisUrl = $(this).data('url');
				if (thisUrl != '') {
					CFPictureChooser.deletePicture(thisUrl);
				}
				$('.profile-iamge').css('background', 'url(images/profile.png)');
				$('.profile-iamge-inner').css('background', 'url(images/profile.png)');
				$('.remove-image').hide();
				$('.change-iamge').css({
					float: 'none',
					margin: '2% auto'
				});

				// delete profile pic from CMS
				var userData = CloudFaces.Helpers.getUserData();
				userData.Image = '';
				CloudFaces.API.User.update(
					userData,
					userData.id,
					userData.token,
					function(addToCFPersistentStorage) {
						CloudFaces.Login.popupStatus('success_save_profil', true);
					}
				);
			});
			$(document).on('click', '.mail_uniqa a', function() {
				var contactMail = CloudFaces.Language.translate('forgotten_pass_mail');
				var subject = 'restore Mail pls!';
				var username = $('.email-input').val();
				var mailLink =
					'mailto:' +
					contactMail +
					'?subject=' +
					subject +
					'&body=Please restore my password, username: ' +
					username;
				$('.mail_uniqa a').attr('href', mailLink);
				console.log(mailLink);
			});
			CloudFaces.Login.init = false;
		}
	}
});

$(document).on('cf-initialized', function() {
	$(document).on('cf-initialized', function() {
		$('#login').each(function() {
			CloudFaces.Login.loginPage.loadContent('#login .content');
		});
	});
	CloudFaces.Login.events();
});
