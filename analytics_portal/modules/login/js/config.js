if (typeof(CloudFaces.Login) == 'undefined') CloudFaces.Login = {Config: {}};
$.extend(CloudFaces.Login.Config,{
  navigatePageAfterLogin: "http://dev.test.cloudfaces.com/files/project/live_3159_sandboxx_test/backend/analytics.php",
  addRulesForTaxi: false,
  graphApiUrl: "https://graph.facebook.com/v2.6/me?fields=email,name&access_token=",
  pages: {
    login: "login_login",
    register: "login_register",
    profil: "login_profil",
    forgotPassword: "login_forgotPassword",
    registerTerms: "login_registerTerms"
  }
});