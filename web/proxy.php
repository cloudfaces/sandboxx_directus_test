<?php
error_reporting(0);

define( 'WWW_ROOT', '' );

function rrmdir($path)
{
    if (is_dir($path) === true)
    {
        $files = array_diff(scandir($path), array('.', '..'));

        foreach ($files as $file)
        {
            rrmdir(realpath($path) . '/' . $file);
        }
		return rmdir($path);
    }
	
    else if (is_file($path) === true)
		
    {
        return unlink($path);
    }

    return false;
}

class Proxy {
	protected $url  = '';
	protected $data = '';

	function __construct() {
		if( ! isset( $_REQUEST[ 'url' ] ) ) {
			throw new Exception( 'No URL provided.' );
		}

		$this->url = $_REQUEST[ 'url' ];
		// alert($this);

		$this->getData();
		$this->fetchContent();
	}

	public function getData() {
		$this->data = file_get_contents( $this->url );




		


		if( $this->data === false ) {
			throw new Exception( 'The content of the URL could not be extracted.' );
		}
	}

	public function fetchContent() {
		if( preg_match( '~<App[^>]+>~i', $this->data, $root_element ) ) {
			$obj = new stdClass();

			if( preg_match( '~content_android="([^"]+)"~i', $root_element[ 0 ], $name ) ) {
				$obj->android = $name[ 1 ];
			}

			if( preg_match( '~content_ios="([^"]+)"~i', $root_element[ 0 ], $name ) ) {
				$obj->ios = $name[ 1 ];
			}

			if( property_exists( $obj, 'android' ) || property_exists( $obj, 'ios' ) ) {
				if( ! file_exists( 'data/' . md5( $this->url ) ) )
					mkdir( 'data/' . md5( $this->url ) );

				if( property_exists( $obj, 'android' ) ) {
					$url = str_replace( basename( $this->url ), '', $this->url ) . $obj->android;
					$this->fetchArchive( 'android', $url );
					$this->content_android = 'data/' . md5( $this->url ) . '/android/';
				}

				if( property_exists( $obj, 'ios' ) ) {
					$url = str_replace( basename( $this->url ), '', $this->url ) . $obj->ios;
					$this->fetchArchive( 'ios', $url );
					$this->content_ios = 'data/' . md5( $this->url ) . '/ios/';
				}

				rrmdir( 'data/temp/' . md5( $this->url ) );
			}
		}
	}

	public function fetchArchive( $folder, $url ) {
		$target_folder = sprintf( "data/temp/%s/$folder/", md5( $this->url ) );
		$target        = $target_folder . 'archive.zip';

		if( ! file_exists( "data/temp/" . md5( $this->url ) ) )
			mkdir( "data/temp/" . md5( $this->url ) );

		if( ! file_exists( $target_folder ) )
			mkdir( $target_folder );

		copy( $url, $target );

		# Try extracting the file
		$archive = new ZipArchive();
		$archive->open( $target );
		$archive->extractTo( $target_folder );
		$archive->close();

		# Walk the content of the target folder and move allowed files where needed
		$main_folder = 'data/' . md5( $this->url ) . '/' . $folder;
		if( ! file_exists( $main_folder ) )
			mkdir( $main_folder );

		$temp_dir = opendir( $target_folder );
		while( $filename = readdir( $temp_dir ) ) {
			$parts = explode( '.', $filename );
			if( count( $parts ) == 1 )
				continue;

			$extension = array_pop( $parts );
			if( ! $extension )
				continue;

			if( in_array( strtolower( $extension ), array( 'jpg', 'png', 'jpeg' ) ) ) {
				copy( $target_folder . '/' . $filename, $main_folder . '/' . $filename );
			}
		}
		closedir( $temp_dir );
	}

	public function output() {
		return array(
			'code'          => $this->data,
			'ios_files'     => isset( $this->content_ios ) ? WWW_ROOT . $this->content_ios : '',
			'android_files' => isset( $this->content_android ) ? WWW_ROOT . $this->content_android : ''
		);
	}
}

try {
	$proxy = new Proxy();




	echo json_encode(array(
		'result'  => 'ok',
		'message' => $proxy->output()
	));
} catch( Exception $e ) {
	echo json_encode(array(
		'result'  => 'exception',
		'message' => $e->getMessage()
	));
}