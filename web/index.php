<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8" />
	<title>Sandboxx</title>
	<link href='http://fonts.googleapis.com/css?family=Coming+Soon' rel='stylesheet' type='text/css' />
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,400italic,700' rel='stylesheet' type='text/css' />
	<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="assets/css/style.css" />
</head>

<body>
	<div id="root"></div>
	<script type="text/html" id="template-start">
<div class="wrap">
	<input id="xml_url" type="text" class="url" placeholder="Enter XML URL here..." value="" />
	<input type="submit" class="submit" value="Load" />
</div>
</script>
	<script type="text/html" id="template-main">
<div class="workspace">
		<div class="website"></div>
</div>
</script>
	<script type="text/html" id="template-header">
<div class="inner">
	<div class="title"></div>
</div>
</script>
	<script type="text/html" id="template-table"></script>

	<script type="text/javascript" src="assets/js/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="assets/js/underscore-min.js"></script>
	<script type="text/javascript" src="assets/js/backbone-min.js"></script>
	<script type="text/javascript" src="js/config.js"></script>
	<script type="text/javascript" src="js/CloudFaces.js"></script>
	<script type="text/javascript" src="js/CloudFaces.Model.js"></script>
	<script type="text/javascript" src="js/CloudFaces.View.js"></script>
	<script type="text/javascript">startDebugger()</script>
	<script>
		$('#xml_url').val('../app/Data_web.xml');
		$('.submit')[0].click();
        $('.wrap').hide();
		// 	var cssLink = document.createElement("link");
		// cssLink.href = "style.css"; 
		// cssLink.rel = "stylesheet"; 
		// cssLink.type = "text/css"; 
		// frames['iframe'].document.body.appendChild(cssLink);
		$(document).on('ready', function () {
			$(document).on('keyup', function (e) {
				if (e.keyCode == 13) {
					$('form').submit();
				}
			});
		});
	</script>
</body>

</html>