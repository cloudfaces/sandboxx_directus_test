$.extend(CloudFaces, {
    Model: {},
    Collection: {},
    View: {}    
});


function startDebugger() {
    var loader = new CloudFaces.View.Loader();
    loader.$el.appendTo('#root');

};

function resizeIframeHeight() {
    var elemH = $(document).height() - $('.iframed .header').outerHeight(true);
    $('.iframed .header').next('iframe').outerHeight(elemH);
    $('.iframed .slide-out-page, .iframed .page.slide-out-page iframe').outerHeight(elemH);
};

window.currentContext = '';
var variableStorageVars = {};

window.addEventListener("message", function(e) {
    // debugger
    var d = e.data.args;
    switch (e.data.method) {
        case 'navigation:navigate':
            CFNavigation.navigate(d.page, d.context);
            break;

        case 'navigation:back':
            CFNavigation.navigateBack(d.context, e.origin);
            // console.log('CloudFaces.js:::::::: ' + d.context)
            break;

        case 'navigation:getContext':
            e.source.postMessage('context:' + currentContext, e.origin);
            break;

        case 'variableWrite':
            CFVariableStorage.writeValue(d.key, d.value);
            if (typeof e.data.source !== 'undefined') {
                if (e.data.source === 'slide' && $('.has-header iframe').length > 0) {
                    $('.has-header iframe').get(0).contentWindow.postMessage(e.data, e.origin);
                } else if (e.data.source === 'page' && $('.slide-out-page iframe').length > 0) {
                    $('.slide-out-page iframe').get(0).contentWindow.postMessage(e.data, e.origin);
                }
            }
            addWebsite.addClassWebsite();
            break;
        case 'variableGet':
            e.source.postMessage('variable:' + JSON.stringify(variableStorageVars), e.origin);
            break;

        case 'contextGet':
            e.source.postMessage('context:' + currentContext, e.origin);
            addWebsite.addClassWebsite();
            break;

        case 'buttonListener':
            RightButton.setOrigin(e.source, e.origin);
            break;
        case 'href:open':
            window.location = d;
            break;
        case 'updateHeader':
            var title = d.title;
            $('.header .title').text(title).css('color','#fff');
            break;            
    }
}, false);

CFNavigation = {
    navigate: function navigate(page, context) {
        // First, save the context if any
        currentContext = (typeof context == 'undefined') ? '' : context;

        // Trigger navigation to that page
        $(document).trigger('goToPage', page);

        return false;
    },
    getNavigationContext: function getNavigationContext() {
        return currentContext;
    },
    navigateBack: function navigateBack(context, source) {
        $(document).trigger('goBack', [{context: context || '', source: source || ''}]);
        // debugger
    }
};
CFVariableStorage = {
    valueExists: function valueExists(key) {
        return typeof variableStorageVars != 'undefined';
    },
    writeValue: function writeValue(key, value) {
        variableStorageVars[key] = value;
    },
    readValue: function readValue(key) {
        return (typeof variableStorageVars[key] == 'undefined') ? null : variableStorageVars[key];
    }
};
RightButton = {
    frame: null,
    origin: null,

    setOrigin: function(frame, origin) {
        this.frame = frame;
        this.origin = origin;


                // var PageBody = $($('iframe')[0].contentWindow);
                var arrPageBody = $($('iframe'));
                // console.log('PageBody before::::::::::::::::::::::::::')
                $.each(arrPageBody, function(key, value) {
                    PageBody = value.contentWindow.$('body');
                    $(PageBody).addClass('website');
                    console.log('iframe: ' + PageBody.attr('id') + ', has:  ' + PageBody.attr('class'));
                });
    },
    callMethod: function(methodName) {
        $('iframe').last().contents()[0].defaultView.eval(methodName);
    }
}
addWebsite = {
    addClassWebsite: function () {
        // var PageBody = $($('iframe')[0].contentWindow);
        var arrPageBody = $($('iframe'));
        // console.log('PageBody before::::::::::::::::::::::::::')
        $.each(arrPageBody, function(key, value) {
            PageBody = value.contentWindow.$('body');
            $(PageBody).addClass('website');
            // console.log('iframe body class:  ' + PageBody.attr('class'));
        });
    }
}
