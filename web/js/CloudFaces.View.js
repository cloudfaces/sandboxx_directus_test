var SlideOutEl = {};

CloudFaces.View.Loader = Backbone.View.extend({
    tagName: 'form',
    className: 'loader',
    events: {
        'submit': 'submit'
    },
    initialize: function() {
        this.render();
    },

    /**
     * Renders the form.
     */
    render: function() {
        var that = this,
            html = $('#template-start').html();

        that.$el.html(_.template(html));
    },

    /**
     * When the form is submitted, load the form.
     */
    submit: function(e) {
        var that = this,
            url = that.$el.find('.url').val();

        localStorage.setItem('last_inserted_xml', url);
        e.preventDefault();

        that.$el.addClass('loading');

        this.loadURL(url, function(view) {
            $('#root').empty().append(view.$el);

            that.$el.removeClass('loading');
        });
    },

    /**
     * Loads the URL
     */
    loadURL: function(url, callback) {
        $.ajax({
            url: 'proxy.php',
            data: {
                url: url
            },
            type: 'post',
            success: function(result) {
                result = $.parseJSON(result);

                if (typeof result.message.code == 'undefined') {
                    alert('The XML file could not be reached!');
                    callback();
                } else {
                    var model = new CloudFaces.Model.Application({
                            code: result.message.code,
                            url: url,
                            root: url.replace(/^(.*\/)[^\/]+$/i, '$1'),
                            serverData: result
                        }),

                        view = new CloudFaces.View.Main({
                            model: model,
                            code: result.code,
                            dataURL: url,
                            serverData: result.message
                        });

                    callback(view);
                }
            },
            error: function() {
                alert('The XML file could not be reached!');
                callback();
            }
        })
    }
});

CloudFaces.View.HiddenLoader = CloudFaces.View.Loader.extend({
    initialize: function() {
        // Don't render
    }
});

CloudFaces.View.Main = Backbone.View.extend({
    className: 'main-interface',
    events: {
        'click a.reload': 'reloadPage',
        'click a.load-other': 'backToLoader'
    },

    initialize: function(args) {
        this.dataURL = args.dataURL;
        // alert(args.dataURL)
        this.serverData = args.serverData;
        this.render();
    },

    /**
     * Renders all the main parts of the regular view.
     */
    render: function() {
        var that = this,
            html = $('#template-main').html();
        this.screenHistory = [];
        this.$el.html(_.template(html, {
            dataURL: this.dataURL
        }));

        // Add the first page
        this.showScreen(this.model.get('firstPage'));

        // Listen to page changes
        $(document).on('goToPage', function(e, page) {
            that.showScreen(page);
        });
        $(document).on('goBack', function(e, data) {
            that.goBack(data);
            // console.log('++++++++++++++ goBack this the data ++++++++++++++')
            // console.log(data)
        });

        if (!$('body').is('.iframed')) {
            // Add the table with details
            var table = new CloudFaces.View.DataTable({
                model: this.model
            });
            table.$el.appendTo(this.$el.find('.workspace'));
        }
    },

    /**
     * Navigates to a screen, without touching the history.
     */
    navigate: function(screen, cssClassArg, callback) {
        var last_screen_id = '';

        if (this.screenHistory.length) {
            last_screen_id = this.screenHistory[this.screenHistory.length-1].model.id;
        }

        if ( screen.model.id == CloudFaces.Config.pages.login && screen.model.id == last_screen_id ) {
            return;
        }
        else if( screen.model.id == CloudFaces.Config.pages.matches && ( last_screen_id == CloudFaces.Config.pages.login || last_screen_id ==  CloudFaces.Config.pages.questionnaire ) ){
            location.reload();
        }
        
        var that = this,
            $screen = that.$el.find('.website'),
            $old = that.$el.find('.website').children(),
            $new = screen.$el;

        cssClass = cssClassArg || 'showing-right';

         
        if (!$old.size()) {
            $new.appendTo($screen);
        } else {
            $new.addClass(cssClass);

            if (typeof cssClassArg == 'undefined') {
                $new.appendTo($screen);
            } else {
                $new.show();
            }

            setTimeout(function() {
                $new.addClass('show');
                setTimeout(function() {
                    $new.removeClass('show showing-right showing-left').siblings().hide();
/** ################## fire the function onIsActive ################## **/
                    if (typeof $new.find('iframe')[0].contentWindow.onIsActive === 'function') {
                        $new.find('iframe')[0].contentWindow.onIsActive();
                    }
                    if (typeof $new.find('iframe')[0].contentWindow.onIsInActive === 'function') {
                        $new.find('iframe')[0].contentWindow.onIsInActive();
                    }                    
/** ################## fire the function onNavigationResult ################## **/
                    // if (typeof $new.find('iframe')[0].contentWindow.onIsActive === 'function') {
                    //     $new.find('iframe')[0].contentWindow.onNavigationResult();
                    // }

                    if ( screen.model.id == CloudFaces.Config.pages.login) {
                        $('.back').hide();
                    }

                    if (typeof callback === 'function') {
                        callback($new.find('iframe')[0].contentWindow);
                    }
                }, 300);
            }, 50);
        }

        // Add the proper screen wherever needed
        this.model.setVariable('page', 'Page', screen.model.get('id'));
        this.model.setVariable('context', 'Navigation Context', currentContext);
        // debugger
    },

    /**
     * Renders a specific screen and adds it to the history.
     */
    showScreen: function(page) {
        var that = this;

        var screen = new CloudFaces.View.Screen({
            app: that,
            model: that.model.get('pages').get(page),
            serverData: this.serverData
        });

        this.navigate(screen);

        // Add the screen to the history
        this.screenHistory.push(screen);
    },

    /**
     * Goes to the previous screen.
     */
    goBack: function(data) {
        // Remove the page
        this.screenHistory.pop();
        var callback = '';
        if (typeof data === 'object' && data.hasOwnProperty('context') && data.hasOwnProperty('source')) {
            callback = function(viewWindow) {
                viewWindow.postMessage('navigation:back' + data.context, data.source);
            }
        }
        // $new = this.screenHistory[this.screenHistory.length - 1].$el;
        $new = SlideOutEl;
        if (typeof $new.find('iframe')[0].contentWindow.onIsActive === 'function') {
            $new.find('iframe')[0].contentWindow.onIsActive();
        }
        this.navigate(this.screenHistory[this.screenHistory.length - 1], 'showing-left', callback);

        // needed for web header title
        if(this.screenHistory.length  === 1) {

           location.reload();
        }

    },

    /**
     * Reloads the current page's webview.
     */
    reloadPage: function() {
        var $iframe = this.screenHistory[this.screenHistory.length - 1].$el.find('iframe');
        $iframe.get(0).contentDocument.location.reload()
    },

    /**
     * Goes back to the loader.
     */
    backToLoader: function(e) {
        e.preventDefault();


        var view = new CloudFaces.View.Loader();
        $('#root').empty().append(view.$el);
    }
});

CloudFaces.View.Screen = Backbone.View.extend({
    className: 'screen',

    events: {
        'click .overlay': 'toggleSlideout'
    },

    initialize: function(args) {
        this.app = args.app;
        this.serverData = args.serverData;
        this.render();
    },

    render: function() {
        var that = this;

        if (this.model.get('type') == 'navdrawer') {
            this.renderSlideoutWrapper();
        } else {
            this.renderStandardScreen();
        }
    },

    renderSlideoutWrapper: function() {
        // debugger
        // Add a background
        this.$el.append('<div class="overlay"></div>');

        // Prepare the slideout view
        var slideoutModel = this.app.model.get('pages').get(this.model.get('drawer').get('drawerpage')),
            pageModel = this.app.model.get('pages').get(this.model.get('drawer').get('mainpage'));

        this.renderMainPart(pageModel, true);

        var slideOutView = new CloudFaces.View.SlideOutPage({
            app: this.app,
            model: slideoutModel,
            serverData: this.serverData
        });

        slideOutView.$el.appendTo(this.$el);
        SlideOutEl = slideOutView.$el
        
    },

    renderStandardScreen: function() {
        this.renderMainPart(this.model, false);
    },

    /**
     * When the screen has a slide-out, toggle it.
     */
    toggleSlideout: function(e) {
        if (typeof e != 'undefined')
            e.preventDefault();

        if(this.$el.hasClass('open-slideout')){
            $('.header .title').text('my matches').css('color','#fff');
        } else {
            $('.header .title').text('profile').css('color','#fff');
        }
        this.$el.toggleClass('open-slideout');
    },

    /**
     * Renders the main part of the screen.
     */
    renderMainPart: function(pageModel, hasSlideout) {
        if (this.model.get('tabview')) {
            var tabView = new CloudFaces.View.Tabs({
                app: this.app,
                model: pageModel,
                screenHasSlideout: hasSlideout,
                screen: this,
                serverData: this.serverData
            });

            tabView.$el.appendTo(this.$el);
        } else {
            var pageView = new CloudFaces.View.Page({
                app: this.app,
                model: pageModel,
                screenHasSlideout: hasSlideout,
                screen: this,
                serverData: this.serverData
            });

            pageView.$el.appendTo(this.$el);
        }
    }
});

/**
 * This view is intended to display a web view and it's surroundings
 */
CloudFaces.View.Page = Backbone.View.extend({
    className: 'page',

    initialize: function(args) {
        this.app = args.app;
        this.screen = args.screen;
        this.serverData = args.serverData;
        this.slideout = (typeof args.screenHasSlideout != 'undefined') && args.screenHasSlideout;
        this.render()

    },

    render: function() {
        var that = this,
            frameURL = this.app.model.get('root') + this.model.get('webview').get('source'),
            $iframe = $('<iframe />');
        $iframe.attr('src', frameURL);
        this.$el.append($iframe);
        $iframe.attr('border', '1px solid red');
        $("iframe").contents().find('body').addClass('website')
        if (this.model.get('header'))
            this.renderHeader();
    },

    /**
     * Renders the header of a page.
     */
    renderHeader: function() {
        var that = this;

        that.$el.addClass('has-header');

        var header = new CloudFaces.View.Header({
            model: this.model.get('header'),
            page: this,
            slideout: this.slideout,
            serverData: this.serverData
        });

        header.$el.prependTo(this.$el);
    }
});

/**
 * This view renders a tabbed screen.
 */
CloudFaces.View.Tabs = CloudFaces.View.Page.extend({
    events: {
        'click .buttons-inner a': 'changeTab'
    },

    render: function() {
        var that = this,
            html = $('#template-tabs').html(),
            selectedTab = that.model.get('tabview').get('selectedtab') ? parseInt(that.model.get('tabview').get('selectedtab')) : 0;

        this.$el.html(html);

        this.model.get('tabview').get('tabs').each(function(tab, i) {
            var $handle = $('<a href="#"><span></span><strong></strong></a>');

            $handle.find('span').append('<img src="' + (that.imagePaths + tab.get('icon')) + '" />');
            $handle.find('strong').text(tab.get('title'));
            $handle.data('content', tab.get('content'));

            that.$el.find('.buttons-inner').append($handle);

            // If this is the first screen, show it
            if (selectedTab == i) {
                that.changeTab({
                    preventDefault: function() {},
                    target: $handle
                });
            }
        });

        if (this.model.get('header'))
            this.renderHeader();
    },

    changeTab: function(e) {
        e.preventDefault();

        // Change the color of the handle
        var $handle = $(e.target).closest('a');
        $handle.addClass('active').siblings().removeClass('active');

        // Change the visible tab
        if ($handle.data('iframe')) {
            $handle.data('iframe').$el.show().siblings().hide();
        } else {
            var webView = new CloudFaces.View.WebView({
                model: this.app.model.get('pages').get($handle.data('content')),
                app: this.app
            });

            this.$el.find('.tab-content').append(webView.$el);
            webView.$el.siblings().hide();
            $handle.data('iframe', webView);
        }
    }


});


/**
 * Handles a single webview
 */
CloudFaces.View.WebView = Backbone.View.extend({
    'tagName': 'iframe',

    initialize: function(args) {
        this.app = args.app;
        this.render();
    },
    render: function() {
        this.$el.attr('src', this.app.model.get('root') + this.model.get('webview').get('source'));

    }
});

/**
 * This view renders the header.
 */
CloudFaces.View.Header = Backbone.View.extend({
    events: {
        'click .sandwitch': 'toggleSlideout',
        'click .back': 'goBack',
        'click .r-button': 'rightButtonClicked'
    },

    className: 'header',

    initialize: function(args) {
        this.page = args.page;
        this.serverData = args.serverData;
        this.imagePaths = args.serverData.ios_files || args.serverData.android_files || '';
        this.slideout = (typeof args.slideout != 'undefined') && args.slideout;
        this.render()
    },

    render: function() {
        var that = this,
            title = '',
            leftButton = '',
            rightButton = '';

        // Fill the standard HTML
        this.$el.html($('#template-header').html());

        // Add a background eventually
        if (this.model.get('backgroundcolor')) {
            this.$el.css('background-color', this.model.get('backgroundcolor'));
        }

        // Add the title
        if (this.model.get('titleimage') || this.page.model.get('title')) {
            var $title = this.$el.find('.title');

            if (this.model.get('titleimage')) {
                titleimg = this.imagePaths + this.model.get('titleimage');
                $title.append('<img src="' + titleimg + '" />');
            } else {
                $title.text(this.page.model.get('title'));

                if (this.model.get('titlecolor')) {
                    $title.css('color', this.model.get('titlecolor'));
                }
            }
        }
        // Add the right button
        if (this.model.get('rightbutton')) {
            var $btn = $('<span class="button r-button" />');

            if (this.model.get('rightbutton').get('icon')) {
                var iconSource = this.imagePaths + this.model.get('rightbutton').get('icon');
                $btn.append('<img src="' + iconSource + '" />');
            } else if (this.model.get('rightbutton').get('text')) {
                $btn.text(this.model.get('rightbutton').get('text'));

                if (this.model.get('rightbutton').get('textcolor')) {
                    $btn.css('color', this.model.get('rightbutton').get('textcolor'));
                }
            }

            $btn.appendTo(this.$el.find('.inner'));
        }

        // Add the sandwitch button
        if (this.page.app.screenHistory.length) {
            this.$el.find('.inner').append('<span class="button back"><span class="fa fa-caret-left"></span></span>');

            if (this.model.get('leftbutton') && this.model.get('leftbutton').get('textcolor')) {
                this.$el.find('.back').css('color', this.model.get('leftbutton').get('textcolor'));
            }
        } else if (this.slideout) {
            this.$el.find('.inner').append('<span class="button sandwitch"><img src="assets/images/white_profile.png" /></span>');
        }
    },

    /**
     * Toggles the slide-out.
     */
    toggleSlideout: function(e) {
        e.preventDefault();

        this.page.screen.toggleSlideout();
    },

    /**
     * Goes to the previous screen.
     */
    goBack: function(e, data) {
        e.preventDefault();

        this.page.screen.app.goBack(data);
    },

    /**
     * Handles the click of the right header button.
     */
    rightButtonClicked: function(e) {
        e.preventDefault();

        var that = this,
            funcName = this.model.get('rightbutton').get('function');

        if (funcName) {
            RightButton.callMethod(funcName);
        }
    }
});

CloudFaces.View.SlideOutPage = CloudFaces.View.Page.extend({
    className: 'page slide-out-page'
});

/**
 * Displays the table with current values.
 */
CloudFaces.View.DataTable = Backbone.View.extend({
    initialize: function() {
        var that = this;

        this.render();

        this.model.on('change:variables', function() {
            that.render();
        });
    },

    render: function() {
        var that = this,
            html = $('#template-table').html();

        this.$el.html(_.template(html, {
            variables: this.model.get('variables')
        }));
    }
});