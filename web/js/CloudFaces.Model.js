CloudFaces.Model.Screen = Backbone.Model.extend({
	defaults: {
		id:    '',
		type:  '',
		title: ''
	},

	initialize: function( args ) {
		this.set( 'id', $( args.node ).attr( 'id' ) );
		this.set( 'type', $( args.node ).attr( 'type' ) );
		this.set( 'title', $( args.node ).attr( 'title' ) );

		// Check for all children of the screen
		if( $( 'pageheader', args.node ).size() && args.node.id !== 'nav' )
			this.set( 'header', new CloudFaces.Model.Header({ node: $( args.node ).find( 'pageheader' ) }) );

		if( $( 'webview', args.node ).size() )
			this.set( 'webview', new CloudFaces.Model.WebView({ node: $( args.node ).find( 'webview' ) }) );

		if( $( 'tabview', args.node ).size() )
			this.set( 'tabview', new CloudFaces.Model.TabView({ node: $( args.node ).find( 'tabview' ) }) );

		if( $( 'drawer', args.node ).size() )
			this.set( 'drawer', new CloudFaces.Model.Drawer({ node: $( args.node ).find( 'drawer' ) }) );

		this.unset( 'node' );
	}
});

CloudFaces.Collection.Screens = Backbone.Collection.extend({
	model: CloudFaces.Model.Screen
});

CloudFaces.Model.Header = Backbone.Model.extend({
	defaults: {
		backgroundcolor: '',
		backgroundimage: '',
		titlecolor: '',
		titleimage: ''
	},

	initialize: function( args ) {
		if( typeof $( args.node ).attr( 'backgroundcolor' ) != 'undefined' )
			this.set( 'backgroundcolor', $( args.node ).attr( 'backgroundcolor' ) );

		if( typeof $( args.node ).attr( 'backgroundimage' ) != 'undefined' )
			this.set( 'backgroundimage', $( args.node ).attr( 'backgroundimage' ) );

		if( typeof $( args.node ).attr( 'titlecolor' ) != 'undefined' )
			this.set( 'titlecolor', $( args.node ).attr( 'titlecolor' ) );

		if( typeof $( args.node ).attr( 'titleimage' ) != 'undefined' )
			this.set( 'titleimage', $( args.node ).attr( 'titleimage' ) );

		if( $( 'rightbutton', args.node ).size() )
			this.set( 'rightbutton', new CloudFaces.Model.RightButton({ node: $( 'rightbutton', args.node ) }) );

		if( $( 'leftbutton', args.node ).size() )
			this.set( 'leftbutton', new CloudFaces.Model.LeftButton({ node: $( 'leftbutton', args.node ) }) );

		this.unset( 'node' );
	}
});

CloudFaces.Model.WebView = Backbone.Model.extend({
	defaults: {
		fullscreen: '',
		backgroundcolor: ''
	},

	initialize: function( args ) {
		if( typeof $( args.node ).attr( 'fullscreen' ) != 'undefined' )
			this.set( 'fullscreen', $( args.node ).attr( 'fullscreen' ) );

		if( typeof $( args.node ).attr( 'source' ) != 'undefined' )
			this.set( 'source', $( args.node ).attr( 'source' ) );

		if( typeof $( args.node ).attr( 'backgroundcolor' ) != 'undefined' )
			this.set( 'backgroundcolor', $( args.node ).attr( 'backgroundcolor' ) );

		this.unset( 'node' );
	}
});

CloudFaces.Model.RightButton = Backbone.Model.extend({
	defaults: {
		text: '',
		textcolor: '',
		icon: '',
		function: ''
	},

	initialize: function( args ) {
		if( typeof $( args.node ).attr( 'text' ) != 'undefined' )
			this.set( 'text', $( args.node ).attr( 'text' ) );

		if( typeof $( args.node ).attr( 'textcolor' ) != 'undefined' )
			this.set( 'textcolor', $( args.node ).attr( 'textcolor' ) );

		if( typeof $( args.node ).attr( 'icon' ) != 'undefined' )
			this.set( 'icon', $( args.node ).attr( 'icon' ) );

		if( typeof $( args.node ).attr( 'function' ) != 'undefined' )
			this.set( 'function', $( args.node ).attr( 'function' ) );

		this.unset( 'node' );
	}
});

CloudFaces.Model.LeftButton = Backbone.Model.extend({
	defaults: {
		text: '',
		textcolor: '',
		icon: '',
		function: ''
	},

	initialize: function( args ) {
		if( typeof $( args.node ).attr( 'text' ) != 'undefined' )
			this.set( 'text', $( args.node ).attr( 'text' ) );

		if( typeof $( args.node ).attr( 'textcolor' ) != 'undefined' )
			this.set( 'textcolor', $( args.node ).attr( 'textcolor' ) );

		if( typeof $( args.node ).attr( 'icon' ) != 'undefined' )
			this.set( 'icon', $( args.node ).attr( 'icon' ) );

		if( typeof $( args.node ).attr( 'function' ) != 'undefined' )
			this.set( 'function', $( args.node ).attr( 'function' ) );

		this.unset( 'node' );
	}
});

CloudFaces.Model.TabView = Backbone.Model.extend({
	defaults: {
		selectedtab: '1'
	},

	initialize: function( args ) {
		var that = this;

		if( typeof $( args.node ).attr( 'selectedtab' ) != 'undefined' )
			this.set( 'selectedtab', $( args.node ).attr( 'selectedtab' ) );

		// Load tabs
		that.set( 'tabs', new CloudFaces.Collection.TabItems );
		$( 'tabitem', args.node ).each(function(){
			that.get( 'tabs' ).push( new CloudFaces.Model.TabItem({
				node: this
			}) );
		});

		this.unset( 'node' );
	}
});

CloudFaces.Model.TabItem = Backbone.Model.extend({
	defaults: {
		content: '',
		icon: '',
		title: ''
	},

	initialize: function( args ) {
		if( typeof $( args.node ).attr( 'content' ) != 'undefined' )
			this.set( 'content', $( args.node ).attr( 'content' ) );

		if( typeof $( args.node ).attr( 'icon' ) != 'undefined' )
			this.set( 'icon', $( args.node ).attr( 'icon' ) );

		if( typeof $( args.node ).attr( 'title' ) != 'undefined' )
			this.set( 'title', $( args.node ).attr( 'title' ) );

		this.unset( 'node' );
	}
});

CloudFaces.Collection.TabItems = Backbone.Collection.extend({
	model: CloudFaces.Model.TabItem
});

CloudFaces.Model.Drawer = Backbone.Model.extend({
	defaults: {
		mainpage: '',
		drawerpage: ''
	},

	initialize: function( args ) {
		if( typeof $( args.node ).attr( 'mainpage' ) != 'undefined' )
			this.set( 'mainpage', $( args.node ).attr( 'mainpage' ) );

		if( typeof $( args.node ).attr( 'drawerpage' ) != 'undefined' )
			this.set( 'drawerpage', $( args.node ).attr( 'drawerpage' ) );

		this.unset( 'node' );
	}
});

CloudFaces.Model.Application = Backbone.Model.extend({
	defaults: {
		contentAndroid: '',
		contentIOS: ''
	},

	initialize: function( args ) {
		var that = this,
			app = $( 'App', $.parseXML( args.code ) );

		this.unset( 'code' );

		this.set( 'contentAndroid', $( app ).attr( 'content_android' ) );
		this.set( 'contentIOS', $( app ).attr( 'content_ios' ) );
		this.set( 'firstPage', $( app ).attr( 'root' ) );

		// Load pages
		that.set( 'pages', new CloudFaces.Collection.Screens );
		$( app ).children( 'navigation' ).children().each(function(){
			that.get( 'pages' ).push( new CloudFaces.Model.Screen({
				node: this
			}) );
		});

		// Add variables
		this.set( 'variables', {
			url: new Backbone.Model({
				title: 'XML URL',
				value: args.url
			}),
			root: new Backbone.Model({
				title: 'Files Root',
				value: args.root
			})
		});
	},

	setVariable: function( key, title, value ){
		var variables = $.extend( {}, this.get( 'variables' ) );

		variables[ key ] = new Backbone.Model({
			title: title,
			value: value
		});

		this.set( 'variables', variables );
	}
});