if (typeof CloudFaces == 'undefined') CloudFaces = { Config: {}}
else CloudFaces = {Config: {}};

CloudFaces.Config.pages = {
	login: "login_login",
	questionnaire: "questionnaire",
	matches: "matches"
};