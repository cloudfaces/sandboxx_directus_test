if (typeof(CloudFaces.Login) == 'undefined') CloudFaces.Login = {Config: {}};
$.extend(CloudFaces.Login.Config,{
  navigatePageAfterLogin: "profile",
  navigatePageAfterRegister: "questionnaire",
  addRulesForTaxi: false,
  graphApiUrl: "https://graph.facebook.com/v2.6/me?fields=email,name&access_token=",
  pages: {
    login: "login_login",
    register: "login_register",
    profil: "login_profil",
    forgotPassword: "login_forgotPassword",
    registerTerms: "terms",
    matches: 'matches',
    cohort:'cohort_select'
  }
});