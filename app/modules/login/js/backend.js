if (typeof CloudFaces == 'undefined') CloudFaces = { Login: {Config: {}}}
else CloudFaces.Login = {Config: {}};
CloudFaces.Login.Config.tables = {
  categories: "20484",
  templates: "20483",
  rights : "20480",
  roles: "20485",
  users: "20481",
  companies: "20487"
};
CloudFaces.Login.Config.relations = {
	user_companies:'313'
};
