/*v1.0.4*/
$.extend(CloudFaces.Login, {
    init: true,
    newArrayForSaveprofile: [],
    type:'',
    user_id:'',
    loginPage: {
        loadContent: function (container) {
            CloudFaces.Login.registerPageAnalytics();
            var userD = CloudFaces.Helpers.getUserData();

            $('#ip-container').each(function () {
                if (typeof CFNavigation.getNavigationContext() != 'undefined' && CFNavigation.getNavigationContext()) {
                    CloudFaces.Helpers.showLoader();
                    var header = $.parseJSON(CFNavigation.getNavigationContext());
                    if (typeof header['hide'] != 'undefined' && header['hide']) {
                        $('.ip-header').hide().remove();
                        CloudFaces.Helpers.hideLoader();
                    }
                }
            });


            CloudFaces.Login.appendPopup(container);
            var $container = $(container);

            $element = $(
                '<div class="container pb-80">' +
                '<div class="my-50">' +
                '<h1 class="title-primary">' + CloudFaces.Language.translate('hi_there') + '</h1>' +
                '<h3 class="title-secondary">' + CloudFaces.Language.translate('lets_get_you_in') + '</h3>' +
                '</div>' +
                '<div class="form-group mb-15">' +
                '<label class="text-sm" for="emailInput">' + CloudFaces.Language.translate('email') + ':</label>' +
                '<input class="email-input required-input form-control" id="emailInput" type="email"  aria-describedby="emailHelp" placeholder="your@email.com">' +
                '<span class="err-taken-mail">' + CloudFaces.Language.translate('not_valide_email') + '</span>' +
                '</div>' +
                '<div class="form-group mb-15">' +
                '<label class="text-sm" for="passInput">' + CloudFaces.Language.translate('password') + ':</label>' +
                '<input class="form-control required-input" type="password" id="passInput"  type="password"  aria-describedby="password" placeholder="password"/>' +
                '<span class="err-taken-password">' + CloudFaces.Language.translate('add_password') + '</span>' +
                '</div>' +
                '<label class="forgot-pass text-sm font-bold mt-30" onclick="CFNavigation.navigate(\'' + CloudFaces.Login.Config.pages.forgotPassword + '\', \'\');">' +
                CloudFaces.Language.translate('forgottenPassword') +
                '</label>' +
                '<div class="btn-fixed-footer">' +
                //
                '<button class="login-btn disabled-btn btn btn-primary">' + CloudFaces.Language.translate('login') + '</button>' +
                '<div class="reg-btn-wrapper" onclick="CFNavigation.navigateAndAddToBackStack(\'' + CloudFaces.Login.Config.pages.register + '\', \'\');">' +
                '<label class="register-btn  d-block text-sm font-bold text-center pt-20 pb-10">' + CloudFaces.Language.translate('create_account') + '</label>' +
                '</div>' +
                '</div>' +
                '</div>'
            );

            $container.append($element);

            new CloudFaces.Analytics('login', 'Login').set(function (result) {
            });
        }
    },

    registerTermsPage: {
        loadContent: function (container) {
            CloudFaces.Login.registerPageAnalytics();
            var $container = $(container);

            $element = $(
                '<h1>' + CloudFaces.Language.translate('register_terms') + '</h1>' +
                '<p>' + CloudFaces.Language.translate('register_terms_text') + '</p>' +
                '<button onclick="CFNavigation.navigateBack(\'\');">' + CloudFaces.Language.translate('back') + '</button>'
            );

            $container.append($element);
            new CloudFaces.Analytics('registerTerms', 'RegisterTerms').set(function (result) {
            });
        }
    },

    registerPage: {
        loadContent: function (container) {
            CloudFaces.Login.registerPageAnalytics();
            var $container = $(container);
            var r_info = CFPersistentStorage.readValue('register_info')?JSON.parse(CFPersistentStorage.readValue('register_info')):{};
            CFPersistentStorage.writeValue('register_info', '');
            CloudFaces.Login.type = r_info && r_info.type ? r_info.type : '';
            CloudFaces.Login.appendPopup(container);

            var title = CloudFaces.Language.translate('title_lets_create_account');
            var btn_text = CloudFaces.Language.translate('next');

            if (CloudFaces.Login.type == "admin") {
                title = CloudFaces.Language.translate('admin_title_lets_create_account');
                btn_text = CloudFaces.Language.translate('create');
            }

            $element = $(
                '<div class="container pb-80">' +
                '<div class="my-30">' +
                '<h1 class="title-primary">' + title + '</h1>' +
                '</div>' +
                '<div class="form-group mb-15">' +
                '<label  class="text-sm">' + CloudFaces.Language.translate('email') + ':</label>' +
                '<input class="form-control email-input required-input" type="email" aria-describedby="emailInput" placeholder="your@email.com"/>' +
                '<span class="err-taken-mail">' + CloudFaces.Language.translate('not_valide_email') + '</span>' +
                '</div>' +

                '<div class="form-group mb-15">' +
                '<label  class="text-sm">' + CloudFaces.Language.translate('password') + ':</label>' +
                '<input class="form-control password-input required-input" type="password" aria-describedby="password" placeholder="password"/>' +
                '<span class="err-taken-password">' + CloudFaces.Language.translate('add_password') + '</span>' +
                '</div>' +

                '<div class="form-group mb-15">' +
                '<label class="text-sm">' + CloudFaces.Language.translate('confirm_password') + ':</label>' +
                '<input class="form-control password-confirm required-input" type="password" aria-describedby="password" placeholder="password"/>' +
                '<span class="err-taken-password">' + CloudFaces.Language.translate('password') + '</span>' +
                '</div>' +

                // checked terms and conditions
                '<div class="form-group mb-15 checbox-wrapper">' +
                '<div class="custom-control custom-checkbox">'+
                '<input class="checkbox-appearance custom-control-input" type="checkbox" id="terms-cbx" name="terms-cbx" />' +
                '<label class="text-sm custom-control-label" for="terms-cbx">' +
                CloudFaces.Language.translate('register_accept_terms') +
                // ' <strong><span class="go-to-terms" onclick="CFNavigation.navigate(\'' + CloudFaces.Login.Config.pages.registerTerms + '\', \'\')">' +
                '<strong><span class="go-to-terms">' +
                CloudFaces.Language.translate('register_terms') +
                '</span></strong>' +
                '</label>' +
                '</div>' +
                '</div>' +

                '</div>' +
                '<div class="btn-fixed-footer">' +
                '<button class="register-btn disabled-btn btn btn-primary"> ' + btn_text + '</button>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>'
            );
            if (typeof CFNavigation.getNavigationContext() != 'undefined' && CFNavigation.getNavigationContext()) {
                var context = $.parseJSON(CFNavigation.getNavigationContext());
                $element.find('.password-input').val(context.password);
                $element.find('.password-confirm').val(context.password_confirm);
                $element.find(".email-input").val(context.email);
                $element.find('#terms-cbx').prop('checked', context.terms_checked);
                if (context.terms_checked){
                    $element.find('.register-btn').removeClass('disabled-btn');
                }
            }

            $container.append($element);
            CloudFaces.Login.registerPage.events();
            new CloudFaces.Analytics('register', 'Register').set(function (result) {
            });
        },
        events: function () {
            $('.register-btn').on('click', function () {
                // Validation for registeration term checkbox
                if ($('#terms-cbx').is(':checked')) {
                    $('.terms-cbx-label').removeClass('required');
                    var password = $('.password-input').val();
                    var confirm_password = $('.password-confirm').val();
                    var email = $(".email-input").val();
                    if (CloudFaces.Login.checkPasswords( password, confirm_password )) {
                        var cohort_id = CFPersistentStorage.readValue("selected_cohort");
                        var sendData = {
                            email: email,
                            password: password,
                            cohort_id: cohort_id
                        }
                        CloudFaces.Login.checkEmail(email, function () {
                            if (CloudFaces.Login.type == "admin") {
                                CloudFaces.Directus.directusRegisterUserByAdmin(sendData).then(function (data) {
                                    CFVariableStorage.writeValue('matches_refresh', 'true');
                                    CFNavigation.navigateBack('');
                                }).catch(function (error) {
                                    console.log(error);
                                    if ( error.code == 204 || error.code == 0204 ) {
                                        CloudFaces.Login.showPopup(CloudFaces.Language.translate("login_popup_error"), "User "+email+" is already exist");
                                    } else {
                                        CloudFaces.Login.showPopup(CloudFaces.Language.translate("login_popup_error"), "Something went wrong while creating the user");
                                    }
                                    return false;
                                });
                            }
                            else {
                                CloudFaces.Directus.directusRegisterUser(sendData).then(function (data) {
                                    CloudFaces.Login.startLogin(sendData, true);
                                }).catch(function (error) {
                                    CloudFaces.Login.showPopup(CloudFaces.Language.translate("login_popup_error"),
                                        error.message);
                                    return false;
                                });
                            }
                        });
                    }
                } else {
                    $('.terms-cbx-label').effect("shake")
                    $('.register-btn').effect("shake")
                    $('.terms-cbx-label').addClass('required');
                }
            });
            $('.go-to-terms').on('click', function () {
                if($('body').hasClass('ios')){
                    CFNavigation.navigate(CloudFaces.Login.Config.pages.registerTerms, '');
                } else {
                    var password = $('.password-input').val();
                    var password_confirm = $('.password-confirm').val();
                    var email = $(".email-input").val();
                    var terms_checked = $('#terms-cbx').is(':checked');

                    var context = {
                        email: email,
                        password: password,
                        password_confirm: password_confirm,
                        terms_checked: terms_checked
                    };
                    CFNavigation.navigateAndAddToBackStack(CloudFaces.Login.Config.pages.registerTerms, JSON.stringify(context));
                }
            });
        }
    },

    appendPopup:function(container){
        var $container = $(container);
        $container.append(
            '<div>'+
            '<div class="modal fade modal-from-bottom" id="login-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">'+
            '<div class="modal-dialog" role="document">'+
            '<div class="modal-content modal-native modal-content--info">'+
            '<div class="pt-20 pb-20 px-20">'+
            '<h3 class="font-16 color-primary text-center login-popup-title"></h3>'+
            '<p class="font-12 color-secondary text-center login-popup-text"></p>'+
            '</div>'+
            '<div class="dialog-btn-group--info">'+
            '<button type="button" class="btn dialog-btn-default btn-primary" data-toggle="modal" data-dismiss="modal">Okay</button>'+
            '</div>'+
            '</div>'+
            '</div>'+
            '</div>'+
            '</div>'
        );
    },

    showPopup: function(title, text){
        $('.login-popup-title').text(title);
        $('.login-popup-text').text(text);
        $('#login-popup').modal('toggle');
    },

    checkEmail: function (email, callback) {
        if ( ! CloudFaces.Login.isValidEmail(email) ) {
            CloudFaces.Login.showPopup(CloudFaces.Language.translate("login_popup_error"), CloudFaces.Language.translate("login_popup_incorrect_email"));
            return false;
        }
        callback(true);
    },

    checkPasswords: function (password, confirm_password) {
        if (password != '') {
            if (password.length < 3) {
                CloudFaces.Login.showPopup(CloudFaces.Language.translate("login_popup_error"), CloudFaces.Language.translate('small_password'));
                return false;
            }
            else if (password != confirm_password) {
                CloudFaces.Login.showPopup(CloudFaces.Language.translate("login_popup_error"), CloudFaces.Language.translate("login_popup_pass_mismatch"));
                return false;
            }
            else {
                return true;
            }
        }
        else {
            $('.register-btn').effect("shake");
            return false;
        }
    },

    isValidEmail: function (email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    },
    ensureToken: function(){
        return new Promise(function(resolve, reject) {
            if (!$('body').hasClass('web')) {
                var device_token = CloudFaces.DeviceCode.getDeviceToken();
                if (!device_token || device_token == '') {
                    var ain = CloudFaces.DeviceCode.getDeviceAin();
                    var token = CFPushNotifications.getToken();
                    if (typeof token != 'undefined' && token && token != '' && ain != '' && ain != 0 && token != ain) {
                        CloudFaces.API.registerAppWithToken(token.replace(/\s/g, ''), ain, function(a) {
                            CFPersistentStorage.writeValue(CloudFaces.Config.app + '_device_token', token.replace(/\s/g, ''));
                            resolve();
                        });
                    }
                    else{
                        resolve();
                    }
                }
                else{
                    resolve();
                }
            }
            else{
                resolve();
            }
        })
    },
    startLogin: function (sendData, triggerQuestionnaire) {
        sendData.app_token = CFPersistentStorage.valueExists(CloudFaces.Config.app + '_device_token') ? CFPersistentStorage.readValue(CloudFaces.Config.app + '_device_token') : '';
        CloudFaces.Directus.directusLogin({email: sendData.email, password: sendData.password}).then(function (userData) {
            CloudFaces.Login.updateTimezoneAin(userData, function(){
                CFPersistentStorage.writeValue(CloudFaces.Config.app + '_userData', JSON.stringify(userData));

                if(CFVariableStorage.readValue('app_started') == 'true' || !$('body').hasClass('ios')){
                    CFVariableStorage.writeValue('profile_refresh', 'true');
                }
                if (userData.role == 5 ) { // Admin role 5
                    CFMenuNavigation.navigate(CloudFaces.Login.Config.pages.matches, '1')
                } else if (triggerQuestionnaire || ! userData.role_id) {
                    CFPersistentStorage.writeValue("questionnaire_info", JSON.stringify({type: "role_select"}));
                    CFNavigation.navigateToRoot(CloudFaces.Login.Config.navigatePageAfterRegister, '')
                } else {
                    CFMenuNavigation.navigate(CloudFaces.Login.Config.pages.matches, '1')
                }
            })
        }).catch(function(error){
            $('.wrong_email').remove();
            CloudFaces.Login.showPopup(CloudFaces.Language.translate("login_popup_error"), CloudFaces.Language.translate(error.message));
            $('.register-btn').effect("shake");
        });
    },
    updateTimezoneAin: function (userData, callback) {
        ain = CloudFaces.DeviceCode.getDeviceAin();
        token = CloudFaces.DeviceCode.getDeviceToken();
        if (!token || token == '') {
            CloudFaces.Login.ensureToken()
                .then(function(){
                    ain = CloudFaces.DeviceCode.getDeviceAin();
                    token = CloudFaces.DeviceCode.getDeviceToken();
                    userData.device_ain = ain;
                    userData.timezone = (-1 * (new Date().getTimezoneOffset())) / 60;
                    delete userData['token'];
                    userData.device_token = token;
                    DirectusClient.updateItem("directus_users", userData.id, userData).then(callback);
                })
        }
        else{
            userData.device_ain = ain;
            userData.timezone = (-1 * (new Date().getTimezoneOffset())) / 60;
            delete userData['token'];
            userData.device_token = token;
            DirectusClient.updateItem("directus_users", userData.id, userData).then(callback);
        }
    },
    forgottenPassword: function (content) {
        CloudFaces.Login.registerPageAnalytics();
        var $container = $(content);

        var r_info = CFPersistentStorage.readValue('register_info')?JSON.parse(CFPersistentStorage.readValue('register_info')):{};
        CFPersistentStorage.writeValue('register_info', '');
        CloudFaces.Login.type = r_info && r_info.type ? r_info.type : '';

        CloudFaces.Login.appendPopup(content);

        $container.append(
            '<div class="container pb-80">' +
            '<div class="my-50">' +
            '<h1 class="title-primary">' + CloudFaces.Language.translate('forgot_password') + '</h1>' +
            '<h3 class="title-secondary">' + CloudFaces.Language.translate('lets_get_you_in') + '</h3>' +
            '</div>' +
            '<div class="form-for-key form-group mb-15">' +
            '<label class="text-sm" for="emailForgottenInput">' +
            CloudFaces.Language.translate('email') + ': </label>' +
            '<input type="text" class="form-control email-for-key required-input" name="email" id="emailForgottenInput" aria-describedby="emailInput" placeholder="your@email.com"/>' +
            '<span class="err-taken-mail"></span>' +
            '</div>' +
            '<div class="btn-fixed-footer">' +
            '<button class="get-reset-key disabled-btn btn btn-primary">' + CloudFaces.Language.translate('changePassword') + '</button>' +
            '</div>' +
            '</div>'
        );


        $element = $('<div class="positionDiv ressetForm container">' +
            '<div class="left-login">' +

            '<div class="forgotten_pass">' +
            '<form class="forgottenPassForm form-group wrap-block">' +
            '<label class="text-sm" for="resetPasswordKey">' +
            CloudFaces.Language.translate('resetPasswordKey') +
            '<input type="text" name="resetPasswordKey" id="resetPasswordKey" class="resetKey form-control" placeholder="'+CloudFaces.Language.translate('resetPasswordKey')+'"/>' +
            '<span class="err-taken-mail"></span>' +
            '</label>' +

            '<label for="password" class="text-sm ">' +
            CloudFaces.Language.translate('newPassword') +
            '<input type="password" name="password" id="password" class="form-control" placeholder="'+CloudFaces.Language.translate('newPassword')+'"/>' +
            '</label>' +

            '<label for="repeatNewPass" class="text-sm ">' +
            CloudFaces.Language.translate('repeatNewPassword') +
            '<input type="password" name="repeatNewPass" id="repeatNewPass" class="form-control" placeholder="'+CloudFaces.Language.translate('repeatNewPassword')+'"/>' +
            '</label>' +

            '<button class="restPass disabled-btn btn btn-primary">' + CloudFaces.Language.translate('changePassword') + '</button>' +
            '</form>' +
            '</div>' +
            '</div>' +
            '</div>');
        $container.append($element);
    },
    ressetPassword: function (content) {
        console.log(CloudFaces.Login.type);
        CloudFaces.Login.registerPageAnalytics();
        var $container = $(content);

        var r_info = CFPersistentStorage.readValue('register_info')?JSON.parse(CFPersistentStorage.readValue('register_info')):{};
        console.log('r info '+JSON.stringify( r_info ));
        CFPersistentStorage.writeValue('register_info', '');
        CloudFaces.Login.type = r_info && r_info.type ? r_info.type : '';
        CloudFaces.Login.user_id = r_info && r_info.user_id ? r_info.user_id : '';

        CloudFaces.Login.appendPopup(content);

        $container.append(

            '<form class="container pb-80 reset-password-form">' +
            '<div class="my-50">' +
            '<h1 class="title-primary">' + CloudFaces.Language.translate('change_password') + '</h1>' +
            '<h3 class="title-secondary">' + CloudFaces.Language.translate('lets_get_you_in') + '</h3>' +
            '</div>' +

            '<div class="form-group mb-15">' +
            '<label  class="text-sm">' + CloudFaces.Language.translate('newPassword') + ':</label>' +
            '<input name="password" class="form-control password-input required-input" type="password" aria-describedby="password" placeholder="password"/>' +
            '<span class="err-taken-password">' + CloudFaces.Language.translate('add_password') + '</span>' +
            '</div>' +

            '<div class="form-group mb-15">' +
            '<label class="text-sm">' + CloudFaces.Language.translate('confirm_password') + ':</label>' +
            '<input name="repeatNewPass" class="form-control password-confirm required-input" type="password" aria-describedby="password" placeholder="password"/>' +
            '<span class="err-taken-password">' + CloudFaces.Language.translate('password') + '</span>' +
            '</div>' +

            '<div class="btn-fixed-footer">' +
            '<button class="reset-password disabled-btn btn btn-primary" data-id = "'+ CloudFaces.Login.user_id +'">' + CloudFaces.Language.translate('changePassword') + '</button>' +
            '</div>' +
            '</div>'

        );

    },
    profile: function (conteiner) {
        $conteiner = $(conteiner);
        var userCache = CFPersistentStorage.readValue(CloudFaces.Config.app + '_userData');
        if (userCache) {
            userCache = JSON.parse(userCache);
        } else {
            CFMenuNavigation.navigate(CloudFaces.Login.Config.pages.login, '');
            return;
        }

        CloudFaces.API.User.getUserData(userCache.token, function (userData) {
            if (userData && Array.isArray(userData) && userData[0]) {
                userData = userData[0];
            }

            $templatePage = $('<form class="top-section profile"></form>')
            $conteiner.append($templatePage)
            $buttnSave = $('<div class="saveprofile">' + CloudFaces.Language.translate('save_profil') + '</div>')
            // $buttnSave.data('context', JSON.stringify(userData));
            $templatePage.after($buttnSave)
            $('.top-section').outerHeight($(window).height() - $('.saveprofile').outerHeight(true) - $('.header_page').outerHeight(true))

            if (userData && userData.message_long != "error_token") {
                CloudFaces.API.User.profilTemplate(function (profileTemplate) {
                    profileTemplate.sort(function (p1, p2) {
                        return p1.item_order > p2.item_order;
                    });

                    profileTemplate.forEach(function (profileField) {
                        var findEl = true,
                            key = profileField.name_of_field;
                        if (profileField.type != "text") {
                            if (profileField.type == "checkbox") {
                                CloudFaces.Login.Type.checkBoxBttns(userData, key, profileField, function (callback) {
                                    $('.top-section').append(callback)
                                })
                            } else if (profileField.type == "radio") {
                                CloudFaces.Login.Type.radioBttns(userData, key, profileField, function (callback) {
                                    $('.top-section').append(callback)
                                })
                            } else if (profileField.type == "image") {
                                var getDatafromCamera = false,
                                    base64 = false,
                                    isLocal = false,
                                    getDatafromCamera = false
                                CloudFaces.Login.Type.imageBttns(userData, key, profileField, getDatafromCamera, isLocal, base64, function (callback) {
                                    $('.top-section').prepend(callback)
                                    // ios fix 270* rotation
                                    if ($('body').hasClass('ios')) {
                                        $('.profile-iamge-inner').addClass('ios-rotate-270');
                                    }
                                })
                            }

                            findEl = false;
                            return false;
                        }

                        if (findEl) {
                            text = userData[key];
                            if (text == null) {
                                text = ''
                            }
                            if (key != 'item_token' && key != 'item_order' && key != 'item_parent') {
                                $('.top-section').append('<label class="' + key + '">' + CloudFaces.Language.translate(profileField.text_in_fields) + ':' +
                                    '<input class="email-input" name="' + key + '" type="text" value="' + text + '"/>' +
                                    '<span class="err-taken-mail">' + CloudFaces.Language.translate('not_valide_email') + '</span>' +
                                    '</label>');
                            }
                        }
                    })
                })
            } else {
                CFPersistentStorage.writeValue(CloudFaces.Config.app + '_userData', '');
                CFMenuNavigation.navigate(CloudFaces.Login.Config.pages.login, '')
            }
        })
    },
    popupStatus: function (text, status) {
        img = '<i class="fa fa-times-circle" aria-hidden="true"></i>';
        status ? img = '<i class="fa fa-check-circle" aria-hidden="true"></i>' : '';
        $('.popup-feedback').remove();
        $('body').append(
            '<div class="popup-feedback">' +
            '<div class="popup-feedback-inner">' +
            img +
            CloudFaces.Language.translate(text) +
            '</div>' +
            '</div>'
        );
        $(".popup-feedback").animate({
            opacity: 0.25,

        }, 4000, function () {
            // Animation complete.
            $(this).remove();
        });
    },

    saveImageprofile: function (imgUrl, key, id, token) {
        var imageSendRequest = new CFHttpRequest();
        imageSendRequest.Method = "POST";
        imageSendRequest.Url = CloudFaces.Config.ApiURL + 'project/id/' +
            CloudFaces.Config.app + '/lists/' +
            CloudFaces.Login.Config.tables.users + '/item/' +
            id + '/columnname/' + key + '/';
        imageSendRequest.BinaryFile = imgUrl;
        imageSendRequest.sendBinary(function (result) {
        });
    },

    getPermission: function (permissionType, callback) {
        if ($('body').hasClass('ios')) {
            callback(true);
        } else {
            CFPrivacy.checkPermission(permissionType, function (enabled) {
                if (!enabled) {
                    CFPrivacy.requestPermission(permissionType, function (granted) {
                        callback(true);
                        // return granted;
                    });
                } else {
                    // permission granted , this block will execute always once permission is granted
                    // do not forget to include the logic here also
                    callback(true);
                }
            });
        }
    },

    checkForm: function () {
        var inputs = $('.required-input');
        var checked = 0;
        inputs.each(function (inx, val) {
            if ($(val).val()) {
                checked++;
            };
        });

        var check_tac_checked = true;

        var check_tac = $('#terms-cbx');

        if(check_tac.length != 0) {
            check_tac_checked = check_tac.is(':checked')
        }

        if (inputs.length == checked && check_tac_checked) {
            $('button').removeClass('disabled-btn');
        }
        else {
            $('button').addClass('disabled-btn');
        }

        $('.feedback-popup').remove();
    },
    registerPageAnalytics: function(){
        CFRuntime.findCurrentPage(function(page){
            CFAnalytics.registerPage(page);
        });
    },
    events: function () {
        if (CloudFaces.Login.init) {
            $(document).on('click', '.login-btn', function () {
                if (CloudFaces.Login.checkPasswords($('input[type="password"]').val(), $('input[type="password"]').val())) {
                    var userData = {
                        'email': $('.email-input').val(),
                        'password': $('input[type="password"]').val()
                    };
                    if (!CloudFaces.Login.isValidEmail(userData.email)) {
                        $('.email-input').addClass('not-valid');
                        CloudFaces.Login.showPopup(CloudFaces.Language.translate("login_popup_error"), CloudFaces.Language.translate('login_popup_incorrect_email'));
                        $('.login-btn').effect("shake")
                        $('.wrong_email').remove();
                        return;
                    } else {
                        $('.err-taken-mail').hide();
                        $('.email-input').removeClass('not-valid');
                    }

                    CloudFaces.Login.startLogin(userData);
                }
            });

            $(document).on('click', '.feedback-popup', function () {
                $('.feedback-popup').remove();
            });

            $(document).on('input', '.required-input', function () {
                CloudFaces.Login.checkForm();
            });

            $(document).on('click', '#terms-cbx', function () {
                CloudFaces.Login.checkForm();
            });

            $(document).on('click', '.reset-password', function (e) {
                e.preventDefault();
                var resetPassForm = $('.reset-password-form').serializeArray();
                var newPass = '';
                var repeatNewPass = '';
                $.each(resetPassForm, function (inx, obj) {
                    if (obj.name == "password") {
                        newPass = obj.value;
                    } else if (obj.name == "repeatNewPass") {
                        repeatNewPass = obj.value;
                    }
                });

                var invalidPass = false;
                if (newPass != repeatNewPass) {
                    invalidPass = true;
                    CloudFaces.Login.showPopup(CloudFaces.Language.translate("login_popup_error"), CloudFaces.Language.translate('password_not_same'));
                }

                if (newPass.length < 3) {
                    invalidPass = true;
                    CloudFaces.Login.showPopup(CloudFaces.Language.translate("login_popup_error"), CloudFaces.Language.translate('small_password'));
                }

                if (newPass.length > 20) {
                    invalidPass = true;
                    CloudFaces.Login.showPopup(CloudFaces.Language.translate("login_popup_error"), CloudFaces.Language.translate('long_password'));
                }

                if (!invalidPass) {
                    var userID = ( 'undefined' !== CloudFaces.Login.user_id ) ? CloudFaces.Login.user_id : '';
                    if( userID ){
                        var updatePassword = {
                            password:newPass
                        }
                        DirectusClient.updateItem('directus_users', userID, updatePassword).then(function (response) {
                            if(response.error){
                                CloudFaces.Login.showPopup(CloudFaces.Language.translate("login_popup_error"), CloudFaces.Language.translate('reset_not_valide_password'));
                            }else{
                                var currentUserData = Object.assign(CloudFaces.Directus.getDirectusUser(), updatePassword);
                                $('.reset-password-form').trigger('reset');
                                CloudFaces.Directus.setDirectusUser(currentUserData);
                                CloudFaces.Login.showPopup(CloudFaces.Language.translate("login_popup_success"), CloudFaces.Language.translate('success_save_profil'));
                                if (CloudFaces.Login.type != 'from_profile') {
                                    CFMenuNavigation.navigate(CloudFaces.Login.Config.pages.login, '');
                                }
                            }
                        }).catch(function (error) {
                            CloudFaces.Login.showPopup(CloudFaces.Language.translate("login_popup_error"), CloudFaces.Language.translate('reset_not_valide_password'));
                        });;
                    }
                }
            });

            $(document).on('click', '.restPass', function (e) {
                e.preventDefault();
                if ($('.resetKey').val() != 0) {

                    var forgottenPassForm = $('.forgottenPassForm').serializeArray();
                    var newPass = '';
                    var repeatNewPass = '';
                    $.each(forgottenPassForm, function (inx, obj) {
                        if (obj.name == "password") {
                            newPass = obj.value;
                        } else if (obj.name == "repeatNewPass") {
                            repeatNewPass = obj.value;
                        }
                    });

                    var invalidPass = false;
                    if (newPass != repeatNewPass) {
                        invalidPass = true;
                        CloudFaces.Login.showPopup(CloudFaces.Language.translate("login_popup_error"), CloudFaces.Language.translate('password_not_same'));
                    }

                    if (newPass.length < 3) {
                        invalidPass = true;
                        CloudFaces.Login.showPopup(CloudFaces.Language.translate("login_popup_error"), CloudFaces.Language.translate('small_password'));
                    }

                    if (newPass.length > 20) {
                        invalidPass = true;
                        CloudFaces.Login.showPopup(CloudFaces.Language.translate("login_popup_error"), CloudFaces.Language.translate('long_password'));
                    }

                    if (!invalidPass) {
                        var reset_token = $('.resetKey').val();
                        CloudFaces.Directus.resetPassword( reset_token, newPass, function(response){
                            console.log('response from resetpassword '+ JSON.stringify(response));
                            if (response.error) {
                                CloudFaces.Login.showPopup(CloudFaces.Language.translate("login_popup_error"), CloudFaces.Language.translate(response.error.message));
                            } else {
                                CloudFaces.Login.showPopup(CloudFaces.Language.translate("login_popup_success"), CloudFaces.Language.translate('success_save_profil'));
                                if (CloudFaces.Login.type != 'from_profile') {
                                    CFMenuNavigation.navigate(CloudFaces.Login.Config.pages.login, '')
                                }
                            }
                        });
                    }
                } else {
                    CloudFaces.Login.showPopup(CloudFaces.Language.translate("login_popup_error"), CloudFaces.Language.translate('no_add_key'));
                }
            });

            $(document).on('click', '.get-reset-key', function () {
                if (CloudFaces.Login.isValidEmail($('.form-for-key input').val())) {
                    var entered_email = $('.form-for-key input').val();

                    DirectusClient.requestPasswordReset(entered_email)
                        .then(function (data){
                            $('.form-for-key').hide();
                            $('.get-reset-key').hide();
                            CloudFaces.Login.showPopup(CloudFaces.Language.translate("Check your email"), CloudFaces.Language.translate('tokenInfo'));
                            setTimeout( function(){
                                if (CloudFaces.Login.type != 'from_profile') {
                                    CFMenuNavigation.navigate(CloudFaces.Login.Config.pages.login, '')
                                }
                            } , 2000 );
                        }).catch(function (error){
                        CloudFaces.Login.showPopup(CloudFaces.Language.translate("login_popup_error"), CloudFaces.Language.translate(error.message));
                    });
                } else {
                    CloudFaces.Login.showPopup(CloudFaces.Language.translate("login_popup_error"), CloudFaces.Language.translate('not_valid_email'));
                }
            });

            $(document).on('click', '.saveprofile', function () {
                var userprofile = $('.profile').serializeArray();
                var userCache = CFPersistentStorage.readValue(CloudFaces.Config.app + '_userData');
                if (userCache) {
                    userCache = JSON.parse(userCache);
                } else {
                    CFMenuNavigation.navigate(CloudFaces.Login.Config.pages.login, '');
                    return;
                }

                CloudFaces.API.User.getUserData(userCache.token, function (userData) {
                    if (userData && Array.isArray(userData) && userData[0]) {
                        userData = userData[0];
                    }

                    var newArry = [];
                    for (var y = 0; y < userprofile.length; y++) {
                        addNew = true;
                        for (var i = 0; i < newArry.length; i++) {
                            if (newArry[i].name == userprofile[y].name) {
                                newArry[i].value += '| ' + userprofile[y].value;
                                addNew = false;
                            }
                        }

                        if (addNew) {
                            newArry.push(userprofile[y])
                        }

                        if (y == userprofile.length - 1) {
                            var addToCFPersistentStorage = userData;
                            $.each(newArry, function (ind, val) {
                                var name = val.name
                                addToCFPersistentStorage[name] = val.value;
                            });

                            newArry.push({
                                name: 'id',
                                value: userData.id
                            })

                            valueImg = $('.profile-iamge-inner').data('name');
                            imgSrc = $('.profile-iamge-inner').data('url')
                            if (imgSrc) {
                                CloudFaces.Login.saveImageprofile(imgSrc, valueImg, userData.id, userData.token)
                            }

                            CFPersistentStorage.writeValue(CloudFaces.Config.app + '_userData', JSON.stringify(addToCFPersistentStorage));
                            CloudFaces.API.User.update(newArry, userData.id, userData.token, function (addToCFPersistentStorage) {
                                CloudFaces.Login.popupStatus('success_save_profil', true);
                            })
                        }
                    }
                })
            })

            // Add profile image
            $(document).on('click', '.change-iamge, .addImage', function () {
                key = $(this).data('name');
                CloudFaces.Login.getPermission('camera', function (callbackCamera) {
                    CloudFaces.Login.getPermission('storage', function (callbackStorage) {
                        if (callbackCamera && callbackStorage) {
                            CloudFaces.Helpers.showLoader();
                            var isLocal = location.href.charAt(0) == 'f';

                            if (isLocal) {
                                defaultResult = "URL";
                            } else {
                                defaultResult = "DATA";
                            }

                            CFPictureChooser.capturePicture('PHOTO', 'URL', function (callback) {
                                if (typeof callback != 'undefined' && callback != null) {
                                    var imageUri = callback;
                                    getDatafromCamera = true;
                                    base64 = false;
                                    if (isLocal) {
                                        CloudFaces.Login.Type.imageBttns(imageUri, key, '', getDatafromCamera, isLocal, base64, function (callback) {
                                            $('.top-section').prepend(callback)
                                        })

                                    } else {
                                        CFPictureChooser.getPictureData(imageUri, function (data) {
                                            base64 = data
                                            CloudFaces.Login.Type.imageBttns(imageUri, key, '', getDatafromCamera, isLocal, base64, function (callback) {
                                                $('.top-section').prepend(callback)
                                            })
                                        });
                                    }

                                    $('.addImage').waitForImages(function () {
                                        $('.addImage').outerHeight($('.addImage').outerWidth());
                                        $('.claims_photo').outerHeight($('.addImage').outerWidth());
                                        CloudFaces.Helpers.hideLoader();
                                    });
                                } else {
                                    CloudFaces.Helpers.hideLoader();
                                }
                            });
                        }
                    })
                })
            });

            $(document).on('click', '.remove-image', function () {
                var thisUrl = $(this).data('url');
                if (thisUrl != '') {
                    CFPictureChooser.deletePicture(thisUrl);
                }
                $('.profile-iamge').css('background', 'url(images/profile.png)');
                $('.profile-iamge-inner').css('background', 'url(images/profile.png)');
                $('.remove-image').hide();
                $('.change-iamge').css({
                    'float': 'none',
                    'margin': '2% auto'
                });

                // delete profile pic from CMS
                var userData = CloudFaces.Helpers.getUserData();
                userData.Image = '';
                CloudFaces.API.User.update(userData, userData.id, userData.token, function (addToCFPersistentStorage) {
                    CloudFaces.Login.popupStatus('success_save_profil', true);
                })
            });

            CloudFaces.Login.init = false;
        }
    },
});

$(document).on('cf-initialized', function () {
    CloudFaces.Login.events();
});
