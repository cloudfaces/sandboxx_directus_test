$.extend(CloudFaces.Slideout, {
    init: function() {
        CloudFaces.Slideout.pages();
    },
    homePage: '',
    firstPage: '',
    pages: function() {
        slideJSON = [];

        /**
         * Add Lang
         **/
        appLangs = CloudFaces.Config.languages;
        currentLang = CloudFaces.Config.currentLanguage;
        for (var i = appLangs.length - 1; i >= 0; i--) {
            activeLang = '';
            if (CloudFaces.Config.currentLanguage == appLangs[i]) {
                activeLang = 'active'
            }
            $('#header #lang').append('<a data-value="' + appLangs[i] + '" class="lang-option ' + activeLang + '" href="#">' + appLangs[i] + '</a>')
        }

        /**
         * Add lists
         **/
        navigationID = CloudFaces.Slideout.Config.navigationID;
        navigationID = navigationID.replace(/\s/g, "");
        navigationID = navigationID.replace(';', ',');
        navigationID = navigationID.split(',');

        langKeys = CloudFaces.Slideout.Config.dataLangKeys;
        langKeys = langKeys.replace(/\s/g, "");
        langKeys = langKeys.replace(';', ',');
        langKeys = langKeys.split(',');

        CloudFaces.Slideout.firstPage = navigationID[0];
        for (n = 0; n < navigationID.length; n++) {
            for (l = 0; l < langKeys.length; l++) {
                addActiveClass = '';
                addFirstPageContext = '\'\'';
                if (n == 0 && l == 0) {
                    if (CloudFaces.Slideout.Config.taxiSlideout) {
                        addActiveClass = 'active';
                        addFirstPageContext = '\'\'';
                    } else {
                        CloudFaces.Slideout.homePage = navigationID[n];
                        addFirstPageContext = "JSON.stringify({ 'hide': true })";
                    }
                }

                if (n == l) {
                    if (CloudFaces.Slideout.Config.taxiSlideout && langKeys[l] == "driver") {
                        $('.slide_menu').append('<li class="' + addActiveClass + '">' +
                            '<a  class="' + langKeys[l] + '" data-lang="' + langKeys[l] + '" data-name="' + langKeys[l] + '">' +
                            CloudFaces.Language.translate(langKeys[l]) +
                            '<label class="switch">' +
                            '<input type="checkbox" checked>' +
                            '<div class="slider round">' +
                            '</div>' +
                            '</label>' +
                            '</a>' +
                            '</li>')
                    } else {
                        $('.slide_menu').append('<li class="' + addActiveClass + '" onclick="CFMenuNavigation.navigate(\'' + navigationID[n] + '\', ' + addFirstPageContext + ')">' +
                            '<a  class="' + langKeys[l] + '" data-lang="' + langKeys[l] + '" data-name="' + langKeys[l] + '">' +
                            CloudFaces.Language.translate(langKeys[l]) +

                            '</a>' +
                            '</li>')
                    }
                }
            }
        }

        /**
         * Add buttn for cart
         **/

         if (CloudFaces.Slideout.Config.addButtnCart && CloudFaces.Megacart) {
            $('.slide_menu').after('<div onclick="CFMenuNavigation.navigate(\'' + CloudFaces.Megacart.Config.pages.cart + '\', \'\')" class="slideout_call">' +
                CloudFaces.Language.translate('cart') +
                '</div>')


            // var menuHeight = $(window).height() - $('#header').outerHeight(true) - $('.slideout_call').outerHeight(true) - 20;
            // alert(menuHeight)
            // $('.slide_menu').height($(window).height() - $('#header').outerHeight(true) - $('.slideout_call').outerHeight(true) - 20);

            var bottomPadding  = $('.slideout_call').outerHeight(true) + $('.ext_footer').outerHeight(true) + 10; //+10 for beaty reasons
            // alert(menuHeight)
            $('.ext_footer').css('background','#ffffff')
            $('.slideout_call').css('background','#ffffff')

            $('.slide_menu').css('height','100%');
            $('.slide_menu').css('overflow-y','auto');
            $('.slide_menu').css('paddingBottom', bottomPadding + 'px');

            $('.slideout_call').prepend('<i class="fa fa-shopping-cart" aria-hidden="true"></i>')
            // $('.slide_menu').css('overflow', 'scroll')
            // if (CloudFaces.Slideout.Config.footerMtelBtn) {
            //     $('.slideout_call').css('bottom', $('.slideout_call').outerHeight(true));
            // }
        }
        // console.log(CloudFaces.Slideout.Config.footerMtelBtn);
        // add button for Mtel smart apps
        if (CloudFaces.Slideout.Config.footerMtelBtn) {
            var prevElementSelector = '.slide_menu';
            var footerSelector = '.slideout-footer';


            if (CloudFaces.Slideout.Config.addButtnCart) {
                prevElementSelector = '.slideout_call';
            }

            $(footerSelector).append(
                '<div class="ext_footer">' +
                    '<img src="images/A1-logo-slide-out.png" />' +
                    '<button>Smart App</button>' +
                '</div>');

            $(document).on('click', '.ext_footer', function() {
                if ($('body').hasClass('ios')) {
                    CFExternal.openUrlInSafari(CloudFaces.Slideout.Config.footerBtnLink);
                } else {
                    window.open(CloudFaces.Slideout.Config.footerBtnLink);
                }
            })

            var bottomPadding  = $('.slideout_call').outerHeight(true) + $('.ext_footer').outerHeight(true) + 10; //+10 for beaty reasons
            // alert(menuHeight)
            $('.ext_footer').css('background','#ffffff')
            $('.slideout_call').css('background','#ffffff')            
            $('.slide_menu').css('height','100%');
            $('.slide_menu').css('overflow-y','auto');
            $('.slide_menu').css('paddingBottom', bottomPadding + 'px');



            // $('.slide_menu').css('height', '63vh');
            $('.slideout_call').css('bottom','13%')

        }

        // $('.driver, .select_car, .orders, .dispatcher, .dispatcher').parent().hide();

        /**
         * Add buttn for Log out
         **/
        CloudFaces.Slideout.addLogout()
    },
    addLogout: function() {
        if (CloudFaces.Slideout.Config.addLogOutButtn) {
            CloudFaces.Helpers.checkLogin(function(result) {
                if (result) {
                    var userData = CloudFaces.Helpers.getUserData();
                    if (CloudFaces.Slideout.Config.taxiSlideout) {
                        if ((typeof userData != 'undefined' && userData != '') && JSON.parse(userData.category_id) == CloudFaces.Settings.Config.driverCategory) {
                            $('.driver').parent().show();
                            $('.select_car').parent().show();
                            $('.orders').parent().show();
                            $('.dispatcher').parent().hide();
                            $('.order_taxi').parent().hide();
                            $('.order_history').parent().hide();
                            $('.call_last_driver').parent().hide();
                        } else if ((typeof userData != 'undefined' && userData != '') && JSON.parse(userData.category_id) == CloudFaces.Settings.Config.drispatcher) {
                            $('.dispatcher').parent().show();
                            $('.orders').parent().show();
                            $('.driver').parent().hide();
                            $('.select_car').parent().hide();
                            $('.order_taxi').parent().hide();
                            $('.order_history').parent().hide();
                            $('.call_last_driver').parent().hide();
                        } else {
                            $('.driver').parent().hide();
                            $('.select_car').parent().hide();
                            $('.orders').parent().hide();
                            $('.dispatcher').parent().hide();
                            $('.order_taxi').parent().show();
                            $('.order_history').parent().show();
                            $('.call_last_driver').parent().show();
                        }
                    }

                    if ($('#logout').length == 0) {
                        $('a.profile').parent().show();
                        $("ul.slide_menu").find('a.login').parent().hide();
                        $('.slide_menu')
                            .append(
                                '<li id="logout" onclick="">' +
                                '<a  class="logOut" data-name="">' + CloudFaces.Language.translate('logOut') + '</a>' +
                                '</li>')
                    }
                } else {
                    if ($('#logout').length == 0) {
                        $('.profile').parent('li').hide()
                    }
                }
            })
        }
    },
    align: function() {
        if (CloudFaces.Language.current == 'fa') {
            $('#slideout ul li').addClass('right-align');
            $('#slideout ul li.active').addClass('right-border');
            $('#slideout.ios').addClass('slideout-r2l-ios-fix');
        } else {
            $('#slideout ul li').removeClass('right-align');
            $('#slideout ul li.active').removeClass('right-border');
            $('#slideout.ios').removeClass('slideout-r2l-ios-fix');
        }
    }
})

CloudFaces.Events = {
    init: function() {
        $(document).on('click', '.slider ', function(e) {
            putData = {};
            if ($('.switch input').is(':checked')) {
                putData.available = "false"
            } else {
                putData.available = "true"
            }
            userData = CloudFaces.Helpers.getUserData();
            checkDriver = { 'driver_id': userData.id }
            CloudFaces.API.list.customSql(CloudFaces.Config.app, CloudFaces.Settings.Config.userGetDriverInfo, checkDriver, function(items) {
                items = JSON.parse(items).Data[0];
                if (items) {
                    putData.id = items.id;
                }
                CloudFaces.API.list.put(CloudFaces.Config.app, CloudFaces.Settings.Config.tables.driverInfo, putData, function(data) {})
            })
        })

        $('#slideout .slide_menu li').on('click', function() {
            $('#slideout .slide_menu li').removeClass('active');
            $(this).addClass('active');

            CloudFaces.Slideout.align();
        });

        $(document).on('click', '.lang .lang-option', function() {
            var lang = $(this).data('value');
            if (lang) {
                CloudFaces.Language.set(lang);
                if (CloudFaces.Language.get() == 'fa') {
                    CFRuntime.rightToLeftLayout('right');
                } else {
                    CFRuntime.rightToLeftLayout('left');
                }
                $('#lang .lang-option').removeClass('active');
                $(this).addClass('active');
                $('#slideout .slide_menu li').removeClass('active');
                $('#slideout .slide_menu li:first').addClass('active');
                CloudFaces.Slideout.align();
                $('.slideout_call').text(CloudFaces.Language.translate('cart'));
                if (CloudFaces.Slideout.Config.taxiSlideout) {
                    userData = CloudFaces.Helpers.getUserData();
                    if ((typeof userData != 'undefined' && userData != '') && JSON.parse(userData.category_id) == CloudFaces.Settings.Config.driverCategory) {
                        CFMenuNavigation.navigate('orders', '');
                    } else if ((typeof userData != 'undefined' && userData != '') && JSON.parse(userData.category_id) == CloudFaces.Settings.Config.drispatcher) {
                        CFMenuNavigation.navigate('driverOverview', '');
                    } else {
                        CFMenuNavigation.navigate('callDriver', '');
                    }
                } else {
                    CFMenuNavigation.navigate(CloudFaces.Slideout.homePage, '');
                }
            }
        });

        $(document).on('click', '#foz', function() {
            var link = $(this).data('href');
            if ($('body').hasClass('ios')) {
                CFMenuNavigation.navigate('order_link', '')
            } else {
                location.href = link;
            }
        })

        $(document).on('click', '.login', function() {
            if ($('#logout').length == 0) {
                CloudFaces.Slideout.addLogout();
            }
        })

        $(document).on('click', '.call_last_driver', function(e) {
            telephone = CFPersistentStorage.readValue(CloudFaces.Config.app + '_last_driver')
            CloudFaces.Helpers.phoneCall(telephone);
        })
        $(document).on('click', '#logout', function(e) {
            e.preventDefault();
            var token = CloudFaces.Helpers.getUserData().token;
            CloudFaces.API.User.logOut(token, function(userData) {
                $('#logout').remove();
                $("ul.slide_menu").find('a.login').parent().show();
                $("ul.slide_menu").find('a.profile').parent().hide();
                CloudFaces.Slideout.addLogout();
                if (CloudFaces.Slideout.Config.taxiSlideout) {
                    $('.driver, .select_car, .orders, .dispatcher, .dispatcher').parent().hide();
                    $('.order_taxi').parent().show();
                    $('.order_history').parent().show();
                    $('.call_last_driver').parent().show();
                    CFPersistentStorage.writeValue(CloudFaces.Config.app + '_order', '')
                    CFPersistentStorage.writeValue(CloudFaces.Config.app + '_order_options', '')
                }
                CFPersistentStorage.writeValue(CloudFaces.Config.app + '_userData', '');
                CFMenuNavigation.navigate(CloudFaces.Slideout.homePage, '');
            });

        })
    }
}

$(document).on('cf-initialized', function() {
    CloudFaces.Slideout.init();
    CloudFaces.Events.init();
});
