if (typeof(CloudFaces.Slideout) == 'undefined') CloudFaces.Slideout = {Config: {}};
$.extend(CloudFaces.Slideout.Config,{
  addButtnCart: false,
  addLogOutButtn: false,
  footerMtelBtn: false,
  footerBtnLink: "http://www.mtel.bg/smart-app",
  dataLangKeys: "login, profile, matches",
  navigationID: "login_login, profile, matches",
  pages: {
    navdrawer: "slideout_navdrawer",
    main: "slideout_main"
  }
});