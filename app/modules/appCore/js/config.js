$.extend(CloudFaces, {
	Config:{
		app: CloudFaces.Config.app,
		ApiURL: CloudFaces.Config.ApiURL,
		applicationName: CloudFaces.Config.applicationName,
		AppHash: CloudFaces.Config.applicationHash,
		timThumbUrl:CloudFaces.Config.ApiURL+'/helper/timthumb.php?src=',
		tables:{
			languages:CloudFaces.AppCore.Config.tables.languages,
			users:'',
			profileTemplate:'',
		},
		options:{
			contactCallPhone:'phone'
		},
		currentLanguage:'en',
		languages:['en'],
	}
});
CloudFaces.Welcome={
	Config:{
		moduleName:'Welcome',
		emptyIconUrl:CloudFaces.Config.ApiURL+'/files/project/a'+CloudFaces.Config.app+'/notifications/icons/6.png',
		tables:{
			welcome:CloudFaces.AppCore.Config.tables.welcome
		}
	}
};
CloudFaces.Version={
	Config:{
		moduleName:'Version',
		imagesRelPath:(location.pathname.indexOf('modules')>=0) ? '' : '../appCore/',
		currentVersion:'5.0.0',
	}
};
CloudFaces.Notification={
	Config:{
		path:'notifications'
	}
};
CloudFaces.GeoPush = {
	Config:{
		fencesUrl:CloudFaces.Config.ApiURL+'/fences/project/id/',
		fenceLogUrl:function(appId, ain, os){
			return CloudFaces.Config.ApiURL+'/fences/project/id/'+appId+'/ain/'+ain+'/os/'+os+'/';
		}
	}
};