CloudFaces.Analytics = (function(page_id, page_title) {

    return new function() {

        // PARAMETERS
        var parent = this;

        this.analytics_url = CloudFaces.Config.ApiURL + 'stats/';
        this.project_id = CloudFaces.Config.app;
        this.page_id = page_id || 0;
        this.page_title = page_title || "";
        this.debug = CloudFaces.hasOwnProperty('debug') ? CloudFaces.debug : (window.location.href.indexOf('http') >= 0);

        // METHODS

        // PUBLIC
        this.set = function(callback) {
            var app_instance_id = CloudFaces.DeviceCode.getDeviceCode();
            if (typeof app_instance_id == 'undefined' || !app_instance_id || app_instance_id == 'undefined') {
                return false;
            }

            parent.send(app_instance_id, callback);
        };

        // PRIVATE
        this.send = function(app_instance_id, callback) {
            $.ajax({
                url: parent.analytics_url,
                type: 'PUT',
                data: {
                    device_id: app_instance_id,
                    project_id: parent.project_id,
                    page_id: parent.page_id,
                    page_title: parent.page_title,
                    debug: this.debug
                },
                dataType: 'json',
                success: function(result) {
                    if (result && result['Body']) {
                        var body = JSON.parse(result['Body']);
                        if (typeof callback == 'function' && typeof body != 'undefined' && body && typeof body['Status'] != 'undefined' && body['Status'] == 'Success' && body['Data']) {
                            callback(body);
                        }
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {}
            });
        }
    }
});
