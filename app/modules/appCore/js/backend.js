if (typeof CloudFaces == 'undefined') {
  CloudFaces = { AppCore: {Config: {}}, Directus: { Config: {} }}
} else {
  CloudFaces.AppCore = {Config: {}};
  CloudFaces.Directus = {Config: {}};
}
CloudFaces.AppCore.Config.tables = {
  languages: "20479",
  welcome: "20482"
};
CloudFaces.Config = {};
CloudFaces.Config.app = 3159;
CloudFaces.Config.ApiURL = 'https://api.cloudfaces.com/v1/';
CloudFaces.Config.applicationName = '';
CloudFaces.Config.applicationHash = '9fb1dd033f1c4db172318a124ade81fc2c2bf193689255d31d346afaa203ba60';
CloudFaces.Directus.Config.project = 9877;
// CloudFaces.Directus.Config.url = 'http://203.109.113.157/cf_directus/public';
CloudFaces.Directus.Config.url = 'https://directus.cloudfaces.com';