var url             = CloudFaces.Directus.Config.url;
var projectId       = CloudFaces.Directus.Config.project;
var DirectusClient  = new DirectusSDK({
                        url: url,
                        project: projectId
                    });

$.extend(CloudFaces.Directus, {
    directusLogin: function(params) {
        //console.log('Params for directusLogin:', params);
        return new Promise(function (resolve, reject) {
            DirectusClient.login({
                email: params.email,
                password: params.password,
            }).then(function (token) {
                DirectusClient.getMe().then(function (userData) {
                    var currentUserData = CloudFaces.Directus.getDirectusUser() ? Object.assign(CloudFaces.Directus.getDirectusUser(), params) : params;
                    var newUserData = Object.assign(currentUserData, userData.data, {token: token});
                    CloudFaces.Directus.setDirectusUser(newUserData);
                    resolve(newUserData);
                }).catch(function (error) {
                    reject(error);
                });
            }).catch(function (error) {
                // re-assign token
                DirectusClient.token = CloudFaces.Directus.getCurrentDirectustoken();
                reject(error);
            });
        });
    },

    /**
     * Store user data to local storage
     * @param {*} data 
     */
    setDirectusUser: function(data) {
        // delete data['password'];
        CFPersistentStorage.writeValue('cf_directus_'+projectId+'_user', JSON.stringify(data));
    },

    /**
     * Get current user data
     */
    getDirectusUser: function() {
        return CFPersistentStorage.readValue('cf_directus_'+projectId+'_user') ?
            JSON.parse(CFPersistentStorage.readValue('cf_directus_'+projectId+'_user')) : '';
    },

    /**
     * Get current user directus token
     */
    getCurrentDirectustoken() {
        var currentUserData = CloudFaces.Directus.getDirectusUser() ? CloudFaces.Directus.getDirectusUser() : "";
        var token = currentUserData ? currentUserData.token.token : "";
        return token;
    },

    /**
     * Update user diretus token
     *
     * @param {String} token
     */
    updateDirectusToken: function(token) {
        var userData = CloudFaces.Directus.getDirectusUser();
        if (token && userData) {
            var newTokenData = Object.assign({}, userData.token, {token: token});
            var newUserData = Object.assign(userData, {token: newTokenData});
            DirectusClient.token = token;
            CloudFaces.Directus.setDirectusUser(newUserData);
        }
    },

    /**
     * Check if user is logged in
     */
    checkLogin: function(){
        var userData = CloudFaces.Directus.getDirectusUser();
        if (!userData) {
            CFMenuNavigation.navigate(CloudFaces.SandBoxx.Config.pages.login, '');
            return false;
        }
        return true;
    },

    /**
     * Process the registration
     * 
     * @param {object} params 
     */
    directusRegisterUser: function(params) {
        // console.log('register function params:' + JSON.stringify(params));
        return new Promise(function (resolve, reject) {
            DirectusClient.getMe().then(function (user) {
                var user = user.data;
                // console.log('getMe() user:' + JSON.stringify(user));
                var newUserData = {
                    email: params.email,
                    password: params.password,
                    first_name: params.email, // Since we don't have name field, set email for now
                    registered: true, // Set user as registered user
                };
                DirectusClient.updateItem('directus_users', user.id, newUserData).then(function (response) {
                    var currentUserData = Object.assign(CloudFaces.Directus.getDirectusUser(), newUserData);
                    CloudFaces.Directus.setDirectusUser(currentUserData);
                    resolve(response);
                }).catch(function (error) {
                    reject(error);
                });
            });
        });
    },

    /**
     * Process the registration
     * 
     * @param {object} params 
     */
    directusRegisterUserByAdmin: function(params) {
        return new Promise(function (resolve, reject) {
            var deviceAin = CloudFaces.DeviceCode.getDeviceAin();
            var deviceCode = CloudFaces.DeviceCode.getDeviceCode();
            var  selectedTabRoleId = CloudFaces.Helpers.getTabRoleId();
            var cohort_id = params.cohort_id;
            var newUserData = {
                email: params.email,
                password: params.password,
                device_ain:deviceAin,
                device_code:deviceCode,
                role:3,
                role_id: selectedTabRoleId,
                status:'active'
            };
            DirectusClient.createItem('directus_users', newUserData).then(function (response) {
                console.log(response);
                CFPersistentStorage.writeValue('selectedTabRoleId', '');
                // Assign the user to cohort
                var co_user_params = {
                    'cohorts_id' : cohort_id,
                    'directus_users_id' : response.data.id, // Or response.data.id not sure
                };
                DirectusClient.createItem('cohorts_directus_users', co_user_params).then(function (response_data) {
                    resolve(response);
                });
            }).catch(function (error) {
                reject(error);
            });
        });
    },

    /**
     * Log out the current user
     */
    directusLogoutUser: function() {
        DirectusClient.logout();
    },

    /**
     * Store the feedback to Directus
     * 
     * @param {String} email 
     * @param {String} text 
     */
    sendFeedback: function(email, text) {
        return new Promise(function(resolve, reject) {  
            var feedbackData = {
                email: email,
                text: text,
            };
            DirectusClient.createItem('feedback', feedbackData).then(resolve);
        });
    },

    /**
     * Fetch the list of FAQs
     */
    getFaqs: function() {
        return new Promise(function(resolve, reject) {  
            DirectusClient.getItems('faq').then(resolve);
        });
    },

    /**
     * Get details of single faq
     * @param {int} faq_id 
     */
    getFaq: function(faq_id) {
        return new Promise(function(resolve, reject) {
            DirectusClient.getItem('faq', faq_id).then(resolve);
        });
    },

    /**
     * Request the Reset the password token for user 
     * 
     * @param {String} email 
     */
    requestResetPassword: function(email,callback) {
        return new Promise(function(resolve, reject) {
            DirectusClient.requestPasswordReset(email).then(resolve);
            callback();
        });
    },

    /**
     * Reset the password for user 
     * Note: DirectusClient.resetPassword is not present in sdk-js at this moment
     * 
     * @param {String} token
     * @param {String} password 
     */
    resetPassword: function(token, password, callback) {
        var data = {
            token: token,
            password: password
        };
        var directus_token = CloudFaces.Directus.getDirectusUser().token.token;
        var ajaxParams = {
            url: url + '/' + projectId + '/auth/password/reset',
            type: 'post',
            data: data,
            cache:false,
            headers: {"Authorization": 'Bearer '+ directus_token},
            success: function(data) {
                //console.log('data from success ' + JSON.stringify(data));
                callback(data);
            },
            error: function(error) {
                //console.log('data from error ' + JSON.stringify(error));
                callback(JSON.parse(error.responseText));
            }
        };

        $.ajax(ajaxParams);
    },

    removeItem: function(collection, itemId, callback) {
        var directus_token = CloudFaces.Directus.getDirectusUser().token.token;

        var ajaxParams = {
            url: url + '/' + projectId + '/items/'+ collection +'/'+ itemId,
            type: 'delete',
            cache:false,
            headers: {"Authorization": 'Bearer '+ directus_token},
            success: function(data) {
                //console.log('data from success ' + JSON.stringify(data));
                callback(data);
            },
            error: function(error) {
                //console.log('data from error ' + JSON.stringify(error));
                callback(error);
            }
        };

        $.ajax(ajaxParams);
    },

    /**
     * Fetch the Terms and condition
     */
    getTerms: function() {
        return new Promise(function(resolve, reject) {  
            DirectusClient.getItems('terms_and_conditions').then(resolve);
        });
    },

    /**
     * 
     * Fetch the list of resources
     */
    getResources: function() {
        return new Promise(function(resolve, reject) {  
            DirectusClient.getItems('resources').then(resolve);
        });
    },

    /**
     * Get details of single resource
     * @param {int} resource_id 
     */
    getResource: function(resource_id) {
        return new Promise(function(resolve, reject) {
            DirectusClient.getItem('resources', resource_id).then(resolve);
        });
    },

    S4:function() {
        return (((1+Math.random())*0x10000)|0).toString(16).substring(1); 
    },

    getUserRequests: function (user_1_id, user_2_id, cohort_id) {
        return new Promise(function (resolve, reject) {
            var directus_token = CloudFaces.Directus.getDirectusUser().token.token;
            var ajaxParams = {
                url: url + '/' + projectId + '/custom/sandboxx/get-requests?cohort_id='+ cohort_id + '&user_id_1='+user_1_id+'&user_id_2='+user_2_id,
                type: 'get',
                cache: false,
                headers: {"Authorization": 'Bearer '+ directus_token},
                success: function(mathes) {
                    if(mathes){
                        resolve(mathes);
                    }
                },
                error: function(error) {
                    reject(error);
                }
            };

            $.ajax(ajaxParams);
        })
    },

    sendMatchRequest : function (sponsor_id, sponsoree_id, sender_id, cohort_id, match_percent, ain, timezone, text) {
        return new Promise(function (resolve, reject) {
            var putData = {
                sponsor_id: sponsor_id,
                sponsoree_id: sponsoree_id,
                sender_id: sender_id,
                cohort_id: cohort_id,
                status: 'request',
                match_percent: match_percent
            }
            DirectusClient.createItem("matches", putData).then(function (data) {
                CloudFaces.SandBoxx.DataProxy.sendPush(ain, timezone, text).then(resolve);
            });
        });
    },

    confirmRequest : function (sponsor_id, sponsoree_id, cohort_id, sender_id, match_percent, matched_on, ain, timezone, text,matchesId) {
        return new Promise(function (resolve, reject) {
            var directus_token = CloudFaces.Directus.getDirectusUser().token.token;
            var ajaxParams = {
                url: url + '/' + projectId + '/custom/sandboxx/delete-non-history-matches?cohort_id='+ cohort_id + '&user_id_1='+sponsor_id+'&user_id_2='+sponsoree_id,
                type: 'delete',
                cache: false,
                headers: {"Authorization": 'Bearer '+ directus_token},
                success: function(response) {
                    var putData = {
                        status: 'active',
                        matched_on: matched_on
                    }
                    DirectusClient.updateItem("matches",matchesId,putData).then(function (data) {
                        CloudFaces.SandBoxx.DataProxy.sendPush(ain, timezone, text).then(resolve);
                    });
                },
                error: function(error) {
                    reject(error);
                }
            };

            $.ajax(ajaxParams);
        });
    },
        senderAlreadyHasRequest:function(userId,cohortId,role_id){
            return new Promise(function (resolve, reject) {
                var filter = {};
                if(role_id == CloudFaces.SandBoxx.Config.constants.role_sponsor){
                    filter = {sponsor_id:userId,cohort_id:cohortId,status:'active'}
                }else {
                    filter = {sponsoree_id:userId,cohort_id:cohortId,status:'active'}
                }

                DirectusClient.getItems('matches', {
                    filter,
                    fields: "*.*"
                }).then(function (response) {
                    resolve(response);
                })
            });
        },
        matchForcefullyRequest : function (sponsor_id, sponsoree_id, cohort_id, sender_id, match_percent, matched_on, ain, timezone, text) {
            return new Promise(function (resolve, reject) {
                var directus_token = CloudFaces.Directus.getDirectusUser().token.token;
                var ajaxParams = {
                    url: url + '/' + projectId + '/custom/sandboxx/delete-non-history-matches?cohort_id='+ cohort_id + '&user_id_1='+sponsor_id+'&user_id_2='+sponsoree_id,
                    type: 'delete',
                    cache: false,
                    headers: {"Authorization": 'Bearer '+ directus_token},
                    success: function(response) {
                        var item = {
                            sponsor_id: sponsor_id,
                            sponsoree_id: sponsoree_id,
                            sender_id: sender_id,
                            cohort_id: cohort_id,
                            status: 'active',
                            match_percent: match_percent,
                            matched_on: matched_on
                        }
                        DirectusClient.createItem("matches",item).then(function (data) {
                            CloudFaces.SandBoxx.DataProxy.sendPush(ain, timezone, text).then(resolve);
                        });
                    },
                    error: function(error) {
                        reject(error);
                    }
                };
    
                $.ajax(ajaxParams);
            });
        },
    declineRequest: function (request_id, ain, timezone, text) {
        return new Promise(function (resolve, reject) {
            var updateItem = {
                status:'declined'
            }
            DirectusClient.updateItem("matches", request_id,updateItem).then(function (data) {
                CloudFaces.SandBoxx.DataProxy.sendPush(ain, timezone, text).then(resolve);
            });
            
        })
    },

    cancelSentRequest: function (request_id, ain, timezone, text) {
        return new Promise(function (resolve, reject) {
            DirectusClient.deleteItem("matches", request_id).then(function (data) {
                CloudFaces.SandBoxx.DataProxy.sendPush(ain, timezone, text).then(resolve);
            });
        })
    },

    unmatchRequest: function (request_id, sender_id, unmatch_feedback) {
        return new Promise(function (resolve, reject) {
            var putData = {
                sender_id: sender_id,
                status: 'unmatching',
                unmatch_feedback: unmatch_feedback,
                id: request_id,
            }
            DirectusClient.updateItem("matches",request_id, putData).then(function (data) {
                resolve();
            });
        })
    },

    getActiveRequests: function (user_1_id, cohort_id) {
        return new Promise(function (resolve, reject) {
            var directus_token = CloudFaces.Directus.getDirectusUser().token.token;
            var ajaxParams = {
                url: url + '/' + projectId + '/custom/sandboxx/get-active-matches?cohort_id='+ cohort_id + '&user_id='+user_1_id,
                type: 'GET',
                cache: false,
                headers: {"Authorization": 'Bearer '+ directus_token},
                success: function(request_match_data) {
                    var matchData = JSON.stringify(request_match_data);
                    resolve(CloudFaces.SandBoxx.Helpers.sqlParse(matchData));
                },
                error: function(error) {
                    var errorData = JSON.stringify(error);
                    reject(errorData);
                }
            };

            $.ajax(ajaxParams);
        });
    },

    arhiveRequest: function (request_id, ain_1, timezone_1, ain_2, timezone_2, text) {
        return new Promise(function (resolve, reject) {
            var putData = {
                status: 'history',
                id: request_id
            }
            DirectusClient.updateItem("matches",request_id, putData).then(function (data) {
                CloudFaces.SandBoxx.DataProxy.sendPush(ain_2, timezone_2, text).then(resolve);
            });
        })
    },

    getSentRequests: function (user_1_id, cohort_id,role_id) {
        return new Promise(function (resolve, reject) {
            var filter = {};
            if(role_id == CloudFaces.SandBoxx.Config.constants.role_sponsor){
                filter = {sponsor_id:user_1_id,cohort_id:cohort_id,status:'request'}
            }else {
                filter = {sponsoree_id:user_1_id,cohort_id:cohort_id,status:'request'}
            }

            DirectusClient.getItems('matches', {
                filter,
                fields: "*.*"
            }).then(function (response) {
                resolve(response);
            })
        })
    },

    retriveFile: function (file_id) {
        var directus_token = CloudFaces.Directus.getDirectusUser().token.token;

        var ajaxParams = {
            url: url + '/' + projectId + '/files/'+ file_id,
            type: 'get',
            cache: false,
            headers: {"Authorization": 'Bearer '+ directus_token},
            success: function(data) {
                console.log('data from success ' + JSON.stringify(data));
                if (  typeof data.data !== 'undefined') {
                    return data.data.fullurl;
                } else {
                    return '';
                }
            },
            error: function(error) {
                console.log('data from error ' + JSON.stringify(error));
                return '';
            }
        };

        $.ajax(ajaxParams);
    },
    getCohorts: function (user_id) {
        return new Promise(function(resolve, reject) {
            var cohort_params = {
                filter: {
                    "users.directus_users_id": {
                        eq: user_id
                    },
                },
                fields: "id,name,users.*"
            };
    
            DirectusClient.getItems('cohorts', cohort_params).then(resolve);
        });
        
    }
});

function cfInit() {
    $(document).trigger('cf-initialized');
}

var clearIntervalId = setInterval(function () {
    if (typeof CFPersistentStorage != "undefined") {
        var deviceAin = CloudFaces.DeviceCode.getDeviceAin();
        var deviceCode = CloudFaces.DeviceCode.getDeviceCode();
        if (deviceAin != '0' && deviceCode != '0') {
            clearInterval(clearIntervalId);
            var appUser = CloudFaces.Directus.getDirectusUser();
            var currentDateTimestamp = (new Date()).getTime();
            if (typeof appUser != "undefined" && appUser) {
                var userTokenValue = appUser.token.token;
                if (userTokenValue) {
                    DirectusClient.refresh(userTokenValue).then(function (data) {
                        var newToken = data.data.token;
                        DirectusClient.token = newToken;
                        CloudFaces.Directus.updateDirectusToken(newToken);
                        cfInit();
                    }).catch(function (refreshError) {
                        console.log('refreshError', refreshError);
                        CloudFaces.Directus.directusLogin({
                            email: appUser.email,
                            password: appUser.password
                        }).then(function () {
                            cfInit();
                        });
                    })
                } else {
                    CloudFaces.Directus.directusLogin({
                        email: appUser.email,
                        password: appUser.password
                    }).then(function () {
                        cfInit();
                    });
                }
            } else {
                var dateTimestamp = currentDateTimestamp.toString();
                var userEmail = "a_" + deviceAin + "_" + dateTimestamp + "@cloudfaces.com";
                var userPassword = "Anonymous_" + deviceCode;
                var userData = {
                    status: "active",
                    role: 3,
                    first_name: "Anonymous_" + deviceAin + "_" + dateTimestamp,
                    email: userEmail,
                    password: userPassword,
                    company: "Anonymous Company",
                    title: "Anonymous User_" + deviceAin,
                    device_ain: deviceAin,
                    device_code: deviceCode
                };
                DirectusClient.createItem("directus_users", userData).then(function (data) {
                    CloudFaces.Directus.directusLogin(userData).then(function (response) {
                        cfInit();
                    });
                });
            }
        }
    }
}, 100);

var interval = setInterval(function () {
    clearInterval(interval);
    CFPersistentStorage.writeValue(CloudFaces.Config.app + '_userData', '');
    CFPersistentStorage.writeValue('cf_directus_' + CloudFaces.Directus.Config.project + '_user', '');
    CFVariableStorage.writeValue('profile_refresh', 'true');
    CFVariableStorage.writeValue('app_started', 'true');
    CFPersistentStorage.writeValue('selected_cohort', '');
    CloudFaces.Directus.directusLogoutUser(); //Logout from Directus
    // "login_login" hardcoded value from  "CloudFaces.SandBoxx.Config.pages.login"
    CFMenuNavigation.navigate('login_login', '');
}, 1170000);