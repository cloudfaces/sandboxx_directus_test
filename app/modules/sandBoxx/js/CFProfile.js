$.extend(CloudFaces.SandBoxx, {
    Profile: {

        container: '',
        wrapper: '',
        type: '',
        user_data: '',

        hidden_fields: ['role_id', 'image', 'device_ain', 'timezone', 'location'],
        menu_links: ['faq', 'resources', 'feedback', 'terms'],
        timeout_miliseconds: 1000,
        timeout: '',
        users: [],
        cohort_id: '',
        Main: {
            init: function (container) {
                CloudFaces.SandBoxx.Profile.Main.preparePage(container)
                    .then(CloudFaces.SandBoxx.Profile.Main.appendPopup)
                    .then(CloudFaces.SandBoxx.Profile.Main.renderPicture)
                    .then(CloudFaces.SandBoxx.Profile.Main.prepareTemplates)
                    .then(CloudFaces.SandBoxx.Profile.Main.renderProfileFields)
                    .then(CloudFaces.SandBoxx.Profile.Main.renderQuestionFields)
                    .then(CloudFaces.SandBoxx.Profile.Main.renderLinkedInField)
                    .then(CloudFaces.SandBoxx.Profile.Main.renderCohortSelection)
                    .then(CloudFaces.SandBoxx.Profile.Main.renderSettings)
                    .then(CloudFaces.SandBoxx.Profile.Main.renderForcefullyMatch)
                    .then(CloudFaces.SandBoxx.Profile.Main.renderSentMatchRequest)
                    .then(CloudFaces.SandBoxx.Profile.Main.prepareButtons)
                    .then(CloudFaces.SandBoxx.Profile.Main.renderButtons)
                    .then(CloudFaces.SandBoxx.Profile.Main.renderMenuLinks)
                    .then(CloudFaces.SandBoxx.Profile.Events);
            },
            preparePage: function (container) {
                return new Promise(function (resolve, reject) {
                    CloudFaces.SandBoxx.Profile.cohort_id = CFPersistentStorage.readValue("selected_cohort");
                    CloudFaces.SandBoxx.Helpers.registerPageAnalytics();
                    CloudFaces.Helpers.showLoader();
                    CloudFaces.SandBoxx.Profile.container = container;
                    var userData = CloudFaces.Helpers.getUserData();
                    var p_info = CloudFaces.SandBoxx.Helpers.jsonParse(CFPersistentStorage.readValue("profile_info"));

                    CloudFaces.SandBoxx.Profile.Navigation.clearStorage();

                    if (!p_info.type) {
                        CloudFaces.SandBoxx.Profile.type = userData.role == CloudFaces.SandBoxx.Config.constants.role_admin ? 'admin_view' : 'edit';
                    }
                    else {
                        CloudFaces.SandBoxx.Profile.type = p_info.type
                    }
                    console.log(CloudFaces.SandBoxx.Profile.type);
                    var tpl_path = '';
                    if (CloudFaces.SandBoxx.Profile.type == "admin_view" || CloudFaces.SandBoxx.Profile.type == "edit" || CloudFaces.SandBoxx.Profile.type == "admin_edit") {
                        tpl_path = 'templates/profile_edit_wrapper.html';
                    }
                    else if (CloudFaces.SandBoxx.Profile.type == "view") {
                        tpl_path = 'templates/profile_view_wrapper.html';
                    }
                    else {
                        tpl_path = 'templates/profile_build_wrapper.html';
                    }
                    CloudFaces.SandBoxx.Profile.wrapper = '#profile-wrapper';
                    CloudFaces.SandBoxx.Helpers.loadTpl(tpl_path).then(function (wrapper_tpl) {
                        $(CloudFaces.SandBoxx.Profile.container).html(wrapper_tpl);
                        var user_id = p_info.user_id ? p_info.user_id : userData.id;
                        CloudFaces.SandBoxx.DataProxy.getUserData(user_id)
                            .then(function (user_data) {
                                user_data.match_percent = p_info.percent ? p_info.percent : '';
                                CloudFaces.SandBoxx.Profile.user_data = user_data;
                                if (CloudFaces.SandBoxx.Profile.type != "edit" && CloudFaces.SandBoxx.Profile.type != "create") {
                                    var name = user_data.email;
                                    if (user_data.first_name && user_data.last_name) {
                                        name = user_data.first_name + ' ' + user_data.last_name.charAt(0) + '.';
                                    }
                                    CFHeader.updateHeader(name);
                                }

                                resolve();
                            })
                    })

                });
            },
            appendPopup: function () {
                return new Promise(function (resolve, reject) {
                    var $container = $(CloudFaces.SandBoxx.Profile.container);
                    $container.append(
                        '<div>' +
                        '<div class="modal fade modal-from-bottom" id="profile-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">' +
                        '<div class="modal-dialog" role="document">' +
                        '<div class="modal-content modal-native modal-content--info">' +
                        '<div class="pt-20 pb-20 px-20">' +
                        '<h3 class="font-16 color-primary text-center profile-popup-title"></h3>' +
                        '<p class="font-12 color-secondary text-center profile-popup-text"></p>' +
                        '</div>' +
                        '<div class="dialog-btn-group--info">' +
                        '<button type="button" class="btn dialog-btn-default btn-primary" data-toggle="modal" data-dismiss="modal">Okay</button>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>'
                    );
                    resolve();
                })
            },
            renderPicture: function () {
                return new Promise(function (resolve, reject) {

                    if (CloudFaces.SandBoxx.Profile.type == 'admin_view') {
                        resolve();
                    }
                    else {
                        var getDatafromCamera = false,
                            base64 = false,
                            isLocal = false,
                            getDatafromCamera = false;
                        var image_url = '';
                        var user_avatar = CloudFaces.SandBoxx.Profile.user_data.avatar;
                        if (user_avatar && typeof user_avatar.data.full_url !== 'undefined') {
                            CloudFaces.SandBoxx.Profile.Main.appendImage(user_avatar.data.full_url, 'image', '', getDatafromCamera, isLocal, base64, function (callback) {
                                $(CloudFaces.SandBoxx.Profile.wrapper).append(callback)
                                resolve();
                            });
                        } else {
                            CloudFaces.SandBoxx.Profile.Main.appendImage('', 'image', '', getDatafromCamera, isLocal, base64, function (callback) {
                                $(CloudFaces.SandBoxx.Profile.wrapper).append(callback)
                                resolve();
                            });
                        }
                    }
                });
            },
            appendImage: function (userData, key, templateData, formCamera, isLocal, base64, callback) {
                var wrap_profile_avatar = '';
                if (CloudFaces.SandBoxx.Profile.type == "admin_view" || CloudFaces.SandBoxx.Profile.type == "edit" || CloudFaces.SandBoxx.Profile.type == "admin_edit") {
                    wrap_profile_avatar = 'templates/profile_avatar.html';
                }
                else if (CloudFaces.SandBoxx.Profile.type == "view") {
                    wrap_profile_avatar = 'templates/view_avatar.html';
                }
                else {
                    wrap_profile_avatar = 'templates/profile_avatar.html';
                    $('.profile-build-title').text(CloudFaces.Language.translate('building_your_profile'));
                }
                CloudFaces.SandBoxx.Helpers.loadTpl(wrap_profile_avatar).then(function (avatar_tpl) {
                    var wrapper = $(avatar_tpl);

                    checkData = userData != null && userData != '';
                    if (formCamera) {
                        checkData = true
                    }
                    $('.box-element-profil').remove();

                    var imageClass = CloudFaces.SandBoxx.Profile.type != 'view' ? 'picture-trigger' : '';

                    if (CloudFaces.SandBoxx.Profile.type == 'view') {
                        $('.avatar-label_percent', wrapper).text(CloudFaces.SandBoxx.Profile.user_data.match_percent + '%')
                    }
                    if (CloudFaces.SandBoxx.Profile.type == 'create') {
                        $('.avatar-img', wrapper).removeClass('m-auto');
                    }
                    if (checkData) {
                        var profilImage = '';
                        deleteUrl = '';
                        img = '';
                        if (formCamera) {
                            if (isLocal) {
                                thumb = userData
                                img = userData
                            } else {
                                img = userData
                                thumb = 'data:image/jpeg;base64,' + base64;
                            }
                            deleteUrl = userData;
                        } else {
                            var thumb = userData;
                        }
                        $('.avatar', wrapper).attr('src', thumb).data('url', deleteUrl).data('name', key);
                        $('.img-profile-album', wrapper).addClass(imageClass).data('name', key).text(CloudFaces.Language.translate('photos'));
                        $('.img-profile-camera', wrapper).text(CloudFaces.Language.translate('camera'));
                        callback(wrapper);
                    } else {
                        $('.avatar', wrapper).css('display', 'none');
                        $('.img-none', wrapper).css('display', 'block');
                        $('.img-profile-album', wrapper).addClass(imageClass).data('name', key).text(CloudFaces.Language.translate('photos'));
                        $('.img-profile-camera', wrapper).text(CloudFaces.Language.translate('camera'));
                        callback(wrapper);
                    }

                })
            },
            getPermission: function (permissionType, callback) {
                if ($('body').hasClass('ios')) {
                    callback(true);
                } else {
                    CFPrivacy.checkPermission(permissionType, function (enabled) {
                        if (!enabled) {
                            CFPrivacy.requestPermission(permissionType, function (granted) {
                                callback(true);
                            });
                        } else {
                            callback(true);
                        }
                    });
                }
            },
            takePicture: function (type) {
                key = 'image';
                $('#exampleModal').modal('toggle');
                $('.modal').css('display', 'none');
                CloudFaces.SandBoxx.Profile.Main.getPermission('camera', function (callbackCamera) {
                    CloudFaces.SandBoxx.Profile.Main.getPermission('storage', function (callbackCamera) {
                        if (callbackCamera && callbackCamera) {
                            var isLocal = location.href.charAt(0) == 'f';

                            if (isLocal) {
                                defaultResult = "URL";
                            } else {
                                defaultResult = "DATA";
                            }
                            CFPictureChooser.capturePicture(type, 'URL', function (callback) {
                                CloudFaces.Helpers.showLoader();
                                if (typeof callback != 'undefined' && callback != null) {
                                    var imageUri = callback;
                                    getDatafromCamera = true;
                                    base64 = false;
                                    var filename = imageUri.substring(imageUri.lastIndexOf('/') + 1);
                                    if (isLocal) {
                                        imgSrc = imageUri;
                                        if (imgSrc) {
                                            CloudFaces.SandBoxx.DataProxy.saveProfileImage(imgSrc, 'image', CloudFaces.SandBoxx.Profile.user_data.id, filename)
                                                .then(function () {
                                                    CloudFaces.SandBoxx.Profile.Main.appendImage(imageUri, key, '', getDatafromCamera, isLocal, base64, function (callback) {
                                                        $(CloudFaces.SandBoxx.Profile.wrapper).prepend(callback);
                                                        CloudFaces.Helpers.hideLoader();
                                                    })
                                                });
                                        }
                                    } else {

                                        CFPictureChooser.getPictureData(imageUri, function (data) {
                                            imgSrc = data;
                                            if (imgSrc) {
                                                CloudFaces.SandBoxx.DataProxy.saveProfileImage(imgSrc, 'image', CloudFaces.SandBoxx.Profile.user_data.id, filename)
                                                    .then(function () {
                                                        base64 = data
                                                        CloudFaces.SandBoxx.Profile.Main.appendImage(imageUri, key, '', getDatafromCamera, isLocal, base64, function (callback) {
                                                            $(CloudFaces.SandBoxx.Profile.wrapper).prepend(callback);
                                                            CloudFaces.Helpers.hideLoader();
                                                        })
                                                    });
                                            }

                                        });
                                    }

                                } else {
                                    CloudFaces.Helpers.hideLoader();
                                }
                            });

                        }
                    })

                })
            },
            prepareTemplates: function () {
                // if admin -> no templates
                return new Promise(function (resolve, reject) {
                    if (CloudFaces.SandBoxx.Profile.type == 'admin_view') {
                        resolve();
                    }
                    else {
                        CloudFaces.SandBoxx.DataProxy.loadTemplates(CloudFaces.SandBoxx.Profile.user_data.id)
                            .then(function (templates) {
                                CloudFaces.SandBoxx.DataProxy.loadQuestionnaire(CloudFaces.SandBoxx.Profile.user_data.id, CloudFaces.SandBoxx.Profile.type)
                                    .then(function (questionnaire) {
                                        questionnaire.questions = CloudFaces.SandBoxx.Profile.Main
                                            .mapQuestionAnswers(questionnaire.questions, questionnaire.answers)
                                            .sort(function (a, b) {
                                                if (a.max_answers == 1) {
                                                    if (b.max_answers == 1) {
                                                        return a.item_order - b.item_order;
                                                    }
                                                    return -1;
                                                }
                                                return a.item_order - b.item_order;
                                            });
                                        resolve({ templates: templates, questionnaire: questionnaire.questions })
                                    })
                            });
                    }
                });
            },
            renderProfileFields: function (template_data) {
                return new Promise(function (resolve, reject) {
                    var container = $(CloudFaces.SandBoxx.Profile.wrapper);
                    var wrap_profile_field_2 = 'templates/profile_template_form_fields_2.html';
                    var wrap_profile_field_1 = CloudFaces.SandBoxx.Profile.type == "view" ? 'templates/profile_template_match_info.html' : 'templates/profile_template_form_fields_1.html';
                    var header_modal_tpl = 'templates/profile_header_modal.html';

                    CloudFaces.SandBoxx.Helpers.loadTpl(wrap_profile_field_1).then(function (field_tpl_1) {
                        CloudFaces.SandBoxx.Helpers.loadTpl(wrap_profile_field_2).then(function (field_tpl_2) {
                            CloudFaces.SandBoxx.Helpers.loadTpl(header_modal_tpl).then(function (modal_tpl) {

                                if (CloudFaces.SandBoxx.Profile.type == "admin_view") {
                                    resolve();
                                }
                                else if (CloudFaces.SandBoxx.Profile.type == "view") {
                                    var templates = template_data.templates.data,
                                        template_wrapper = $(field_tpl_1),
                                        modal = $(modal_tpl);
                                    $('.modal-email', modal).attr('href', 'mailto:' + CloudFaces.SandBoxx.Profile.user_data.email)
                                    for (var template of templates) {
                                        if (template.name_of_field == 'title') {
                                            $('.match-title', template_wrapper).text(template.value)
                                            continue
                                        }
                                        if (template.name_of_field == 'location') {
                                            $('.match-location', template_wrapper).text(template.value)
                                            continue
                                        }
                                        if (template.name_of_field == 'first_name') {
                                            continue
                                        }
                                        if (template.name_of_field == 'last_name') {
                                            template_data.linked_in = template;
                                            continue
                                        }
                                        if (template.name_of_field == 'linked_in') {
                                            if (CloudFaces.SandBoxx.Helpers.isUrl(template.value)) {
                                                $('.modal-linkedin', modal).attr('href', template.value)
                                                if ($('body').hasClass('web')) {
                                                    $('.modal-linkedin', modal).attr('target', '_blank')
                                                }
                                            }
                                        }
                                    }
                                    container
                                        .append(template_wrapper)
                                        .append(modal);
                                    resolve(template_data);

                                }
                                else if (CloudFaces.SandBoxx.Profile.type == "create") {
                                    var template_form = $('<form class="template-form wrap-block">');
                                    var templates = template_data.templates.data;

                                    for (var template of templates) {

                                        if (CloudFaces.SandBoxx.Profile.hidden_fields.indexOf(template.name_of_field) == -1) {

                                            var wrapper_field = $(field_tpl_1);

                                            if (template.name_of_field == 'linked_in') {
                                                template_data.linked_in = template;
                                                continue
                                            }
                                            var i_class = CloudFaces.SandBoxx.Profile.type == "edit" || CloudFaces.SandBoxx.Profile.type == "admin_edit" ? "ajax-input" : "required-input";

                                            $('.title-template-field', wrapper_field).text(template.text_in_fields).attr('for', 'id_' + template.id);
                                            $('.detail-template-field', wrapper_field).attr('placeholder', template.text_in_fields).attr('id', 'id_' + template.id).attr('name', template.name_of_field).attr('value', template.value).addClass(i_class);

                                            template_form.append(wrapper_field);
                                        }
                                    }

                                    container.append(template_form);

                                    resolve(template_data);


                                }
                                else {
                                    var templates = template_data.templates.data;
                                    var inline_count = 2;
                                    var current = 1;
                                    var wrapper_field = $(field_tpl_2);
                                    var modal = $(modal_tpl);
                                    $('.profile-title', wrapper_field).text(CloudFaces.Language.translate('profile_info'));
                                    $('.modal-email', modal).attr('href', 'mailto:' + CloudFaces.SandBoxx.Profile.user_data.email)
                                    for (var template of templates) {
                                        if (CloudFaces.SandBoxx.Profile.hidden_fields.indexOf(template.name_of_field) == -1) {
                                            if (template.name_of_field == 'linked_in') {
                                                template_data.linked_in = template;
                                                if (CloudFaces.SandBoxx.Helpers.isUrl(template.value)) {

                                                    $('.modal-linkedin', modal).attr('href', template.value)
                                                    if ($('body').hasClass('web')) {
                                                        $('.modal-linkedin', modal).attr('target', '_blank')
                                                    }
                                                }
                                                continue
                                            }
                                            var i_class = CloudFaces.SandBoxx.Profile.type == "edit" || CloudFaces.SandBoxx.Profile.type == "admin_edit" ? "ajax-input" : "required-input";

                                            if (current <= inline_count) {
                                                $('.template_field:nth-of-type(' + current + ') .title-template-field', wrapper_field).text(template.text_in_fields).attr('for', 'id_' + template.id);
                                                $('.template_field:nth-of-type(' + current + ') .detail-template-field', wrapper_field).attr('placeholder', template.text_in_fields).attr('id', 'id_' + template.id).attr('name', template.name_of_field).attr('value', template.value).addClass(i_class);
                                            }
                                            else {
                                                container.append(wrapper_field);
                                                var wrapper_field = $(field_tpl_1);
                                                $('.title-template-field', wrapper_field).text(template.text_in_fields).attr('for', 'id_' + template.id);
                                                $('.detail-template-field', wrapper_field).attr('placeholder', template.text_in_fields).attr('id', 'id_' + template.id).attr('name', template.name_of_field).attr('value', template.value).addClass(i_class);
                                            }
                                            current++;
                                        }
                                    }
                                    container
                                        .append(wrapper_field)
                                        .append(modal);

                                    resolve(template_data);
                                }
                            })
                        })
                    })
                })
            },
            renderQuestionFields: function (template_data) {
                return new Promise(function (resolve, reject) {
                    if (CloudFaces.SandBoxx.Profile.type == 'admin_view') {
                        resolve();
                    }
                    else {
                        var container = $(CloudFaces.SandBoxx.Profile.wrapper);
                        var questionnaire = template_data.questionnaire;
                        var questionnaire_wrapper = $('<form class="questionnaire-form wrap-block--25">');


                        var wrap_multi_answers = '';
                        var wrap_profile_answers = '';
                        var tpl_path = '';
                        // console.log(CloudFaces.SandBoxx.Profile.user_data);
                        if (CloudFaces.SandBoxx.Profile.type == "admin_view" || CloudFaces.SandBoxx.Profile.type == "edit" || CloudFaces.SandBoxx.Profile.type == "admin_edit") {
                            wrap_multi_answers = 'templates/wrapper_multi_answers.html';
                            wrap_profile_answers = 'templates/wrapper_profile_answers.html';
                        }
                        else if (CloudFaces.SandBoxx.Profile.type == "view") {
                            wrap_multi_answers = 'templates/wrapper_multi_answers.html';
                            wrap_profile_answers = 'templates/wrapper_view_profile_answers.html';
                        }
                        else {
                            wrap_multi_answers = 'templates/wrapper_multi_answers.html';
                            wrap_profile_answers = 'templates/wrapper_profile_answers.html';
                            questionnaire_wrapper.removeClass('wrap-block--25').addClass('wrap-block');

                        }
                        CloudFaces.SandBoxx.Helpers.loadTpl(wrap_multi_answers).then(function (multi_tpl) {
                            CloudFaces.SandBoxx.Helpers.loadTpl(wrap_profile_answers).then(function (profile_tpl) {

                                for (var question of questionnaire) {

                                    var title_text = '';

                                    if (CloudFaces.SandBoxx.Profile.user_data.role_id == CloudFaces.SandBoxx.Config.constants.role_sponsor) {
                                        if (CloudFaces.SandBoxx.Profile.type == "edit" || CloudFaces.SandBoxx.Profile.type == "admin_edit") {
                                            title_text = question.profile_sponsor_question_text;
                                        }
                                        else {
                                            title_text = question.public_sponsor_question_text;
                                        }
                                    }
                                    else {
                                        if (CloudFaces.SandBoxx.Profile.type == "edit" || CloudFaces.SandBoxx.Profile.type == "admin_edit") {
                                            title_text = question.profile_sponsoree_question_text;
                                        }
                                        else {
                                            title_text = question.public_sponsoree_question_text;
                                        }
                                    }


                                    if (question.type == 'multi') {
                                        CloudFaces.SandBoxx.Profile.Main.appendMultiAnswers(questionnaire_wrapper, multi_tpl, question, title_text)
                                    }
                                    else if (question.type == 'profile') {
                                        CloudFaces.SandBoxx.Profile.Main.appendProfileAnswers(questionnaire_wrapper, profile_tpl, question, title_text)
                                    }
                                    else if (question.type == 'text') {
                                        CloudFaces.SandBoxx.Profile.Main.appendTextAnswers(questionnaire_wrapper, multi_tpl, question, title_text)
                                    }

                                }
                                container
                                    .append(questionnaire_wrapper);
                                resolve(template_data)

                            })
                        })
                    }
                })
            },
            renderCohortSelection: function (template_data) {
                return new Promise(function (resolve, reject) {

                    var wrap_profile_field_cohort = 'templates/profile_template_form_fields_cohort.html';
                    CloudFaces.SandBoxx.Helpers.loadTpl(wrap_profile_field_cohort).then(function (field_tpl) {
                        var wrapper = $(field_tpl);

                        if ( CloudFaces.SandBoxx.Profile.type == 'edit' || CloudFaces.SandBoxx.Profile.type == 'admin_view' ) {
                            var current_cohort = CFPersistentStorage.readValue("selected_cohort");
                            var userData       = CloudFaces.Helpers.getUserData();
                            CloudFaces.Directus.getCohorts(userData.id).then( function(cohorts){
                                var select = $('#cohortSelect', wrapper);
                                for (var cohort of cohorts.data) {
                                    cohort_id   = cohort.id;
                                    cohort_name = cohort.name;
                                    selected    =  ( cohort_id == current_cohort ) ? 'selected' : '';
                                    select.append('<option value="' + cohort_id + '" '+ selected +'>' + CloudFaces.SandBoxx.Helpers.htmlDecode(cohort_name) + '</option>')
                                }
                                var container = $(CloudFaces.SandBoxx.Profile.wrapper);
                                container.append(wrapper);

                                resolve(template_data);
                            } );
                        } else {
                            resolve(template_data);
                        }

                    })
                })
            },
            renderLinkedInField: function (template_data) {
                return new Promise(function (resolve, reject) {

                    var wrap_profile_field_1 = 'templates/profile_template_form_fields_1.html';
                    CloudFaces.SandBoxx.Helpers.loadTpl(wrap_profile_field_1).then(function (field_tpl) {
                        var wrapper = $(field_tpl);


                        if (CloudFaces.SandBoxx.Profile.type == 'edit' || CloudFaces.SandBoxx.Profile.type == "admin_edit") {
                            var linked_in = template_data.linked_in;
                            var container = $(CloudFaces.SandBoxx.Profile.wrapper);
                            var i_class = CloudFaces.SandBoxx.Profile.type == "edit" || CloudFaces.SandBoxx.Profile.type == "admin_edit" ? "ajax-input" : "required-input";

                            $('.template-field-title', wrapper).text(linked_in.text_in_fields);
                            $('.title-template-field', wrapper).text(CloudFaces.Language.translate('linkedin_url'));
                            $('.detail-template-field', wrapper).attr('placeholder', linked_in.text_in_fields).attr('value', linked_in.value).attr('name', linked_in.name_of_field).addClass(i_class);
                            $('.detail-template-field--specify', wrapper).text(CloudFaces.Language.translate('sponsorees_can_view_your_linkedin'));

                            container.append(wrapper);
                        }
                        resolve(template_data);
                    })
                })
            },
            prepareButtons: function () {
                return new Promise(function (resolve, reject) {
                    var cohort_id = CFPersistentStorage.readValue("selected_cohort");
                    if (CloudFaces.SandBoxx.Profile.type == 'view') {
                        var userData = CloudFaces.Helpers.getUserData();
                        CloudFaces.Directus.getUserRequests(userData.id, CloudFaces.SandBoxx.Profile.user_data.id, cohort_id)
                            .then(function (user_requests) {
                                if (user_requests.length) {
                                    var request_data = user_requests[0];
                                    request_data.type = request_data.sender_id == userData.id ? "sent" : 'recieved';
                                    resolve(request_data);
                                }
                                else {
                                    resolve();
                                }
                            });
                    }
                    else if (CloudFaces.SandBoxx.Profile.type == 'admin_edit') {
                        CloudFaces.Directus.getActiveRequests(CloudFaces.SandBoxx.Profile.user_data.id, cohort_id)
                            .then(function (request_data) {
                                request_data = request_data[0] ? request_data[0] : {};
                                if (request_data.sponsor_id) {
                                    var user_id = request_data.sponsor_id == CloudFaces.SandBoxx.Profile.user_data.id ? request_data.sponsoree_id : request_data.sponsor_id;
                                    CloudFaces.SandBoxx.DataProxy.getUserData(user_id)
                                        .then(function (user_data) {
                                            resolve({
                                                request_data: request_data,
                                                user_data: user_data
                                            })
                                        })
                                }
                                else {
                                    resolve();
                                }
                            });

                    }
                    else {
                        resolve();
                    }
                })
            },
            renderButtons: function (request_data) {
                return new Promise(function (resolve, reject) {
                    var container = $(CloudFaces.SandBoxx.Profile.wrapper);

                    if (CloudFaces.SandBoxx.Profile.type == 'create') {

                        var wrap_btn_fixed_footer = 'templates/btn_fixed_footer.html';
                        CloudFaces.SandBoxx.Helpers.loadTpl(wrap_btn_fixed_footer).then(function (btn_tpl) {
                            var wrapper = $(btn_tpl);
                            wrapper.removeClass('btn-fixed-footer').addClass('mt-80');
                            var user_data = CloudFaces.SandBoxx.Profile.user_data
                            if (user_data.location) {
                                $('.btn-fixed-footer__title', wrapper).removeClass('disabled-btn');
                            }
                            $('.btn-fixed-footer__title', wrapper).text(CloudFaces.Language.translate('next'))

                            container.append(wrapper);
                        });

                    }
                    else if (CloudFaces.SandBoxx.Profile.type == 'view') {


                        if (!request_data) {

                            var wrap_match_info = "templates/btn_request_fixed_footer.html";
                            CloudFaces.SandBoxx.Helpers.loadTpl(wrap_match_info).then(function (field_tpl) {
                                var wrapper = $(field_tpl);

                                $('.btn-match', wrapper)
                                    .addClass('btn-primary')
                                    //.addClass('request-match-btn')
                                    .attr('data-target', '#sendConfirmModal')
                                    .attr('data-toggle', 'modal')
                                    .text(CloudFaces.Language.translate('btn_request_match'));

                                $('.yes-send-request-btn', wrapper).text('Yes');
                                $('.yes-send-request-btn', wrapper)
                                    .attr('data-dismiss', 'modal');
                                $('.send-title-confirm', wrapper).text('Are you sure you want to send match request?');

                                container.append(wrapper)

                            })
                        }
                        else if (request_data.status == "request") {
                            if (request_data.type == "sent") {

                                var wrap_match_info = "templates/btn_request_fixed_footer.html";
                                CloudFaces.SandBoxx.Helpers.loadTpl(wrap_match_info).then(function (field_tpl) {
                                    var wrapper = $(field_tpl);


                                    $('.btn-match', wrapper)
                                        .attr('data-target', '#cancelRequest')
                                        .attr('data-toggle', 'modal')
                                        .addClass('btn-success')
                                        .text(CloudFaces.Language.translate('btn_request_sent'));

                                    $('.cancel-request-btn', wrapper)
                                        .attr('data-id', request_data.id)
                                        .attr('data-dismiss', 'modal')
                                        .text(CloudFaces.Language.translate('btn_request_cancel'));
                                    $('.btn-cancel', wrapper).text(CloudFaces.Language.translate('btn_cancel'));

                                    $('.yes-cancel-request-btn', wrapper).text('Yes');
                                    $('.yes-cancel-request-btn', wrapper)
                                        .attr('data-id', request_data.id)
                                        .attr('data-dismiss', 'modal');
                                    $('.title-confirm', wrapper).text('Are you sure you want to cancel your request?');

                                    container.append(wrapper)

                                })

                            }
                            else {
                                var wrap_match_info = "templates/btn_confirm-decline_fixed_footer.html";
                                CloudFaces.SandBoxx.Helpers.loadTpl(wrap_match_info).then(function (field_tpl) {
                                    var wrapper = $(field_tpl);
                                    // Decline Request Buttn Pop up
                                    $('.decline-request-btn', wrapper).text(CloudFaces.Language.translate('decline')).data('id', request_data.id);
                                    $('.decline-request-btn', wrapper)
                                        .attr('data-target', '#declineConfirmModal')
                                        .attr('data-toggle', 'modal');
                                    $('.can-btn', wrapper).text(CloudFaces.Language.translate('btn_cancel'));
                                    $('.yes-decline-request-btn', wrapper).text('Yes');
                                    $('.yes-decline-request-btn', wrapper)
                                        .attr('data-id', request_data.id)
                                        .attr('data-dismiss', 'modal');
                                    $('.title-confirm', wrapper).text('Are you sure you want to decline request?');

                                    // Confirm Request Buttn Pop up
                                    $('.confirm-request-btn', wrapper).text(CloudFaces.Language.translate('confirm')).data('id', request_data.id);
                                    $('.confirm-request-btn', wrapper)
                                        .attr('data-target', '#confirmAcceptModal')
                                        .attr('data-toggle', 'modal');
                                    $('.can-btn', wrapper).text(CloudFaces.Language.translate('btn_cancel'));
                                    $('.yes-accept-request-btn', wrapper).text('Yes');
                                    $('.yes-accept-request-btn', wrapper)
                                        .attr('data-id', request_data.id)
                                        .attr('data-dismiss', 'modal');
                                    $('.title-confirm-accept', wrapper).text('Are you sure you want to accept request?');
                                    container.append(wrapper);
                                });

                            }
                        }
                        else {
                            var wrap_match_info = "templates/btn_unmatch_fixed_footer.html";
                            CloudFaces.SandBoxx.Helpers.loadTpl(wrap_match_info).then(function (field_tpl) {
                                var wrapper = $(field_tpl);
                                $('.btn-unmatch-request', wrapper).text(CloudFaces.Language.translate('unmatch'));
                                $('.request-data', wrapper).text('Matched on ' + CloudFaces.SandBoxx.Helpers.textDate(request_data.matched_on));

                                $('.unmatch-btn', wrapper).attr('data-id', request_data.id).text(CloudFaces.Language.translate('unmatch'));
                                $('.title-reason', wrapper).text(CloudFaces.Language.translate('unmatch_reason'));
                                $('.unmatch-title', wrapper).text(CloudFaces.Language.translate('unmatch'));
                                $('.text-send-request', wrapper).text(CloudFaces.Language.translate('send_request'));
                                $('.text-yes', wrapper).text(CloudFaces.Language.translate('yes'));
                                $('.text-no', wrapper).text(CloudFaces.Language.translate('no'));
                                $('.btn-cancel', wrapper).text(CloudFaces.Language.translate('btn_cancel'));

                                container.append(wrapper);
                            });

                        }
                    }
                    else if (CloudFaces.SandBoxx.Profile.type == 'admin_edit') {
                        var wrap_match_info = "templates/wrapper_match_info.html";
                        CloudFaces.SandBoxx.Helpers.loadTpl(wrap_match_info).then(function (field_tpl) {
                            var wrapper = $(field_tpl);

                            if (request_data && request_data.user_data) {
                                if (request_data.user_data.image) {
                                    var image = CloudFaces.API.url(request_data.user_data.image);
                                    var thumb = CloudFaces.Helpers.createThumb(image, 70, 70);
                                    $('.avatar', wrapper).attr('src', thumb);
                                } else {
                                    $('.avatar-icon', wrapper).show();
                                }


                                $('.autocomplete').hide();
                                $('.forcefully_match').attr('disabled', true);
                                $('#search').attr('disabled', true);
                                // // $('.forcefully_match').text('Already Matched With Another User');
                                // // $('.forcefully_match').val('Already Matched With Another User');
                                $('.avatar-img',wrapper).addClass('disabled_match_request');
                                $('.match-info', wrapper).data('id', request_data.user_data.id);
                                $('.match-info-title', wrapper).text(CloudFaces.Language.translate('match_info'));
                                $('.match-date', wrapper).text(CloudFaces.Language.translate('matched_on') + ' ' + CloudFaces.SandBoxx.Helpers.textDate(request_data.request_data.matched_on));
                                $('.matches-item_title', wrapper).text(request_data.user_data.first_name + ' ' + request_data.user_data.last_name);
                                $('.admin-unmatch-btn', wrapper).data('ain', request_data.user_data.ain).data('id', request_data.request_data.id).text(CloudFaces.Language.translate('admin_unmatch'));

                            }
                            else {
                                $('.match-info, .match-details', wrapper).hide();
                            }
                            $('.profile-type-title', wrapper).text(CloudFaces.Language.translate('profile_type'));
                            var sponsor_class = CloudFaces.SandBoxx.Profile.user_data.role_id == CloudFaces.SandBoxx.Config.constants.role_sponsor ? 'color-primary' : 'color-secondary';
                            var sponsoree_class = CloudFaces.SandBoxx.Profile.user_data.role_id == CloudFaces.SandBoxx.Config.constants.role_sponsoree ? 'color-primary' : 'color-secondary';
                            $('.profile-type-sponsor', wrapper).text(CloudFaces.Language.translate('sponsor')).addClass(sponsor_class);
                            $('.profile-type-sponsoree', wrapper).text(CloudFaces.Language.translate('sponsoree')).addClass(sponsoree_class);

                            container.append(wrapper);
                        })
                    }

                    resolve();


                })
            },
            renderSettings: function () {
                return new Promise(function (resolve, reject) {
                    var container = $(CloudFaces.SandBoxx.Profile.wrapper);
                    var wrap_account_settings = "templates/wrapper_account_settings.html";
                    CloudFaces.SandBoxx.Helpers.loadTpl(wrap_account_settings).then(function (field_tpl) {
                        var wrapper = $(field_tpl);

                        if (CloudFaces.SandBoxx.Profile.type == 'create' || CloudFaces.SandBoxx.Profile.type == 'view') {
                            resolve();
                        }
                        else {

                            $('.profile-field-title', wrapper).text(CloudFaces.Language.translate('account_settings'));
                            $('.profile-field_title-mail', wrapper).text(CloudFaces.Language.translate('email'));
                            $('.profile-field-mail', wrapper).text(CloudFaces.Language.translate(CloudFaces.SandBoxx.Profile.user_data.email));
                            $('.profile-field_title-password', wrapper).text(CloudFaces.Language.translate('password'));

                            container.append(wrapper);

                            resolve();
                        }
                    })
                })
            },
            renderForcefullyMatch: function (searchedDat) {
                return new Promise(function (resolve, reject) {
                    if (CloudFaces.SandBoxx.Profile.type == 'admin_edit') {
                        var container = $(CloudFaces.SandBoxx.Profile.wrapper);
                        var forcefullyMatch = "templates/forcefully-match.html";
                        CloudFaces.SandBoxx.Helpers.loadTpl(forcefullyMatch).then(function (forcefullyMatch_tpl) {
                            var wrapper = $(forcefullyMatch_tpl);
                            var reverse_role = CloudFaces.SandBoxx.Profile.user_data.role_id == CloudFaces.SandBoxx.Config.constants.role_sponsor ? CloudFaces.SandBoxx.Config.constants.role_sponsoree : CloudFaces.SandBoxx.Config.constants.role_sponsor;
                            CloudFaces.SandBoxx.DataProxy.getAllUsersListByRole(CloudFaces.SandBoxx.Profile.cohort_id, reverse_role)
                                .then(function (filterUsers) {
                                    CloudFaces.SandBoxx.DataProxy.getAllMatchesByCohort(CloudFaces.SandBoxx.Profile.cohort_id)
                                        .then(function (matches) {
                                            var users = [];
                                            filterUsers.forEach(function (v) {
                                                users.push(v.directus_users_id)
                                            });
                                            if (users && users.length > 0) {
                                                for (var i = 0; i < users.length; i++) {

                                                    var user = users[i];
                                                    user.filterusername = user.email;
                                                    user.username = user.email;
                                                    if (user.first_name && user.last_name) {
                                                        user.filterusername = user.first_name + ' ' + user.last_name + user.email;
                                                        user.username = user.first_name + ' ' + user.last_name;
                                                    }

                                                    for (var j = 0; j < matches.data.length; j++) {
                                                        var match = matches.data[j];
                                                        if (reverse_role == CloudFaces.SandBoxx.Config.constants.role_sponsor) {
                                                            if (match.sponsor_id == user.id) {
                                                                user.match_status =  ( match.status == 'unmatching' ) ? 'active' : match.status;
                                                            } // pass unmatching request as active to disable later
                                                        }
                                                        else if (reverse_role == CloudFaces.SandBoxx.Config.constants.role_sponsoree) {
                                                            if (match.sponsoree_id == user.id) {
                                                                user.match_status =  ( match.status == 'unmatching' ) ? 'active' : match.status;
                                                            } // pass unmatching request as active to disable later
                                                        }

                                                    }
                                                }
                                            }
                                            CloudFaces.SandBoxx.Profile.users = users;
                                            container.append(wrapper);
                                            CloudFaces.SandBoxx.Profile.Main.autocomplete(document.getElementById("search"), users);
                                            resolve();
                                        });
                                });
                        });

                    } else {
                        resolve();
                    };
                })
            },
            renderSentMatchRequest: function () {
                return new Promise(function (resolve, reject) {
                    if (CloudFaces.SandBoxx.Profile.type == 'admin_edit') {
                        var container = $(CloudFaces.SandBoxx.Profile.wrapper);
                        var profileSentMatchRequest = "templates/profile-sent-match-request.html";
                        var match_list_item_tpl = 'templates/match_list_item.html';
                        var cohort_id = CFPersistentStorage.readValue("selected_cohort");
                        CloudFaces.SandBoxx.Helpers.loadTpl(profileSentMatchRequest).then(function (field_tpl) {
                            CloudFaces.SandBoxx.Helpers.loadTpl(match_list_item_tpl).then(function (item_tpl) {
                                var wrapper = $(field_tpl);

                                $('.cancel-match-title', wrapper).text("Match Status");
                                $('.sent-request-title', wrapper).text("Sent/Receive Match Requests");

                                CloudFaces.SandBoxx.DataProxy.getUsersList(cohort_id)
                                    .then(function (users_list) {
                                        CloudFaces.Directus.getSentRequests(CloudFaces.SandBoxx.Profile.user_data.id, cohort_id, CloudFaces.SandBoxx.Profile.user_data.role_id)
                                            .then(function (request_data) {
                                                if (request_data && request_data.data.length > 0) {
                                                    var filtered = [];
                                                    var users = [];
                                                    users_list.forEach(function (v) {
                                                        users.push(v.directus_users_id)
                                                    });
                                                    for (var arr in users) {
                                                        for (var filter in request_data.data) {
                                                            if (CloudFaces.SandBoxx.Profile.user_data.role_id ==
                                                                CloudFaces.SandBoxx.Config.constants.role_sponsor &&
                                                                users[arr].id == request_data.data[filter].sponsoree_id) {
                                                                users[arr].sender_id = request_data.data[filter].sender_id;
                                                                users[arr].matches_id = request_data.data[filter].id;
                                                                filtered.push(users[arr]);
                                                            }
                                                            else if (CloudFaces.SandBoxx.Profile.user_data.role_id ==
                                                                CloudFaces.SandBoxx.Config.constants.role_sponsoree &&
                                                                users[arr].id == request_data.data[filter].sponsor_id) {
                                                                users[arr].sender_id = request_data.data[filter].sender_id;
                                                                users[arr].matches_id = request_data.data[filter].id;
                                                                filtered.push(users[arr]);
                                                            }
                                                        }
                                                    }

                                                    for (var match of filtered) {
                                                        CloudFaces.SandBoxx.Profile.Main.appendMatch($('.list-matchrequest', wrapper), item_tpl, match, false, false);
                                                        CloudFaces.SandBoxx.Profile.Main.appendButton(match);
                                                    }
                                                }
                                                else {
                                                    $('.no-matchrequest', wrapper).text('No match request found.')
                                                }
                                            });
                                    });

                                container.append(wrapper);

                                resolve();
                            });
                        });
                    }
                    else {
                        resolve();
                    }
                })
            },

            appendButton: function (match) {
                var button_template = "templates/btn_cancel_pending_request.html";
                CloudFaces.SandBoxx.Helpers.loadTpl(button_template).then(function (btn_tpl) {
                    var btn_template = $(btn_tpl);
                    var btn_text = CloudFaces.SandBoxx.Profile.user_data.id == match.sender_id ? "Cancel Pending (SENT) Request" : "Cancel Pending (RECEIVE) Request"
                    var userId = match.id;
                    var li = $(".list-matchrequest li#" + userId);

                    $(btn_template).attr('id', 'match-' + userId);

                    $('.btn_cancel_pending_request', btn_template)
                        .addClass('btn-primary')
                        .text(btn_text)
                        .attr('data-id', userId)
                        .attr('data-roleid', match.role_id)
                        .attr('data-matches_id', match.matches_id);

                    li.append(btn_template);
                });

            },
            showPopup: function (title, text) {
                $('.profile-popup-title').text(title);
                $('.profile-popup-text').text(text);
                $('#profile-popup').modal('toggle');
                setTimeout(function () {
                    $('.dialog-btn-default').removeAttr('style');
                    $('.admin-unmatch-btn').removeAttr('style');
                }, 1000);
            },
            appendMatch: function (wrapper, item_tpl, match, unclickable, requested) {

                var item_wrapper = $(item_tpl);
                var image = match.image ? CloudFaces.API.url(match.image) : 'images/default-user.svg';
                var thumb = match.image ? CloudFaces.Helpers.createThumb(image, 150, 150) : 'images/default-user.svg';
                var img = thumb;

                //if there is avatar image, display image
                // otherways display icon

                if (image) {
                    $('.avatar', item_wrapper).css('display', 'block');
                    $('.avatar', item_wrapper).siblings().css('display', 'none');
                } else {
                    $('.avatar', item_wrapper).css('display', 'none');
                    $('.avatar', item_wrapper).siblings().css('display', 'block');
                }

                if (!match.image) {
                    $('.avatar', item_wrapper).css('padding', '8px');
                }

                $('.avatar-img',item_wrapper).addClass('disabled_match_request');
                var name = match.email;
                if (match.first_name && match.last_name) {
                    name = match.first_name + ' ' + match.last_name.charAt(0) + '.';
                }

                if (requested) {
                    var request_text = CloudFaces.SandBoxx.Matches.type == "admin" ? CloudFaces.Language.translate('unmatch_request_sent') : CloudFaces.Language.translate('match_request_sent');
                    $('.match-request', item_wrapper).text(CloudFaces.Language.translate(request_text));
                } else if (match.matched_status == 'request') {
                    $('.match-request', item_wrapper).text("Pending Match Request");
                }
                else {
                    var title = match.title ? match.title : '';
                    var location = match.location ? match.location : '';

                    $('.match-title', item_wrapper).text(title);
                    $('.match-location', item_wrapper).text(location);
                }

                var style = requested ? 'sent-request' : '';
                item_wrapper.addClass(style);
                if (unclickable) {
                    item_wrapper.addClass('disabled_match_request')
                }

                $('.next_profile',item_wrapper).css('display','none');

                item_wrapper.data('id', match.id).data('percent', match.percent);
                $('.avatar', item_wrapper).attr('src', img);
                $('.matches-item_title', item_wrapper).text(name);


                if (CloudFaces.SandBoxx.Profile.type == "admin_edit") {
                    $('.match-percent', item_wrapper).parent().css('display', 'none');
                    $('.wrap-matches_item_details', item_wrapper).removeClass('col-6').addClass('col-7');
                }
                else {
                    $('.match-percent', item_wrapper).text(match.percent + '%');
                }

                item_wrapper.attr('id', match.id);

                wrapper.append(item_wrapper);

            },
            appendProfileAnswers: function (question_wrapper, profile_tpl, question, title) {
                var wrapper = $(profile_tpl);

                if (CloudFaces.SandBoxx.Profile.type == "view") {
                    if (question.id != CloudFaces.SandBoxx.Config.constants.location_question_id) {
                        $('.field-title', wrapper).text(title);
                        var answer_wrapper = $('<div>')
                        var answers = question.answers;
                        for (var answer of answers) {
                            if (answer.checked) {
                                $('.field-value', wrapper).text(CloudFaces.SandBoxx.Helpers.htmlDecode(answer.answers_id.answer_text));
                            }
                        }
                        wrapper.append(answer_wrapper);
                        question_wrapper.append(wrapper);
                    }
                }
                else {
                    $('.profile-select_title', wrapper).text(title).attr('for', 'id_' + question.id);
                    var s_class = CloudFaces.SandBoxx.Profile.type == "edit" || CloudFaces.SandBoxx.Profile.type == "admin_edit" ? "ajax-select" : "required-input";

                    var select = $('.profile-select_choices', wrapper);
                    select.addClass(s_class).attr('name', question.id).attr('id', 'id_' + question.id).attr('required', 'required');

                    select.append('<option value="" disabled selected hidden>choose ' + question.sponsor_question_text + '</option>');
                    var answers = question.answers;
                    for (var answer of answers) {
                        var selected = answer.checked ? 'selected="selected"' : '';
                        select.append('<option value="' + answer.answers_id.id + '" ' + selected + '>' + CloudFaces.SandBoxx.Helpers.htmlDecode(answer.answers_id.answer_text) + '</option>')
                    }

                    question_wrapper.append(wrapper);
                }

            },
            appendMultiAnswers: function (question_wrapper, multi_tpl, question, title) {
                var wrapper = $(multi_tpl);

                $('.profile-multi_title', wrapper).text(title);

                if (CloudFaces.SandBoxx.Profile.type == "view") {
                    $('.profile-multi_title', wrapper).attr('for', 'id_' + question.id);

                    var answers = question.answers;
                    var multi_wrapper = $('.multi-question', wrapper);
                    $('.profile-multi_icon', wrapper).hide();
                    for (var answer of answers) {
                        if (answer.checked) {
                            var li = $('<li class="text-input text-word-wrap font-16 answer">' + answer.answers_id.answer_text + '</li>');
                            multi_wrapper.append(li)
                        }
                    }
                    question_wrapper.append(wrapper);

                }
                else {

                    $('.profile-multi_title', wrapper)
                        .attr('for', 'id_' + question.id)
                        .addClass('questionnaire-question')
                        .attr('data-q-id', question.id);

                    var answers = question.answers;
                    var multi_wrapper = $('.multi-question', wrapper);
                    multi_wrapper.addClass('question-' + question.id);
                    for (var answer of answers) {
                        var li = $('<li class="text-input text-word-wrap font-16 answer">' + answer.answers_id.answer_text + '</li>');
                        li.addClass('answer-' + answer.answers_id.id);
                        if (!answer.checked) { 
                            li.hide();
                        }
                        multi_wrapper.append(li)
                    }

                    question_wrapper.append(wrapper);
                }

            },
            appendTextAnswers: function (question_wrapper, text_tpl, question, title) {

                var wrapper = $(text_tpl);

                $('.profile-multi_title', wrapper).text(title);

                if (CloudFaces.SandBoxx.Profile.type == "view") {
                    $('.profile-multi_icon', wrapper).hide();
                }
                if (CloudFaces.SandBoxx.Profile.type == "edit" || CloudFaces.SandBoxx.Profile.type == "admin_edit") {

                    $('.profile-multi_title', wrapper)
                        .attr('for', 'id_' + question.id)
                        .addClass('questionnaire-question')
                        .attr('data-q-id', question.id);
                }

                var q_text = question.answers[0] ? question.answers[0].answer_text : "";
                $('.text-question', wrapper).addClass('question-' + question.id);
                $('.answer-text', wrapper).text(q_text);

                question_wrapper.append(wrapper);

            },
            renderMenuLinks: function () {
                return new Promise(function (resolve, reject) {

                    var container = $(CloudFaces.SandBoxx.Profile.wrapper);
                    var wrap_account_settings = "templates/wrapper_profile_links.html";
                    CloudFaces.SandBoxx.Helpers.loadTpl(wrap_account_settings).then(function (field_tpl) {
                        var wrapper = $(field_tpl);

                        if (CloudFaces.SandBoxx.Profile.type != "edit" && CloudFaces.SandBoxx.Profile.type != "admin_view") {
                            resolve();
                        }
                        else {
                            var menu_wrapper = $('.profile-links', wrapper);
                            for (var link of CloudFaces.SandBoxx.Profile.menu_links) {
                                menu_wrapper
                                    .append('<li class="link-item text-input text-word-wrap" data-link="' + link + '">' + CloudFaces.Language.translate(link) + '</li>')
                            }
                            menu_wrapper
                                .append('<li class="link-logout text-input text-word-wrap">' + CloudFaces.Language.translate('logout') + '</li>')

                            container.append(wrapper);
                            resolve();
                        }
                    })
                });
            },
            refreshProfile: function (container) {
                var new_q = CFPersistentStorage.readValue("edited_question");
                CFPersistentStorage.writeValue("edited_question", "");
                if (new_q) {
                    // refresh question after edit
                    new_q = CloudFaces.SandBoxx.Helpers.jsonParse(new_q);

                    var question_wrapper = $('.question-' + new_q.q_id);

                    if (question_wrapper.hasClass('multi-question')) {
                        question_wrapper.find('.answer').hide();
                        for (var answer of new_q.answers) {
                            question_wrapper.find('.answer-' + answer.value).show();
                        }
                    }
                    else if (new_q.answers[0]) {
                        question_wrapper.find('.answer-text').text(new_q.answers[0].value)
                    }

                }
                else {
                    //refresh profile page after logout/login
                    var context = CFVariableStorage.readValue('profile_refresh');
                    if (context) {
                        CFRuntime.findCurrentPage(function (page) {
                            if (page == CloudFaces.SandBoxx.Config.pages.slideout) {
                                CloudFaces.SandBoxx.Helpers.ensureToken()
                                $(container).empty();
                                if (CloudFaces.SandBoxx.Helpers.checkLogin()) {
                                    CFVariableStorage.writeValue('profile_refresh', '');
                                    $(document).off('click')
                                    CloudFaces.SandBoxx.Profile.Main.init(container);
                                }
                            }
                        });
                    }

                }
            },
            prepareMatchesPage: function () {

                var userData = CloudFaces.Helpers.getUserData();
                if (userData) {
                    CFRuntime.findCurrentPage(function (page) {
                        // CFHeader.updateHeader('my matches');
                        if (page == CloudFaces.SandBoxx.Config.pages.slideout) {

                            CloudFaces.SandBoxx.Helpers.disableMatchesRefreshInterval();


                            // CFPersistentStorage.writeValue('profile_header_update', '1');
                            //     var header = CFPersistentStorage.readValue('profile_header_update');
                        }
                        else {
                            CloudFaces.SandBoxx.Helpers.enableMatchesRefreshInterval();
                        }

                        if ($('body').hasClass('android')) {
                            if (page == CloudFaces.SandBoxx.Config.pages.slideout) {
                                CFMenuNavigation.navigate(CloudFaces.SandBoxx.Config.pages.matches, '1');
                            }
                        } else {
                            if (page == CloudFaces.SandBoxx.Config.pages.match_profile) {

                                CFPersistentStorage.writeValue('profile_header_update', '1');
                                var header = CFPersistentStorage.readValue('profile_header_update');

                            }
                        }
                    })
                }
            },
            mapQuestionAnswers: function (questions, user_answers) {
                var question_answers = {};
                for (u_answer of user_answers) {
                    if (!question_answers[u_answer["question"]['id']]) {
                        question_answers[u_answer["question"]['id']] = {};
                    }

                    var ans_id = ( u_answer["answer"] ) ? u_answer["answer"]["id"] : 0;
                    question_answers[u_answer["question"]['id']][ans_id] = u_answer;
                }
                return (
                    questions.map(function (question) {

                        if (question.type == "text") {
                            question.answers = [];
                            if (question_answers[question.id]) {
                                question.answers = [question_answers[question.id][0]];
                            }
                            return question;
                        }
                        var questionn_answers = question.answers;
                        question.answers = questionn_answers.map(function (answer) {
                            if (question_answers[question.id]) {
                                if (question_answers[question.id][answer.answers_id.id]) {
                                    answer.checked = true;
                                }
                            }

                            return answer;

                        });
                        return question;
                    })
                )
            },
            handleQuestionClick: function (question) {
                var q_id = $(question).attr('data-q-id');
                CloudFaces.SandBoxx.Helpers.disableButtons();
                CloudFaces.SandBoxx.Profile.Navigation.navigateToQuestion(q_id);
            },
            handleInput: function (input) {
                var templates_total = $('.template-form').find('.required-input');
                var questions_total = $('.questionnaire-form').find('.required-input');
                var templates_completed = $(templates_total).filter(function () {
                    return !!this.value;
                }).length;
                var questions_completed = $(questions_total).filter(function () {
                    return !!this.value;
                }).length;

                var total = templates_total.length + questions_total.length;
                var completed = templates_completed + questions_completed;

                var btn = $('.save-question-btn');
                if (total == completed) {
                    btn.removeClass('disabled-btn');
                }
                else {
                    btn.addClass('disabled-btn');
                }
            },
            handleAjaxInput: function (input) {
                clearTimeout(CloudFaces.SandBoxx.Profile.timeout);
                CloudFaces.SandBoxx.Profile.timeout = setTimeout(function () {
                    var input_el = $(input);
                    var field_name = input_el.attr('name');
                    var field_value = input_el.val();
                    var putData = {
                        id: CloudFaces.SandBoxx.Profile.user_data.id
                    }
                    putData[field_name] = field_value;
                    CloudFaces.SandBoxx.DataProxy.saveUserData(putData)
                        .then(function (response) {
                            console.log("Profile update response: " + JSON.stringify(response));
                        });
                }, CloudFaces.SandBoxx.Profile.timeout_miliseconds);
            },
            handleAjaxSelect: function (select) {
                var select_e = $(select);
                var question_id = select_e.attr('name')
                var answer_id = select_e.val()
                var answer = [{
                    value: answer_id
                }];
                if (question_id == CloudFaces.SandBoxx.Config.constants.location_question_id) {
                    answer[0].a_text = select_e.closest('select').find('option:selected').text();
                }
                CloudFaces.SandBoxx.DataProxy.saveQuestionAnswers(question_id,
                    CloudFaces.SandBoxx.Profile.user_data.role_id,
                    CloudFaces.SandBoxx.Profile.user_data.id,
                    CloudFaces.SandBoxx.Profile.user_data.role_id,
                    answer)

                    .then(function (response) {
                        // console.log("Question update response: " + response)
                    });
            },
            handleCohortChange: function (cohortSelection) {
                var selected = $(cohortSelection).children("option:selected").val();
                CFPersistentStorage.writeValue("after_cohort", '');
                CFPersistentStorage.writeValue("selected_cohort", selected);
                CFNavigation.navigate(CloudFaces.SandBoxx.Config.pages.matches, '');
            },
            handleRequestSend: function () {
                CloudFaces.SandBoxx.Helpers.disableButtons();
                var userData = CloudFaces.Helpers.getUserData();
                var cohort_id = CFPersistentStorage.readValue("selected_cohort");
                var sponsor_id = '';
                var sponsoree_id = '';
                var match_percent = CloudFaces.SandBoxx.Profile.user_data.match_percent;
                var sender_id = userData.id;
                var ain = CloudFaces.DeviceCode.getDeviceAin();
                var notification_text = CloudFaces.Language.translate('request_send');
                var timezone = CloudFaces.SandBoxx.Profile.user_data.timezone;

                if (userData.role_id == CloudFaces.SandBoxx.Config.constants.role_sponsor) {
                    sponsor_id = userData.id;
                    sponsoree_id = CloudFaces.SandBoxx.Profile.user_data.id;
                }
                else {
                    sponsor_id = CloudFaces.SandBoxx.Profile.user_data.id;
                    sponsoree_id = userData.id;
                }

                CloudFaces.Directus.sendMatchRequest(sponsor_id, sponsoree_id, sender_id, cohort_id, match_percent, ain, timezone, notification_text)
                    .then(function (response) {
                        CloudFaces.SandBoxx.Profile.Navigation.navigateBackToMatches()
                    });
            },
            handleRequestCancel: function (btn) {
                CloudFaces.SandBoxx.Helpers.disableButtons();
                var id = $(btn).data('id');
                var ain = CloudFaces.SandBoxx.Profile.user_data.ain;
                var notification_text = CloudFaces.Language.translate('request_cancel');
                var timezone = CloudFaces.SandBoxx.Profile.user_data.timezone;

                CloudFaces.Directus.cancelSentRequest(id, ain, timezone, notification_text)
                    .then(function (response) {
                        CloudFaces.SandBoxx.Profile.Navigation.navigateBackToMatches()
                    });
            },
            handleCancelButton: function (btn) {
                CloudFaces.SandBoxx.Profile.Navigation.navigateBackToMatches()
            },
            handleForcefullyMatch: function (btn) {
                if ($(btn)[0].dataset.id && $('#search').val()) {
                    CloudFaces.SandBoxx.Helpers.disableButtons();
                    var sponsorId = 0;
                    var sponsoreeId = 0;
                    var senderId = $(btn)[0].dataset.id;
                    if (CloudFaces.SandBoxx.Profile.user_data.role_id == CloudFaces.SandBoxx.Config.constants.role_sponsor) {
                        sponsorId = CloudFaces.SandBoxx.Profile.user_data.id;
                        sponsoreeId = senderId;
                    } else if (CloudFaces.SandBoxx.Profile.user_data.role_id == CloudFaces.SandBoxx.Config.constants.role_sponsoree) {
                        sponsoreeId = CloudFaces.SandBoxx.Profile.user_data.id;
                        sponsorId = senderId;
                    }

                    var cohort_id = CFPersistentStorage.readValue("selected_cohort");
                    var match_percent = 100; //( We can't get match percentage in user details)
                    var ain = CloudFaces.DeviceCode.getDeviceAin();
                    var notification_text = CloudFaces.Language.translate('request_confirm');
                    var match_date = CloudFaces.SandBoxx.Helpers.textDate('', true);
                    var timezone = CloudFaces.SandBoxx.Profile.user_data.timezone;

                    CloudFaces.Directus.senderAlreadyHasRequest(CloudFaces.SandBoxx.Profile.user_data.id, cohort_id, CloudFaces.SandBoxx.Profile.user_data.role_id)
                        .then(function (response) {
                            if (response && response.data && response.data.length == 0) {
                                $('.btn_cancel_pending_request').each(function () {
                                    if ($(this).data('id') == senderId) {
                                        var matches_Id = $(this).data('matches_id');
                                        var ain = CloudFaces.SandBoxx.Profile.user_data.ain;
                                        var notification_text = CloudFaces.Language.translate('request_cancel');
                                        var timezone = CloudFaces.SandBoxx.Profile.user_data.timezone;

                                        CloudFaces.Directus.cancelSentRequest(matches_Id, ain, timezone, notification_text)
                                            .then(function (response) {
                                            });
                                    }
                                });

                                CloudFaces.Directus.matchForcefullyRequest(sponsorId, sponsoreeId, cohort_id, senderId, match_percent, match_date, ain, timezone, notification_text)
                                    .then(function (response) {
                                        CloudFaces.SandBoxx.Profile.Navigation.navigateBackToMatches()
                                    });
                            } else {
                                $('#search')[0].dataset.id = '';
                                $('#match')[0].dataset.id = '';
                                $('#search').val('');
                                CloudFaces.SandBoxx.Profile.Main.showPopup('Profile user is already matched with another user.', '');
                                return false;
                            }

                        })

                } else {
                    CloudFaces.SandBoxx.Profile.Main.showPopup('Please select user from option.', '');
                    $('#search').data('id', '');
                    $('#match').data('id', '');
                    return false;
                }
            },
            handleCancelPendingRequestByAdmin: function (btn) {
                // debugger
                var matches_Id = $(btn).data('matches_id');
                var ain = CloudFaces.SandBoxx.Profile.user_data.ain;
                var notification_text = CloudFaces.Language.translate('request_cancel');
                var timezone = CloudFaces.SandBoxx.Profile.user_data.timezone;

                CloudFaces.Directus.cancelSentRequest(matches_Id, ain, timezone, notification_text)
                    .then(function (response) {
                        CloudFaces.SandBoxx.Profile.Navigation.navigateBackToMatches()
                    });
            },
            autocomplete: function (inp, arr) {
                /*the autocomplete function takes two arguments,
            the text field element and an array of possible autocompleted values:*/
                var currentFocus;
                // execute a function when someone writes in the text field:
                inp.addEventListener("input", function (e) {
                    var a, b, i, val = this.value;
                    // close any already open lists of autocompleted values
                    closeAllLists();
                    if (!val) { return false; }
                    currentFocus = -1;

                    // create a DIV element that will contain the items (values):
                    a = document.createElement("DIV");
                    a.setAttribute("id", this.id + "autocomplete-list");
                    a.setAttribute("class", "autocomplete-items");

                    // append the DIV element as a child of the autocomplete container:
                    this.parentNode.appendChild(a);
                    $('#searchautocomplete-list').hide();
                    // for each item in the array...
                    for (i = 0; i < arr.length; i++) {
                        // check if the item starts with the same letters as the text field value:
                        if (arr[i].filterusername.toUpperCase().includes(val.toUpperCase())) {
                            $('#searchautocomplete-list').show();
                            var match_status = arr[i].match_status ? '- ' + arr[i].match_status : '';
                            // create a DIV element for each matching element:
                            b = document.createElement("div");

                            // make the matching letters bold:
                            // b.innerHTML = "<strong>" + arr[i].username.substr(0, val.length) + "</strong>";
                            b.innerHTML += arr[i].username;
                            // insert a input field that will hold the current array item's value:
                            if ( arr[i].match_status == 'active' ) {
                                b.setAttribute("class", "disabled_matched");
                            }

                            if (arr[i].match_status == 'active') {
                                b.innerHTML += "<input disabled type='hidden' data-id='" + arr[i].id + "' value='" + arr[i].username + "'> " + match_status;
                            } else {
                                b.innerHTML += "<input type='hidden' data-id='" + arr[i].id + "' value='" + arr[i].username + "'>";
                            }

                            // execute a function when someone clicks on the item value (DIV element):
                            if (arr[i].match_status != 'active') {
                                b.addEventListener("click", function (e) {
                                    // insert the value for the autocomplete text field:
                                    inp.value = this.getElementsByTagName("input")[0].value;
                                    var selectedId = this.getElementsByTagName("input")[0].dataset.id;
                                    inp.dataset.id = selectedId;
                                    $("#match")[0].dataset.id = selectedId;

                                    /*close the list of autocompleted values,
                                    (or any other open lists of autocompleted values:*/
                                    closeAllLists();
                                });
                            }

                            a.appendChild(b);
                        }
                    }
                });
                // execute a function presses a key on the keyboard:
                inp.addEventListener("keydown", function (e) {
                    $('#search')[0].dataset.id = '';
                    $('#match')[0].dataset.id = '';
                    var x = document.getElementById(this.id + "autocomplete-list");
                    if (x) x = x.getElementsByTagName("div");
                    if (e.keyCode == 40) {
                        /*If the arrow DOWN key is pressed,
                        increase the currentFocus variable:*/
                        currentFocus++;
                        // and and make the current item more visible:
                        addActive(x);
                    } else if (e.keyCode == 38) { //up
                        /*If the arrow UP key is pressed,
                        decrease the currentFocus variable:*/
                        currentFocus--;
                        // and and make the current item more visible:
                        addActive(x);
                    } else if (e.keyCode == 13) {
                        // If the ENTER key is pressed, prevent the form from being submitted,
                        e.preventDefault();
                        if (currentFocus > -1) {
                            // and simulate a click on the "active" item:
                            if (x) x[currentFocus].click();
                        }
                    }
                });
                function addActive(x) {
                    // a function to classify an item as "active":
                    if (!x) return false;
                    // start by removing the "active" class on all items:
                    removeActive(x);
                    if (currentFocus >= x.length) currentFocus = 0;
                    if (currentFocus < 0) currentFocus = (x.length - 1);
                    // add class "autocomplete-active":
                    x[currentFocus].classList.add("autocomplete-active");
                }
                function removeActive(x) {
                    // a function to remove the "active" class from all autocomplete items:
                    for (var i = 0; i < x.length; i++) {
                        x[i].classList.remove("autocomplete-active");
                    }
                }
                function closeAllLists(elmnt) {
                    /*close all autocomplete lists in the document,
                    except the one passed as an argument:*/
                    var x = document.getElementsByClassName("autocomplete-items");
                    for (var i = 0; i < x.length; i++) {
                        if (elmnt != x[i] && elmnt != inp) {
                            x[i].parentNode.removeChild(x[i]);
                        }
                    }
                }
                // execute a function when someone clicks in the document:
                document.addEventListener("click", function (e) {
                    closeAllLists(e.target);
                });
            },
            handleRequestDecline: function (btn) {
                CloudFaces.SandBoxx.Helpers.disableButtons();
                var id = $(btn).data('id');
                var ain = CloudFaces.DeviceCode.getDeviceAin();
                var notification_text = CloudFaces.Language.translate('request_decline');
                var timezone = CloudFaces.SandBoxx.Profile.user_data.timezone;
                CloudFaces.Directus.declineRequest(id, ain, timezone, notification_text)
                    .then(function (response) {
                        CloudFaces.SandBoxx.Profile.Navigation.navigateBackToMatches()
                    });
            },
            handleRequestConfirm: function (element) {
                CloudFaces.SandBoxx.Helpers.disableButtons();
                var userData = CloudFaces.Helpers.getUserData();
                var cohort_id = CFPersistentStorage.readValue("selected_cohort");
                var sponsor_id = '';
                var sponsoree_id = '';
                var match_percent = CloudFaces.SandBoxx.Profile.user_data.match_percent;
                var sender_id = userData.id;
                var ain = CloudFaces.DeviceCode.getDeviceAin();
                var notification_text = CloudFaces.Language.translate('request_confirm');
                var match_date = CloudFaces.SandBoxx.Helpers.textDate('', true);
                var timezone = CloudFaces.SandBoxx.Profile.user_data.timezone;
                var matchesId = $(element).data('id');

                if (userData.role_id == CloudFaces.SandBoxx.Config.constants.role_sponsor) {
                    sponsor_id = userData.id;
                    sponsoree_id = CloudFaces.SandBoxx.Profile.user_data.id;
                }
                else {
                    sponsor_id = CloudFaces.SandBoxx.Profile.user_data.id;
                    sponsoree_id = userData.id;
                }

                CloudFaces.Directus.confirmRequest(sponsor_id, sponsoree_id, cohort_id, sender_id, match_percent, match_date, ain, timezone, notification_text, matchesId)
                    .then(function (response) {
                        CloudFaces.SandBoxx.Profile.Navigation.navigateBackToMatches()
                    });
            },
            handleAdminUnmatch: function (btn) {
                CloudFaces.SandBoxx.Helpers.disableButtons();
                var request_id = $(btn).data('id')
                var ain_1 = $(btn).data('ain')
                var timezone_1 = $(btn).data('timezone')
                var ain_2 = CloudFaces.SandBoxx.Profile.user_data.ain;
                var timezone_2 = CloudFaces.SandBoxx.Profile.user_data.timezone;

                var notification_text = CloudFaces.Language.translate('request_unmatch');

                CloudFaces.Directus.arhiveRequest(request_id, ain_1, timezone_1, ain_2, timezone_2, notification_text)
                    .then(CloudFaces.SandBoxx.Profile.Navigation.navigateBackToMatches);
            },
            handleUnmatch: function (btn) {
                CloudFaces.SandBoxx.Helpers.disableButtons();
                var id = $(btn).data('id');
                var reason = $("#unmatchDescription").val();
                var userData = CloudFaces.Helpers.getUserData();
                CloudFaces.Directus.unmatchRequest(id, userData.id, reason)
                    .then(function (response) {
                        CloudFaces.SandBoxx.Profile.Navigation.navigateBackToMatches()
                    });
            },
            saveProfile: function () {
                CloudFaces.SandBoxx.Helpers.disableButtons();
                var templates = $('.template-form').serializeArray();
                var questionnaire = $('.questionnaire-form').serializeArray();
                var questions = questionnaire.map(function (el) {
                    var rel_q_id = $('select[name="' + el.name + '"]').data('rel')
                    var a_data = { q_id: el.name, a_id: el.value, rel_q_id: rel_q_id };
                    if (el.name == CloudFaces.SandBoxx.Config.constants.location_question_id) {
                        a_data.a_text = $('select[name="' + el.name + '"]').find('option:selected').text();
                    }
                    return a_data;
                });

                templates.push({
                    name: "id",
                    value: CloudFaces.SandBoxx.Profile.user_data.id
                });
                CloudFaces.SandBoxx.DataProxy.saveUserData(templates)
                    .then(function () {
                        var profile_type = CloudFaces.SandBoxx.Profile.type;
                        CloudFaces.SandBoxx.DataProxy.saveProfileQuestions(CloudFaces.SandBoxx.Profile.user_data.id, CloudFaces.SandBoxx.Profile.user_data.role_id, questions, profile_type)
                            .then(CloudFaces.SandBoxx.Profile.Navigation.navigateToQuestionnaire);
                    })
            },
            triggerModal: function () {
                $('#exampleModal').modal('toggle');
            },
            triggerHeaderModal: function () {
                $('#messageModal').modal('toggle');
            },
        },

        Navigation: {
            navigateToPage: function (link) {
                var page = $(link).attr('data-link')
                CFNavigation.navigate(page, '');
            },
            navigateToPasswordChange: function () {
                CFPersistentStorage.writeValue('register_info', JSON.stringify({ type: 'from_profile', user_id: CloudFaces.SandBoxx.Profile.user_data.id }));
                CFNavigation.navigate(CloudFaces.SandBoxx.Config.pages.password_change, '');
            },
            navigateToQuestionnaire: function () {
                console.log('navigateToQuestionnaire');
                CFPersistentStorage.writeValue("questionnaire_info", JSON.stringify({ type: "questionnaire" }));
                CFNavigation.navigateAndAddToBackStack(CloudFaces.SandBoxx.Config.pages.questionnaire, '');
            },
            navigateToQuestion: function (q_id) {
                CFPersistentStorage.writeValue("questionnaire_info", JSON.stringify({
                    type: "profile",
                    q_id: q_id,
                    user_id: CloudFaces.SandBoxx.Profile.user_data.id
                }));
                CFNavigation.navigate(CloudFaces.SandBoxx.Config.pages.questionnaire, '');
            },
            clearStorage: function () {
                CFPersistentStorage.writeValue("profile_info", "");
            },
            navigateBackToMatches: function () {
                CloudFaces.SandBoxx.Helpers.enableMatchesRefreshInterval();
                CFNavigation.navigateBack('');
            },
            navigateToAdminMatchProfile: function (btn) {
                var user_id = $(btn).data('id')
                var store_data = {
                    type: "admin_edit",
                    user_id: user_id
                }
                CFPersistentStorage.writeValue('profile_info', JSON.stringify(store_data));
                CFNavigation.navigate(CloudFaces.SandBoxx.Config.pages.match_profile, '');
            },
        },

        Events: function () {
            return new Promise(function (resolve, reject) {
                $(document)
                    .on('click', '.save-question-btn', function (ev) {
                        ev.preventDefault();
                        CloudFaces.SandBoxx.Profile.Main.saveProfile();
                    })
                    .on('click', '.avatar-img', function () {
                        CloudFaces.SandBoxx.Profile.Main.triggerModal();
                    })
                    .on('input', '.required-input', function () {
                        CloudFaces.SandBoxx.Profile.Main.handleInput(this);
                    })
                    .on('click', '.img-profile-camera', function (ev) {
                        ev.preventDefault();
                        CloudFaces.SandBoxx.Profile.Main.takePicture('PHOTO');
                    })
                    .on('click', '.img-profile-album', function (ev) {
                        ev.preventDefault();
                        CloudFaces.SandBoxx.Profile.Main.takePicture('ALBUM');
                    })
                    .on('click', '.questionnaire-question', function () {
                        CloudFaces.SandBoxx.Profile.Main.handleQuestionClick(this);
                    })
                    .on('click', '.password-wrapper', function () {
                        CloudFaces.SandBoxx.Profile.Navigation.navigateToPasswordChange();
                    })
                    .on('click', '.link-item', function () {
                        CloudFaces.SandBoxx.Profile.Navigation.navigateToPage(this);
                    })
                    .on('click', '.link-logout', function () {
                        CloudFaces.SandBoxx.DataProxy.handleLogout();
                    })
                    .on('input', '.ajax-input', function () {
                        CloudFaces.SandBoxx.Profile.Main.handleAjaxInput(this);
                    })
                    .on('change', '.ajax-select', function () {
                        CloudFaces.SandBoxx.Profile.Main.handleAjaxSelect(this);
                    })
                    .on('click', '.yes-send-request-btn', function () {
                        CloudFaces.SandBoxx.Profile.Main.handleRequestSend(this);
                    })
                    .on('click', '.yes-cancel-request-btn', function () {
                        CloudFaces.SandBoxx.Profile.Main.handleRequestCancel(this);
                    })
                    .on('click', '.yes-decline-request-btn', function () {
                        CloudFaces.SandBoxx.Profile.Main.handleRequestDecline(this);
                    })
                    .on('click', '.yes-accept-request-btn', function () {
                        CloudFaces.SandBoxx.Profile.Main.handleRequestConfirm(this);
                    })
                    .on('click', '.unmatch-btn', function () {
                        CloudFaces.SandBoxx.Profile.Main.handleUnmatch(this);
                    })
                    .on('click', '.match-info', function () {
                        CloudFaces.SandBoxx.Profile.Navigation.navigateToAdminMatchProfile(this);
                    })
                    .on('click', '.admin-unmatch-btn', function () {
                        CloudFaces.SandBoxx.Profile.Main.handleAdminUnmatch(this);
                    })
                    /* .on('click', '.btn-cancel', function () {
                        CloudFaces.SandBoxx.Profile.Main.handleCancelButton(this);
                    }) */
                    .on('click', '.forcefully_match', function () {
                        CloudFaces.SandBoxx.Profile.Main.handleForcefullyMatch(this);
                    })
                    .on('click', '.btn_cancel_pending_request', function () {
                        CloudFaces.SandBoxx.Profile.Main.handleCancelPendingRequestByAdmin(this);
                    })
                    .on('change', '#cohortSelect', function () {
                        CloudFaces.SandBoxx.Profile.Main.handleCohortChange(this);
                    })
                CloudFaces.Helpers.hideLoader();
                resolve();
            });
        }

    }
})