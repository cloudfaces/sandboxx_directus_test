$.extend(CloudFaces.SandBoxx, {
    Helpers: {
        jsonParse: function(json){
            return json?JSON.parse(json):{};
        },
        sqlParse: function(json){
            json = CloudFaces.SandBoxx.Helpers.jsonParse(json);
            return json.Data?json.Data:json;
        },
        loadTpl: function(template) {
            return new Promise(function(resolve, reject) {
                $.get(template, function(returnedTpl) {
                    resolve(returnedTpl)
                })
            })
        },
        htmlDecode: function(value) {
           return (typeof value === 'undefined') ? '' : $('<div/>').html(value).text();
        },
        checkLogin: function(){
            var userData = CloudFaces.Helpers.getUserData();
            if (!userData) {
                CFMenuNavigation.navigate(CloudFaces.SandBoxx.Config.pages.login, '');
                return false;
            }
            return true;
        },
        textDate :function(textDate, fullTime){
            var date = textDate ? new Date(textDate.replace(/-/g, '/')) : new Date();
            var returnDate = date.getFullYear() + '-' + CloudFaces.Helpers.getMonth(date) + '-' + CloudFaces.Helpers.getDate(date);
            if (fullTime) {
                returnDate += ' ' + CloudFaces.SandBoxx.Helpers.returnTwoDigit(date.getHours()) + ':' + CloudFaces.SandBoxx.Helpers.returnTwoDigit(date.getMinutes()) + ':' + CloudFaces.SandBoxx.Helpers.returnTwoDigit(date.getSeconds()); 
            }
            return returnDate;
        },
        returnTwoDigit: function(num){
            return num>9?num:'0'+num;
        },
        isUrl: function(str) {
            return /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(str);
        },
        handleSplashie: function(){
            $('#ip-container').each(function () {
                $('.ip-header').show()
                if (typeof CFNavigation.getNavigationContext() != 'undefined') {
                    var header = CFNavigation.getNavigationContext() ? $.parseJSON(CFNavigation.getNavigationContext()) : '';
                    if (typeof header != 'undefined') {
                        $('.ip-header').hide().remove();
                    }
                }
            });
        },
        ensureToken: function(){
            if (!$('body').hasClass('web')) {
                var device_token = CloudFaces.DeviceCode.getDeviceToken();
                if (!device_token || device_token == '') {
                    var ain = CloudFaces.DeviceCode.getDeviceAin();
                    var token = CFPushNotifications.getToken();
                    if (typeof token != 'undefined' && token && token != '' && ain != '' && ain != 0 && token != ain) {
                        CloudFaces.API.registerAppWithToken(token.replace(/\s/g, ''), ain, function(a) {
                            CFPersistentStorage.writeValue(CloudFaces.Config.app + '_device_token', token.replace(/\s/g, ''));
                        });
                    } 
                }
            }
        },
        enableButtons: function(){
            $('.btn, .questionnaire-question, .match-wrapper').css('pointer-events','auto');
        },
        disableButtons: function(){
            $('.btn, .questionnaire-question, .match-wrapper').css('pointer-events','none');
        },
        registerPageAnalytics: function(){
            CFRuntime.findCurrentPage(function(page){
                CFAnalytics.registerPage(page);
            });
        },


        /* 
            slideout interaction with matches refresh interval is VERY problematic and the following functions are used to conteract it:
            -- isPreviousPageSlideout, matchesShouldRefresh, disableMatchesRefreshInterval, enableMatchesRefreshInterval, prepareMatchesRefreshInterval
            EXCERCISE EXTREME CAUTION when changing those functions.

            p.s. Ako neshto grumne ot tiah, otidete i zapalete po edna svesht v curkvata
        */

        isPreviousPageSlideout: function(){
            return CFVariableStorage.readValue('prev_page') == CloudFaces.SandBoxx.Config.pages.slideout;
        },
        matchesShouldRefresh: function(){
            return CFVariableStorage.readValue('matches_refresh');
        },
        disableMatchesRefreshInterval: function(){
            CFVariableStorage.writeValue('matches_refresh', '');
        },
        enableMatchesRefreshInterval: function(){
            CFVariableStorage.writeValue('matches_refresh', 'true');  
        },
        prepareMatchesRefreshInterval: function(){
            CFRuntime.findCurrentPage(function(page){
                CFVariableStorage.writeValue('prev_page', page); 
                if(page != CloudFaces.SandBoxx.Config.pages.slideout){
                    CloudFaces.SandBoxx.Helpers.enableMatchesRefreshInterval();
                }
            })
        }
    }	
});