if (typeof CloudFaces == 'undefined') CloudFaces = { SandBoxx: {Config: {}}}
else CloudFaces.SandBoxx = {Config: {}};

CloudFaces.SandBoxx.Config.tables = {
	users:'20481',
	questions:'20492',
	answers:'20493',
	user_answers:'20494',
	companies:'20487',
	feedback:'20489',
	terms:'20490',
	resources:'20491',
	templates:'20483',
	matches: '20486',
	faq: '20488'
};

CloudFaces.SandBoxx.Config.relations = {
	user_companies:'313',
	question_answers:'314',
	question_question:'316'
};

CloudFaces.SandBoxx.Config.customSqls = {
	question_ids: '2054',
	user_answers_delete:'2056',
	get_question_answers:'2055',
	get_user_answers:'2057',
	get_user_requests: '2063',
	delete_non_history_requests: '2064',
	get_unmatch_requests: '2065',
	get_active_requests: '2066',
	delete_old_answers: '2058'
};

CloudFaces.SandBoxx.Config.pages = {
	questionnaire:'questionnaire',
	profile:'profile',
	match_profile:'match_profile',
	password_change: 'login_resetPassword',
	login:'login_login',
	register: 'login_register',
	matches: 'matches',
	faq_detailed: 'faq_detailed',
	resources_detailed: 'resources_detailed',
	slideout: 'slideout_main',
	cohort_select: 'cohort_select'
};

CloudFaces.SandBoxx.Config.constants = {
	application_token:"a04b223963e15a4c86bf76352cc26d4e",
	// matches_api_url:"http://203.109.113.157/sandboxx_directus/backend/matches.php",
	matches_api_url:"http://dev.test.cloudfaces.com/files/cmarix/backend/matches.php",
	// matches_api_url: "http://localhost/sandboxx_directus/backend/matches.php",
	// mutual_api_url: "http://localhost/sandboxx_directus/backend/mutual.php",
	// mutual_api_url: "http://203.109.113.157/sandboxx_directus/backend/mutual.php",
	mutual_api_url: "http://dev.test.cloudfaces.com/files/cmarix/backend/mutual.php",
	location_question_id: "6", //Directus timezone question ID
	role_sponsor: "1", // directus_users role_id field
	role_sponsoree: "2", // directus_users role_id field
	role_admin: "5", // Directus role ID
	match_admin: "7", // Directus role ID
	main_admin: "7", // Directus role ID
	refresh_interval: 60000
};
