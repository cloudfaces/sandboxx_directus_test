$.extend(CloudFaces.SandBoxx, {
    Terms: {

        container: '',
        wrapper: '',

        Main: {
            init: function (container) {
                CloudFaces.SandBoxx.Terms.Main.preparePage(container)
                    .then(CloudFaces.SandBoxx.Terms.Main.prepareTerms)
                    .then(CloudFaces.SandBoxx.Terms.Main.renderTerms)
                    .then(CloudFaces.SandBoxx.Terms.Events);
            },
            preparePage: function (container) {
                return new Promise(function (resolve, reject) {
                    CloudFaces.SandBoxx.Helpers.registerPageAnalytics();
                    CloudFaces.SandBoxx.Terms.container = container;
                    resolve();
                });
            },
            prepareTerms: function () {
                return new Promise(function (resolve, reject) {
                    CloudFaces.Directus.getTerms().then(resolve);
                });
            },
            renderTerms: function (terms_data) {
                return new Promise(function (resolve, reject) {
                    var termData = (typeof terms_data.data[0] !== 'undefined') ? terms_data.data[0] : [];
                    var tpl_path = 'templates/terms_wrapper.html'; 
                    CloudFaces.SandBoxx.Helpers.loadTpl(tpl_path).then(function (item_tpl) {
                        var item = $(item_tpl);
                        $('.terms-wrapper', item).html(CloudFaces.SandBoxx.Helpers.htmlDecode(termData.text));
                        $(CloudFaces.SandBoxx.Terms.container).append(item);
                        resolve();
                    })
                }); 
            }
        },

        Navigation: {
        },

        Events: function () {
            return new Promise(function (resolve, reject) {
                resolve();
            });
        }

    }
});