$.extend(CloudFaces.SandBoxx, {
    Resources: {

        container: '',
        wrapper: '',
        type: '',
        resource_id:'',

        Main: {
            init: function (container) {
                CloudFaces.SandBoxx.Resources.Main.preparePage(container)
                    .then(CloudFaces.SandBoxx.Resources.Main.prepareResources)
                    .then(CloudFaces.SandBoxx.Resources.Main.renderResources)
                    .then(CloudFaces.SandBoxx.Resources.Events);
            },
            preparePage: function (container) {
                return new Promise(function (resolve, reject) {
                    CloudFaces.SandBoxx.Helpers.registerPageAnalytics();
                    CloudFaces.SandBoxx.Resources.container = container;
                    var r_info = CloudFaces.SandBoxx.Helpers.jsonParse(CFPersistentStorage.readValue("resources_info")); 
                    CloudFaces.SandBoxx.Resources.Navigation.clearStorage();   
                    if (r_info.id) {
                        CloudFaces.SandBoxx.Resources.type = r_info.type;
                        CloudFaces.SandBoxx.Resources.resource_id = r_info.id;        
                    }    
                    var tpl_path = CloudFaces.SandBoxx.Resources.type == 'details' ? 'templates/resources_detailed_wrapper.html' : 'templates/resources_wrapper.html'; 
                    CloudFaces.SandBoxx.Helpers.loadTpl(tpl_path).then(function (wrapper_tpl) {
                        $(CloudFaces.SandBoxx.Resources.container).html(wrapper_tpl)
                        CloudFaces.SandBoxx.Resources.wrapper = '.resources-wrapper';
                        resolve();
                    })
                });
            },
            prepareResources: function () {
                return new Promise(function (resolve, reject) {
                    if (CloudFaces.SandBoxx.Resources.type == 'details') {
                        CloudFaces.Directus.getResource(CloudFaces.SandBoxx.Resources.resource_id).then(resolve);
                    }
                    else{
                        CloudFaces.Directus.getResources().then(resolve);
                    }
                });
            },
            renderResources: function (resource_data) {
                return new Promise(function (resolve, reject) {
                    resource_data = resource_data.data;
                    var tpl_path = CloudFaces.SandBoxx.Resources.type == 'details' ? 'templates/resources_detailed_item.html' : 'templates/resources_item.html'; 
                    CloudFaces.SandBoxx.Helpers.loadTpl(tpl_path).then(function (item_tpl) {

                        var container = $(CloudFaces.SandBoxx.Resources.wrapper);
                        if (CloudFaces.SandBoxx.Resources.type == 'details') {
                            var item = $(item_tpl);
                            $('.resources-detailed-title', item).text(resource_data.title);
                            if (resource_data.video_url) {
                                $('.resources-detailed-text', item).hide();
                                $('.video-wrapper iframe', item).attr('src', resource_data.video_url);
                            }
                            else{
                                $('.resources-youtube', item).hide();
                                $('.resources-detailed-text', item).html(CloudFaces.SandBoxx.Helpers.htmlDecode(resource_data.descriptions));
                            }
                            container.append(item);
                        }
                        else{
                            for (resource of resource_data) {
                                var item = $(item_tpl);
                                item.data('id', resource.id);
                                $('.text-input', item).text(resource.title);
                                if (resource.video_url) {
                                    $('.resource-type', item).text(CloudFaces.Language.translate('resources_video'));
                                }
                                else{
                                    $('.resource-type', item).text(CloudFaces.Language.translate('resources_article'));    
                                }
                                container.append(item);
                            }
                        }

                        resolve();
                    })
                }); 
            }, 
            handleResourceClick: function(resource_item){
                var resource_id = $(resource_item).data('id');
                CloudFaces.SandBoxx.Resources.Navigation.navigateToResourceDetails(resource_id);
            }
        },

        Navigation: {
            navigateToResourceDetails: function(id){
                CFPersistentStorage.writeValue("resources_info", JSON.stringify( {type:"details", id:id} ));
                CFNavigation.navigate(CloudFaces.SandBoxx.Config.pages.resources_detailed, id);
            },
            clearStorage: function(){
                CFPersistentStorage.writeValue("resources_info", '');
            }
        },

        Events: function () {
            return new Promise(function (resolve, reject) {
                $(document)
                    .on('click', '.resources-item', function(ev){
                        CloudFaces.SandBoxx.Resources.Main.handleResourceClick(this);
                    })
                resolve();
            });
        }

    }
});