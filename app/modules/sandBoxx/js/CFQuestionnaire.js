$.extend(CloudFaces.SandBoxx, {
    Questionnaire: {

        container: '',
        type: '',
        user_data:'',
        q_id: '',
        q_ids: [],
        q_count: '',
        q_current: '',

        Main: {
            init: function (container) {
                CloudFaces.SandBoxx.Questionnaire.Main.preparePage(container)
                    .then(CloudFaces.SandBoxx.Questionnaire.Main.prepareQuestion)
                    .then(CloudFaces.SandBoxx.Questionnaire.Main.renderQuestion)
                    .then(CloudFaces.SandBoxx.Questionnaire.Events);
            },
            preparePage: function (container) {
                return new Promise(function (resolve, reject) {
                    CloudFaces.SandBoxx.Helpers.registerPageAnalytics();
                    CloudFaces.Helpers.showLoader();
                    CloudFaces.SandBoxx.Questionnaire.container = container;
                    var q_info = CloudFaces.SandBoxx.Helpers.jsonParse(CFPersistentStorage.readValue("questionnaire_info"));
                    CloudFaces.SandBoxx.Questionnaire.type = q_info.type ? q_info.type : '';
                    CloudFaces.SandBoxx.Questionnaire.q_id = q_info.q_id ? q_info.q_id : '';
                    CloudFaces.SandBoxx.Questionnaire.q_ids = q_info.q_ids ? q_info.q_ids : [];
                    CloudFaces.SandBoxx.Questionnaire.q_count = q_info.q_count ? q_info.q_count : '';
                    CloudFaces.SandBoxx.Questionnaire.q_current = q_info.q_current ? q_info.q_current : '';


                    if (CloudFaces.SandBoxx.Questionnaire.type == 'questionnaire') {
                        //this takes question id dynamically based on current page and allows free navigation up and down the stack
                        var q_current = q_info.q_current?q_info.q_current:1;
                        CloudFaces.SandBoxx.Questionnaire.q_id = CloudFaces.SandBoxx.Questionnaire.q_ids[q_current-1];
                    }


                    var userData = CloudFaces.Helpers.getUserData();
                    var user_id = q_info.user_id ? q_info.user_id : userData.id;
                
                    CloudFaces.SandBoxx.DataProxy.getUserData(user_id)
                    .then(function(user_data){
                        
                        CloudFaces.SandBoxx.Questionnaire.user_data = user_data;

                        if (CloudFaces.SandBoxx.Questionnaire.type == 'questionnaire' && ! CloudFaces.SandBoxx.Questionnaire.q_id) {
                            CloudFaces.SandBoxx.DataProxy.loadQuestionIds()
                                .then(function (q_data) {
                                    for (var q of q_data) {
                                        CloudFaces.SandBoxx.Questionnaire.q_ids.push(q.id)
                                    }
                                    CloudFaces.SandBoxx.Questionnaire.q_count = q_data.length;
                                    CloudFaces.SandBoxx.Questionnaire.q_current = 1;
                                    CloudFaces.SandBoxx.Questionnaire.q_id = CloudFaces.SandBoxx.Questionnaire.q_ids[CloudFaces.SandBoxx.Questionnaire.q_current-1];
                                    resolve();
                                });
                        }
                        else {
                            resolve();
                        }
                    })

                });
            },
            prepareQuestion: function () {
                return new Promise(function (resolve, reject) {
                    if (CloudFaces.SandBoxx.Questionnaire.type == 'role_select') {
                        resolve({
                            question_text: CloudFaces.Language.translate('are_you_a'),
                            type: 'multi',
                            min_answers: '1',
                            max_answers: '1',
                            answers: [{
                                id: CloudFaces.SandBoxx.Config.constants.role_sponsor,
                                answers_id: {
                                    id: CloudFaces.SandBoxx.Config.constants.role_sponsor,
                                    answer_text: CloudFaces.Language.translate('sponsor')
                                }
                            }, {
                                id: CloudFaces.SandBoxx.Config.constants.role_sponsoree,
                                answers_id: {
                                    id: CloudFaces.SandBoxx.Config.constants.role_sponsoree,
                                    answer_text: CloudFaces.Language.translate('sponsoree')
                                }
                            }],
                            related_question_id: ''
                        });
                    }
                    else {
                        CloudFaces.SandBoxx.DataProxy.loadQuestion()
                            .then(function (q_data) {
                                var question = q_data;
                                var userData = CloudFaces.SandBoxx.Questionnaire.user_data;
                                CloudFaces.SandBoxx.DataProxy.loadQuestionAnswers(question.id, userData.id)
                                    .then(function (a_data) {
                                        resolve({
                                            question_text: userData.role_id == CloudFaces.SandBoxx.Config.constants.role_sponsor ? question.sponsor_question_text : question.sponsoree_question_text,
                                            type: question.type,
                                            subtitle: question.subtitle,
                                            min_answers: question.min_answers,
                                            max_answers: question.max_answers,
                                            answers: CloudFaces.SandBoxx.Questionnaire.Main.mapAnswers(question.answers, a_data),
                                            related_question_id: (typeof question.related_questions !== 'undefined' && question.related_questions.length > 0) ? question.related_questions[0].related_question_id.id : ''
                                        });
                                    })
                            })
                    }

                });
            },
            renderQuestion: function (question) {
                return new Promise(function (resolve, reject) {
                    var disabled = CloudFaces.SandBoxx.Questionnaire.type != 'profile' ? 'disabled-btn' : '';
                    var btn_text = CloudFaces.SandBoxx.Questionnaire.type != 'profile' ? CloudFaces.Language.translate('next') : CloudFaces.Language.translate('save');
                    if (CloudFaces.SandBoxx.Questionnaire.q_count && (CloudFaces.SandBoxx.Questionnaire.q_count == CloudFaces.SandBoxx.Questionnaire.q_current)) {
                        btn_text =  CloudFaces.Language.translate('see_matches');
                    }

                    if (CloudFaces.SandBoxx.Questionnaire.type == 'role_select') {
                        var question_wrapper_tpl = 'templates/wrapper_question_role_select.html';
                        var answer_wrapper_tpl = 'templates/btn_question_role_select.html';

                    } else if (question.type == 'multi') {
                        var question_wrapper_tpl = 'templates/wrapper_question_multi_select.html';
                        var answer_wrapper_tpl = 'templates/btn_question_multi_select.html';
                    } else {
                        var question_wrapper_tpl = 'templates/wrapper_question_multi_select.html';
                        var answer_wrapper_tpl = 'templates/text_question.html';
                    }

                    CloudFaces.SandBoxx.Helpers.loadTpl(question_wrapper_tpl).then(function (wrapper_tpl) {
                        CloudFaces.SandBoxx.Helpers.loadTpl(answer_wrapper_tpl).then(function (answer_tpl) {
                            //check if the questions are sponsor or sponsoree
                            //and add different layout
                            if (CloudFaces.SandBoxx.Questionnaire.type == 'role_select') {
                                var wrapper = $(wrapper_tpl);
                                $('.title-primary', wrapper).html(question.question_text);
                                $('.save-question-btn', wrapper).addClass(disabled).text(btn_text);
                                $('.multi-box', wrapper).data('min', question.min_answers).data('max', question.min_answers);


                                $(CloudFaces.SandBoxx.Questionnaire.container).append(wrapper);
                                
                                CloudFaces.SandBoxx.Questionnaire.Main.constructAnswers(question, answer_tpl, $('.multi-box', wrapper));

                                resolve();

                            } else {
                                // check if the questions are NOT sponsor or sponsoree,
                                // but the other questions, which creates profile info then
                                // add different layout
                                var wrapper = $(wrapper_tpl);
                                $('.title-primary', wrapper).html(question.question_text);
                                $('.title-secondary', wrapper).html(question.subtitle ? question.subtitle : '');

                                //set question counter number and number for all of questions
                                if (CloudFaces.SandBoxx.Questionnaire.type == 'questionnaire') {
                                    $('.number-count-questions', wrapper).html(CloudFaces.SandBoxx.Questionnaire.q_current);
                                    $('.number-sum-questions', wrapper).html(CloudFaces.SandBoxx.Questionnaire.q_count);
                                    $('.questionnaire-counter', wrapper).toggleClass('d-none');
                                }

                                $('.save-question-btn', wrapper).addClass(disabled).text(btn_text);
                                $('.multi-box', wrapper).data('min', question.min_answers).data('max', question.max_answers);


                                $(CloudFaces.SandBoxx.Questionnaire.container).append(wrapper);
                                CloudFaces.SandBoxx.Questionnaire.Main.constructAnswers(question, answer_tpl, $('.multi-box', wrapper));

                                resolve();
                            }
                        })
                    })
                });
            },
            constructAnswers: function (question, answer_tpl, wrapper) {
                console.log(question);
                var btn_disabled = true;
                if (question.type == 'multi') {
                    var answers = question.answers.sort(function (a, b) {
                        return a.answers_id.id - b.answers_id.id
                    });


                    for (var i = 0; i < answers.length; i++) {

                        var answer_wrapper = $(answer_tpl);
                        var label_class = answers[i].checked ? "active" : "";
                        $('.box-check_inner', answer_wrapper).html(answers[i].answers_id.answer_text);
                        $('.answer-wrapper', answer_wrapper).attr('for', 'multi_' + i + '').addClass(label_class);
                        $('.answer-check', answer_wrapper)
                            .attr('id', 'multi_' + i + '')
                            .val(answers[i].answers_id.id)
                            .prop('checked', answers[i].checked);

                        if (answers[i].checked) {
                            btn_disabled = false;
                        }

                        wrapper.append(answer_wrapper);
                    }
                }
                else {
                    var text = question.answers ? question.answers : '';

                    var answer_wrapper = $(answer_tpl);
                    $('.questions-textarea', answer_wrapper).text(text);
                    $('.char-counter', answer_wrapper).text(text.length);

                    if (text) {
                        btn_disabled = false;
                    }

                    wrapper.append(answer_wrapper);
                }

                if (!btn_disabled) {
                    $('.save-question-btn').removeClass('disabled-btn')
                }

            },
            mapAnswers: function (answers, user_answers) {
                console.log(answers);
                console.log(user_answers);
                if (!answers && user_answers[0]) {
                    return user_answers[0].answer_text;
                }
                else if (!answers && !user_answers[0]) {
                    return '';
                }

                var checked = user_answers.map(function (el) {
                    return el.answer
                });
                return (
                    answers.map(function (el) {
                        if (checked.indexOf(el.answers_id.id) !== -1) {
                            el.checked = true;
                        }
                        return el;
                    })
                )
            },
            answerQuestion: function () {
                CloudFaces.Helpers.showLoader();
                var answers = $('.question-form').serializeArray();
                var rel_q_id = $('.question-form').attr('data-rel');
                var q_id = CloudFaces.SandBoxx.Questionnaire.q_id;

                if (CloudFaces.SandBoxx.Questionnaire.type == 'questionnaire') {
                    //this takes question id dynamically based on current page and allows free navigation up and down the stack                  
                    var q_info = CloudFaces.SandBoxx.Helpers.jsonParse(CFPersistentStorage.readValue("questionnaire_info"));
                    var q_current = q_info.q_current?q_info.q_current:1;
                    q_id = CloudFaces.SandBoxx.Questionnaire.q_ids[q_current-1];
                }

                var userData = CloudFaces.SandBoxx.Questionnaire.user_data;

                if (CloudFaces.SandBoxx.Questionnaire.type == 'role_select') {
                    CloudFaces.SandBoxx.DataProxy.saveUserRole(userData.id, answers[0].value)
                        .then(function () {
                            CloudFaces.SandBoxx.Questionnaire.Navigation.updateUserStorage(answers[0].value);
                        })
                        .then(CloudFaces.SandBoxx.Questionnaire.Navigation.manageNavigation);
                }
                else {
                    CloudFaces.SandBoxx.DataProxy.saveQuestionAnswers(q_id, userData.role_id, userData.id, userData.role_id, answers)
                        .then(function () {
                            if (CloudFaces.SandBoxx.Questionnaire.type == 'profile') {
                                CFPersistentStorage.writeValue("edited_question", JSON.stringify({
                                    q_id: q_id,
                                    answers: answers
                                }))
                            }
                            CloudFaces.SandBoxx.Questionnaire.Navigation.manageNavigation();
                        });
                }
            },
            handleSave: function (btn) {
                var answers_container = $('.multi-box');
                var min = answers_container.data('min');
                var checked = answers_container.find('.answer-check:checked');
                if (checked.length >= min) {
                    CloudFaces.SandBoxx.Helpers.disableButtons();
                    CloudFaces.SandBoxx.Questionnaire.Main.answerQuestion();
                }
                else if (!checked.length) {
                    CloudFaces.SandBoxx.Helpers.disableButtons();
                    CloudFaces.SandBoxx.Questionnaire.Main.answerQuestion();
                }
            },
            handleCheckbox: function (checkbox) {
                var label = $(checkbox);
                var check = label.find('.answer-check');
                check.prop('checked') ? check.prop('checked', false) : check.prop('checked', true);
                var answers_container = check.closest('.multi-box');
                var min = answers_container.data('min');
                var max = answers_container.data('max');
                var checked = answers_container.find('.answer-check:checked');


                if (checked.length > max) {
                    if (max == 1) {
                        checked.attr('checked', false);
                        checked.closest('label').removeClass('active');
                        check.prop('checked', true);
                    }
                    else {
                        check.attr('checked', false);
                    }
                }

                if (check.prop('checked')) {
                    label.addClass('active');
                }
                else {
                    label.removeClass('active');
                }

                var btn = $('.save-question-btn');
                if (checked.length >= min) {
                    btn.removeClass('disabled-btn');
                }
                else {
                    btn.addClass('disabled-btn');
                }
            },
            handleTextBox: function (input) {
                $('.char-counter').text($(input).val().length);
                if ($(input).val().length) {
                    $('.save-question-btn').removeClass('disabled-btn');
                }
                else {
                    $('.save-question-btn').addClass('disabled-btn');
                }
            },
            refreshQuestionChain: function () {
                //this ensures question position is kept dynamically based on current page and allows free navigation up and down the stack. It is called only on back navigation
                var q_info = CloudFaces.SandBoxx.Helpers.jsonParse(CFPersistentStorage.readValue("questionnaire_info"));
                if (!$.isEmptyObject(q_info)) {
                    if (q_info.skip_inactive) {
                        q_info.skip_inactive = false;
                    }
                    else if(q_info.q_current){
                        q_info.q_current = --q_info.q_current;
                    }
                    CFPersistentStorage.writeValue("questionnaire_info", JSON.stringify(q_info));
                }
            }
        },

        Navigation: {
            manageNavigation: function () {
                CloudFaces.Helpers.hideLoader();
                if (CloudFaces.SandBoxx.Questionnaire.type == 'role_select') {
                    CloudFaces.SandBoxx.Questionnaire.Navigation.clearStorage();
                    CFPersistentStorage.writeValue("profile_info", JSON.stringify({type: "create"}));
                    CFNavigation.navigateAndAddToBackStack('profile', '');
                }
                else if (CloudFaces.SandBoxx.Questionnaire.type == 'questionnaire') {
                    CloudFaces.SandBoxx.Questionnaire.q_id = CloudFaces.SandBoxx.Questionnaire.q_ids[CloudFaces.SandBoxx.Questionnaire.q_current];
                    if (!CloudFaces.SandBoxx.Questionnaire.q_id) {
                        CloudFaces.SandBoxx.Questionnaire.Navigation.clearStorage();
                        if(CFVariableStorage.readValue('app_started') == 'true' || !$('body').hasClass('ios')){
                            CFVariableStorage.writeValue('profile_refresh', 'true');
                        }
                        CloudFaces.SandBoxx.Helpers.disableMatchesRefreshInterval();
                        CFNavigation.navigateToRoot(CloudFaces.SandBoxx.Config.pages.matches, '1');
                    }
                    else {
                        CloudFaces.SandBoxx.Questionnaire.Navigation.writeNext();
                        CFNavigation.navigateAndAddToBackStack('questionnaire', '');
                    }
                }
                else {
                    CloudFaces.SandBoxx.Questionnaire.Navigation.clearStorage();
                    CFNavigation.navigateBack('');
                }
            },
            clearStorage: function () {
                CFPersistentStorage.writeValue("questionnaire_info", '');
            },
            writeNext: function () {
                var q_info = CloudFaces.SandBoxx.Helpers.jsonParse(CFPersistentStorage.readValue("questionnaire_info"));
                var q_current = q_info.q_current?q_info.q_current:1;
                CFPersistentStorage.writeValue("questionnaire_info", JSON.stringify({
                    skip_inactive: true,
                    type: CloudFaces.SandBoxx.Questionnaire.type,
                    q_id: CloudFaces.SandBoxx.Questionnaire.q_id,
                    q_ids: CloudFaces.SandBoxx.Questionnaire.q_ids,
                    q_count: CloudFaces.SandBoxx.Questionnaire.q_count,
                    q_current: ++q_current
                }));
            },
            updateUserStorage: function (role_id) {
                var userData = CloudFaces.SandBoxx.Helpers.jsonParse(CFPersistentStorage.readValue(CloudFaces.Config.app + '_userData'));
                userData.role_id = role_id;
                CFPersistentStorage.writeValue(CloudFaces.Config.app + '_userData', JSON.stringify(userData));
            }
        },

        Events: function () {
            return new Promise(function (resolve, reject) {
                $(document)
                    .on('click', '.save-question-btn', function (ev) {
                        ev.preventDefault();
                        CloudFaces.SandBoxx.Questionnaire.Main.handleSave(this);
                    })
                    .on('click', '.answer-wrapper', function (ev) {
                        ev.stopPropagation();
                        ev.preventDefault();

                        CloudFaces.SandBoxx.Questionnaire.Main.handleCheckbox(this);
                    })
                    .on('input', '.required-text', function () {
                        CloudFaces.SandBoxx.Questionnaire.Main.handleTextBox(this);
                    });
                CloudFaces.Helpers.hideLoader();
                resolve();
            });
        }

    }
});