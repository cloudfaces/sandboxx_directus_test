$.extend(CloudFaces.SandBoxx, {
    Faq: {

        container: '',
        wrapper: '',
        type: '',
        faq_id:'',

        Main: {
            init: function (container) {
                CloudFaces.SandBoxx.Faq.Main.preparePage(container)
                    .then(CloudFaces.SandBoxx.Faq.Main.prepareFaq)
                    .then(CloudFaces.SandBoxx.Faq.Main.renderFaq)
                    .then(CloudFaces.SandBoxx.Faq.Events);
            },
            preparePage: function (container) {
                return new Promise(function (resolve, reject) {
                    CloudFaces.SandBoxx.Helpers.registerPageAnalytics();
                    CloudFaces.SandBoxx.Faq.container = container;
                    var f_info = CloudFaces.SandBoxx.Helpers.jsonParse(CFPersistentStorage.readValue("faq_info")); 
                    CloudFaces.SandBoxx.Faq.Navigation.clearStorage();   
                    if (f_info.id) {
                        CloudFaces.SandBoxx.Faq.type = f_info.type;
                        CloudFaces.SandBoxx.Faq.faq_id = f_info.id;        
                    }    
                    var tpl_path = CloudFaces.SandBoxx.Faq.type == 'details' ? 'templates/faq_detailed_wrapper.html' : 'templates/faq_wrapper.html'; 
                    CloudFaces.SandBoxx.Helpers.loadTpl(tpl_path).then(function (wrapper_tpl) {
                        $(CloudFaces.SandBoxx.Faq.container).html(wrapper_tpl)
                        CloudFaces.SandBoxx.Faq.wrapper = '.faq-wrapper';
                        resolve();
                    })
                });
            },
            prepareFaq: function () {
                return new Promise(function (resolve, reject) {
                    if (CloudFaces.SandBoxx.Faq.type == 'details') {
                        CloudFaces.Directus.getFaq(CloudFaces.SandBoxx.Faq.faq_id).then(resolve);
                    }
                    else{
                        CloudFaces.Directus.getFaqs().then(resolve);
                    }
                });
            },
            renderFaq: function (faq_data) {
                return new Promise(function (resolve, reject) {
                    var tpl_path = CloudFaces.SandBoxx.Faq.type == 'details' ? 'templates/faq_detailed_item.html' : 'templates/faq_item.html'; 
                    CloudFaces.SandBoxx.Helpers.loadTpl(tpl_path).then(function (item_tpl) {
                        var container = $(CloudFaces.SandBoxx.Faq.wrapper);
                        if (CloudFaces.SandBoxx.Faq.type == 'details') {
                            var item = $(item_tpl);
                            $('.faq-detailed-title', item).text(faq_data.data.title);
                            $('.faq-detailed-text', item).html(CloudFaces.SandBoxx.Helpers.htmlDecode(faq_data.data.description));
                            container.append(item);
                        }
                        else{
                            for (faq of faq_data.data) {
                                var item = $(item_tpl);
                                item.data('id', faq.id);
                                $('.text-input', item).text(faq.title);
                                container.append(item);
                            }
                        }

                        resolve();
                    })
                }); 
            }, 
            handleFaqClick: function(faq_item){
                var faq_id = $(faq_item).data('id');
                CloudFaces.SandBoxx.Faq.Navigation.navigateToFaqDetails(faq_id);
            }
        },

        Navigation: {
            navigateToFaqDetails: function(id){
                CFPersistentStorage.writeValue("faq_info", JSON.stringify( {type:"details", id:id} ));
                CFNavigation.navigate(CloudFaces.SandBoxx.Config.pages.faq_detailed, id);
            },
            clearStorage: function(){
                CFPersistentStorage.writeValue("faq_info", '');
            }
        },

        Events: function () {
            return new Promise(function (resolve, reject) {
                $(document)
                    .on('click', '.faq-item', function(ev){
                        CloudFaces.SandBoxx.Faq.Main.handleFaqClick(this);
                    })
                resolve();
            });
        }

    }
});