$.extend(CloudFaces.SandBoxx, {
    Matches: {

        container: '',
        type: '',
        refresh_interval: null,
        refresh_title: 'no',
        match_data: [],
        search: '',
        isSponsoreeSelected: true,
        isSearch: false,
        Main: {
            init: function (container) {
                CloudFaces.SandBoxx.Matches.Main.preparePage(container)
                    .then(CloudFaces.SandBoxx.Matches.Main.prepareMatches)
                    .then(CloudFaces.SandBoxx.Matches.Main.renderMatches)
                    .then(CloudFaces.SandBoxx.Matches.Main.renderButtons)
                    .then(CloudFaces.SandBoxx.Matches.Events)
            },
            preparePage: function (container) {
                return new Promise(function (resolve, reject) {
                    CloudFaces.SandBoxx.Matches.Main.clearPageRefreshInterval();
                    CloudFaces.SandBoxx.Helpers.registerPageAnalytics();
                    CloudFaces.Helpers.showLoader();

                    CloudFaces.SandBoxx.Matches.container = container;
                    var userData = CloudFaces.Helpers.getUserData();
                    // check if user has selected cohort
                    var current_cohort = CFPersistentStorage.readValue("selected_cohort");
                    if (!current_cohort) {
                        // redirect to cohort page
                        CFPersistentStorage.writeValue("after_cohort", JSON.stringify({ page: 'matches' }));
                        CFNavigation.navigateToRoot(CloudFaces.SandBoxx.Config.pages.cohort_select, '')
                    } else {
                        CloudFaces.SandBoxx.Matches.type = userData.role == CloudFaces.SandBoxx.Config.constants.role_admin ? 'admin' : 'regular';
                        resolve();
                    }

                })
            },
            prepareMatches: function () {
                return new Promise(function (resolve, reject) {
                    var userData = CloudFaces.Helpers.getUserData();
                    var cohort_id = CFPersistentStorage.readValue("selected_cohort");
                    if (CloudFaces.SandBoxx.Matches.type == 'admin') {
                        CloudFaces.SandBoxx.DataProxy.getAllRequests(cohort_id)
                            .then(function (request_match_data) {
                                // IF cohort is expired
                                if ( request_match_data.Status && request_match_data.Status == 'Error' ) {
                                    CloudFaces.SandBoxx.Matches.Main.renderProfileWarning(request_match_data.Status, request_match_data.ErrorMessage);
                                } else {
                                    var unmatching_user_ids = [];
                                    var userIds = [];
                                    var Matchedrequests = [];
                                    var pendingRequests = [];
                                    var unmatchRequests = [];

                                    // Get all userids from the matched request
                                    for (var j = 0; j < request_match_data.length; j++) {
                                        var match = request_match_data[j];
                                        userIds.push(match.sponsor_id);
                                        userIds.push(match.sponsoree_id);
                                    }

                                    // Get active userids from the matched request start
                                    var matched_requests = request_match_data.filter(function (matched_request) {
                                        return matched_request.status && matched_request.status == 'active'
                                    });

                                    for (var k = 0; k < matched_requests.length; k++) {
                                        var request = matched_requests[k];
                                        Matchedrequests.push(request.sponsor_id);
                                        Matchedrequests.push(request.sponsoree_id);
                                    }

                                    // end

                                    // Get request userids from the matched request start
                                    var pending_requests = request_match_data.filter(function (pending_request) {
                                        return pending_request.status && pending_request.status == 'request'
                                    });

                                    for (var k = 0; k < pending_requests.length; k++) {
                                        var pending_request = pending_requests[k];
                                        pendingRequests.push(pending_request.sponsor_id);
                                        pendingRequests.push(pending_request.sponsoree_id);
                                    }

                                    //end

                                    // Get unmatch userids from the matched request start
                                    var unmatching_requests = request_match_data.filter(function (unmatching_request) {
                                        return unmatching_request.status && unmatching_request.status == 'unmatching'
                                    });

                                    for (var i = 0; i < unmatching_requests.length; i++) {
                                        var unmatch = unmatching_requests[i];
                                        unmatchRequests.push(unmatch.sponsor_id);
                                        unmatchRequests.push(unmatch.sponsoree_id);
                                        unmatching_user_ids.push(unmatch.sender_id);
                                    }

                                    // end

                                    CloudFaces.SandBoxx.DataProxy.getUsersList(cohort_id)
                                        .then(function (users) {
                                            var users_list = [];
                                            users.forEach(function (v) {
                                                users_list.push(v.directus_users_id)
                                            });
                                            for (var i = 0; i < users_list.length; i++) {
                                                var user = users_list[i];
                                                if (Matchedrequests.indexOf(user.id) > -1) {
                                                    user.matched_status = 'active';
                                                } else if (pendingRequests.indexOf(user.id) > -1) {
                                                    user.matched_status = 'request';
                                                } else if (unmatchRequests.indexOf(user.id) > -1) {
                                                    user.matched_status = 'unmatch';
                                                } else if (userIds.indexOf(user.id) < 0) {
                                                    user.matched_status = 'noactivity';
                                                }
                                            }
                                            resolve({
                                                users_list: users_list
                                                , unmatching_user_ids: unmatching_user_ids
                                            });
                                        })
                                } 
                                
                            });
                    }
                    else {
                        CloudFaces.SandBoxx.DataProxy.getMatches(userData.id, cohort_id)
                            .then(function (match_data) {
                                if (match_data.Data.matches) {
                                    var suggested_users = '';;

                                    var userData = CloudFaces.Helpers.getUserData();
                                    CloudFaces.SandBoxx.Helpers.handleSplashie()
                                    var title = userData.role == CloudFaces.SandBoxx.Config.constants.role_admin ? 'employees' : 'my matches';

                                    if ($('body').hasClass('web')) {
                                        // needed for web header title
                                        var refresh_title = CloudFaces.SandBoxx.Matches.refresh_title;
                                        if (refresh_title === 'no') {
                                            CFHeader.updateHeader('my matches');
                                            CloudFaces.SandBoxx.Matches.refresh_title = 'yes';
                                        }

                                    } else {
                                        CFHeader.updateHeader(title);
                                    }

                                    if (match_data.Data.matches && match_data.Data.matches.length > 0) {
                                        var users = [];
                                        for (var i = 0; i < match_data.Data.matches.length; i++) {
                                            users.push(match_data.Data.matches[i].id + '-' + match_data.Data.matches[i].percent+'-'+match_data.Data.matches[i].match_status);
                                        }
                                        suggested_users = users.join(',');
                                        CloudFaces.SandBoxx.DataProxy.getMutuals(userData.id, suggested_users, cohort_id)
                                            .then(function (mutual_data) {
                                                CloudFaces.SandBoxx.Matches.Main.startPageRefreshInterval();
                                                match_data.Data.mutuals = mutual_data.Data;
                                                resolve(match_data.Data);
                                            });
                                    } else {
                                        CloudFaces.SandBoxx.Matches.Main.startPageRefreshInterval();
                                        match_data.Data.mutuals = [];
                                        resolve(match_data.Data);
                                    }
                                }
                                else {
                                    CloudFaces.SandBoxx.Matches.Main.renderProfileWarning(match_data.Status, match_data.ErrorMessage);
                                }
                            })
                    }
                });
            },
            renderMatches: function (match_data) {
                return new Promise(function (resolve, reject) {
                    //ADD TEMPLATES FOR:
                    // - WRAP THE TABS SECTION IN ADMIN'S MATCHES
                    // - WRAP THE LIST WITH MATCHES
                    // - TEMPLATES FOR EVERY ITEM IN LIST
                    if (CloudFaces.SandBoxx.Matches.isSearch == false) {
                        CloudFaces.SandBoxx.Matches.match_data = match_data;
                    }

                    var wrapper_match_list_tpl = CloudFaces.SandBoxx.Matches.type == "admin" ? 'templates/wrapper_admin_match_list.html' : 'templates/wrapper_match_list.html';
                    var match_list_item_tpl = 'templates/match_list_item.html';

                    CloudFaces.SandBoxx.Helpers.loadTpl(wrapper_match_list_tpl).then(function (wrapper_tpl) {
                        CloudFaces.SandBoxx.Helpers.loadTpl(match_list_item_tpl).then(function (item_tpl) {
                            var container = $(CloudFaces.SandBoxx.Matches.container);
                            var userData = CloudFaces.Helpers.getUserData();
                            var wrapper = $(wrapper_tpl);

                            $('.search-box', wrapper).val(CloudFaces.SandBoxx.Matches.search);

                            if (CloudFaces.SandBoxx.Matches.type == "admin") {
                                var sponsor_user_list = [];
                                var has_sponsorees = false;
                                var has_matchSponsoree = false;
                                var has_pendingRequestSponsoree = false;
                                var has_noActivitySponsoree = false;
                                $('#sponsorees-tab', wrapper).text(CloudFaces.Language.translate('sponsorees'));
                                for (var match of match_data.users_list) {
                                    if (match.role == CloudFaces.SandBoxx.Config.constants.role_admin || match.role == CloudFaces.SandBoxx.Config.constants.match_admin || match.role == CloudFaces.SandBoxx.Config.constants.main_admin) { // Admin role
                                        continue;
                                    }
                                    if (match.role_id == CloudFaces.SandBoxx.Config.constants.role_sponsor) {
                                        sponsor_user_list.push(match);
                                        continue;
                                    }
                                    var requesting_unmatch = false;
                                    if (match_data.unmatching_user_ids.indexOf(match.id) != -1) {
                                        requesting_unmatch = true;
                                    }
                                    CloudFaces.SandBoxx.Matches.Main.appendMatch($('.list-sponsorees', wrapper), item_tpl, match, false, requesting_unmatch);
                                    if (match.matched_status == 'active') {
                                        CloudFaces.SandBoxx.Matches.Main.appendMatch($('.list-matchedsponsorees', wrapper), item_tpl, match, false, false);
                                        has_matchSponsoree = true;
                                    } else if (match.matched_status == 'request') {
                                        CloudFaces.SandBoxx.Matches.Main.appendMatch($('.list-paendingrequestsponsorees', wrapper), item_tpl, match, false, false);
                                        has_pendingRequestSponsoree = true;
                                    } else if (match.matched_status == 'noactivity') {
                                        CloudFaces.SandBoxx.Matches.Main.appendMatch($('.list-noactivitysponsorees', wrapper), item_tpl, match, false, false);
                                        has_noActivitySponsoree = true;
                                    }
                                    has_sponsorees = true;
                                }
                                if (!has_sponsorees) {
                                    $('.no-sponsorees', wrapper).text(CloudFaces.Language.translate('no_sponsorees'));
                                }
                                if (!has_matchSponsoree) {
                                    $('.no-matchedsponsorees', wrapper).text('No Matched Sponsoree Found.');
                                }
                                if (!has_pendingRequestSponsoree) {
                                    $('.no-paendingrequestsponsorees', wrapper).text(CloudFaces.Language.translate('No Pending Request Found'));
                                }
                                if (!has_noActivitySponsoree) {
                                    $('.no-noactivitysponsorees', wrapper).text(CloudFaces.Language.translate('No Activity Request Found'));
                                }

                                $('#sponsors-tab', wrapper).text(CloudFaces.Language.translate('sponsors'));
                                var has_matchSponsor = false;
                                var has_pendingRequestSponsor = false;
                                var has_noActivitySponsor = false;
                                for (var match of sponsor_user_list) {
                                    if (match.role == CloudFaces.SandBoxx.Config.constants.role_admin || match.role == CloudFaces.SandBoxx.Config.constants.match_admin || match.role == CloudFaces.SandBoxx.Config.constants.main_admin) { // Admin role
                                        continue;
                                    }
                                    var requesting_unmatch = false;
                                    if (match_data.unmatching_user_ids.indexOf(match.id) != -1) {
                                        requesting_unmatch = true;
                                    }
                                    CloudFaces.SandBoxx.Matches.Main.appendMatch($('.list-sponsors', wrapper), item_tpl, match, false, requesting_unmatch);
                                    if (match.matched_status == 'active') {
                                        CloudFaces.SandBoxx.Matches.Main.appendMatch($('.list-matchedsponsors', wrapper), item_tpl, match, false, false);
                                        has_matchSponsor = true;
                                    } else if (match.matched_status == 'request') {
                                        CloudFaces.SandBoxx.Matches.Main.appendMatch($('.list-pendingrequestsponsors', wrapper), item_tpl, match, false, false);
                                        has_pendingRequestSponsor = true;
                                    } else if (match.matched_status == 'noactivity') {
                                        CloudFaces.SandBoxx.Matches.Main.appendMatch($('.list-noactivitysponsors', wrapper), item_tpl, match, false, false);
                                        has_noActivitySponsor = true;
                                    }
                                }
                                if (sponsor_user_list.length == 0) {
                                    $('.no-sponsors', wrapper).text(CloudFaces.Language.translate('no_sponsors'));
                                }
                                if (!has_matchSponsor) {
                                    $('.no-matchedsponsors', wrapper).text('No Matched Sponsor Found.');
                                }
                                if (!has_pendingRequestSponsor) {
                                    $('.no-pendingrequestsponsors', wrapper).text(CloudFaces.Language.translate('No Pending Request Found'));
                                }
                                if (!has_noActivitySponsor) {
                                    $('.no-noactivitysponsors', wrapper).text(CloudFaces.Language.translate('No Activity Request Found'));
                                }
                                container.append(wrapper);
                                $('.search-box', wrapper).focus();
                                if (CloudFaces.SandBoxx.Matches.isSponsoreeSelected) {
                                    $("#sponsorees-tab").trigger('click');
                                } else {
                                    $("#sponsors-tab").trigger('click');
                                }
                                resolve();
                            }
                            else {
                                var title = userData.role_id == CloudFaces.SandBoxx.Config.constants.role_sponsor ? CloudFaces.Language.translate('sponsorees') : CloudFaces.Language.translate('sponsors');
                                $('.title-list', wrapper).text(title);
                                var unclickable = false;
                                for (var match of match_data.active_matches) {
                                    CloudFaces.SandBoxx.Matches.Main.appendMatch($('.active-matches', wrapper), item_tpl, match, unclickable, false);
                                    unclickable = true;
                                }

                                // TO DO

                                for (var match of match_data.mutuals) {
                                    CloudFaces.SandBoxx.Matches.Main.appendMatch($('.list-mutual', wrapper), item_tpl, match, unclickable, false);
                                }

                                if (match_data.mutuals.length == 0) {
                                    $(".no-mutual", wrapper).text("No Mutuals found");
                                }

                                if (match_data.active_matches.length > 0) {
                                    $(".no-mutual", wrapper).addClass('mt-90');
                                    $(".no-suggested", wrapper).addClass('mt-90');
                                    $(".no-sent", wrapper).addClass('mt-90');
                                    $(".no-received", wrapper).addClass('mt-90');
                                    $(".no-past", wrapper).addClass('mt-90');
                                } else {
                                    $(".no-mutual", wrapper).removeClass('mt-90');
                                    $(".no-suggested", wrapper).removeClass('mt-90');
                                    $(".no-sent", wrapper).removeClass('mt-90');
                                    $(".no-received", wrapper).removeClass('mt-90');
                                    $(".no-past", wrapper).removeClass('mt-90');
                                }

                                if (match_data.active_matches.length == 0) {
                                    $("#active-matches", wrapper).hide();
                                } else {
                                    $("#active-matches", wrapper).show();
                                }

                                for (var match of match_data.requested_matches) {
                                    CloudFaces.SandBoxx.Matches.Main.appendMatch($('.list-sent', wrapper), item_tpl, match, unclickable, true);
                                }

                                if (match_data.requested_matches.length == 0) {
                                    $(".no-sent", wrapper).text("No sent request found");
                                }
                                for (var match of match_data.matches) {
                                    CloudFaces.SandBoxx.Matches.Main.appendMatch($('.list-suggested', wrapper), item_tpl, match, unclickable, false);
                                }

                                if (match_data.matches.length == 0) {
                                    $(".no-suggested", wrapper).text("No suggestion found");
                                }

                                if (match_data.active_matches.length == 0 && match_data.requested_matches.length == 0 && match_data.matches == 0) {
                                    $('.no-matches', wrapper).text(CloudFaces.Language.translate('no_matches'));
                                }
                                container.append(wrapper);
                                if (match_data.requesting_matches.length) {
                                    var requesting_wrapper = $(wrapper_tpl);
                                    for (var match of match_data.requesting_matches) {
                                        CloudFaces.SandBoxx.Matches.Main.appendMatch($('.list-received', wrapper), item_tpl, match, unclickable, false);
                                    }
                                }

                                if (match_data.requesting_matches.length == 0) {
                                    $(".no-received", wrapper).text("No received request found");
                                }

                                if (match_data.history_matches.length) {
                                    var history_wrapper = $(wrapper_tpl);
                                    for (var match of match_data.history_matches) {
                                        CloudFaces.SandBoxx.Matches.Main.appendMatch($('.list-past', wrapper), item_tpl, match, unclickable, false);
                                    }
                                }

                                if (match_data.history_matches.length == 0) {
                                    $(".no-past", wrapper).text("No past request found");
                                }
                                resolve();
                            }
                        })
                    })
                });
            },
            handleTextBoxSearch: function (searchBox) {
                var search = $(searchBox).val();
                if (search) {

                    // Sponsoree
                    var totalSponsoreeCount = $('ul.list-sponsorees li').length;
                    var totalSponsoreeMatches = $('ul.list-matchedsponsorees li').length;
                    var totalSponsoreePending = $('ul.list-paendingrequestsponsorees li').length;
                    var totalSponsoreeNoActivity = $('ul.list-noactivitysponsorees li').length;

                    // Sponsor
                    var totalSponsorCount =  $('ul.list-sponsors li').length;
                    var totalSponsorMatches  =  $('ul.list-matchedsponsors li').length;
                    var totalSponsorPending = $('ul.list-pendingrequestsponsors li').length;
                    var totalSponsorNoActivity  = $('ul.list-noactivitysponsors li').length;

                    var responserArray;
                    if (CloudFaces.SandBoxx.Matches.match_data) {
                        var responserArray = CloudFaces.SandBoxx.Matches.match_data.users_list.filter(function (userDetail) {
                            if (userDetail.first_name) {
                                return userDetail.first_name.toLowerCase().includes(search.toLowerCase())
                            }
                            else if (userDetail.last_name) {
                                return userDetail.last_name.toLowerCase().includes(search.toLowerCase())
                            }
                            else if (userDetail.email) {
                                return userDetail.email.toLowerCase().includes(search.toLowerCase())
                            }
                        });
                        var isSponsoreeTabSelected = $("#sponsorees-tab").hasClass('active');
                        if (isSponsoreeTabSelected) {
                            CloudFaces.SandBoxx.Matches.isSponsoreeSelected = true;
                        } else {
                            CloudFaces.SandBoxx.Matches.isSponsoreeSelected = false;
                        }

                        $('.matches-item').each(function(itm){
                            var user_name = $(this).data('user_name');
                            if(user_name.toLowerCase().includes(search.toLowerCase())){
                                $(this).show();
                                $(this).removeClass('hide_users');
                            }else{
                                $(this).hide();
                                $(this).addClass('hide_users');
                            }
                        });

                        // Sponsoree
                        var totalSponsoreeSearchCount = $('ul.list-sponsorees li.hide_users').length;
                        var totalSponsoreeSearchMatches = $('ul.list-matchedsponsorees li.hide_users').length;
                        var totalSponsoreeSearchPending = $('ul.list-paendingrequestsponsorees li.hide_users').length;
                        var totalSponsoreeSearchNoActivity = $('ul.list-noactivitysponsorees li.hide_users').length;

                        // Sponsor
                        var totalSponsorSearchCount =  $('ul.list-sponsors li.hide_users').length;
                        var totalSponsorSearchMatches  =  $('ul.list-matchedsponsors li.hide_users').length;
                        var totalSponsorSearchPending = $('ul.list-pendingrequestsponsors li.hide_users').length;
                        var totalSponsorSearchNoActivity  = $('ul.list-noactivitysponsors li.hide_users').length;

                        //// When No Data Found for Sponsoree
                        if(totalSponsoreeSearchCount == totalSponsoreeCount){
                            $('.no-sponsorees').text('No sponsoree found');
                        }else{
                            $('.no-sponsorees').text('');
                        }

                        if(totalSponsoreeSearchMatches == totalSponsoreeMatches){
                            $('.no-matchedsponsorees').text('No Match Request found');
                        }else{
                            $('.no-matchedsponsorees').text('');
                        }

                        if(totalSponsoreeSearchPending == totalSponsoreePending){
                            $('.no-paendingrequestsponsorees').text('No Pending Request found');
                        }else{
                            $('.no-paendingrequestsponsorees').text('');
                        }

                        if(totalSponsoreeSearchNoActivity == totalSponsoreeNoActivity){
                            $('.no-noactivitysponsorees').text('No Activity Request found');
                        }else{
                            $('.no-noactivitysponsorees').text('');
                        }


                        //// When No Data Found for Sponsor
                        if (totalSponsorSearchCount == totalSponsorCount) {
                            $('.no-sponsors').text('No sponsor found');
                        } else {
                            $('.no-sponsors').text('');
                        }

                        if (totalSponsorSearchMatches == totalSponsorMatches) {
                            $('.no-matchedsponsors').text('No Match Request found');
                        } else {
                            $('.no-matchedsponsors').text('');
                        }

                        if (totalSponsorSearchPending == totalSponsorPending) {
                            $('.no-pendingrequestsponsors').text('No Pending Request found');
                        } else {
                            $('.no-pendingrequestsponsors').text('');
                        }

                        if (totalSponsorSearchNoActivity == totalSponsorNoActivity) {
                            $('.no-noactivitysponsors').text('No Activity Request found');
                        } else {
                            $('.no-noactivitysponsors').text('');
                        }
                    }
                } else {
                    var isSponsoreeTabSelected = $("#sponsorees-tab").hasClass('active');
                    if (isSponsoreeTabSelected) {
                        CloudFaces.SandBoxx.Matches.isSponsoreeSelected = true;
                    } else {
                        CloudFaces.SandBoxx.Matches.isSponsoreeSelected = false;
                    }

                    $('.matches-item').each(function(itm){
                        $(this).show();
                    })

                    $('.no-sponsorees').text('');
                    $('.no-matchedsponsorees').text('');
                    $('.no-paendingrequestsponsorees').text('');
                    $('.no-noactivitysponsorees').text('');

                    $('.no-sponsors').text('');
                    $('.no-matchedsponsors').text('');
                    $('.no-pendingrequestsponsors').text('');
                    $('.no-noactivitysponsors').text('');
                }
            },
            appendMatch: function (wrapper, item_tpl, match, unclickable, requested) {

                var item_wrapper = $(item_tpl);

                var image = match.image ? CloudFaces.API.url(match.image) : 'images/default-user.svg';
                var thumb = match.image ? CloudFaces.Helpers.createThumb(image, 150, 150) : 'images/default-user.svg';
                var img = thumb;

                //if there is avatar image, display image
                // otherways display icon

                if (image) {
                    $('.avatar', item_wrapper).css('display', 'block');
                    $('.avatar', item_wrapper).siblings().css('display', 'none');
                } else {
                    $('.avatar', item_wrapper).css('display', 'none');
                    $('.avatar', item_wrapper).siblings().css('display', 'block');
                }

                if (!match.image) {
                    $('.avatar', item_wrapper).css('padding', '8px');
                }


                var name = match.email;
                var filter_username = '';
                if (match.first_name && match.last_name) {
                    name = match.first_name + ' ' + match.last_name.charAt(0) + '.';
                    filter_username = match.first_name.toLowerCase() + ' ' + match.last_name.toLowerCase();
                }

                filter_username = filter_username.toLowerCase() + ' ' + match.email.toLowerCase();

                var title = match.title ? match.title : '';
                $('.match-title', item_wrapper).text(title);

                if (requested) {
                    var request_text = CloudFaces.SandBoxx.Matches.type == "admin" ? CloudFaces.Language.translate('unmatch_request_sent') : CloudFaces.Language.translate('match_request_sent');
                    $('.match-request', item_wrapper).text(CloudFaces.Language.translate(request_text));
                } else if (match.matched_status == 'request') {
                    $('.match-request', item_wrapper).text("Pending Match Requests");
                    item_wrapper.addClass('sent-request');
                }else if(match.matched_status == "active"){
                    $('.match-request', item_wrapper).text("Matched");
                    item_wrapper.addClass('sent-request');
                }else if(match.matched_status == "unmatch"){
                    var request_text = CloudFaces.SandBoxx.Matches.type == "admin" ? CloudFaces.Language.translate('unmatch_request_sent') : CloudFaces.Language.translate('match_request_sent');
                    $('.match-request', item_wrapper).text(CloudFaces.Language.translate(request_text));
                    item_wrapper.addClass('sent-request');
                } else {
                    var location = match.location ? match.location : '';
                    $('.match-location', item_wrapper).text(location);
                }


                var style = requested ? 'sent-request' : '';
                item_wrapper.addClass(style);
                if (unclickable) {
                    item_wrapper.addClass('disabled')
                }
                var userDetail = CloudFaces.Helpers.getUserData();
                if (userDetail && match && match.match_status && (match.match_status == 'declined' || match.match_status == 'active')) {
                    item_wrapper.addClass('disabled');
                    $('.match-request', item_wrapper).text("Unvailable");
                }

                item_wrapper.data('id', match.id).data('percent', match.percent);
                item_wrapper.attr('data-user_name',filter_username);

                $('.avatar', item_wrapper).attr('src', img);
                $('.matches-item_title', item_wrapper).text(name);


                if (CloudFaces.SandBoxx.Matches.type == "admin") {
                    $('.match-percent', item_wrapper).parent().css('display', 'none');
                    $('.wrap-matches_item_details', item_wrapper).removeClass('col-6').addClass('col-7');
                }
                else {
                    $('.match-percent', item_wrapper).text(match.percent + '%');
                }

                wrapper.append(item_wrapper);

            },
            renderProfileWarning: function (status, message) {
                // console.log(status, message)
                return new Promise(function (resolve, reject) {
                    var incoplete_profile_tpl = 'templates/wrapper_incomplete_profile.html';
                    CloudFaces.SandBoxx.Helpers.loadTpl(incoplete_profile_tpl).then(function (wrapper_tpl) {
                        var container = $(CloudFaces.SandBoxx.Matches.container);
                        var wrapper = $(wrapper_tpl);

                        if (message == 'Invalid credentials') {
                            $('.incomplete-profile', wrapper).text(CloudFaces.Language.translate('logged_expired'));
                        }
                        if (message == 'Questionnaire is not completed' || message == 'Missing User data') {
                            $('.incomplete-profile', wrapper).text(CloudFaces.Language.translate('incomplete_profile'));
                        }
                        if (message == 'Selected cohort is not started yet') {
                            $('.incomplete-profile', wrapper).text(CloudFaces.Language.translate('cohort_not_started'));
                        }
                        if (message == 'Selected cohort is expired') {
                            $('.incomplete-profile', wrapper).text(CloudFaces.Language.translate('cohort_expired'));
                        }
                        //Selected cohort is not started yet
                        container.append(wrapper);
                        CloudFaces.Helpers.hideLoader();
                        resolve();
                    })
                })
            },

            refreshPage: function (container) {
                var header = CloudFaces.SandBoxx.Helpers.jsonParse(CFNavigation.getNavigationContext())

                function isEmpty(obj) {
                    return !obj || Object.keys(obj).length === 0;
                }

                if (isEmpty(header)) {
                    header = CFPersistentStorage.readValue('profile_header_update');
                }

                if ((typeof header != 'undefined' && header == '1') || !$('body').hasClass('ios')) {
                    // var userData = CloudFaces.Helpers.getUserData();
                    // var title = userData.category_id == CloudFaces.SandBoxx.Config.constants.role_admin ? 'employees' : 'my matches';
                    // CFHeader.updateHeader(title);
                }

                CloudFaces.SandBoxx.Matches.Main.clearPageRefreshInterval();

                if (CloudFaces.SandBoxx.Helpers.matchesShouldRefresh()) {
                    CloudFaces.SandBoxx.Matches.Main.clearPage(container);
                    CloudFaces.SandBoxx.Matches.Main.init(container);
                }
                else {
                    if (CloudFaces.SandBoxx.Helpers.isPreviousPageSlideout()) {
                        CloudFaces.SandBoxx.Matches.Main.clearPage(container);
                    }
                }
            },


            handleMatchClick: function (match) {
                CloudFaces.SandBoxx.Helpers.disableButtons();
                var match_e = $(match);
                var match_id = match_e.data('id');
                if (CloudFaces.SandBoxx.Matches.type == 'admin') {
                    CloudFaces.SandBoxx.Matches.Navigation.navigateToAdminMatchProfile(match_id);
                }
                else {
                    var percent = match_e.data('percent');
                    CloudFaces.SandBoxx.Matches.Navigation.navigateToMatchProfile(match_id, percent);
                }
            },
            clearPageRefreshInterval: function () {
                clearInterval(CloudFaces.SandBoxx.Matches.refresh_interval);
            },
            startPageRefreshInterval: function () {
                CloudFaces.SandBoxx.Matches.refresh_interval =
                    setInterval(function () {
                        CloudFaces.SandBoxx.Helpers.enableMatchesRefreshInterval();
                        CloudFaces.SandBoxx.Matches.Main.refreshPage('#matches-list-wrapper');
                        CloudFaces.SandBoxx.Matches.Main.clearPageRefreshInterval();
                    }, CloudFaces.SandBoxx.Config.constants.refresh_interval);
            },
            clearPage: function (container) {
                CloudFaces.SandBoxx.Helpers.disableMatchesRefreshInterval();
                $(container).empty();
                $(document)
                    .off('click', '.match-wrapper')
                    .off('click', '.create-acc-btn');
            }

        },

        Navigation: {
            navigateToMatchProfile: function (user_id, percent) {
                var store_data = {
                    type: "view",
                    user_id: user_id,
                    percent: percent
                }
                CFPersistentStorage.writeValue('profile_info', JSON.stringify(store_data));
                CFNavigation.navigate(CloudFaces.SandBoxx.Config.pages.match_profile, '');
            },
            navigateToAdminMatchProfile: function (user_id) {
                var store_data = {
                    type: "admin_edit",
                    user_id: user_id
                };
                CFPersistentStorage.writeValue('profile_info', JSON.stringify(store_data));
                CFNavigation.navigate(CloudFaces.SandBoxx.Config.pages.match_profile, '');
            },
            navigateToRegister: function () {
                CloudFaces.SandBoxx.Helpers.disableButtons();
                CFPersistentStorage.writeValue('register_info', JSON.stringify({ type: 'admin' }));
                CFNavigation.navigate(CloudFaces.SandBoxx.Config.pages.register, '');
            }
        },
        Events: function () {

            return new Promise(function (resolve, reject) {
                $(document)
                    .on('click', '.match-wrapper', function () {
                        CloudFaces.SandBoxx.Matches.Main.handleMatchClick(this);
                    })
                    .on('click', '.create-acc-btn', function () {
                        var selectedTabRoleId = CloudFaces.Helpers.getSelectedTabRole();
                        CFPersistentStorage.writeValue('selectedTabRoleId', JSON.stringify({ role_id: selectedTabRoleId }));
                        CloudFaces.SandBoxx.Matches.Navigation.navigateToRegister();
                    }).on('keyup', '.search-box', function (e) {
                    if (e.keyCode != 37 && e.keyCode != 38 && e.keyCode != 39 && e.keyCode != 40) {
                        CloudFaces.SandBoxx.Matches.Main.handleTextBoxSearch(this);
                    }

                });
                CloudFaces.Helpers.hideLoader();
                resolve();
            });
        }
    }
});