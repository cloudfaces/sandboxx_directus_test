$.extend(CloudFaces.SandBoxx, {
    DataProxy: {

        multiPut: function (project_id, list_id, data, callback) {
            var url = 'import'
            var data = {
                app: project_id,
                list: list_id,
                token: CloudFaces.SandBoxx.Config.constants.application_token,
                data: JSON.stringify(data)
            }
            CloudFaces.API.query(url, 'post', data, callback);
        },
        getMatches: function (user_id, cohort_id, role_id) {
            var params = {
                'user_id' : user_id,
                'cohort_id' : cohort_id,
                'role_id' : role_id,
            }
            return new Promise(function (resolve, reject) {
                CloudFaces.API.query_two_other_api(
                    CloudFaces.SandBoxx.Config.constants.matches_api_url,
                    "get",
                    params,
                    function (matches_response) {
                        resolve(CloudFaces.SandBoxx.Helpers.jsonParse(matches_response));
                    });
            });
        },
        getMutuals: function (user_id, suggested_users,cohort_id) {
            var params = {
                'user_id' : user_id,
                'suggested_users' : suggested_users,
                'cohort_id':cohort_id
            }
            return new Promise(function (resolve, reject) {
                CloudFaces.API.query_two_other_api(
                    CloudFaces.SandBoxx.Config.constants.mutual_api_url,
                    "get",
                    params,
                    function (matches_response) {
                        resolve(CloudFaces.SandBoxx.Helpers.jsonParse(matches_response));
                    });
            });
        },
        loadQuestion: function () {
            return new Promise(function (resolve, reject) {
                DirectusClient.getItem('questions', CloudFaces.SandBoxx.Questionnaire.q_id, {fields: "*,answers.answers_id.*,related_questions.related_question_id.*"}).then(function (question_data) {
                    // console.log('question data from loadQuestion' + JSON.stringify(question_data.data));
                    resolve(question_data.data);
                });
            });
        },
        loadQuestionAnswers: function (q_id, user_id) {
            return new Promise(function (resolve, reject) {
                var u_answer_params = {
                    filter: {
                        user: {
                            eq: user_id
                        },
                        question: {
                            eq: q_id
                        },
                    }
                };
                DirectusClient.getItems('user_answers', u_answer_params).then(function (udata) {
                    // console.log('user answers data from loadQuestionAnswers '+ JSON.stringify(udata) );
                    resolve(udata.data);
                });
            });
        },
        loadQuestionIds: function () {
            // Load non profile questions
            return new Promise(function (resolve, reject) {
                DirectusClient.getItems('questions', {
                    filter: {
                        type: {
                            neq: 'profile'
                        },
                    },
                    fields: "*.*"
                }).then(function (questions) {
                    resolve(questions.data);
                });
            });
        },
        loadTemplates: function (user_id) {
            return new Promise(function (resolve, reject) {
                DirectusClient.getItems('templates').then(function (templates) {
                    DirectusClient.getItem('directus_users', user_id).then(function (u_data) {
                        templates.data.map(function (el) {
                            el.value = u_data.data[el.name_of_field] ? u_data.data[el.name_of_field] : '';
                        });
                        resolve(templates);
                    });
                });
            });
        },
        loadQuestionnaire: function (user_id, type) {
            var q_params = {
                fields: "*.*,answers.answers_id.*.*"
            };
            if (type == "create") {
                q_params.filter = {
                    type: {
                        eq: 'profile'
                    }
                };
            } else if (type == "view") {
                q_params.filter = {
                    is_public: {
                        eq: '1'
                    }
                };
            }
            return new Promise(function (resolve, reject) {
                DirectusClient.getItems('user_answers', {
                    filter: {
                        user: {
                            eq: user_id
                        },
                    },
                    fields: "*.*"
                }).then(function (user_answers) {
                    DirectusClient.getItems('questions', q_params,).then(function (question_data) {
                        resolve({questions: question_data.data, answers: user_answers.data})
                    });
                });
            });
        },
        saveUserRole: function (user_id, role_id) {
            return new Promise(function (resolve, reject) {
                DirectusClient.updateItem('directus_users', user_id, {id: user_id, role_id: role_id}).then(resolve);
            });
        },
        saveUserData: function (user_data) {
            // console.log('saveuserdata userdata '+ JSON.stringify(user_data));
            if ( ! user_data.hasOwnProperty('location') && user_data != null && typeof user_data[Symbol.iterator] === 'function') {
                var userData = {};
                for (var data of user_data) {
                    var key = data.name;
                    userData[key] = data.value;
                }
            } else {
                var userData = user_data;
            }

            return new Promise(function (resolve, reject) {
                DirectusClient.updateItem('directus_users', userData.id, userData).then(resolve);
            });
        },
        getUserData: function (user_id) {
            return new Promise(function (resolve, reject) {
                var params = {
                    fields: "*,avatar.*"
                };
                DirectusClient.getItem('directus_users', user_id, params).then(function (user) {
                    resolve(user.data);
                });
            });
        },
        saveQuestionAnswers: function (q_id, user_role, user_id, user_role, answers) {
            return new Promise(function (resolve, reject) {
                var u_answer_params = {
                    filter: {
                        user: {
                            eq: user_id
                        },
                        question: {
                            eq: q_id
                        },
                    }
                };
                DirectusClient.getItems('user_answers', u_answer_params).then(function (udata) {
                    if (typeof udata.data[0] != "undefined") {
                        for (var u_answer of udata.data) {
                            CloudFaces.Directus.removeItem('user_answers', u_answer.id, function (response) {
                                // console.log("Previous Ans Delete Response : " + JSON.stringify(response));
                            });
                        }
                    }
                    // Insert the selected answers
                    for (var answer of answers) {
                        var ans_id = ( answer.name == 'answer_text' ) ? 0 : answer.value;
                        var data = {
                            question: q_id,
                            user_role: user_role,
                            answer: ans_id,
                            user: user_id,
                            answer_text: answer.value,
                        };
                        DirectusClient.createItem('user_answers', data).then(function () {
                            if (q_id == CloudFaces.SandBoxx.Config.constants.location_question_id && answers[0].a_text) {
                                CloudFaces.SandBoxx.DataProxy.saveUserData({
                                    id: user_id,
                                    location: answers[0].a_text
                                }).then(resolve);
                            } else {
                                resolve();
                            }
                        });
                    }

                });
            });
        },

        createUserAnswers: function (questions, userRole, userId, resolve) {
            var url = CloudFaces.Directus.Config.url;
            var projectId = CloudFaces.Directus.Config.project;

            if (questions && questions.length > 0) {
                var question = questions[0];
                var objectToPush = {
                    question: question.q_id,
                    related_question: question.rel_q_id,
                    user_role: userRole,
                    answer: question.a_id,
                    user: userId,
                    answer_text: question.a_id,
                };

                var directus_token = CloudFaces.Directus.getDirectusUser().token.token;
                var ajaxParams = {
                    url: url + '/' + projectId + '/items/user_answers',
                    type: 'post',
                    data: objectToPush,
                    cache: false,
                    headers: {"Authorization": 'Bearer ' + directus_token},
                    success: function (data) {
                        //console.log('data from success ' + JSON.stringify(data));
                        if (question.q_id == CloudFaces.SandBoxx.Config.constants.location_question_id) {
                            CloudFaces.SandBoxx.DataProxy.saveUserData({
                                id: userId,
                                location: question.a_text
                            }).then(resolve);
                        } else {
                            resolve();
                        }
                    },
                    error: function (error) {
                        console.log('data from error ' + JSON.stringify(error));
                    }
                };

                $.ajax(ajaxParams);
                questions.shift();
                CloudFaces.SandBoxx.DataProxy.createUserAnswers(questions, userRole, userId, resolve)
            }
        },

        saveProfileQuestions: function (user_id, user_role, question_data, profile_type = null) {
            //console.log(question_data);
            return new Promise(function (resolve, reject) {
                if (profile_type !== 'create') {
                    var question_ids = [];
                    var u_answers_ids = [];
                    for (var i = 0; i < question_data.length; i++) {
                        question_ids.push(question_data[i].q_id);
                    }
                    // Get existing user answers by looping from question_id
                    for (var question_id of question_ids) {
                        var u_answer_params = {
                            filter: {
                                user: {
                                    eq: user_id
                                },
                                question: {
                                    eq: question_id
                                },
                            }
                        };

                        DirectusClient.getItems('user_answers', u_answer_params).then(function (udata) {
                            if (typeof udata.data[0] != "undefined") {
                                for (var udataId of udata.data) {
                                    u_answers_ids.push(udataId.id);
                                }
                            }
                        });
                    }

                    if (typeof u_answers_ids !== 'undefined' && u_answers_ids.length > 0) {
                        for (u_answers_id of u_answers_ids) {
                            CloudFaces.Directus.removeItem('user_answers', u_answers_id, function (response) {
                                // console.log("Previous Ansss Deleted: " + response);
                            });
                        }
                    }
                }

                CloudFaces.SandBoxx.DataProxy.createUserAnswers(question_data, user_role, user_id, resolve);
            });
        },
        saveProfileImage: function (imgUrl, key, id, filename) {
            return new Promise(function (resolve, reject) {
                var appUser = CloudFaces.Directus.getDirectusUser();
                var url = CloudFaces.Directus.Config.url;
                var projectId = CloudFaces.Directus.Config.project;
                var guidGenerator = CloudFaces.Directus;
                var directus_token = CloudFaces.Directus.getDirectusUser().token.token;
                var filename_disk = filename.replace(/^.*[\\\/]/, '');
                var obj = {
                    filename_download: filename,
                    filename_disk: filename_disk,
                    data: imgUrl
                };

                var ajaxParams = {
                    url: url + '/' + projectId + '/files',
                    type: 'post',
                    data: obj,
                    enctype: 'multipart/form-data',
                    cache: false,
                    headers: {"Authorization": 'Bearer ' + directus_token},
                    success: function (data) {
                        var appUser = CloudFaces.Directus.getDirectusUser();
                        if (appUser && typeof data.data !== 'undefined') {
                            var updateAvatar = {
                                avatar: data.data.id
                            }

                            DirectusClient.updateItem('directus_users', appUser.id, updateAvatar).then(function (response) {
                                if (response.error) {
                                    reject();
                                } else {
                                    var currentUserData = Object.assign(CloudFaces.Directus.getDirectusUser(), updateAvatar);
                                    CloudFaces.Directus.setDirectusUser(currentUserData);
                                    resolve();
                                }
                            }).catch(function (error) {
                                reject();
                            });
                        }

                        resolve();
                    },
                    error: function (error) {
                        reject()
                    }
                };

                $.ajax(ajaxParams);
            });
        },
        getUserRequests: function (user_1_id, user_2_id, cohort_id) {
            return new Promise(function (resolve, reject) {
                DirectusClient.getItems('matches', {
                    filter: {
                        cohort_id:cohort_id
                    },
                    fields: "*.*"
                }).then(function (response) {
                    var mathes = [];
                    if(response && response.data){
                        mathes = response.data.filter(function(user){
                            return ((user.sponsor_id == user_1_id && user.sponsoree_id == user_2_id) || (user.sponsoree_id == user_1_id && user.sponsor_id == user_2_id)) && user.status != 'history'
                        })
                    }
                    
                    resolve(mathes);
                })
            })
        },
        getAllMatchesByCohort: function (cohort_id) {
            return new Promise(function (resolve, reject) {
                DirectusClient.getItems('matches', {
                    filter: {
                        cohort_id:cohort_id
                    },
                    fields: "*.*"
                }).then(function (matches) {
                    resolve(matches);
                })
            })
        },
        getAllUsersListByRole: function (cohort_id,role_id) {
                    return new Promise(function (resolve, reject) {
                        var user_params = {
                            filter: {
                                "cohorts_id.id": {
                                    eq: cohort_id
                                },
                                "directus_users_id.role_id":{
                                    eq: role_id
                                }
                            },
                            fields: "directus_users_id.*"
                        };

                        DirectusClient.getItems('cohorts_directus_users', user_params).then(function (userData) {
                            userData = ( typeof userData.data !== 'undefined' ) ? userData.data : {};
                            resolve(userData);
                        });
                    });
                },
        getUsersList: function (cohort_id) {
            return new Promise(function (resolve, reject) {
                var user_params = {
                    filter: {
                        "cohorts_id.id": {
                            eq: cohort_id
                        },
                    },
                    fields: "directus_users_id.*"
                };

                DirectusClient.getItems('cohorts_directus_users', user_params).then(function (userData) {
                    userData = ( typeof userData.data !== 'undefined' ) ? userData.data : {};
                    resolve(userData);
                });
            });
        },

        getAllRequests: function (cohort_id) {
            return new Promise(function (resolve, reject) {
                 // Check if cohort is expired
                 DirectusClient.getItem('cohorts', cohort_id).then( function(cohortData) {
                    var cohortData = cohortData.data;
                    var today      = new Date();
                    var end_date   = new Date(cohortData.end_date);
                     if ( today > end_date ) {
                         resolve({"Status": "Error", "ErrorMessage" : "Selected cohort is expired"});
                     } else {
                        var match_params = {
                            filter: {
                                cohort_id: {
                                    eq: cohort_id
                                }
                            }
                        };

                        DirectusClient.getItems('matches', match_params).then(function (matchData) {
                            matchData = ( typeof matchData.data !== 'undefined' ) ? matchData.data : {};
                            resolve(matchData);
                        });
                     }
                } );
            });
        },

        getUnmatchRequests: function (cohort_id) {
            return new Promise(function (resolve, reject) {
                var match_params = {
                    filter: {
                        status: {
                            eq: 'unmatching'
                        },
                        cohort_id: {
                            eq: cohort_id
                        }
                    }
                };

                DirectusClient.getItems('matches', match_params).then(function (matchData) {
                    matchData = ( typeof matchData.data !== 'undefined' ) ? matchData.data : {};
                    resolve(matchData);
                });
            });
        },
        sendPush: function (ain, timezone, text) {
            return new Promise(function (resolve, reject) {
                if (!ain) {
                    resolve();
                } else {
                    timezone = timezone ? timezone : (-1 * (new Date().getTimezoneOffset())) / 60;
                    var data = {
                        project_hash: CloudFaces.Config.AppHash,
                        from_ain: ain,
                        to_ain: ain,
                        notification_text: text,
                        time_zone: timezone
                    }
                    // var data={
                    //     project_hash: 'b4aa501285453b602331805f56c41681f4c8a1a16c7bc69a080baf2895b5b882',
                    //     from_ain: '595351',
                    //     to_ain: '595351',
                    //     notification_text: text,
                    //     time_zone: timezone
                    // }                
                    CloudFaces.API.registerPush.setNotification(CloudFaces.Config.app, data, resolve);
                }
            })
        },
        handleLogout: function () {
            CloudFaces.SandBoxx.Helpers.disableButtons();
            CFPersistentStorage.writeValue(CloudFaces.Config.app + '_userData', '');
            CFPersistentStorage.writeValue('cf_directus_' + CloudFaces.Directus.Config.project + '_user', '');
            CFVariableStorage.writeValue('profile_refresh', 'true');
            CFVariableStorage.writeValue('app_started', 'true');
            CFPersistentStorage.writeValue('selected_cohort', '');
            CloudFaces.Directus.directusLogoutUser(); //Logout from Directus
            CFMenuNavigation.navigate(CloudFaces.SandBoxx.Config.pages.login, '');
        },
    }
});