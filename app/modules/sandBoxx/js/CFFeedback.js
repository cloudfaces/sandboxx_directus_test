$.extend(CloudFaces.SandBoxx, {
    Feedback: {

        container: '',
        wrapper: '',

        Main: {
            init: function (container) {
                CloudFaces.SandBoxx.Feedback.Main.preparePage(container)
                    .then(CloudFaces.SandBoxx.Feedback.Main.renderFeedback)
                    .then(CloudFaces.SandBoxx.Feedback.Events);
            },
            preparePage: function (container) {
                return new Promise(function (resolve, reject) {
                    CloudFaces.SandBoxx.Helpers.registerPageAnalytics();
                    CloudFaces.SandBoxx.Feedback.container = container;
                    resolve();
                });
            },
            renderFeedback: function (terms_data) {
                return new Promise(function (resolve, reject) {
                    var tpl_path = 'templates/feedback.html'; 
                    CloudFaces.SandBoxx.Helpers.loadTpl(tpl_path).then(function (item_tpl) {
                        var item = $(item_tpl);
                        $('.thanks-wrapper', item).hide();
                        $('.submit-btn', item).text(CloudFaces.Language.translate('submit'));
                        $('#feedbackTextArea', item).attr('placeholder', CloudFaces.Language.translate('submit_placeholder'));
                        $('.thanks-title', item).text(CloudFaces.Language.translate('thanks_title'));
                        $('.thanks-text', item).text(CloudFaces.Language.translate('thanks_text'));                        
                        $(CloudFaces.SandBoxx.Feedback.container).append(item);
                        resolve();
                    })
                }); 
            },
            handleTextBox: function (input) {
                if ($(input).val().length) {
                    $('.submit-btn').removeClass('disabled-btn');
                }
                else {
                    $('.submit-btn').addClass('disabled-btn');
                }
            },
            handleSubmit: function(){
                var email = CloudFaces.Directus.getDirectusUser().email;
                var text = $('#feedbackTextArea').val();
                CloudFaces.Directus.sendFeedback(email, text).then(CloudFaces.SandBoxx.Feedback.Main.showSuccess)
            },
            showSuccess: function(){
                $('.thanks-wrapper').show()
                $('.feedback-wrapper').hide()
            },
        },

        Navigation: {
        },

        Events: function () {
            return new Promise(function (resolve, reject) {
                $(document)
                    .on('input', '.required-text', function () {
                        CloudFaces.SandBoxx.Feedback.Main.handleTextBox(this);
                    })
                    .on('click', '.submit-btn', function () {
                        CloudFaces.SandBoxx.Feedback.Main.handleSubmit(this);
                    })
                resolve();
            });
        }

    }
});