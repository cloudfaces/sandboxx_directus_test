$.extend(CloudFaces.SandBoxx, {
    Cohort: {

        container: '',
        wrapper: '',

        Main: {
            init: function (container) {
                CloudFaces.SandBoxx.Cohort.Main.preparePage(container)
                    .then(CloudFaces.SandBoxx.Cohort.Main.prepareCohorts)
                    .then(CloudFaces.SandBoxx.Cohort.Main.renderCohorts)
                    .then(CloudFaces.SandBoxx.Cohort.Events);
            },
            preparePage: function (container) {
                return new Promise(function (resolve, reject) {
                    CloudFaces.SandBoxx.Helpers.registerPageAnalytics();
                    CloudFaces.SandBoxx.Cohort.container = container;
                    var current_cohort = CFPersistentStorage.readValue("selected_cohort");
                    if ( current_cohort ) {
                        // redirect to cohort page
                        CFPersistentStorage.writeValue("after_cohort", JSON.stringify({page: 'matches'}));
                        CFNavigation.navigateToRoot(CloudFaces.SandBoxx.Config.pages.cohort_select, '')
                    } else {
                        resolve();
                    }
                });
            },
            prepareCohorts: function () {
                return new Promise(function (resolve, reject) {
                    var userData = CloudFaces.Helpers.getUserData();
                    CloudFaces.Directus.getCohorts(userData.id).then(resolve);
                });
            },
            renderCohorts: function (cohort_data) {
                var cohort_data = cohort_data.data ? cohort_data.data : [];
                if ( cohort_data.length == 1 ) {
                    var selcted_cohort = cohort_data[0].id;
                    CFPersistentStorage.writeValue("selected_cohort", selcted_cohort);
                    CFPersistentStorage.writeValue("after_cohort", '');
                    CFMenuNavigation.navigate(CloudFaces.SandBoxx.Config.pages.matches, '');
                }
                return new Promise(function (resolve, reject) {
                    var tpl_path = 'templates/wrapper_cohort_select.html';
                    CloudFaces.SandBoxx.Helpers.loadTpl(tpl_path).then(function (item_tpl) {
                        var wrapper = $(item_tpl);
                        $('.title-primary', wrapper).html('Select a Program');
                        // Append cohort options
                        CloudFaces.SandBoxx.Cohort.Main.appendCohorts(wrapper, cohort_data);
                        $(CloudFaces.SandBoxx.Cohort.container).append(wrapper);
                        resolve();
                    })
                }); 
            },
            appendCohorts: function (cohort_tpl, cohorts) {
                var action_btn = $('.cohort-action-btn', cohort_tpl);

                if ( cohorts.length == 0 ) {
                    var select_wrap = $('.select-wrap', cohort_tpl);
                    select_wrap.empty();
                    select_wrap.append('<h4>You have not been assigned to a cohort yet. Please contact your administrator.</h4>')
                    select_wrap.removeClass('select-wrap');

                    action_btn.addClass('cohort-logout');
                    action_btn.removeClass('disabled-btn');  
                    action_btn.text('Logout');
                } else {
                    var select = $('#cohortSelect', cohort_tpl);
                    select.append('<option value="" selected>Select a Program</option>')
                    for (var cohort of cohorts) {
                        cohort_id = cohort.id;
                        cohort_name = cohort.name;
                        select.append('<option value="' + cohort_id + '">' + CloudFaces.SandBoxx.Helpers.htmlDecode(cohort_name) + '</option>')
                    }
                    action_btn.addClass('submit-cohort');
                    action_btn.text('Next');
                }
            },
            handleSubmit: function(btn){
                var selcted_cohort = $( "#cohortSelect option:selected" ).val();
                // Store the current cohort and redirect to matches
                CFPersistentStorage.writeValue("after_cohort", '');
                CFPersistentStorage.writeValue("selected_cohort", selcted_cohort);
                CFMenuNavigation.navigate(CloudFaces.SandBoxx.Config.pages.matches, '1');
            },
        },

        Events: function () {
            return new Promise(function (resolve, reject) {
                $(document)
                    .on('change', '#cohortSelect', function (){
                        if( $(this).val() == '' ){
                            $('.submit-cohort').addClass('disabled-btn');
                        }else{
                            $('.submit-cohort').removeClass('disabled-btn');  
                        }
                    })
                    .on('click', '.submit-cohort', function () {
                        CloudFaces.SandBoxx.Cohort.Main.handleSubmit(this);
                    })
                    .on('click', '.cohort-logout', function () {
                        CloudFaces.SandBoxx.DataProxy.handleLogout();
                    })
                resolve();
            });
        }

    }
});