<?php 

define("CONFIG_API_URL", 				"https://api.cloudfaces.com/v1/");
define("CONFIG_PROJECT_ID", 			"3159");
define("CONFIG_ANALYTICS_SQL", 			"2067");
define("CONFIG_USERS_LIST_ID",			"20481");
define("CONFIG_REL_USER_COMPANY",		"313");

define("CONFIG_ADMIN_ROLE_ID", 			"2");
define("CONFIG_SUPER_ADMIN_ROLE_ID", 	"4");
define("CONFIG_SPONSOR_ROLE_ID", 		"1");
define("CONFIG_SPONSOREE_ROLE_ID", 		"2");

define("CONFIG_QUESTION_IDS", 			"1,2,3,4,5,6,7,8,9,10,11,12");

function curlRequest($route, $method="GET", $params=array(), $d=false) 
{
	$url = CONFIG_API_URL . $route;
	// var_dump($url); die();
	$ch = curl_init();

	if ($method=="GET") {
		$url =  $url."?".http_build_query($params);
	}
	
	curl_setopt($ch, CURLOPT_URL, $url);

	if ($method=="DELETE") {
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
		$params = http_build_query($params);
	}
	else if($method=="POST"){
		curl_setopt($ch, CURLOPT_POST, 1);	
	}
	else if ($method=="PUT") {
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
		$params = http_build_query($params);
	}
	else if ($method=="OPTIONS") {
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "OPTIONS");
		$params = http_build_query($params);
	}

	if ($d) {
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
	}

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

	if ($method!="GET" && (count($params) || strlen($params))) {
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
	}

	$output = curl_exec ($ch);
	curl_close ($ch);
	return json_decode($output, true)["Data"];
}

function array2csv(array &$array, $question_titles, $highest_match_user_id)
{
	if (count($array) == 0) {
	 return null;
	}

	ob_start();
	$df = fopen("php://output", 'w');
		
	$header_row = array_keys(reset($array));								//getting static user keys for the header row
	$header_row = array_slice($header_row, 0, count($header_row) - 2); 		//removing questionnaire and matches keys

	foreach ($question_titles as $question_title) {			
		$header_row []= $question_title; 									//adding question titles to the header row
	}

	foreach ($array[$highest_match_user_id]['matches'] as $match_data) {
		$header_row = array_merge($header_row, array_keys($match_data));	//adding match data titles as many times as the longest user row match length to ensure all columns have a header
	}

	fputcsv($df, $header_row);												

	foreach ($array as $row) {
			foreach ($row['questionnaire'] as $questions) {
			$answers = array();
			$question_answers = isset($questions['answers']) ? $questions['answers'] : array();
			foreach ($question_answers as $answer) {
				$answers[]=$answer['answer_text'];
			}
			$row[] = implode('+', $answers);								//multiple answers are joined with '+'
		}
		foreach($row['matches'] as $match){
			foreach($match as $detail){
				$row[]=$detail;
			}
		}
		unset($row['questionnaire']);									
		unset($row['matches']);				
	  fputcsv($df, $row);
	}
	fclose($df);
	return ob_get_clean();

}


function download_send_headers($filename) {
    // disable caching
    $now = gmdate("D, d M Y H:i:s");
    header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");

    // force download  
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");

    // disposition / encoding on response body
    header("Content-Disposition: attachment;filename={$filename}");
    header("Content-Transfer-Encoding: binary");
}


function outputFormat($result, $status="Success", $err="") {
	$resultArr = array();

	$resultArr['Status'] = $status;
	$resultArr['Data'] = $result;
	if (!empty($err)) {
		$resultArr['ErrorCode'] = $err;
		$resultArr['ErrorMessage'] = $err;		
	}
	return json_encode($resultArr);
}

// $st = microtime(true);

$user_token = isset($_GET["token"]) ? $_GET["token"] : '';

if (empty($user_token)) {
	echo outputFormat(array(), "Error", "Missing parameters: user_token");
	exit();
}

$params = [
	'filters' => [
		[
			'col_name' => 'token',
			'operator' => '=',
			'value'    => $user_token
		]
	],
	'returned_fields' => 'id,category_id',
	'fetch_relations' => 'true',
	'relation_filters' => [
		CONFIG_REL_USER_COMPANY =>[
			'returned_fields' => 'id'
		]
	]

];

$user_data = curlRequest("project/id/".CONFIG_PROJECT_ID."/lists/".CONFIG_USERS_LIST_ID."/items/", "GET", $params);

if (empty($user_data)) {
	echo outputFormat(array(), "Error", "Invalid credentials");
	exit();
}

$user_id = isset($user_data[0]["id"]) ? $user_data[0]["id"] : '';
$company_id = isset($user_data[0]["_rel"][CONFIG_REL_USER_COMPANY][0]["id"]) ? $user_data[0]["_rel"][CONFIG_REL_USER_COMPANY][0]["id"] : '';
$role_id = isset($user_data[0]["category_id"]) ? $user_data[0]["category_id"] : '';

if ($role_id != CONFIG_ADMIN_ROLE_ID && $role_id != CONFIG_SUPER_ADMIN_ROLE_ID) {
	echo outputFormat(array(), "Error", "You dont have permissions");
	exit();
}

$raw_data = curlRequest("project/id/".CONFIG_PROJECT_ID."/customsql/".CONFIG_ANALYTICS_SQL."/", "POST", json_encode([]));

$mapped_data = array();

$question_ids = explode(",", CONFIG_QUESTION_IDS);

$question_titles = array();

$highest_match_id = 0;
$highest_match_count = 0;
$current_match_count = 0;
$current_match_id = 0;


for ($i=0; $i<count($raw_data); $i++) {

	$data = $raw_data[$i];

	if($role_id == CONFIG_ADMIN_ROLE_ID && $data["company_id"] != $company_id){
		continue;
	}

	if(!isset($mapped_data[$data["id"]]) || $i==count($raw_data)-1){
		//collecting dynamic match data for the csv header row of the csv
		if ($current_match_count >= $highest_match_count) {
			$highest_match_id = $current_match_id;
			$highest_match_count = $current_match_count;
		}

		$current_match_id = $data["id"];
		$current_match_count = 0;

	}

	if(!isset($mapped_data[$data["id"]])){
		$mapped_data[$data["id"]] = array(
		      'id' 					=> $data["id"],
		      'email' 				=> $data["email"],
		      'company_id' 			=> $data["company_id"],
		      'company_name' 		=> $data["company_name"],
		      'company_domain' 		=> $data["company_domain"],
		      'company_added_on' 	=> $data["company_added_on"],
		      'first_name' 			=> $data["first_name"],
		      'last_name' 			=> $data["last_name"],
		      'role' 				=> $data["category_id"] == CONFIG_ADMIN_ROLE_ID ? 'admin' : ( $data["role_id"] == CONFIG_SPONSOR_ROLE_ID ? "sponsor" : "sponsoree"),
		      'registered_on' 		=> $data["registered_on"],
		      'image' 				=> !empty($data["image"]) ? CONFIG_API_URL . $data["image"] : '',
		      'title' 				=> $data["title"],
		      'linked_in' 			=> $data["linked_in"],
		      'location' 			=> $data["location"],
		      'questionnaire' 		=> array(),
		      'matches' 			=> array()	      
		);
		foreach ($question_ids as $question_id) {
			$mapped_data [$data["id"]] ['questionnaire'] [$question_id] = array();
		}
	}

	if(!empty($data["question_id"]) && empty( $mapped_data [$data["id"]] ['questionnaire'] [$data["question_id"]] )){
		$mapped_data [$data["id"]] ['questionnaire'] [$data["question_id"]] = array(
			// 'question_id' 			=> $data['question_id'],
			// 'question_text' 		=> $data['sponsor_question_text'],
			'answers' 				=> array()
		); 
		if(!isset($question_titles[$data["question_id"]])){
			$question_titles[$data["question_id"]] = $data['sponsor_question_text'];	//getting each unique question title needed for the header row of the csv
		}
	}

	if(!empty($data["answer_id"]) && empty( $mapped_data [$data["id"]] ['questionnaire'] [$data["question_id"]] ['answers'] [$data["answer_id"]] )){
		$mapped_data [$data["id"]] ['questionnaire'] [$data["question_id"]] ['answers'] [$data["answer_id"]] = array(
			// 'answer_id'				=> $data["answer_id"],
			'answer_text'			=> $data["closed_answer_text"]
		);
	}

	if (empty($data["answer_id"]) && !empty($data["open_answer_text"])) {
		$mapped_data [$data["id"]] ['questionnaire'] [$data["question_id"]] ['answers'] ['open'] = array(
			// 'answer_id'				=> "open",
			'answer_text'			=> $data["open_answer_text"]
		);
	}

	if(!empty($data["match_id"]) && !isset( $mapped_data [$data["id"]] ['matches'] [$data["match_id"]] )){
		$mapped_data [$data["id"]] ['matches'] [$data["match_id"]] = array(
			'match_id' 				=> $data['match_id'],
			'match_status' 			=> $data['match_status'],
			'match_sponsor_id' 		=> $data['match_sponsor_id'],
			'match_sponsoree_id' 	=> $data['match_sponsoree_id'],
			'sponsor_email'			=> $data['sponsor_email'],
			'sponsoree_email'		=> $data['sponsoree_email'],
			'matched_on' 			=> $data['matched_on'],
			'match_sender_id' 		=> $data['match_sender_id'],
			'unmatch_feedback' 		=> $data['unmatch_feedback']					
		); 
		$current_match_count++;
	}

}
download_send_headers("data_export_" . date("Y-m-d") . ".csv");
echo array2csv($mapped_data, $question_titles, $highest_match_id);
// $et = microtime(true);
// echo "\n" . "\n" . 'execution time: ' . ($et-$st) . ' sec. ';
die();