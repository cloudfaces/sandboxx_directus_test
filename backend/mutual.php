<?php
header('Access-Control-Allow-Origin: *');
define( 'MATCH_URL', 'http://dev.test.cloudfaces.com/files/cmarix/backend/matches.php' );
define( 'DIRECTUS_URL', 'https://directus.cloudfaces.com/' );
// define( 'DIRECTUS_URL', 'http://203.109.113.157/cf_directus/public/' );
// define( 'DIRECTUS_URL', 'http://localhost/cf_directus/public/' );
define( 'DIRECTUS_PROJECT', 9877 );
define( 'DIRECTUS_API_URL', DIRECTUS_URL.DIRECTUS_PROJECT . '/' );
define( 'DIRECTUS_CUSTOM_API_URL', DIRECTUS_URL . DIRECTUS_PROJECT . '/custom/sandboxx/' );
define( 'ADMIN_TOKEN', '56f233bb82c3c8a561b00e2c9b5b45cbb9c4e1ec433fc0330a5c9fee111ee6cd8f6fbb87d4540f6a' );

$user_id         = isset( $_GET['user_id'] ) ? $_GET['user_id'] : '';
$suggested_users = isset( $_GET['suggested_users'] ) ? $_GET['suggested_users'] : '';
$cohort_id       = isset( $_GET['cohort_id'] ) ? $_GET['cohort_id'] : '';
if ( empty( $user_id ) || $suggested_users == '' || $cohort_id == '' ) {
	echo outputFormat(array(), "Error", "Missing required parameters");
	exit();
}
$suggested_users = explode(",", $suggested_users);
$mutual_matches  = array();
foreach ( $suggested_users as $key => $user ) {
	$arr = explode("-", $user);
	$muser_id     = $arr[0];
	$percent      = isset( $arr[1] ) ? $arr[1] : '';
	$match_status = isset( $arr[2] ) ? $arr[2] : '';
	$match_data = getMatches($muser_id,$cohort_id);
	if ( in_array( $user_id, array_column($match_data, 'id') ) ) {
		$params    = [ 'fields' => '*' ];
		$user_data = curlRequest( DIRECTUS_API_URL."users/".$muser_id, $params, ADMIN_TOKEN );
		$user_data['data']['percent'] = $percent;
		$user_data['data']['match_status'] = $match_status;
		$mutual_matches[] = $user_data['data'];
	}
}

echo outputFormat($mutual_matches);

function curlRequest($url, $params = null, $accessToken) {
	$ch = curl_init();

	$params = (array)$params;
	$url =  $url."?".http_build_query($params);

	curl_setopt($ch, CURLOPT_URL, $url);

	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
	curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );

	$headers = array(
		'Content-Type: application/json',
		'Authorization: Bearer '.$accessToken,
	);
	curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );

	$output = curl_exec ($ch);
	curl_close ($ch);

	return json_decode($output, true);
}


function getMatches( $user_id, $cohort_id ) {
	$ch = curl_init();
	$params = array(
		'user_id' => $user_id,
		'cohort_id' => $cohort_id
	);
	$url =  MATCH_URL."?".http_build_query($params);
	curl_setopt($ch, CURLOPT_URL, $url);

	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
	curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );

	$output = curl_exec ($ch);
	curl_close ($ch);

	$response = json_decode($output, true);

	return ( $response['Status'] == 'Success' && $response['Data']['matches'] ) ? $response['Data']['matches'] : [];
}

function outputFormat($result, $status="Success", $err="") {
	$resultArr           = array();
	$resultArr['Status'] = $status;
	$resultArr['Data']   = $result;

	if ( ! empty( $err ) ) {
		$resultArr['ErrorCode']    = $err;
		$resultArr['ErrorMessage'] = $err;		
	}
	return json_encode($resultArr);
}
