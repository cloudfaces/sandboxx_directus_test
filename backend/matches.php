<?php

/** 
 * 1. Get questions with relations and arrange them in usable format
 * 2. Get the users answers and arrange them in usable format
 * 3. Get the remaining users answers that are not in active match
 * 4. Apply the algorithm
 * -- cycle through all remaining users answers
 * -- questions with 1 min answer are 1 match 100%
 * -- questions with more then 1 min answer are ammount matched divided by sum of matched and mismatched answers for user or sum of all answers for other user for that question (whatever is bigger) 
 * 5. Find top 5 user ids
 * 6. Get those users
 * 7. Return them
 * 
*/

header('Access-Control-Allow-Origin: *');
date_default_timezone_get();
define("REL_QUESTION_ANSWERS",	"314");
define("REL_QUESTION_QUESTION",	"316");
define("MAX_MATCHES",			"5");

define( 'DIRECTUS_URL', 'https://directus.cloudfaces.com/' );
// define( 'DIRECTUS_URL', 'http://203.109.113.157/cf_directus/public/' );
// define( 'DIRECTUS_URL', 'http://localhost/cf_directus/public/' );
define( 'DIRECTUS_PROJECT', 9877 );
define( 'DIRECTUS_API_URL', DIRECTUS_URL.DIRECTUS_PROJECT . '/' );
define( 'DIRECTUS_CUSTOM_API_URL', DIRECTUS_URL . DIRECTUS_PROJECT . '/custom/sandboxx/' );
define( 'ADMIN_TOKEN', '56f233bb82c3c8a561b00e2c9b5b45cbb9c4e1ec433fc0330a5c9fee111ee6cd8f6fbb87d4540f6a' );

$user_id = isset( $_GET['user_id'] ) ? $_GET['user_id'] : '';
$role_id = isset( $_GET['role_id'] ) ? $_GET['role_id'] : '';
$cohort_id = isset( $_GET['cohort_id'] ) ? $_GET['cohort_id'] : '';
if ( empty( $user_id ) || $cohort_id == '' ) {
	echo outputFormat(array(), "Error", "Missing required parameters");
	exit();
}

// Get the Access token from Admin user
$accessToken = ADMIN_TOKEN;

$params = [ 'fields' => '*' ];
$user_data = curlRequest( DIRECTUS_API_URL."users/".$user_id, "GET", $params, $accessToken );
if ( ! isset( $user_data['data'] ) || ! $user_data['data'] ) {
	echo outputFormat(array(), "Error", "Invalid credentials");
	exit();
}
$user_data = $user_data['data'];

$role_id    = ( $role_id !== '' ) ? $role_id : $user_data['role_id'];
if (empty($user_id) || empty($role_id)) {
	echo outputFormat(array(), "Error", "Missing User data");
	exit();
}

// Check if cohort is expired
$c_params    = [ 'fields' => '*' ];
$cohort_data = curlRequest( DIRECTUS_API_URL."items/cohorts/".$cohort_id, "GET", $c_params, $accessToken );
$cohort_data = $cohort_data['data'];
$today       = date('Y-m-d');

if( $today < $cohort_data['start_date'] ){
    echo outputFormat(array(), "Error", "Selected cohort is not started yet");
	exit();
}

if( $today > $cohort_data['end_date'] ){
    echo outputFormat(array(), "Error", "Selected cohort is expired");
	exit();
}

// Getting all questions with their related answers and related questions
$question_params = [ 'fields' => '*,answers.answers_id.*,related_questions.related_question_id.*' ];
$questions_data  = curlRequest( DIRECTUS_API_URL."items/questions", "GET", $question_params, $accessToken );
$questions       = array();

$related_q_count = 0;
if ( isset( $questions_data['data'] ) && $questions_data['data'] ) {
	foreach( $questions_data['data'] as $question ) {
		$question['rq_id'] = '';
		
		if ( ! empty( $question['related_questions'] ) ) {
			$related_question = $question['related_questions'][0]['related_question_id'];
			$question['rq_id'] = $related_question['id'];
			$question['_rel'][REL_QUESTION_QUESTION][] = $related_question;
			$related_q_count++;
		}
		$answer_array = $question['answers'];
		foreach ( $answer_array as $key => $answer ) {
			$ansArray[] = $answer['answers_id'];
			$question['_rel'][REL_QUESTION_ANSWERS][] = $answer['answers_id'];
		}
		$questions[$question["id"]] = $question;
	}
}

// Getting all current users answers
$user_answers_params = 'filter[user][eq]='.$user_id;
$user_answers_data   = curlRequest( DIRECTUS_API_URL."items/user_answers", "GET", $user_answers_params, $accessToken, false, $type = 'filter_query' );

// Formating user answers array with related question id as key for each question for easier access later
$user_answers = array();
if ( isset( $user_answers_data['data'] ) && $user_answers_data['data'] ) {
	foreach ( $user_answers_data['data'] as $key => $user_answer ) {
		if ( empty( $questions[$user_answer["question"]]["rq_id"] ) ) {
			continue;
		}
		$user_answers[$questions[$user_answer["question"]]["rq_id"]] [$user_answer["answer"]] = $user_answer;
	}
}

// Script stops if profile is incomplete
if(count($user_answers) != $related_q_count){
	echo outputFormat(array(), "Error", "Questionnaire is not completed");
	exit();
}

// Getting all user request data
$requests_data_params = [ 'user_id' => $user_id, 'cohort_id' => $cohort_id ];
$requests_data          = curlRequest( DIRECTUS_CUSTOM_API_URL."get-matches", "GET", $requests_data_params, $accessToken );

$user_requests = [];
$user_history  = [];
$request_user_ids = [];
$ignored_user_ids = [];

foreach ( $requests_data as $request ) {
	$id = $request["sponsor_id"] == $user_id ? $request["sponsoree_id"] : $request["sponsor_id"];
	$request_user_ids[] = $id;
	if ( $request["status"] == "history" ) {
		if ( ! isset( $user_history[$id] ) ) {
			$user_history[$id] = array();
		}
		$user_history[$id][] = $request;
	}
	else{
		$user_requests[$id] = $request;
	}
	if ( $request["status"] == "active" || $request["status"] == "unmatching" || ( $request["status"] == "request" && $request["sender_id"] == $user_id ) ) {
		$ignored_user_ids[]= $id;
	} 
}

//getting all remaining users answers
$params_all_answers = [ 'user_id' => $user_id, 'role_id' => $role_id, 'cohort_id' => $cohort_id ];
$all_answers        = curlRequest( DIRECTUS_CUSTOM_API_URL."remaining-users-answers", "GET", $params_all_answers, $accessToken );
$all_answers        = ( isset( $all_answers['data'] ) && ! empty ($all_answers['data']) ) ? $all_answers['data'] : [];

$current_user_id  = 0;
$current_q_id     = 0;
$current_answer_i = 0;

$current_q_matches    = 0;
$current_q_mismatches = 0;

$current_percentages  = [];
$user_percentages     = array();

$answers_count = count($all_answers);

// Matching algorithm
if ( $answers_count ) {
    for ($i = 0; $i <= $answers_count; $i++) {

        // We need a final cycle to calculate the previous question that is at the end of the array
        if ( $i == $answers_count ) {
            $answer = $all_answers[$current_answer_i];
        } else {
            $answer = $all_answers[$i];
        }

        if ( empty( $questions[$answer["question"]]["rq_id"] ) ) {
            continue;
        }

        if ( $i == 0 ) {
            $current_user_id = $answer["user"];
            $current_q_id = $answer["question"];
        }

        // Calculating percent match for current question
        if ( $current_q_id != $answer["question"] || $i == $answers_count ) {

            $total = $current_q_matches + $current_q_mismatches;
            $user_a_count = isset($user_answers[$current_q_id]) ? count($user_answers[$current_q_id]) : 0;

            $total = $user_a_count > $total ? $user_a_count : $total;

            if ( $total > 0 ) {

                $q_max_answers = $questions[$current_q_id]["max_answers"];
                $rq_max_answers = $questions[$questions[$current_q_id]["rq_id"]]["max_answers"];

                if ( $q_max_answers != $rq_max_answers ) {
                    $current_percentages[] = $current_q_matches >= 1 ? 1 : 0;
                } else {
                    $current_percentages[] = $current_q_matches / $total;
                }
            } else {
                $current_percentages[] = 0;
            }

            $current_q_id         = $answer["question"];
            $current_q_matches    = 0;
            $current_q_mismatches = 0;
        }

        //calculating percent match for current user based on percent matches for that users questions
        if ( $answer["user"] != $current_user_id || $i == $answers_count ) {

            if ( count( $current_percentages ) > 0 ) {
                $user_percentages[$current_user_id] = (array_sum($current_percentages) / count($current_percentages)) * 100;
            } else {
                $user_percentages[$current_user_id] = 0;
            }

            $current_user_id      = $answer["user"];
            $current_q_matches    = 0;
            $current_q_mismatches = 0;
            $current_percentages  = [];

        }

        // Adding answer match/mismatch for current question
        if (isset($user_answers[$answer["question"]])) {

            if (isset($user_answers[$answer["question"]][$answer["answer"]])) {
                $current_q_matches++;
            } else {
                $current_q_mismatches++;
            }

        }
        $current_answer_i = $i;
    }
}

// Creating user match list
arsort($user_percentages);

$user_ids    = array();
$id_count    = 0;
$max_matches = MAX_MATCHES - 1 - count($ignored_user_ids);

foreach ( $user_percentages as $key => $value ) {
	if ( $id_count > $max_matches ) {
		break;
	}
	if ( ! in_array( $key, $ignored_user_ids ) ) {
		$user_ids[]= $key;
		$id_count++;
	}
}

// Get all the matched users list
$user_ids = array_filter( array_unique( array_merge( $user_ids, $request_user_ids ) ) );
$user_ids = implode( ",", $user_ids );
$user_ids = strlen( $user_ids ) > 0 ? $user_ids : ' ';

// Get these user details with their match status
$params_users = [ 'user_ids' => $user_ids, 'current_user' => $user_id, 'cohort_id' => $cohort_id];
$users        = curlRequest( DIRECTUS_CUSTOM_API_URL."get-final-users-details", "GET", $params_users, $accessToken );
$match_data = array(
	'matches' => array(),
	'active_matches' => array(),
	'requested_matches' => array(),
	'requesting_matches' => array(),
	'history_matches' => array(), 
);

// Mapping users to request and algorithm data
for ($i=0; $i < count($users); $i++) { 
	$user = $users[$i];
	
	if (isset($user_history[$user["id"]])) {
		$history = $user_history[$user["id"]];
		foreach ($history as $history_match) {
			$user["percent"] = $history_match["match_percent"];
			$match_data["history_matches"][] = $user;
		}
	}

	if (isset($user_requests[$user["id"]])) {
		$skip_matches = false;
		$user_request = $user_requests[$user["id"]];
		$user["percent"] = $user_request["match_percent"];

		if ($user_request["status"] == "request") {
			if ($user_request["sender_id"] == $user_id) {
				$match_data["requested_matches"][] = $user;
				$skip_matches = true;
			}
			else{
				$match_data["requesting_matches"][] = $user;
			}
		}
		if ($user_request["status"] == "active" || $user_request["status"] == "unmatching"){
			$match_data["active_matches"][] = $user;
			$skip_matches = true;
		}
		if ($skip_matches) {
			continue;
		}
	}

	if (isset($user_percentages[$user["id"]])) {
		$user["percent"] = round($user_percentages[$user["id"]]);
		$match_data["matches"][] = $user;
	}

}

$matches = $match_data["matches"];
usort($matches, function($u1, $u2){
	return $u1["percent"] > $u2["percent"] ? -1 : 1;
});
$matches = array_slice($matches, 0, $max_matches+1, true);
$match_data["matches"] = $matches;

echo outputFormat($match_data);


function curlRequest($url, $method = "GET", $params = null, $accessToken, $d = false, $type = null ) 
{
	$ch = curl_init();

	if ($method=="GET") {
		if ( $type == 'filter_query' ) {
			$url = $url. '?' . $params;
		} else {
			$params = (array)$params;
			$url =  $url."?".http_build_query($params);
		}
	}

	curl_setopt($ch, CURLOPT_URL, $url);

	if ($method=="DELETE") {
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
		$params = http_build_query($params);
	}
	else if($method=="POST"){
		curl_setopt($ch, CURLOPT_POST, 1);
	}

	if ($d) {
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
	}

	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
	curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );

	if ( 'GET' !== $method && ( count( $params ) || strlen( $params ) ) ) {
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $params );
	}
 
	$headers = array(
		'Content-Type: application/json',
		'Authorization: Bearer '.$accessToken,
	);
	curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );

	$output = curl_exec ($ch);
	curl_close ($ch);

	return json_decode($output, true);
}

function outputFormat($result, $status="Success", $err="") {
	$resultArr = array();

	$resultArr['Status'] = $status;
	$resultArr['Data'] = $result;
	if (!empty($err)) {
		$resultArr['ErrorCode'] = $err;
		$resultArr['ErrorMessage'] = $err;		
	}
	return json_encode($resultArr);
}
