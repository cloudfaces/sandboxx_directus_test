var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

(function() {
    document.addEventListener("DOMContentLoaded", function(event) {
        var src, cssClass;

        var scr = document.createElement('script');
        console.log("DOM fully loaded and parsed");
        var dirPrefix = '../';
        if (location.pathname.indexOf('customModules') >= 0) {
            dirPrefix = '../modules/';
        }

        if (isMobile.Android()) {
            src = dirPrefix + 'appCore/nativeBridge/native_bridge_android.js';
            cssClass = 'android';
        } else if (isMobile.iOS()) {
            src = dirPrefix + 'appCore/nativeBridge/native_bridge_ios.js';
            cssClass = 'ios';
        } else {

            src = dirPrefix + 'appCore/nativeBridge/native_bridge_web.js';
            cssClass = 'web';


        }

        scr.setAttribute('src', src);
        document.body.className = (document.body.className + ' ' + cssClass).trim();
        document.head.appendChild(scr);
    });
})();
