function CFHeaderButton() {
    this.text;
    this.textColor;
    this.icon;
    this.function;
}

function CFPageHeader() {
    this.backgroundColor;
    this.titleColor;
    this.backgroundImage;
    this.titleImage;
    this.rightButton;
    this.leftButton;
    this.statusBarColor;
}

function CFPage() {
    this.uniqueId;
    this.type = "html";
    this.title;
    this.pageHeader;
}

function CFTabItem() {
    this.title;
    this.icon;
}

function CFTabPage() {
    this.type = "tab";
    this.tabItems = [];
}

CFTabPage.prototype = new CFPage();

function CFHttpRequest() {
    this.Method = "GET";
    this.Body = "";
    this.Url = "http://localhost";
    this.BinaryFile = "";
    this.DatabasePaths = "";
    this.PhonePaths = "";
    this.Headers = {};

    this.setRequestHeader = function(header, value) {
        this.Headers[header] = value;
    };

    this.send = function(callback) {
        var json = JSON.stringify(this);
        var _callback = function(res) { callback.call(this, res); };
        NativeBridge.call(function(cbId) { CFNetwork_.sendHttpRequest(json, cbId); }, _callback);
    };

    this.sendBinary = function(callback) {
        var json = JSON.stringify(this);
        var _callback = function(res) { callback.call(this, res); };
        NativeBridge.call(function(cbId) { CFNetwork_.sendHttpRequestBinary(json, cbId); }, _callback);
    };
};

var CFNetwork = {
    getConnectionType: function(callback) {
        var c = function(cbId) {
            CFNetwork_.getConnectionType(cbId);
        };
        NativeBridge.call(c, callback);
    },
    getCurrentWifi: function(callback) {
        var c = function(cbId) {
            CFNetwork_.getCurrentWifi(cbId);
        };
        NativeBridge.call(c, callback);
    }

};

function CFLocation() {
    this.long = 0.0;
    this.lat = 0.0;
}

function CFGeoFence() {
    this.lat = 0.0;
    this.long = 0.0;
    this.id = "GeoFence"; // each fence has a unique identifier which is used to remove or replace a fence
    this.radius = 1; // meter, the minimum may be quite larger, typically about 100m
}

var NativeBridge = {
    callbacksCount: 1,

    callbacks: {},

    resultForCallback: function resultForCallback(callbackId, resultArray) {

        try {
            var callback = NativeBridge.callbacks[callbackId];

            if (!callback) return;

            callback.apply(null, resultArray);
        } catch (e) {
            alert(e)
        }
    },

    call: function call(f, callback) {

        var hasCallback = callback && typeof callback == "function";
        var callbackId = hasCallback ? NativeBridge.callbacksCount++ : 0;

        if (hasCallback) {
            NativeBridge.callbacks[callbackId] = callback;
        }
        f.call(null, callbackId);
    }
};

var CFScanner = {
    scanBarCode: function scanBarCode(callback) {

        var c = function(cbId) {
            CFScanner_.scanBarCode(cbId);
        };

        NativeBridge.call(c, callback);
    }
}

var CFPictureChooser = {
    capturePicture: function capturePicture(source, resultType, callback) {
        var c = function(cbId) {
            CFPictureChooser_.capturePicture(source, resultType, cbId);
        }
        NativeBridge.call(c, callback);
    },

    getCapturedPictures: function getCapturedPictures(callback) {
        var result = JSON.parse(CFPictureChooser_.getCapturedPictures());
        callback.call(null, result);
    },

    getPictureData: function getPictureData(imageUrl, callback) {
        var result = CFPictureChooser_.getPictureData(imageUrl);
        callback.call(null, result);
    },

    deletePicture: function deletePicture(imageUrl) {
        CFPictureChooser_.deletePicture(imageUrl);
    },

    deleteAllPictures: function deleteAllPictures() {
        CFPictureChooser_.deleteAllPictures();
    },
}

var CFFacebook = {
    // callback: function(token)
    login: function login(permissions, callback) {
        var c = function(cbId) {
            CFFacebook_.login(permissions, cbId);
        };
        NativeBridge.call(c, callback);
    },

    isLoggedIn: function isLoggedIn(callback) {
        var c = function(cbId) {
            CFFacebook_.isLoggedIn(cbId);
        };
        NativeBridge.call(c, callback);
    },

    appInvites: function appInvites(appLinkUrl, previewImageUrl) {
        CFFacebook_.appInvites(appLinkUrl, previewImageUrl);
    },
    getToken: function getToken(callback) {
        var c = function(cbId) {
            CFFacebook_.getToken(cbId);
        };
        NativeBridge.call(c, callback);
    },

    logout: function logout() {
        CFFacebook_.logout();
    }
};

var CFRuntime = {
    loadApp: function loadApp(value) {
        CFRuntime_.loadApp(value)
    },

    log: function(message) {
        CFRuntime_.log(message)
    },

    findPage: function(pageName, callback) {
        var c = function(cbid) {
            CFRuntime_.findPage(pageName, cbid);
        }

        NativeBridge.call(c, callback);
    },

    findCurrentPage: function(callback) {
        var c = function(cbid) {
            CFRuntime_.findCurrentPage(cbid);
        }
        NativeBridge.call(c, callback);

    },


    updatePage: function(pageName, page) {
        CFRuntime_.updatePage(pageName, JSON.stringify(page));
    },
    rightToLeftLayout: function(rightToLeftLayoutString) {
        CFRuntime_.rightToLeftLayout(rightToLeftLayoutString)
    },
    setOrientation: function(orientation){
            CFRuntime_.setOrientation(orientation);
    },

    unsetOrientation: function(){
        CFRuntime_.unsetOrientation();
    },
};

var CFCalendarEvents = {

    createEvent: function(title, startYear, startMonth, startDay, startHour, startMinute, duration, callback) {

        var c = function(cbId) {
            CFCalendarEvents_.createEvent(title, startYear, startMonth, startDay, startHour, startMinute, duration, cbId);
        };

        NativeBridge.call(c, callback);
    },

    getCalendars: function(callback) {

        var c = function(cbId) {
            CFCalendarEvents_.getCalendars(cbId);
        };

        NativeBridge.call(c, callback);

    },

    insertEvent: function(calendar_ids, beginTime, endTime, title, description, reminderTime, location, callback) {
        var c = function(cbId) {
            CFCalendarEvents_.insertEvent(cbId, calendar_ids, beginTime, endTime, title, description, reminderTime, location);
        };

        NativeBridge.call(c, callback);
    },

    updateEvent: function(event_id, beginTime, endTime, title, description, reminderTime, location, callback) {

        var c = function(cbId) {
            CFCalendarEvents_.updateEvent(cbId, event_id, beginTime, endTime, title, description, reminderTime, location);
        };

        NativeBridge.call(c, callback);
    },

    deleteEvent: function(event_ID, callback) {
        var c = function(cbId) {
            CFCalendarEvents_.deleteEvent(cbId, event_ID);
        };

        NativeBridge.call(c, callback);
    }
};

var CFLocation = {
    // callback function: function(position)
    getCurrentPosition: function(callback) {
        var c = function(cbId) {
            CFLocation_.getCurrentPosition(cbId);
        };
        NativeBridge.call(c, callback);
    },

    startTracking: function(url) {
        CFLocation_.startTracking(url);
    },

    stopTracking: function() {
        CFLocation_.stopTracking();
    },

    getLocationQueue: function(callback) {
        var c = function(cbId) {
            CFLocation_.getLocationQueue(cbId);
        };
        NativeBridge.call(c, callback);
    },

    getFences: function(callback) {
        var c = function(cbId) {
            CFLocation_.getFences(cbId);
        };
        NativeBridge.call(c, callback);
    },

    addFence: function(fence) {
        CFLocation_.addFence(JSON.stringify(fence));
    },

    removeFence: function(fenceId) {
        CFLocation_.removeFence(fenceId);
    },

    setFencingUrl: function(url) {
        CFLocation_.setFencingUrl(url);
    }
};


var CFNotification = {
    addObserverForName: function addObserverForName(notificationName, callback) {
        var c = function(cbId) {
            CFNotification_.addObserverForName(notificationName, cbId);
        };
        NativeBridge.call(c, callback);
    },
    removeObserverForName: function removeObserverForName(notificationName) {
        CFNotification_.removeObserverForName(notificationName);
    },
    postNotificationWithName: function postNotificationWithName(notificationName) {
        CFNotification_.postNotificationWithName(notificationName);
    },
};

var CFIdentity = {
    getDeviceIdentifier: function(callback) {

        var c = function(cbId) {
            CFIdentity_.getDeviceIdentifier(cbId);
        };
        NativeBridge.call(c, callback);

    },
};

var CFShare = {
    shareString: function(text) {
        CFShare_.shareString(text);
    }
};
var CFExternal = {
    showExternalContent: function(title, url) {
        CFExternal_.showExternalContent(title, url);
    },
    showExternalContentPopUp: function(title, pageName) {
        CFExternal_.showExternalContentPopUp(title, pageName);
    }
};

var CFPrivacy = {

    // not wokring , only in android
    getAccounts: function(callback) {
        var c = function(cbId) {
            CFPrivacy_.getAccounts(cbId);
        };
        NativeBridge.call(c, callback);
    },
    getContacts: function(callback) {
        var c = function(cbId) {
            CFPrivacy_.getContacts(cbId);
        };
        NativeBridge.call(c, callback);
    },
    getContactsDataByIds: function(ids, callback) {
        var c = function(cbId) {
            CFPrivacy_.getContactsDataByIds(ids, cbId);
        };
        NativeBridge.call(c, callback);
    },

    checkPermission: function(type, callback) {
        var c = function(cbId) {
            CFPrivacy_.checkPermission(cbId, type);
        };
        NativeBridge.call(c, callback);
    },

    requestPermission: function(type, callback) {
        var c = function(cbId) {
            CFPrivacy_.requestPermission(cbId, type);
        };
        NativeBridge.call(c, callback);
    }

};

var CFDialog = {
    show: function(type, title, actions, data, callback) {
        var c = function(cbId) {
            CFDialog_.show(type, title, actions, data, cbId);
        };
        NativeBridge.call(c, callback);
    }
};

var CFAnalytics = {

    registerPage: function(pageName) {
        CFAnalytics_.registerPage(pageName);
    },

    sendEvent: function(category, action, label, value) {
        CFAnalytics_.sendEvent(category, action, label, value);
    },

    sendProduct: function(transactionId, name, category, price, quantity, currencyCode, sku) {
        CFAnalytics_.sendProduct(transactionId, name, category, price, quantity, currencyCode, sku);
    },

    sendTransaction: function(transactionId, affiliation, revenue, tax, shipping, currencyCode) {
        CFAnalytics_.sendTransaction(transactionId, affiliation, revenue, tax, shipping, currencyCode);
    },
    setUserProperty: function(id, userProperty) {
        CFAnalytics_.setUserProperty(id, userProperty);
    },

    logEventWithName: function(logEventWithName) {
        CFAnalytics_.logEventWithName(logEventWithName);
    },

    setUserId: function(setUserId) {
        CFAnalytics_.setUserId(setUserId);
    },

    handleOpenURL: function(handleOpenURL) {
        CFAnalytics_.handleOpenURL(handleOpenURL);
    },

    handleEventsForBackgroundURLSession: function(handleEventsForBackgroundURLSession) {
        CFAnalytics_.handleEventsForBackgroundURLSession(handleEventsForBackgroundURLSession);
    },

    handleUserActivity: function(handleUserActivity) {
        CFAnalytics_.handleUserActivity(handleUserActivity);
    }

};

var CFFirebaseAnalytics = {

    registerPage: function(pageName) {
        CFFirebaseAnalytics_.registerPage(pageName);
    },

    sendEvent: function(category, action, label, value) {
        CFFirebaseAnalytics_.sendEvent(category, action, label, value);
    },

    sendProduct: function(transactionId, name, category, price, quantity, currencyCode, sku) {
        CFFirebaseAnalytics_.sendProduct(transactionId, name, category, price, quantity, currencyCode, sku);
    },

    sendTransaction: function(transactionId, affiliation, revenue, tax, shipping, currencyCode) {
        CFFirebaseAnalytics_.sendTransaction(transactionId, affiliation, revenue, tax, shipping, currencyCode);
    }


};


var CFBadge = {
    update: function(count) {
        CFBadge_.update(count);
    },
    get: function(callback) {
        var c = function(cbId) {
            CFBadge_.get(cbId);
        };
        NativeBridge.call(c, callback);
    }
};

var CFHeader = {

    updateToggle: function(title, isVisible) {
        CFHeader_.updateToggle(title, isVisible);
    },
    updateHeader: function(title) {
        CFHeader_.updateHeader(title);
    }

};

var CFPayPal = {

    singlePayment: function(stuffToBuy, details, callback) {
        var c = function(cbId) {
            CFPayPal_.singlePayment(stuffToBuy, details, cbId);
        };
        NativeBridge.call(c, callback);
    }

};



// native bridge has been loaded
$(document).trigger('cf-loaded');

//callback to the native code
CFNotification_.postNotificationWithName("CFLoaded");