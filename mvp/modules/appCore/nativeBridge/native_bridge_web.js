var CFVars = {
    navigationContext: '',
    variable: {},
    triggered: false
};

window.addEventListener("message", function(e) {
    if (typeof e.data.indexOf !== 'undefined') {
        if (e.data.indexOf('context:') === 0) {
            CFVars.navigationContext = e.data.replace('context:', '');

            if (!CFVars.triggered) {
                $(document).trigger('cf-ready');
                CFVars.triggered = true;
            }
        } else if (e.data.indexOf('variable:') === 0) {
            CFVars.variable = JSON.parse(e.data.replace('variable:', ''));
        }
    } else if (typeof e.data === 'object') {
        var d = e.data.args;
        var skipSendMessage = e.data.source !== '' ? true : false;
        if (e.data.method === 'variableWrite') {
            CFVariableStorage.writeValue(d.key, d.value, skipSendMessage);
        }
    }
});

function sendMessage(method, args, source) {
    return window.parent.postMessage({
        method: method,
        args: args || {},
        source: source || ''
    }, '*');
}

var CFNavigation = {
    navigate: function navigate(page, context) {
        sendMessage('navigation:navigate', {
            page: page,
            context: context || ''
        });
    },

    getNavigationContext: function getNavigationContext() {
        return CFVars.navigationContext;
    },

    navigateBack: function navigateBack(result) {
        sendMessage('navigation:back');
    }
};

var CFMenuNavigation = {
    navigate: function navigate(page, context) {
        return CFNavigation.navigate(page, context);
    },

    getNavigationContext: function getNavigationContext() {
        return CFNavigation.getNavigationContext();
    },

    navigateBack: function navigateBack(result) {
        return CFNavigation.navigateBack(result);
    }
};

var CFRuntime = {


    rightToLeftLayout: function(value, callback) {
        return true;
    }
};

CFHttpRequest = function() {
    var req = this;

    this.Method = "GET";
    this.Body = "";
    this.Url = "http://localhost";
    this.Headers = {};

    this.setRequestHeader = function(header, value) {
        this.Headers[header] = value;
    };

    this.send = function(callback) {
        $.ajax({
            type: req.Method.toLowerCase(),
            data: req.Body,
            url: req.Url,
            success: function(data) {
                callback({
                    Body: data
                });
            }
        });
    }
}

CFPersistentStorage = {
    valueExists: function valueExists(key) {
        return localStorage.getItem(key) != null;
    },

    writeValue: function writeValue(key, value) {
        localStorage.setItem(key, value);
    },

    readValue: function readValue(key) {
        return localStorage.getItem(key);
    }
};

var CFVariableStorage = {
    valueExists: function valueExists(key) {
        return typeof CFVars.variable[key] != 'undefined';
    },

    writeValue: function writeValue(key, value, skipSendMessage) {
        CFVars.variable[key] = value;
        if (!skipSendMessage) {
            var source = $('#slide').length > 0 ? 'slide' : 'page';
            sendMessage('variableWrite', {
                    key: key,
                    value: value
                },
                source);
        }
    },

    readValue: function readValue(key) {
        return CFVars.variable[key];
    }
};

var CFLocation = {
    // callback is
    getCurrentPosition: function(callback) {
        return null;
    },

    startTracking: function(url) {
        return null;
    },

    stopTracking: function() {
        return null;
    },

    getLocationQueue: function(callback) {
        return null;
    },

    getFences: function(callback) {
        return null;
    },

    addFence: function(fence) {
        return null;
    },

    removeFence: function(fence) {
        return null;
    }
};
var CFPayPal = {

    singlePayment: function(stuffToBuy,details,callback){
        var c = function(cbId){
            CFPayPal_.singlePayment(stuffToBuy,details,cbId);
        };
       
    }

};
// Retrieve the initial variable storage
sendMessage('variableGet');
sendMessage('contextGet');