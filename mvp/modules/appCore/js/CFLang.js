CloudFaces.Language = {
    current: CloudFaces.Config.currentLanguage,
    languages: CloudFaces.Config.languages,
    variables: {},

    menuDirectionCheck: function() {
        if (CFVariableStorage.valueExists(CloudFaces.Config.app + '_menu_direction')) {
            return CFVariableStorage.readValue(CloudFaces.Config.app + '_menu_direction')
        } else {
            return false
        }
    },

    menuDirectionSet: function(v) {
        CFVariableStorage.writeValue(CloudFaces.Config.app + '_menu_direction', v)
    },

    get: function() {
        return CloudFaces.Language.current;
    },

    set: function(lang) {
        if (CloudFaces.Language.languages.indexOf(lang) != -1) {
            if (CloudFaces.Language.current != lang) {
                CloudFaces.Language.current = lang;

                CFVariableStorage.writeValue(CloudFaces.Config.app + '_language', JSON.stringify(lang));
                CloudFaces.Language.refresh();
            }
        }
    },

    refresh: function() {
        CloudFaces.Language.translatePage();
    },

    translate: function(inx) {
        if (typeof CloudFaces.Language.variables[inx] != 'undefined') {
            return CloudFaces.Language.variables[inx][CloudFaces.Language.current];
        }

        return inx;
    },

    init: function() {
        if (CFVariableStorage.valueExists(CloudFaces.Config.app + '_language') && CFVariableStorage.readValue(CloudFaces.Config.app + '_language')) {
            CloudFaces.Language.current = JSON.parse(CFVariableStorage.readValue(CloudFaces.Config.app + '_language')) || CloudFaces.Language.current;
        }

        if (CFVariableStorage.valueExists(CloudFaces.Config.app + '_language_variables') && CFVariableStorage.readValue(CloudFaces.Config.app + '_language_variables')) {
            CloudFaces.Language.variables = JSON.parse(CFVariableStorage.readValue(CloudFaces.Config.app + '_language_variables'));

            CloudFaces.Language.translatePage();
            $(document).trigger('cf-initialized');
        } else {
            new CloudFaces.Caching(CloudFaces.Config.tables.languages, 'Languages', 0, false).getAll(function(items) {

                $.each(items, function(inx, value) {
                    CloudFaces.Language.variables[value['inx']] = value;
                });
                CFVariableStorage.writeValue(CloudFaces.Config.app + '_language_variables', JSON.stringify(CloudFaces.Language.variables));
                CloudFaces.Language.translatePage();
                $(document).trigger('cf-initialized');
            });
        }
    },

    empty: function() {
        if (CloudFaces.Language.variables.size > 0) {
            return false;
        }

        return true;
    },

    translatePage: function() {
        var $element = {};

        $.each(CloudFaces.Language.variables, function(inx, value) {
            $element = $('[data-lang="' + value['inx'] + '"]');
            if (typeof $element != 'undefined' && $element.length) {
                var word = value[CloudFaces.Language.current];
                var element_type = $element.get(0).tagName.toLowerCase();
                switch (element_type) {
                    case 'input':
                        var type = $element.attr('type');
                        if (type && (type == 'submit' || type == 'button')) {
                            $element.attr('value', word);
                        } else {
                            $element.attr('placeholder', word);
                        }
                        break;
                    case 'textarea':
                        $element.attr('placeholder', word);
                        break;
                    default:
                        $element.html(word);
                }
            }
        });

        return true;
    }
}

$(document).on('cf-ready', function() {
    CloudFaces.Language.init();
    if (CloudFaces.Language.menuDirectionCheck()) {
        //menu is already handled
    } else {
        if (!$('body').hasClass('web'))
            CloudFaces.Language.menuDirectionSet(true)

        if (CloudFaces.Language.get() == 'fa') {
            $('body').addClass('fa-lang-rtl');
            CFRuntime.rightToLeftLayout('right');
        } else {
            CFRuntime.rightToLeftLayout('left');
        }
    }
});