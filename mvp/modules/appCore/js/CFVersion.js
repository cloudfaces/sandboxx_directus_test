/*v1.0.0*/
$.extend(CloudFaces.Version, {
    openPopup: function(textPopUp, closeButt) {
        $element = $(
            '<div class="versions">' +
            '<div class="popUpVersion">' +
            closeButt +
            '<div class="textInfo" data-lang="' + textPopUp + '">' +
            '</div>' +
            '</div>' +
            '</div>'
        )

        $('body').append($element);
    },

    check: function() {
        var deviceCode = CloudFaces.DeviceCode.getDeviceCode();
        $.ajax({
            // url: CloudFaces.Config.ApiURL + 'project_version/id/' + CloudFaces.Config.app + '/instance_id/' + deviceCode + '/',
            url: CloudFaces.Config.ApiURL + 'project/id/' + CloudFaces.Config.app + '/developers/versions/last/',
            type: 'GET',
            dataType: 'json',
            success: function(versions) {
                if (typeof versions != 'undefined' && versions['Status'] == 'Success') {
                    if (typeof versions['Data'] != 'undefined' && versions['Data']) {

                        var checkVersion = CFPersistentStorage.valueExists(CloudFaces.Config.app + '_checkVersion') && CFPersistentStorage.readValue(CloudFaces.Config.app + '_checkVersion') != '' ? JSON.parse(CFPersistentStorage.readValue(CloudFaces.Config.app + '_checkVersion')) : '';
                        var storageRequiredV = checkVersion && checkVersion.hasOwnProperty('version') ? checkVersion.version : '';

                        var textPopUp = '';
                        var closeButt = '';
                        if (versions['Data'][0] && typeof versions['Data'][0].current_version !== 'undefined' && CloudFaces.Version.Config.currentVersion <= versions['Data'][0].current_version) {
                            closeButt = '<img data-version="' + versions['Data'][0].current_version + '" id="closeVersionPopup" src="../appCore/images/appCore_close.png">';
                            if (CloudFaces.Version.Config.currentVersion >= versions['Data'][0].required_version) {
                                if (storageRequiredV !== '') {
                                    if (checkVersion.hasOwnProperty('check') && checkVersion.check === 'yes' && storageRequiredV < versions['Data'][0].current_version) {
                                        CloudFaces.Version.openPopup('version_updateVersion', closeButt);
                                    }
                                } else {
                                    CloudFaces.Version.openPopup('version_updateVersion', closeButt);
                                }
                            } else {
                                CloudFaces.Version.openPopup('version_versionsNoClose', '');
                            }
                        }

                        CloudFaces.Language.translatePage();
                    }
                }

                $(document).on('click', '#closeVersionPopup', function() {
                    $(".versions").animate({
                        top: '100%',
                        display: 'none'
                    });

                    var currVersion = {
                        version: $('#closeVersionPopup').data('version'),
                        check: 'yes'
                    };

                    currVersion = JSON.stringify(currVersion);

                    CFPersistentStorage.writeValue(CloudFaces.Config.app + '_checkVersion', currVersion);
                });
            }
        });
    }
});

$(document).on('cf-initialized', function() {
    CloudFaces.Version.check();
});



/**
 * Add welcome popup
 **/
document.write('<script type="text/javascript" src="../appCore/js/CFWelcome.js" ></script>');
/**
 * Add notification popup
 **/
var notification_page = CloudFaces.Notification.Config.path;

document.write('<link rel="stylesheet" href="../' + notification_page + '/css/style.css">');
document.write('<link rel="stylesheet" href="../' + notification_page + '/css/appSpecific.css">');
document.write('<script type="text/javascript" src="../' + notification_page + '/js/config.js" ></script>');
document.write('<script type="text/javascript" src="../' + notification_page + '/js/CFNotifications.js" ></script>');
