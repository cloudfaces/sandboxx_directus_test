/*v1.0.16*/
CloudFaces.debug = (window.location.href.indexOf('http') >= 0);

CloudFaces.Exception = function(type, code, message) {
    this.isException = true;
    this.type = type;
    this.code = code;
    this.message = message;
}

CloudFaces.Exception.prototype.alert = function() {
    alert(this.message);
}

/**
 * Static methods that check for exceptions.
 *
 * @type {object} e The (eventually) exception.
 * @type {string|null} type The type or code of the exception. Optional.
 */
CloudFaces.isException = function(e, type) {
    // If it's not an object, it's not an exception
    if (typeof e != 'object')
        return false;

    // If it's an object, but it does not have isException, it's not one
    if (typeof e.isException == 'undefined')
        return false;

    // If no particular type is needed, it passes
    if (typeof type != 'string')
        return true;

    // Check type / type.code
    if (type.indexOf('.') != -1)
        return e.type + '.' + e.code == type;
    else
        return e.type == type;
}

var api = CloudFaces.API = {
    /**
     * Calls the PHP proxy so it can access the API.
     */
    query: function(url, method, params, successCallback, errorCallback) {
        var success = function(args) {
            if (typeof successCallback != 'undefined')
                successCallback(args);
        }


        var error = function(args) {
            if (typeof errorCallback != 'undefined')
                errorCallback(args);
            else if (typeof successCallback != 'undefined')
                successCallback(args);
        };
        if (!$('body').is('.web') && typeof CFHttpRequest != 'undefined') {
            var request = new CFHttpRequest();
            request.Method = method.toUpperCase();
            if (request.Method=="GET") {
                request.Url = CloudFaces.Config.ApiURL + url + (url.indexOf('?')>=0 ? '&' : '?') + $.param(params);
            }
            else{
                request.Url = CloudFaces.Config.ApiURL + url;
                request.Body = $.param(params);
            }
            request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            request.send(function(result) {
                if (typeof result === 'undefined') {
                    error(new CloudFaces.Exception('api', 'no_internet', 'No internet'));
                } else {
                    if (result.Code == 200) {
                        success($.parseJSON(result.Body).Data);
                    } else {
                        error(new CloudFaces.Exception('api', 'invalid_response', 'Invalid Response'));
                    }
                }
            });

            return;
        }

        var ajaxParams = {
            url: CloudFaces.Config.ApiURL + url,
            type: method.toLowerCase(),
            data: params,
            success: function(data) {
                // Upon an empty request
                if (!data)
                    return error(new CloudFaces.Exception('api', 'empty_result', 'Empty Result'));

                // Try parsing
                try {
                    data = $.parseJSON(data);
                } catch (e) {
                    return error(new CloudFaces.Exception('api', 'invalid_json', 'Invalid JSON'));
                }

                // In case there is no exception, but no data too
                if (!data)
                    return error(new CloudFaces.Exception('api', 'empty_json', 'Empty JSON'));

                // If there is a JSON, but it contains an error
                if (data.Status == 'Error') {
                    if (data.ErrorCode == 'wrong_token') {
                        var view = new CloudFaces.View.ExpiredToken();

                        // Don't call the callback
                        if (url.indexOf('user/me/token') !== 0)
                            return;
                    }

                    if (data.hasOwnProperty('message_long')) {
                        success(data);
                        return;
                    } else {
                        return error(new CloudFaces.Exception('api', data.ErrorCode, data.ErrorMessage));
                    }
                }

                // There is JSON and it does actually have data
                success(data.Data);
            },
            error: function(error) {
                if (error.error) {
                    error.error(new CloudFaces.Exception('api', 'server_unreachable', 'Our servers seem to be unreachable at the moment.'));
                } else {
                    error(new CloudFaces.Exception('api', 'server_unreachable', 'Our servers seem to be unreachable at the moment.'));
                }
            }
        };

        if (params instanceof FormData) {
            ajaxParams.cache = false;
            ajaxParams.contentType = false;
            ajaxParams.processData = false;
        }

        $.ajax(ajaxParams);
    },

    queryForLogin: function(url, method, params, successCallback, errorCallback) {
        var success = function(args) {
            if (typeof successCallback != 'undefined')
                successCallback(args);
        }

        var error = function(args) {
            if (typeof errorCallback != 'undefined')
                errorCallback(args);
            else if (typeof successCallback != 'undefined')
                successCallback(args);
        };

        var ajaxParams = {
            url: CloudFaces.Config.ApiURL + url,
            type: method.toLowerCase(),
            data: params,
            success: function(data) {
                // Upon an empty request
                if (!data)
                    return error(new CloudFaces.Exception('api', 'empty_result', 'Empty Result'));

                // Try parsing
                try {
                    data = $.parseJSON(data);
                } catch (e) {
                    return error(new CloudFaces.Exception('api', 'invalid_json', 'Invalid JSON'));
                }

                // In case there is no exception, but no data too
                if (!data)
                    return error(new CloudFaces.Exception('api', 'empty_json', 'Empty JSON'));

                // If there is a JSON, but it contains an error
                if (data.Status == 'Error') {
                    if (data.ErrorCode == 'wrong_token') {
                        var view = new CloudFaces.View.ExpiredToken();

                        // Don't call the callback
                        if (url.indexOf('user/me/token') !== 0)
                            return;
                    }
                    if (data.hasOwnProperty('message_long')) {
                        success(data);
                    } else {
                        return error(new CloudFaces.Exception('api', data.ErrorCode, data.ErrorMessage));
                    }
                }

                // There is JSON and it does actually have data
                success(data.Data);
            },
            error: function(error) {
                if (error.error) {
                    error.error(new CloudFaces.Exception('api', 'server_unreachable', 'Our servers seem to be unreachable at the moment.'));
                } else {
                    error(new CloudFaces.Exception('api', 'server_unreachable', 'Our servers seem to be unreachable at the moment.'));
                }
            }
        };

        if (params instanceof FormData) {
            ajaxParams.cache = false;
            ajaxParams.contentType = false;
            ajaxParams.processData = false;
        }

        $.ajax(ajaxParams);
    },



    queryForRelations: function(url, successCallback, errorCallback) {
        var success = function(args) {
            if (typeof successCallback != 'undefined')
                successCallback(args);
        }

        var error = function(args) {
            if (typeof errorCallback != 'undefined')
                errorCallback(args);
            else if (typeof successCallback != 'undefined')
                successCallback(args);
        };

        var ajaxParams = {
            url: CloudFaces.Config.ApiURL + url,
            type: 'get',
            success: function(data) {
                // Upon an empty request
                if (!data)
                    return error(new CloudFaces.Exception('api', 'empty_result', 'Empty Result'));

                // Try parsing
                try {
                    data = $.parseJSON(data);
                } catch (e) {
                    return error(new CloudFaces.Exception('api', 'invalid_json', 'Invalid JSON'));
                }

                // In case there is no exception, but no data too
                if (!data)
                    return error(new CloudFaces.Exception('api', 'empty_json', 'Empty JSON'));

                // If there is a JSON, but it contains an error
                if (data.Status == 'Error') {
                    if (data.ErrorCode == 'wrong_token') {
                        var view = new CloudFaces.View.ExpiredToken();

                        // Don't call the callback
                        if (url.indexOf('user/me/token') !== 0)
                            return;
                    }
                    if (data.hasOwnProperty('message_long')) {
                        success(data);
                    } else {
                        return error(new CloudFaces.Exception('api', data.ErrorCode, data.ErrorMessage));
                    }
                }

                // There is JSON and it does actually have data
                success(data.Data);
            },
            error: function(error) {
                error(new CloudFaces.Exception('api', 'server_unreachable', 'Out servers seem to be unreachable at the moment.'));
            }
        };

        $.ajax(ajaxParams);
    },


    /**
     *Calls the PHP proxy so it can access the  other API. // success($.parseJSON(result.Body));
     **/
    query_two_other_api: function(url, method, params, successCallback, errorCallback) {
        var success = function(args) {
            if (typeof successCallback != 'undefined')
                successCallback(args);
        }

        var error = function(args) {
            if (typeof errorCallback != 'undefined')
                errorCallback(args);
            else if (typeof successCallback != 'undefined')
                successCallback(args);
        };

        if (!$('body').is('.web') && typeof CFHttpRequest != 'undefined') {
            var request = new CFHttpRequest();
            request.Method = method.toUpperCase();
            request.Url = url;
            request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            request.send(function(result) {
                if (result.Code == 200) {
                    success($.parseJSON(result.Body));
                } else {
                    error(new CloudFaces.Exception('api', 'invalid_response', 'Invalid Response'));
                }
            });

            return;
        }
        var ajaxParams = {
            url: url,
            type: method.toLowerCase(),
            data: params,
            success: function(data) {
                // Upon an empty request
                if (!data)
                    return error(new CloudFaces.Exception('api', 'empty_result', 'Empty Result'));

                // Try parsing
                try {
                    data = data;
                } catch (e) {
                    return error(new CloudFaces.Exception('api', 'invalid_json', 'Invalid JSON'));
                }

                // In case there is no exception, but no data too
                if (!data)
                    return error(new CloudFaces.Exception('api', 'empty_json', 'Empty JSON'));

                // If there is a JSON, but it contains an error
                if (data.Status == 'Error') {
                    if (data.ErrorCode == 'wrong_token') {
                        var view = new CloudFaces.View.ExpiredToken();

                        // Don't call the callback
                        if (url.indexOf('user/me/token') !== 0)
                            return;
                    }

                    return error(new CloudFaces.Exception('api', data.ErrorCode, data.ErrorMessage));
                }

                // There is JSON and it does actually have data
                success(data);
            },
            error: function(error) {
                error(new CloudFaces.Exception('api', 'server_unreachable', 'Out servers seem to be unreachable at the moment.'));
            }
        };

        if (params instanceof FormData) {
            ajaxParams.cache = false;
            ajaxParams.contentType = false;
            ajaxParams.processData = false;
        }

        $.ajax(ajaxParams);
    },

    /**
     * Adds the API root to paths.
     */
    url: function(url) {
        if (url && url.indexOf('http') >= 0) {
            return url;
        }
        return CloudFaces.Config.ApiURL + url;
    },

    relations: {
        getRelations: function(relId, listId, itemId, callback) {
            var url = 'project/id/%s/relations/relationid/%s/listid/%s/itemid/%s/'
                .replace('%s', CloudFaces.Config.app)
                .replace('%s', relId)
                .replace('%s', listId)
                .replace('%s', itemId)
            api.queryForRelations(url, callback);
        },
        addRelation: function(relation_id, list1_id, item1_Id, list2_id, item2_id, callback) {
            var url = 'project/id/%s/relation/listid/%s/itemid/%s/'
                .replace('%s', CloudFaces.Config.app)
                .replace('%s', list1_id)
                .replace('%s', item1_Id)
            var args = {
                relation_id: relation_id,
                list_id_2: list2_id,
                item_id_2: item2_id
            }
            api.query(url, 'post', args, callback);
        },
        deleteRelation: function(relation_id, list1_id, item1_Id, list2_id, item2_id, callback) {
            var url = 'project/id/%s/relation/listid/%s/itemid/%s/'
                .replace('%s', CloudFaces.Config.app)
                .replace('%s', list1_id)
                .replace('%s', item1_Id)
            var args = {
                relation_id: relation_id,
                list_id_2: list2_id,
                item_id_2: item2_id
            }
            api.query(url, 'delete', args, callback);
        } 
    },

    User: {
        // updated for relations
        register: function(params, callback) {
            var url = 'project/id/%s/lists/%s/items/'
                .replace('%s', CloudFaces.Config.app)
                .replace('%s', CloudFaces.Login.Config.tables.users)
            api.queryForLogin(url, 'put', params, callback);
        },
        // stays the same for relations
        Login: function(params, callback) {
            var url = 'project/id/%s/appuser/login/'
                .replace('%s', CloudFaces.Config.app)
            api.queryForLogin(url, 'post', params, callback);
        },
        // stays the same for relations
        logOut: function(token, callback) {
            var url = 'project/id/%s/appuser/logout/token/%s/'
                .replace('%s', CloudFaces.Config.app)
                .replace('%s', token)
            api.queryForLogin(url, 'post', {}, callback);
        },
        // updated for relations
        update: function(params, user_id, token, callback) {
            var url = 'project/id/%s/lists/%s/items/'
                .replace('%s', CloudFaces.Config.app)
                .replace('%s', CloudFaces.Login.Config.tables.users)
            api.queryForLogin(url, 'put', params, callback);
        },
        // stays the same for relations
        getKey: function(email, callback) {
            var url = 'project/id/%s/appuser/generate/'
                .replace('%s', CloudFaces.Config.app)
            api.queryForLogin(url, 'post', email, callback);
        },
        // stays the same for relations
        resetPasswaord: function(key, password, callback) {
            var url = 'project/id/%s/appuser/reset/key/%s/'
                .replace('%s', CloudFaces.Config.app)
                .replace('%s', key)
            api.queryForLogin(url, 'post', password, callback);
        },
        // updated for relations
        profilTemplate: function(callback) {
            var url = 'project/id/%s/lists/%s/items/'
                .replace('%s', CloudFaces.Config.app)
                .replace('%s', CloudFaces.Login.Config.tables.templates)
            api.queryForLogin(url, 'get', {}, callback);
        },
        // updated for relations
        getUserData: function(token, callback) {
            var url = 'project/id/%s/lists/%s/items/where/%s/'
                .replace('%s', CloudFaces.Config.app)
                .replace('%s', CloudFaces.Login.Config.tables.users)
                .replace('%s', 'token="' + token + '"');
            api.queryForLogin(url, 'get', {}, callback);
        }
    },

    /**
     * Generates a thumbnail.
     */
    thumbnail: function(src, width, height) {
        var url = CloudFaces.Config.ApiURL + '/helper/timthumb.php?zc=1&src=' + encodeURI(src);

        if (width)
            url += '&w=' + parseInt(width);

        if (height)
            url += '&h=' + parseInt(height);

        return url;
    },

    list: {
        /**
         * Retrieves the items of a list.
         *
         * @type <int> appId The ID of the application the list belongs to.
         * @type <int|int[]> list Either the ID of the list or [ parentId, listId ].
         * @type <callable> callback A callback that will be called with the items or an exception.
         */
        getItems: function(appId, listId, userToken, callback, params) {
            var url, args;

            url = 'project/id/%s/lists/%s/items/%s'
                .replace('%s', appId)
                .replace('%s', (typeof listId == 'object') ? listId[1] : listId)
                .replace('%s', '?token=' + userToken)

            args = params?params:{};

            if (typeof listId == 'object') {
                args.parent = listId[0];
            }

            api.query(url, 'get', args, callback);
        },

        getItem: function(appId, listId, item, userToken, callback, params) {
            var url, args;

            url = 'project/id/%s/lists/%s/items/%s/%s'
                .replace('%s', appId)
                .replace('%s', (typeof listId == 'object') ? listId[1] : listId)
                .replace('%s', item)
                .replace('%s', '?token=' + userToken)

            args = params?params:{};

            if (typeof listId == 'object') {
                args.parent = listId[0];
            }

            api.query(url, 'get', args, callback);
        },



        getItemsWhere: function(appId, listId, where, callback) {
            where = encodeURI(where);
            var token, url, args;

            url = 'project/id/%s/lists/%s/items/where/%s/'
                .replace('%s', appId)
                .replace('%s', (typeof listId == 'object') ? listId[1] : listId)
                .replace('%s', where);

            args = {};

            if (typeof listId == 'object') {
                args.parent = listId[0];
            }

            api.query(url, 'get', args, callback);
        },

        customSql: function(appId, customSqlId, params, callback) {
            var $url;

            $url = 'project/id/%s/customsql/%s/'
                .replace('%s', appId)
                .replace('%s', (typeof customSqlId == 'object') ? customSqlId[1] : customSqlId);

            // var request = new CFHttpRequest();
            // request.Method = "POST";
            // request.Url = CloudFaces.Config.ApiURL + $url;
            // request.Body = JSON.stringify(params);
            // request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            // request.send(callback);

            $.ajax({
                method: 'POST',
                url: CloudFaces.Config.ApiURL + $url,
                data: JSON.stringify(params),
                success: callback
            });
        },

        put: function(app, listId, data, callback) {
            var url;

            url = 'project/id/%s/lists/%s/items/'
                .replace('%s', app)
                .replace('%s', (typeof listId == 'object') ? listId[1] : listId);

            if (typeof listId == 'object') {
                data.parent = listId[0];
            }

            CloudFaces.API.query(url, 'put', data, callback);
        },


        delete: function(app, listId, data, callback) {
            var url;

            url = 'project/id/%s/list/%s/item/%s/'
                .replace('%s', app)
                .replace('%s', (typeof listId == 'object') ? listId[1] : listId)
                .replace('%s', data.id)

            CloudFaces.API.query(url, 'delete', data, callback);
        },
    },

    option: {
        /**
         * Retrieves the items of a list.
         *
         * @type <int> appId The ID of the application the list belongs to.
         * @type <int|int[]> list Either the ID of the list or [ parentId, listId ].
         * @type <callable> callback A callback that will be called with the items or an exception.
         */
        get: function(appId, key, callback) {
            var token, url, args;

            url = 'project/id/%s/options/option/%s/'
                .replace('%s', appId)
                .replace('%s', key);

            args = {};

            api.query(url, 'get', args, callback);
        }
    },
    registerAppWithToken: function(token, ain, callback) {
        var args = {
            os: $('body').is('.android') ? 'android' : 'ios',
            token: token,
            device_id: api.getDeviceId(),
            app: CloudFaces.Config.AppHash,
            ain: ain
        }
        api.query('tokens/', 'put', args, callback);
    },

    setDeviceId: function() {
        if (!CFPersistentStorage.valueExists('device_id')) {
            var device_id = api.getRandomString();
            CFPersistentStorage.writeValue('device_id', device_id);
        }
        return device_id;
    },

    getDeviceId: function() {
        if (CFPersistentStorage.valueExists('device_id')) {
            return CFPersistentStorage.readValue('device_id');
        } else {
            return api.setDeviceId();
        }
    },

    getRandomString: function() {
        var symbols = 'UXbxo5HVB2Y9isl4E1Mqgc7vKudIQe3FRyLaNWwzOhCnp08Pkm6DfTJrSjtZAG',
            str = new Date().getTime().toString(),
            i;

        while (str.length < 64) {
            str += symbols.charAt(Math.round(Math.random() * symbols.length));
        }
        return str;
    },

    setAnalyticData: function(page_id) {
        if (page_id) {
            var stat = 'device_id=' + api.getDeviceId() + '&' + 'project_id=' + CloudFaces.app + '&' + 'page_id=' + encodeURI(page_id);
            var request = new CFHttpRequest();

            request.Method = "PUT";
            request.Url = CloudFaces.Config.ApiURL + 'stats/';
            request.Body = stat;
            request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            request.send(function(result) {});
        }
    },
    
    registerPush: {
        setNotification: function(appId, data, successCallback, errorCallback){
            var url = 'project/id/%s/register/push/notifications/'
                .replace('%s', appId)    
            api.query(url, 'post', data, successCallback, errorCallback);
        },
        getNotifications: function(data, successCallback, errorCallback){
            var url = 'get/registered/push/notifications/'    
            api.query(url, 'post', data, successCallback, errorCallback);
        },
        deleteNotification: function(data, successCallback, errorCallback){
            var url = 'delete/registered/push/notifications/'    
            api.query(url, 'post', data, successCallback, errorCallback);
        }
    }
};

$(document).on('click', 'a[href*="cf:"]', function(e) {
    e.preventDefault();

    var $link = $(this),
        page, ctx, storage;


    page = $link.attr('href').replace('cf:', '');
    ctx = JSON.stringify($link.data('context') || '');

    if ($link.data('storage')) {
        $.each($link.data('storage'), function(key, data) {
            CFVariableStorage.writeValue(key, JSON.stringify(data));
        })
    }

    ($('body').is('.slideout') ? CFMenuNavigation : CFNavigation).navigate(page, ctx);
});

/**
 * Set Device ID
 */
$(document).on('cf-loaded', function(e) {
    $(document).trigger('cf-ready');
});