$(document).on('click', '[data-link="link-to-profile"]', function () {
    CFNavigation.navigateAndAddToBackStack('profile_build', '');
});

$(document).on('click', '[data-link="link-to-profile-account"]', function () {
    CFNavigation.navigateAndAddToBackStack('profile_account', '');
});

$(document).on('click', '[data-link="link-to-profile-account-matched"]', function () {
    CFNavigation.navigateAndAddToBackStack('profile_account_matched', '');
});

$(document).on('click', '[data-link="link-to-forgotten-password"]', function () {
    CFNavigation.navigateAndAddToBackStack('forgotten_password', '');
});

$(document).on('click', '[data-link="link-to-questions-text"]', function () {
    CFNavigation.navigateAndAddToBackStack('questions_text', '');
});

$(document).on('click', '[data-link="link-to-questions"]', function () {
    CFNavigation.navigateAndAddToBackStack('questions', '');
});

$(document).on('click', '[data-link="link-to-profile-account-request-match"]', function () {
    CFNavigation.navigateAndAddToBackStack('profile_account_request_match', '');
});

$(document).on('click', '[data-link="link-to-profile-account-match-request-receive"]', function () {
    CFNavigation.navigateAndAddToBackStack('profile-account-match-request-receive', '');
});

$(document).on('click', '[data-link="link-to-matches"]', function () {
    CFNavigation.navigateToRoot('matches', '');
});

$(document).on('click', '[data-link="link-to-faq"]', function () {
    CFNavigation.navigateAndAddToBackStack('faq', '');
});

$(document).on('click', '[data-link="link-to-faq-detailed"]', function () {
    CFNavigation.navigateAndAddToBackStack('faq_detailed', '');
});

$(document).on('click', '[data-link="link-to-resources"]', function () {
    CFNavigation.navigateAndAddToBackStack('resources', '');
});

$(document).on('click', '[data-link="link-to-resources-detailed"]', function () {
    CFNavigation.navigateAndAddToBackStack('resources_detailed', '');
});

$(document).on('click', '[data-link="link-to-feedback-thanks-page"]', function () {
    CFNavigation.navigateAndAddToBackStack('feedback_thanks_page', '');
});

$(document).on('click', '[data-link="link-to-feedback"]', function () {
    CFNavigation.navigateAndAddToBackStack('feedback', '');
});

$(document).on('click', '[data-link="link-to-tac"]', function () {
    CFNavigation.navigateAndAddToBackStack('tac', '');
});

$(document).on('click', '[data-link="link-to-admin-matches"]', function () {
    CFNavigation.navigateAndAddToBackStack('admin_matches', '');
});

$(document).on('click', '[data-link="link-to-profile-admin"]', function () {
    CFNavigation.navigateAndAddToBackStack('profile_admin', '');
});



$('.img-profile-add').on('click', function () {
    $('[data-img="profile-img"]').css('display','block');
    $('[data-img="profile-img"]').siblings('.img-none').css('display','none')
});

$('.img-profile-remove').on('click', function () {
    $('[data-img="profile-img"]').css('display','none');
    $('[data-img="profile-img"]').siblings('.img-none').css('display','block')
});



$('.switch-btns').on('click', function () {
    $(this).find('.col').toggle();
});


$('[ data-request="confirm"]').on('click', function () {
    $(this).parent().toggle();
    $('[data-request="confirmed-message-appear"]').toggle();
    $('html, body').animate({scrollTop:$(document).height()}, 'slow');
});

$('[data-unmatch="unmatch"]').on('click', function () {
    $('[ data-request="confirm"]').parent().toggle();
    $('[data-request="confirmed-message-appear"]').toggle();
});


function popup() {
    $('[data-modal="messageModal"]').trigger('click');
}
