/*v1.0.0*/
$.extend(CloudFaces.Notifications, {
    Config: CloudFaces.Notifications.Config,
    init: true,

    /*Inital notifications popup on welcome page*/
    showPastNotifications: function(container) {
        var $popup_notifications = $('<div class="popup_notifications"><div class="box_notification"></div><iframe src="" frameborder="0" id="iframe_notifications"></iframe></div>')
        var $box_notification = $popup_notifications.find('.box_notification');
        var userid = '';
        var userData = CloudFaces.Helpers.getUserData();
        if (userData != '') {
            userid = userData.id;
        }
        var app_id = CloudFaces.Config.app,
            //item_id = 1,
            items_limit = 10,
            parameters = {
                'app_id': app_id,
                //'item_id': item_id, // optional;
                'items_limit': items_limit, // optional
                'user_id': userid
            };
        var ain = CloudFaces.DeviceCode.getDeviceAin();
        $.ajax({
            url: CloudFaces.Notifications.Config.notificationsUrl + '?ain=' + ain,
            type: 'GET',
            dataType: 'json',
            data: parameters,
            success: function(notifications) {

                var welcome_message = CFPersistentStorage.readValue(CloudFaces.Config.app + '_welcomeMessage');
                if (typeof notifications != 'undefined' && notifications['Status'] == 'Success') {
                    itemsPush = notifications['Data']['items'];
                    if (typeof itemsPush != 'undefined' && itemsPush.length > 0) {
                        // $.each(itemsPush, function(inx, notification) {
                        notification = itemsPush[0];
                        if (notification.visibility != '0') {
                            var cn = CFPersistentStorage.readValue(CloudFaces.Config.app + '_checkedNotificationsfromPopup');
                            d = '';
                            var cn_arr = [];
                            if (cn == null) {
                                cn = parseInt(d);
                            } else {
                                if (typeof cn.indexOf !== 'undefined' && cn.indexOf(',') != -1) {
                                    cn_arr = cn.split(',');
                                } else {
                                    cn_arr.push(cn);
                                }
                                var exist_n = 0;
                                for (var ni = 0; ni < cn_arr.length; ni++) {
                                    if (parseInt(cn_arr[ni]) == d) {
                                        //if so skip
                                        exist_n = 1;
                                    }
                                }
                                if (exist_n == 0) {
                                    if (cn == '') {
                                        cn += d;
                                    } else {
                                        cn += ',' + d;
                                    }
                                    cn_arr.push(d);
                                }
                            }

                            /*Open PopUp*/
                            if (cn_arr.indexOf(parseInt(notification.id)) != -1 || cn_arr.indexOf(notification.id) != -1) {
                                //alert('vew 1');
                            } else {
                                if (notification != '' && (welcome_message == CloudFaces.Config.app + '_success')) {
                                    var urlLink = '';
                                    if (CloudFaces.LayoutPromotions && CloudFaces.LayoutPromotions.checkPromotions()) {
                                        urlLink = '<div class="close_pop_up buttonOk backgroundTemplate textColorButton">' +
                                            CloudFaces.Language.translate('promotions_ok') +
                                            '</div>';
                                    }

                                    var fb_yes = ''; //facebook Icon Yes
                                    var close_ads = ''; // Close Button for ADS
                                    var width = $(document).width() || 320;
                                    image = notification.phone_icon;
                                    if (image == '') {
                                        image = CloudFaces.Config.ApiURL + 'files/project/a' + CloudFaces.Config.app + '/notifications/icons/6.png';
                                    }
                                    var dirPrefix = '../' + CloudFaces.Notification.Config.path + '/';
                                    if (location.pathname.indexOf(CloudFaces.Notification.Config.path) >= 0) {
                                        dirPrefix = '';
                                    }
                                    if (notification.phone_facebook != 0) {
                                        urlLink = '<a id="fb_link" data-value="' + notification.id + '" href="' +
                                            notification.phone_facebook + '" class="buttonOk fb textColorButton">' +
                                            CloudFaces.Language.translate('notifications_like_us_fb') +
                                            '</a>';

                                        fb_yes = '<div class="fb_img"><img src="' + dirPrefix + 'images/facebook.png"></div>'
                                    }
                                    close_ads = '<div id="" data-value="' + notification.id + '"  class="closeAds">' +
                                        '<i class="fa fa-times" aria-hidden="true"></i>'+
                                        '</div>'

                                    /*Check Date*/
                                    var d = new Date();
                                    var day = CloudFaces.Helpers.getDate(d);
                                    var month = CloudFaces.Helpers.getMonth(d);
                                    var year = '' + d.getFullYear();

                                    /*Push Date*/
                                    var date_month = '',
                                        date_day = '',
                                        date = '';
                                    if (typeof notification.date != 'undefined' && notification.date) {
                                        var date = new Date(CloudFaces.Helpers.dateDBFormat(notification.date));
                                        date_day = date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear();
                                    }

                                    var dataPush = CloudFaces.Helpers.getDate(date) + '/' + CloudFaces.Helpers.getMonth(date) + '/' + date.getFullYear();

                                    if (year + month + day == date.getFullYear() + '' + CloudFaces.Helpers.getMonth(date) + '' + CloudFaces.Helpers.getDate(date)) {
                                        dataPush = CloudFaces.Language.translate('notifications_today');
                                    }
                                    /*Push Image*/

                                    var pushImages = '';
                                    var addTitleMarginTop = 'addTitleMarginTop';
                                    if (notification.phone_image !== '') {
                                        var width = $(document).width() || 320,
                                            image1 = CloudFaces.API.url(notification.phone_image);
                                        thumb1 = CloudFaces.Helpers.createThumb(image1, width * 2);
                                        //$push.filter('.icon').before('<div class="imgPush"><img src="' + thumb + '"><div class="show_img_tittle2"></div></div>').addClass('show_img_icon');
                                        pushImages = ('<div class="imgPush"><img src="' + thumb1 + '"><div class="show_img_tittle2"></div></div>');
                                        //$('.icon').addClass('show_img_icon');
                                        // $push.filter('.closeAds').addClass('show_img_closeAds');
                                        //$push.filter('.tittle').addClass('show_img_tittle');
                                        addTitleMarginTop = '';
                                    }

                                    var $push = $('<div>' +
                                        close_ads +
                                        pushImages +
                                        '<div data-id="' + notification.id + '" class="icon show_img_icon">' +
                                        '<img src="' + image + '">' +
                                        '</div>' +
                                        '<div class="tittle colorTemplate ' + addTitleMarginTop + '">' +
                                        notification.phone_title +
                                        '</div>' +
                                        // fb_yes +
                                        '<div class="description ">' +
                                        notification.phone_text +
                                        '</div>' +
                                        '<div id="countdown"></div>' +
                                        '<div class="date">' +
                                        // '<img src="../' + CloudFaces.Notification.Config.path + '/images/clock.png"/> ' +
                                        '<i class="fa fa-clock-o" aria-hidden="true"></i>' +
                                        dataPush +
                                        ', ' +
                                        CloudFaces.Helpers.getHours(date) + ':' +
                                        CloudFaces.Helpers.getMinutes(date) +
                                        '</div>' +
                                        urlLink +
                                        '</div>');

                                    $push.filter('a#fb_link').on('click', function(e) {
                                        e.preventDefault();
                                        var hrefLinks = $(this).attr('href');
                                        window.open(hrefLinks);
                                    });


                                    if (CloudFaces.LayoutPromotions && notification.phone_promotion_link == "last_two") {
                                        /***
                                         * add Promotions to Notifications Begin
                                         ***/
                                        CloudFaces.LayoutPromotions.addPromotions($box_notification, 2);
                                        /***
                                         * add Promotions to Notifications END
                                         ***/
                                    }

                                    if (CloudFaces.LayoutPromotions && notification.phone_promotion_link == "last_one") {
                                        /***
                                         * add Promotions to Notifications Begin
                                         ***/
                                        CloudFaces.LayoutPromotions.addPromotions($box_notification, 1);
                                        /***
                                         * add Promotions to Notifications END
                                         ***/
                                    }

                                    //show Notification Popup
                                    $box_notification.prepend($push);

                                    if (pushImages != '') {
                                        $('.closeAds').addClass('show_img_closeAds');
                                        $('.tittle').addClass('show_img_tittle');
                                        $('.icon').addClass('show_img_icon');
                                    }

                                    $popup_notifications.show();
                                    $box_notification.show();
                                    $box_notification.animate({
                                        top: '10%'
                                    });

                                    // return false;

                                    //});
                                }
                            }
                        }
                        // });

                        $('.icon').height($('.icon').width());
                        var headerHeight = $('.page-header').outerHeight(true);
                        $box_notification.css('margin-top', headerHeight);
                        $(container).append($popup_notifications);
                    } else {
                        // CloudFaces.Functions.setMessage($box_notification, CloudFaces.Language.translate('notifications_no_new'));
                    }
                } else {
                    // error handling
                }
            },
            error: function(result) {
                // error handling
            }
        });
    },

    /*Page notifications*/
    loadContent: function(container) {
        var $popup_notifications = $('<div class="popup_notifications"><div class="box_notification"></div><iframe src="" frameborder="0" id="iframe_notifications"></iframe></div>')
        var $box_notifications = $('<div class="box_notifications"></div>');
        var $box_notification = $popup_notifications.find('.box_notification');

        new CloudFaces.Analytics('notifications', 'Notifications').set(function(result) {});
        var paddingTOp = $('.pred_events').outerHeight(true);
        $box_notifications.css('padding-top', paddingTOp)
        var userid = '';
        var userData = CloudFaces.Helpers.getUserData();
        if (userData != '') {
            userid = userData.id;
        }
        var app_id = CloudFaces.Config.app
            //item_id = 1,
        items_limit = 10,
            parameters = {
                'app_id': app_id,
                //'item_id': item_id, // optional;
                'items_limit': items_limit, // optional
                'user_id': userid
            };
        var ain = CloudFaces.DeviceCode.getDeviceAin();

        $.ajax({
            url: CloudFaces.Config.ApiURL + 'notifications/?ain=' + ain,
            type: 'GET',
            dataType: 'json',
            data: parameters,
            success: function(notifications) {
                if (typeof notifications != 'undefined' && notifications['Status'] == 'Success') {
                    if (typeof notifications['Data']['items'] != 'undefined' && notifications['Data']['items'].length > 0) {
                        $.each(notifications['Data']['items'], function(inx, notification) {
                            if (notification.visibility != '0') {
                                //CloudFaces.Functions.loadCFstorage(CloudFaces.Config.app + '_checkedNotificationsfromPopup');
                                //CloudFaces.Functions.loadCFstorage(CloudFaces.Config.app + '_checkedNotificationsFromIframe');
                                var cn = CFPersistentStorage.readValue(CloudFaces.Config.app + '_checkedNotificationsfromPopup');
                                d = '';
                                var cn_arr = [];
                                if (cn == null) {
                                    cn = parseInt(d);
                                } else {
                                    if (typeof cn.indexOf !== 'undefined' && cn.indexOf(',') != -1) {
                                        cn_arr = cn.split(',');
                                    } else {
                                        cn_arr.push(cn);
                                    }
                                    var exist_n = 0;
                                    for (var ni = 0; ni < cn_arr.length; ni++) {
                                        if (parseInt(cn_arr[ni]) == d) {
                                            //if so skip
                                            exist_n = 1;
                                        }
                                    }
                                    if (exist_n == 0) {
                                        if (cn == '') {
                                            cn += d;
                                        } else {
                                            cn += ',' + d;
                                        }
                                        cn_arr.push(d);
                                    }
                                }

                                /*Check Date*/
                                var d = new Date();
                                var day = CloudFaces.Helpers.getDate(d);
                                var month = CloudFaces.Helpers.getMonth(d);
                                var year = '' + d.getFullYear();

                                /*Push Date*/
                                var date_month = '',
                                    date_day = '',
                                    date = '';
                                if (typeof notification.date != 'undefined' && notification.date) {
                                    var date = new Date(CloudFaces.Helpers.dateDBFormat(notification.date));
                                    date_day = date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear();
                                }

                                var dataPush = CloudFaces.Helpers.getDate(date) + '/' + CloudFaces.Helpers.getMonth(date) + '/' + date.getFullYear();

                                if (year + month + day == date.getFullYear() + '' + CloudFaces.Helpers.getMonth(date) + '' + CloudFaces.Helpers.getDate(date)) {
                                    dataPush = CloudFaces.Language.translate('notifications_today');
                                }


                                var width = $(document).width() || 320,
                                    image = notification.phone_icon;
                                if (image == '') {
                                    image = CloudFaces.Config.ApiURL + 'files/project/a' + CloudFaces.Config.app + '/notifications/icons/6.png';
                                }
                                var row_color = ((cn_arr.indexOf(parseInt(notification.id)) != -1 || cn_arr.indexOf(notification.id) != -1) ? '' : 'noWiew');

                                var $category = $(
                                    '<tr data-url="' + notification.phone_url + '" data-id="' + notification.id + '" class="open_popup ' + row_color + '">' +
                                    '<td class="left">' +
                                    '<div class="icon">' +
                                    '<img src="' + image + '">' +
                                    '</div>' +
                                    '</td>' +
                                    '<td class="center">' +
                                    '<div class="tittle colorTemplate">' +
                                    notification.phone_title +
                                    '</div>' +
                                    '<div class="date">' +
                                    '<i class="fa fa-clock-o" aria-hidden="true"></i>' +
                                    dataPush +
                                    ', ' +
                                    CloudFaces.Helpers.getHours(date) + ':' + CloudFaces.Helpers.getMinutes(date) +
                                    '</div>' +
                                    '</td>' +
                                    '<td class="right colorTemplate">' +
                                    '<i class="fa fa-chevron-right" aria-hidden="true"></i>' +
                                    '</td>' +
                                    '</tr>'
                                );
                                $category.dataPush = dataPush;
                                $category.data('context', notification);

                                //append notification
                                $box_notifications.append($category);
                            }
                        });

                        $('.icon').height($('.icon').width());
                        var headerHeight = $('.page-header').outerHeight(true);
                        $box_notifications.css('margin-top', headerHeight);
                        $(container).append($popup_notifications);
                        $(container).append($box_notifications);
                        CloudFaces.Helpers.hideLoader();

                    } else {
                        $('#notifications .content').append('<div class="no-information">' + CloudFaces.Language.translate('no-information') + '</div>');
                        CloudFaces.Helpers.hideLoader();
                    }
                } else {
                    // error handling
                }
            },
            error: function(result) {
                // error handling
            }
        });
    },

    events: function() {
        /*START NOTIFICATIONS Save to Persistent Storage*/
        if (CloudFaces.Notifications.init) {
            $(document).on('click', '.open_popup', function() {
                /*Open PopUp*/
                var urlIframe = $(this).data('urls');
                var idElement = $(this).data('id');
                if (urlIframe == "") {

                    newUrl = urlIframe + '?item=' + idElement

                    $('#iframe_notifications').attr('src', newUrl);
                    $('#iframe_notifications').show();
                    $('.popup_notifications').show();
                    $("#iframe_notifications").animate({
                        top: '0'
                    });
                } else {
                    /*must open POPUp url*/
                    var notification = $(this).data('context');
                    var $list = $('.box_notification');

                    var urlLink = '';
                    if (CloudFaces.LayoutPromotions && CloudFaces.LayoutPromotions.checkPromotions()) {
                        urlLink = '<div class="close_pop_up buttonOk backgroundTemplate textColorButton">' +
                            CloudFaces.Language.translate('promotions_ok') +
                            '</div>';
                    }
                    var fb_yes = '';
                    //facebook Icon Yes
                    var close_ads = ''; // Close Button for ADS
                    var width = $(document).width() || 320,
                        image = notification.phone_icon;
                    if (image == '') {
                        image = CloudFaces.Config.ApiURL + 'files/project/a' + CloudFaces.Config.app + '/notifications/icons/6.png';
                    }
                    var dirPrefix = '../' + CloudFaces.Notification.Config.path + '/';
                    if (location.pathname.indexOf('customModules') >= 0) {
                        dirPrefix = '';
                    }
                    if (notification.phone_facebook != 0) {
                        urlLink = '<a id="fb_link" data-value="' + notification.id + '" href="' + notification.phone_facebook + '" class="buttonOk fb textColorButton" >' + CloudFaces.Language.translate('notifications_like_us_fb') + '</a>'

                        fb_yes = '<div id="" class="fb_img" ><img src="' + dirPrefix + 'images/facebook.png"></div>'

                    }
                    close_ads = '<div id="" data-value="' + notification.id + '"  class="closeAds" ><i class="fa fa-times" aria-hidden="false"></i></div>'
                        /*Check Date*/
                    var d = new Date();
                    var day = CloudFaces.Helpers.getDate(d);
                    var month = CloudFaces.Helpers.getMonth(d);
                    var year = '' + d.getFullYear();

                    /*Push Date*/
                    var date_month = '',
                        date_day = '',
                        date = '';
                    if (typeof notification.date != 'undefined' && notification.date) {
                        var date = new Date(CloudFaces.Helpers.dateDBFormat(notification.date));
                        date_day = date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear();
                    }

                    var dataPush = CloudFaces.Helpers.getDate(date) + '/' + CloudFaces.Helpers.getMonth(date) + '/' + date.getFullYear();

                    if (year + month + day == date.getFullYear() + '' + CloudFaces.Helpers.getMonth(date) + '' + CloudFaces.Helpers.getDate(date)) {
                        dataPush = CloudFaces.Language.translate('notifications_today');
                    }

                    var $push = $(
                        close_ads +
                        '<div data-id="' + notification.id + '" class="icon">' +
                        '<img src="' + image + '">' +
                        '</div>' +
                        '<div class="tittle colorTemplate">' +
                        notification.phone_title +
                        '</div>' +
                        '<div class="description ">' +
                        notification.phone_text +
                        '</div>' +
                        '<div id="countdown"></div>' +
                        '<div class="date">' +
                        '<i class="fa fa-clock-o" aria-hidden="true"></i>'+
                        dataPush +
                        ', ' +
                        CloudFaces.Helpers.getHours(date) + ':' + CloudFaces.Helpers.getMinutes(date) +
                        '</div>' +
                        urlLink

                    );

                    /*Push Image*/
                    var width = $(document).width() || 320,
                        image = CloudFaces.API.url(notification.phone_image);
                    thumb = CloudFaces.Helpers.createThumb(image, width * 2);

                    var pushImages = '';

                    $push.filter('a#fb_link').on('click', function(e) {
                        e.preventDefault();
                        var hrefLinks = $(this).attr('href');
                        window.open(hrefLinks);
                    });

                    if (CloudFaces.LayoutPromotions && notification.phone_promotion_link == "last_two") {
                        /***
                         * add Promotions to Notifications Begin
                         ***/
                        CloudFaces.LayoutPromotions.addPromotions($list, 2);
                        /***
                         * add Promotions to Notifications END
                         ***/
                    }

                    if (CloudFaces.LayoutPromotions && notification.phone_promotion_link == "last_one") {
                        /***
                         * add Promotions to Notifications Begin
                         ***/
                        CloudFaces.LayoutPromotions.addPromotions($list, 1);
                        /***
                         * add Promotions to Notifications END
                         ***/
                    }

                    $list.prepend($push);

                    if (notification.phone_image !== '') {
                        $push.filter('.icon').before('<div class="imgPush"><img src="' + thumb + '"><div class="show_img_tittle2"></div></div>').addClass('show_img_icon');
                        $push.filter('.closeAds').addClass('show_img_closeAds');
                        $push.filter('.tittle').addClass('show_img_tittle');
                    }

                    $('.popup_notifications').show();
                    $('.box_notification').show();
                    $(".box_notification").animate({
                        top: '7%'
                    });
                }
            });

            $(document).on('click', '#fb_link', function(e) {
                setTimeout(function() {

                    var notID = $('.box_notification .icon:first').data('id');
                    var cn = CFPersistentStorage.readValue(CloudFaces.Config.app + '_checkedNotificationsfromPopup');
                    d = parseInt(notID);
                    var cn_arr = [];
                    if (cn == null) {
                        cn = parseInt(d);
                    } else {
                        if (typeof cn.indexOf !== 'undefined' && cn.indexOf(',') != -1) {
                            cn_arr = cn.split(',');
                        } else {
                            cn_arr.push(cn);
                        }
                        var exist_n = 0;
                        for (var ni = 0; ni < cn_arr.length; ni++) {
                            if (parseInt(cn_arr[ni]) == d) {
                                //if so skip
                                exist_n = 1;
                            }
                        }
                        if (exist_n == 0) {
                            if (cn == '') {
                                cn += d;
                            } else {
                                cn += ',' + d;
                            }
                            cn_arr.push(d);
                        }
                        $('.popup_notifications').hide();
                        $('.box_notification').empty();
                        $('.iframe_notifications').hide();
                        $('#iframe_notifications').hide();
                    }


                    CFPersistentStorage.writeValue(CloudFaces.Config.app + '_checkedNotificationsfromPopup', cn);


                    $('.open_popup').each(function() {
                        var data_id = $(this).attr('data-id');
                        for (var ni = 0; ni < cn_arr.length; ni++) {
                            if (parseInt(cn_arr[ni]) == parseInt(data_id)) {
                                $(this).removeClass('noWiew');
                            }
                        }
                    });
                }, 1000)
            });

            $(document).on('click', '.close_pop_up, .closeAds', function(e) {
                $('.box_notification').animate({
                    top: '100%'
                }, 300, function() {
                    $('.popup_notifications').hide();
                    $('.box_notification').empty();
                    $('.iframe_notifications').hide();
                });

                var notID = $('.box_notification .icon:first').data('id');

                var cn = CFPersistentStorage.readValue(CloudFaces.Config.app + '_checkedNotificationsfromPopup');
                d = parseInt(notID);
                var cn_arr = [];
                if (cn == null) {
                    cn = parseInt(d);
                } else {
                    if (typeof cn.indexOf !== 'undefined' && cn.indexOf(',') != -1) {
                        cn_arr = cn.split(',');
                    } else {
                        cn_arr.push(cn);
                    }
                    var exist_n = 0;
                    for (var ni = 0; ni < cn_arr.length; ni++) {
                        if (parseInt(cn_arr[ni]) == d) {
                            //if so skip
                            exist_n = 1;
                        }
                    }
                    if (exist_n == 0) {
                        if (cn == '') {
                            cn += d;
                        } else {
                            cn += ',' + d;
                        }
                        cn_arr.push(d);
                    }
                }

                CFPersistentStorage.writeValue(CloudFaces.Config.app + '_checkedNotificationsfromPopup', cn);
                $('.open_popup').each(function() {
                    var data_id = $(this).attr('data-id');
                    //for (var ni = 0; ni < cn_arr.length; ni++) {
                    if (parseInt(d) == parseInt(data_id)) {
                        //if (parseInt(cn_arr[ni]) == parseInt(data_id)) {
                        $(this).removeClass('noWiew');
                        $('#iframe_notifications').hide();
                    }
                    // }
                });
            });
            /*END NOTIFICATIONS Save to Persistent Storage*/

            $(document).on('click', '#notifications .notifications-settings-button', function() {
                CFNavigation.navigate("notificationsSettings", '');
            });

            CloudFaces.Notifications.init = false;
        }
    },

    Settings: {
        init: true,

        showStatusPopup: function showStatusPopup(isSuccess, popupMsg) {
            var $popup = $('.notification-settings-status-popup');
            var popupImg = isSuccess ? 'images/notifications-settings-success.png' : 'images/notifications-settings-error.png';

            if ($popup.length == 0) {
                $('body').append(
                    '<div class="notification-settings-status-popup">' +
                    '<div class="notification-settings-status-popup-img">' +
                    '<img src="' + popupImg + '" alt="" />' +
                    '</div>' +
                    '<div class="notification-settings-status-popup-text">' +
                    popupMsg +
                    '</div>' +
                    '</div>');

                $popup = $('.notification-settings-status-popup');
            }

            $popup.find('img').attr('src', popupImg).end().find('.notification-settings-status-popup-text').text(popupMsg).end().fadeIn().delay(3000).fadeOut();
        },

        hideStatusPopup: function hideStatusPopup(time) {
            $('.notification-settings-status-popup:visible').stop().fadeOut(typeof time !== 'undefined' ? time : 300);
        },

        reloadContent: function reloadContent() {
            var container = CloudFaces.Notifications.Settings.container;
            $(container).empty();

            CloudFaces.Notifications.Settings.loadContent(container, true);
        },

        save: function save() {
            var ain = CloudFaces.DeviceCode.getDeviceAin();
            if (typeof ain !== 'undefined' && ain != '') {
                var days = [];
                var categories = [];

                $('.notifications-settings-days-wrapper input:checked').each(function(inx, item) {
                    days.push(item.value);
                });

                $('.notifications-settings-category-wrapper input').each(function(inx, item) {
                    categories.push({
                        "id": item.value,
                        "subscribed": item.checked ? 1 : 0
                    });
                });

                var data = {
                    "project_id": CloudFaces.Config.app,
                    "ain": ain,
                    "days": days.join(),
                    "categories": categories,
                    "geo_push_notifications": "1",
                    "push_notifications": "1"
                };

                var settingsId = $('#notificationsSettings').data('id') || "";
                if (settingsId) {
                    data.id = settingsId;
                }

                CloudFaces.API.query(CloudFaces.Notifications.Config.settingsUrl(ain), 'put', data, function(data) {
                    CloudFaces.Notifications.Settings.showStatusPopup(true, CloudFaces.Language.translate('notifications_settings_saved'));
                    CloudFaces.Notifications.Settings.reloadContent();
                });
            }
        },

        showSettingsButton: function addSettingsButton() {
            $('#notifications .header_page').append(
                '<div class="notifications-settings-button">' +
                '<span>' +
                CloudFaces.Language.translate('notifications_settings') +
                '</span>' +
                '</div>');
        },

        generateDaySetting: function generateDaySetting(i, daysSettings, receiveAllDays) {
            var nameParam = 'day' + i;
            var checked = receiveAllDays || (daysSettings.indexOf('' + i) > -1) ? 'checked="checked"' : '';

            return '<input type="checkbox" id="' + nameParam + '" name="' + nameParam + '" value="' + i + '" ' + checked + ' />' +
                '<label for="' + nameParam + '">' +
                CloudFaces.Language.translate('notifications_settings_day' + i) +
                '</label>';
        },

        showDaysSettings: function showDaysSettings(settings) {
            var $content = '';
            var daysCnt = 7;
            var daysSettings = [];
            var receiveAllDays = typeof settings.id === 'undefined' ? true : false;
            var mon = CloudFaces.Notifications.Config.firstDayMon;

            if (settings.days && settings.days != '') {
                daysSettings = settings.days.split(',');
            }

            $content = $('<div class="notifications-settings-days-wrapper">' +
                '<div class="notifications-settings-title">' +
                CloudFaces.Language.translate("notifications_settings_days_desc") +
                '</div>' +
                '<div class="settings-days"></div>' +
                '</div>');

            var $settingsContainer = $content.find('.settings-days');

            for (var i = (mon ? 1 : 0); i < daysCnt; i++) {
                $settingsContainer.append(CloudFaces.Notifications.Settings.generateDaySetting(i, daysSettings, receiveAllDays));
            }

            if (CloudFaces.Notifications.Config.firstDayMon) {
                $settingsContainer.append(CloudFaces.Notifications.Settings.generateDaySetting(0, daysSettings, receiveAllDays));
            }

            return $content;
        },

        showCategorySettings: function showCategorySettings($container, settings) {
            var $content = '';
            var $tmpContent = $('<div />');
            var categoriesSettings = [];
            var defaultOn = true;

            if (settings.categories && settings.categories != '') {
                defaultOn = false;
                categoriesSettings = settings.categories.split(',');
            }

            $container.append($tmpContent);

            CloudFaces.API.query(CloudFaces.Notifications.Config.categoriesUrl, 'GET', '', function(categories) {

                if (categories && $.isArray(categories) && categories.length > 0) {
                    $content = $('<div class="notifications-settings-category-wrapper">' +
                        '<div class="notifications-settings-title">' +
                        CloudFaces.Language.translate("notifications_settings_category_desc") +
                        '</div>' +
                        '<div class="settings-categories"></div>' +
                        '</div>');

                    var $settingsContainer = $content.find('.settings-categories');
                    var $categoryTbl = $('<table></table>');
                    $settingsContainer.append($categoryTbl);

                    $.each(categories, function(inx, category) {
                        var nameParam = 'cat' + inx;
                        var checked = (categoriesSettings.indexOf('' + category.id) > -1) ? 'checked="checked"' : '';
                        $categoryTbl.append('<tr class="' + (checked ? 'active' : '') + '"><td>' +
                            category.name +
                            '</td>' +
                            '<td>' +
                            '<input type="checkbox" id="' + nameParam + '" name="' + nameParam + '" value="' + category.id + '" ' + checked + ' />' +
                            '<label for="' + nameParam + '">' +
                            '</label>' +
                            '</td>' +
                            '</tr>');
                    });

                    $tmpContent.append($content);
                } else {
                    $tmpContent.remove();
                }
            });
        },

        loadContent: function loadContent(container, reload) {
            CloudFaces.Notifications.Settings.container = container;

            var $container = $(container);
            if ($container.length) {
                var ain = CloudFaces.DeviceCode.getDeviceAin();
                CloudFaces.API.query(CloudFaces.Notifications.Config.settingsUrl(ain), 'GET', '', function(settings) {
                    if (settings && $.isArray(settings) && settings.length) {
                        settings = settings[0];
                    }


                    if (settings.id) {
                        $('#notificationsSettings').data('id', settings.id);
                    }

                    if (CloudFaces.Notifications.Config.showDaysSettings) {
                        var $content = CloudFaces.Notifications.Settings.showDaysSettings(settings);
                        $container.append($content);
                    }

                    if (CloudFaces.Notifications.Config.showCategorySettings) {
                        CloudFaces.Notifications.Settings.showCategorySettings($container, settings);
                    }

                    $container.append('<div class="notifications-settings-save" type="button">' + CloudFaces.Language.translate('notifications_settings_save') + '</div>');

                    if (reload != true) {
                        CloudFaces.Notifications.Settings.events();
                    }
                    CloudFaces.Helpers.hideLoader();

                });
            }
        },

        events: function events() {
            if (CloudFaces.Notifications.Settings.init) {
                $(document).on('click', '.notifications-settings-save', function() {
                    CloudFaces.Notifications.Settings.save();
                });

                $(document).on('click', '.settings-categories label', function() {
                    var $el = $(this);
                    if ($el.parent().find('input').is(':checked')) {
                        $el.closest('tr').removeClass('active');
                    } else {
                        $el.closest('tr').addClass('active');
                    }
                });

                $(document).on('click', '.notification-settings-status-popup', function() {
                    CloudFaces.Notifications.Settings.hideStatusPopup(0);
                });

                CloudFaces.Notifications.Settings.init = false;
            }
        }
    }
});

$(document).on('cf-initialized', function() {
    CloudFaces.Notifications.events();

});
