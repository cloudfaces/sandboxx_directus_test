if (typeof(CloudFaces.Notifications) == 'undefined') CloudFaces.Notifications = {Config: {}};
$.extend(CloudFaces.Notifications.Config,{
  notificationsUrl: CloudFaces.Config.ApiURL + 'notifications/',
  showDaysSettings: true,
  showCategorySettings: true,
  categoriesUrl: 'project/id/' + CloudFaces.Config.app + '/push/categories/',
  firstDayMon: true,
  settingsUrl: function(ain){return 'project/id/' + CloudFaces.Config.app + '/ain/' + ain + '/push/settings/';},
  pages: {
    main: "notifications_main",
    settings: "notifications_settings"
  }
});