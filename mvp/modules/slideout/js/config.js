if (typeof(CloudFaces.Slideout) == 'undefined') CloudFaces.Slideout = {Config: {}};
$.extend(CloudFaces.Slideout.Config,{
  addButtnCart: true,
  addLogOutButtn: false,
  footerMtelBtn: false,
  footerBtnLink: "http://www.mtel.bg/smart-app",
  dataLangKeys: "login, profile, login,           sponsor_or_sponsoree, profile_build, questions, matches",
  navigationID: "login_login, login_profil, login,sponsor_or_sponsoree, profile_build, questions, matches",
  pages: {
    navdrawer: "slideout_navdrawer",
    main: "slideout_main"
  }
});