==========15.03.2018==========
v1.0.9 - changed structure some additional fields required for OAUTH

==========22.01.2018==========
v1.0.8 - changed colors and icons to forntAwesome
added placeholders with icons from fontAwesome

==========20.10.2017==============
v1.0.7 - add optional for multilingual support for profile templates.
not right way, but at the moment we can't edit the structure of fields in cms
look in templateProfileFields.js ==>> radioBttns

==========15.08.2017==============
v1.0.6 - add optional splash loader

==========04.07.2017==============
some fixes on email reset and Profile link in slideout

==========19.05.2017==============
add rules for no show 
-item_token
-item_order
-item_parent

==========11.05.2017==============
v1.0.4 - add rules for taxi app

==========24.02.2017==============
v1.0.1 - after login send to data base app_token - line 196
