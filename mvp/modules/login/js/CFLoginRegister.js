/*v1.0.4*/
$.extend(CloudFaces.Login, {
    init: true,
    newArrayForSaveprofile: [],
    loginPage: {
        loadContent: function (container) {
            userD = CloudFaces.Helpers.getUserData();

            // if (userD != '') {
            //     CFMenuNavigation.navigate(CloudFaces.Login.Config.pages.profile, '')
            // }

            $('#ip-container').each(function () {
                if (typeof CFNavigation.getNavigationContext() != 'undefined' && CFNavigation.getNavigationContext()) {
                    CloudFaces.Helpers.showLoader();
                    var header = $.parseJSON(CFNavigation.getNavigationContext());
                    if (typeof header['hide'] != 'undefined' && header['hide']) {
                        $('.ip-header').hide().remove();
                        CloudFaces.Helpers.hideLoader();
                    }
                }
            });

            var $container = $(container);
            $element = $(
                '<div class="container pb-80">' +
                '<div class="my-50">' +
                '<h1 class="title-primary">Hi there!</h1>' +
                '<h3 class="title-secondary">Let’s get you in</h3>' +
                '</div>' +
                '<div class="form-group mb-15">' +
                '<label class="text-sm" for="emailInput">' + CloudFaces.Language.translate('email') + ':</label>' +
                '<input class="email-input required-input form-control" id="emailInput" type="email"  aria-describedby="emailHelp" placeholder="your@email.com">' +
                '<span class="err-taken-mail">' + CloudFaces.Language.translate('not_valide_email') + '</span>' +
                '</div>' +
                '<div class="form-group mb-15">' +
                '<label class="text-sm" for="passInput">' + CloudFaces.Language.translate('password') + ':</label>' +
                '<input class="form-control required-input" type="password" id="passInput"  type="password"  aria-describedby="password" placeholder="password"/>' +
                '<span class="err-taken-password">' + CloudFaces.Language.translate('add_password') + '</span>' +
                '</div>' +
                '<label class="forgot-pass text-sm font-bold mt-30" onclick="CFNavigation.navigate(\'' + CloudFaces.Login.Config.pages.forgotPassword + '\', \'\');">' +
                CloudFaces.Language.translate('forgottenPassword') +
                '</label>' +
                '<div class="btn-fixed-footer">' +
                //
                '<button class="login-btn disabled-btn btn btn-primary" onclick="CFMenuNavigation.navigate(\'matches\', \'\')">' + CloudFaces.Language.translate('login') + '</button>' +
                '<div class="reg-btn-wrapper" onclick="CFNavigation.navigate(\'' + CloudFaces.Login.Config.pages.register + '\', \'\');">' +
                '<label class="register-btn  d-block text-sm font-bold text-center pt-20 pb-10">' + CloudFaces.Language.translate('create_account') + '</label>' +
                '</div>' +
                '</div>' +
                '</div>'
            );

            $container.append($element);
            new CloudFaces.Analytics('login', 'Login').set(function (result) {
            });
        }
    },

    registerTermsPage: {
        loadContent: function (container) {
            var $container = $(container);

            $element = $(
                '<h1>' + CloudFaces.Language.translate('register_terms') + '</h1>' +
                '<p>' + CloudFaces.Language.translate('register_terms_text') + '</p>' +
                '<button onclick="CFNavigation.navigateBack(\'\');">' + CloudFaces.Language.translate('back') + '</button>'
            );

            $container.append($element);
            new CloudFaces.Analytics('registerTerms', 'RegisterTerms').set(function (result) {
            });
        }
    },

    registerPage: {
        loadContent: function (container) {
            var $container = $(container);

            $element = $(
                '<div class="container pb-80">' +
                '<div class="my-30">' +
                '<h1 class="title-primary">Let’s create your account</h1>' +
                '</div>' +
                '<div class="form-group mb-15">' +
                '<label  class="text-sm">' + CloudFaces.Language.translate('email') + ':</label>' +
                '<input class="form-control email-input required-input" type="text" aria-describedby="emailInput" placeholder="your@email.com"/>' +
                '<span class="err-taken-mail">' + CloudFaces.Language.translate('not_valide_email') + '</span>' +

                '</div>' +
                '<div class="form-group mb-15">' +
                '<label  class="text-sm">' + CloudFaces.Language.translate('password') + ':</label>' +
                '<input class="form-control password-input required-input" type="password" aria-describedby="password" placeholder="password"/>' +
                '<span class="err-taken-password">' + CloudFaces.Language.translate('add_password') + '</span>' +

                '</div>' +
                '<div class="form-group mb-15">' +
                '<label class="text-sm">' + CloudFaces.Language.translate('confirm_password') + ':</label>' +
                '<input class="form-control password-confirm required-input" type="password" aria-describedby="password" placeholder="password"/>' +
                '<span class="err-taken-password">' + CloudFaces.Language.translate('password') + '</span>' +

                '</div>' +
                '<div class="btn-fixed-footer">' +
                '<button class="register-btn disabled-btn btn btn-primary" onclick="CFMenuNavigation.navigate(\'sponsor_or_sponsoree\', \'\')">' + CloudFaces.Language.translate('next') + '</button>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>'
            );

            $container.append($element);
            CloudFaces.Login.registerPage.events();
            new CloudFaces.Analytics('register', 'Register').set(function (result) {
            });
        },
        events: function () {
            $('.register-btn').on('click', function () {
                var password = $('.password-input').val();
                var password_confirm = $('.password-confirm').val();
                var email = $(".email-input").val();
                CloudFaces.Login.handleRegister(email, password, password_confirm);
            });
        }
    },

    handleRegister: function (email, password, password_confirm) {
        if (CloudFaces.Login.checkPasswords(password, password_confirm)) {
            var sendData = {
                email: email,
                password: password
            };

            if (typeof items != 'undefined' && items.length > 0) {
                $('.email-input').addClass('taken-mail');
            } else {
                $('.email-input').removeClass('taken-mail');
                $('.err-taken-password').hide()
                CloudFaces.Login.checkEmail(email, function (company_id) {
                    CloudFaces.Login.registerSaveData(sendData, company_id);
                });
            }

        }
    },

    checkEmail: function (email, callback) {
        var email_data = email.split('@');

        if (email_data.length < 2) {
            alert('Incorrect email');
            return false;
        }
        CloudFaces.API.list.getItems(CloudFaces.Config.app, 20487, '', function (items) {
            if (!items.length) {
                alert('Incorrect email');
                return false;
            }
            callback(items[0].id);
        }, {
            filters: [
                {
                    col_name: "domain",
                    operator: "=",
                    value: email_data[1]
                }
            ]
        })

    },
    assignToCompany: function (user_id, company_id, callback) {
        CloudFaces.API.relations.addRelation(313, 20487, company_id, 20481, user_id, callback);
    },

    checkPasswords: function (password, confirm_password) {
        if (password != '') {
            if (password.length < 3) {
                alert(CloudFaces.Language.translate('small_password'))
                return false;
            }
            else if (password != confirm_password) {
                alert('Passwords dont match');
                return false;
            }
            else {
                return true;
            }
        }
        else {
            $('.register-btn').effect("shake")
        }
    },

    registerSaveData: function (sendData, company) {
        sendData.device_indentifier = CloudFaces.DeviceCode.getDeviceToken();

        CloudFaces.API.User.register(sendData, function (userData) {
            if (userData.Status == "Error" || userData.isException) {
                $('.err-taken-mail').show();
                $('.err-taken-mail').text(CloudFaces.Language.translate(userData.message_long));
                $('.register-btn').effect("shake")
            } else {
                $('.err-taken-mail').hide();
                CloudFaces.Login.assignToCompany(userData, company, function () {
                    CloudFaces.Login.startLogin(sendData);
                });
            }
        })
    },

    isValidEmail: function (email) {
        return /\S+@\S+\.\S+/.test(email);
    },

    // startLogin: function (sendData) {
    //     sendData.app_token = CFPersistentStorage.valueExists(CloudFaces.Config.app + '_device_token') ? CFPersistentStorage.readValue(CloudFaces.Config.app + '_device_token') : '';
    //     CloudFaces.API.User.Login(sendData, function (userData) {
    //         if (userData.Status == "Error") {
    //             $('.wrong_email').remove();
    //             $('.err-taken-mail').show()
    //             $('.err-taken-mail').text(CloudFaces.Language.translate(userData.message_long));
    //             $('.register-btn').effect("shake")
    //         } else {
    //             CFPersistentStorage.writeValue(CloudFaces.Config.app + '_userData', JSON.stringify(userData));
    //             if (CloudFaces.Login.Config.addRulesForTaxi) {
    //                 if (JSON.parse(userData.category_id) == CloudFaces.Settings.Config.driverCategory) {
    //                     CFMenuNavigation.navigate('chooseCar', '');
    //                 } else if (JSON.parse(userData.category_id) == CloudFaces.Settings.Config.drispatcher) {
    //                     CFMenuNavigation.navigate('orders', '');
    //                 } else {
    //                     CFMenuNavigation.navigate(CloudFaces.Login.Config.navigatePageAfterLogin, '')
    //                 }
    //             } else {
    //                 CFMenuNavigation.navigate(CloudFaces.Login.Config.navigatePageAfterLogin, '')
    //             }
    //         }
    //     })
    // },

    ressetPassword: function (content) {
        var $container = $(content);

        $container.append(
            '<div class="container pb-80">' +
            '<div class="my-50">' +
            '<h1 class="title-primary">Forgot password</h1>' +
            '<h3 class="title-secondary">Let’s get you in</h3>' +
            '</div>' +
            '<div class="form-for-key form-group mb-15">' +
                '<label class="text-sm" for="emailForgottenInput">' +
                CloudFaces.Language.translate('email') +': </label>' +
                '<input type="text" class="form-control email-for-key" name="email" id="emailForgottenInput" aria-describedby="emailInput" placeholder="your@email.com"/>' +
                '<span class="err-taken-mail"></span>' +
            '</div>' +
            '<div class="btn-fixed-footer">' +
                '<button class="get-reset-key disabled-btn btn btn-primary">' + CloudFaces.Language.translate('changePassword') + '</button>' +
            '</div>' +
            '</div>'
        );


        $element = $('<div class="positionDiv ressetForm">' +
            '<div class="left-login">' +

            '<div class="forgotten_pass">' +
            CloudFaces.Language.translate('tokenInfo') +
            '<form class="forgottenPassForm">' +
            '<label for="">' +
            CloudFaces.Language.translate('resetPasswordKey') +
            '<input type="text" name="resetPasswordKey" class="resetKey"/>' +
            '<span class="err-taken-mail"></span>' +
            '</label>' +

            '<label for="">' +
            CloudFaces.Language.translate('newPassword') +
            '<input type="password" name="password"/>' +
            '</label>' +

            '<label for="">' +
            CloudFaces.Language.translate('repeatNewPassword') +
            '<input type="password" name="repeatNewPass"/>' +
            '</label>' +

            '<button class="restPass disabled-btn">' + CloudFaces.Language.translate('changePassword') + '</button>' +
            '</form>' +
            '</div>' +
            '</div>' +
            '</div>');
        $container.append($element);
    },

    profile: function (conteiner) {
        $conteiner = $(conteiner);
        var userCache = CFPersistentStorage.readValue(CloudFaces.Config.app + '_userData');
        if (userCache) {
            userCache = JSON.parse(userCache);
        } else {
            CFMenuNavigation.navigate(CloudFaces.Login.Config.pages.login, '');
            return;
        }

        CloudFaces.API.User.getUserData(userCache.token, function (userData) {
            if (userData && Array.isArray(userData) && userData[0]) {
                userData = userData[0];
            }

            $templatePage = $('<form class="top-section profile"></form>')
            $conteiner.append($templatePage)
            $buttnSave = $('<div class="saveprofile">' + CloudFaces.Language.translate('save_profil') + '</div>')
            // $buttnSave.data('context', JSON.stringify(userData));
            $templatePage.after($buttnSave)
            $('.top-section').outerHeight($(window).height() - $('.saveprofile').outerHeight(true) - $('.header_page').outerHeight(true))

            if (userData && userData.message_long != "error_token") {
                CloudFaces.API.User.profilTemplate(function (profileTemplate) {
                    profileTemplate.sort(function (p1, p2) {
                        return p1.item_order > p2.item_order;
                    });

                    profileTemplate.forEach(function (profileField) {
                        var findEl = true,
                            key = profileField.name_of_field;
                        if (profileField.type != "text") {
                            if (profileField.type == "checkbox") {
                                CloudFaces.Login.Type.checkBoxBttns(userData, key, profileField, function (callback) {
                                    $('.top-section').append(callback)
                                })
                            } else if (profileField.type == "radio") {
                                CloudFaces.Login.Type.radioBttns(userData, key, profileField, function (callback) {
                                    $('.top-section').append(callback)
                                })
                            } else if (profileField.type == "image") {
                                var getDatafromCamera = false,
                                    base64 = false,
                                    isLocal = false,
                                    getDatafromCamera = false
                                CloudFaces.Login.Type.imageBttns(userData, key, profileField, getDatafromCamera, isLocal, base64, function (callback) {
                                    $('.top-section').prepend(callback)
                                    // ios fix 270* rotation
                                    if ($('body').hasClass('ios')) {
                                        $('.profile-iamge-inner').addClass('ios-rotate-270');
                                    }
                                })
                            }

                            findEl = false;
                            return false;
                        }

                        if (findEl) {
                            text = userData[key];
                            if (text == null) {
                                text = ''
                            }
                            if (key != 'item_token' && key != 'item_order' && key != 'item_parent') {
                                $('.top-section').append('<label class="' + key + '">' + CloudFaces.Language.translate(profileField.text_in_fields) + ':' +
                                    '<input class="email-input" name="' + key + '" type="text" value="' + text + '"/>' +
                                    '<span class="err-taken-mail">' + CloudFaces.Language.translate('not_valide_email') + '</span>' +
                                    '</label>');
                            }
                        }
                    })
                })
            } else {
                CFPersistentStorage.writeValue(CloudFaces.Config.app + '_userData', '');
                CFMenuNavigation.navigate(CloudFaces.Login.Config.pages.login, '')
            }
        })
    },
    popupStatus: function (text, status) {
        img = '<i class="fa fa-times-circle" aria-hidden="true"></i>';
        status ? img = '<i class="fa fa-check-circle" aria-hidden="true"></i>' : '';
        $('.popup-feedback').remove();
        $('body').append(
            '<div class="popup-feedback">' +
            '<div class="popup-feedback-inner">' +
            img +
            CloudFaces.Language.translate(text) +
            '</div>' +
            '</div>'
        );
        $(".popup-feedback").animate({
            opacity: 0.25,

        }, 4000, function () {
            // Animation complete.
            $(this).remove();
        });
    },

    saveImageprofile: function (imgUrl, key, id, token) {
        var imageSendRequest = new CFHttpRequest();
        imageSendRequest.Method = "POST";
        imageSendRequest.Url = CloudFaces.Config.ApiURL + 'project/id/' +
            CloudFaces.Config.app + '/lists/' +
            CloudFaces.Login.Config.tables.users + '/item/' +
            id + '/columnname/' + key + '/';
        imageSendRequest.BinaryFile = imgUrl;
        imageSendRequest.sendBinary(function (result) {
        });
    },

    getPermission: function (permissionType, callback) {
        if ($('body').hasClass('ios')) {
            callback(true);
        } else {
            CFPrivacy.checkPermission(permissionType, function (enabled) {
                if (!enabled) {
                    CFPrivacy.requestPermission(permissionType, function (granted) {
                        callback(true);
                        // return granted;
                    });
                } else {
                    // permission granted , this block will execute always once permission is granted
                    // do not forget to include the logic here also
                    callback(true);
                }
            });
        }
    },

    events: function () {
        if (CloudFaces.Login.init) {
            // $(document).on('click', '.login-btn', function () {
            //     if (CloudFaces.Login.checkPasswords($('input[type="password"]').val(), $('input[type="password"]').val())) {
            //         var userData = {
            //             'email': $('.email-input').val(),
            //             'password': $('input[type="password"]').val()
            //         };
            //
            //         if (!CloudFaces.Login.isValidEmail(userData.email)) {
            //             $('.email-input').addClass('not-valid');
            //             $('.err-taken-mail').show();
            //             $('.login-btn').effect("shake");
            //             $('.wrong_email').remove();
            //             return;
            //         } else {
            //             $('.err-taken-mail').hide();
            //             $('.email-input').removeClass('not-valid');
            //         }
            //
            //         CloudFaces.Login.startLogin(userData);
            //         // CloudFaces.Slideout.Config.addLogOutButtn();
            //     }
            // });

            $(document).on('click', '.feedback-popup', function () {
                $('.feedback-popup').remove();
            });

            $(document).on('input', '.required-input', function () {
                var inputs = $('.required-input');
                var checked = 0;
                inputs.each(function (inx, val) {
                    if ($(val).val()) {
                        checked++;
                    }
                    ;

                });
                if (inputs.length == checked) {
                    $('.disabled-btn').removeClass('disabled-btn');
                }

                $('.feedback-popup').remove();
            });

            $(document).on('click', '.restPass', function (e) {
                e.preventDefault();
                if ($('.resetKey').val() != 0) {

                    var forgottenPassForm = $('.forgottenPassForm').serializeArray();
                    var newPass = '';
                    var repeatNewPass = '';
                    $.each(forgottenPassForm, function (inx, obj) {
                        if (obj.name == "password") {
                            newPass = obj.value;
                        } else if (obj.name == "repeatNewPass") {
                            repeatNewPass = obj.value;
                        }
                    });

                    var invalidPass = false;
                    if (newPass != repeatNewPass) {
                        invalidPass = true;
                        CloudFaces.Login.popupStatus('password_not_same', false);
                    }

                    if (newPass.length < 3) {
                        invalidPass = true;
                        CloudFaces.Login.popupStatus('small_password', false);
                    }

                    if (newPass.length > 20) {
                        invalidPass = true;
                        CloudFaces.Login.popupStatus('long_password', false);
                    }

                    if (!invalidPass) {
                        CloudFaces.API.User.resetPasswaord($('.resetKey').val(), forgottenPassForm, function (userData) {
                            if (userData.message_long) {
                                $('.positionDiv.ressetForm .err-taken-mail').show().text(CloudFaces.Language.translate('reset_not_valide_password'))
                            } else {
                                CloudFaces.Login.popupStatus('success_save_profil', true);
                                CFMenuNavigation.navigate(CloudFaces.Login.Config.pages.login, '')
                            }
                        })
                    }
                } else {
                    $('.positionDiv.ressetForm .err-taken-mail').show().text(CloudFaces.Language.translate('no_add_key'))
                }
            });

            $(document).on('click', '.get-reset-key', function () {
                if (CloudFaces.Login.isValidEmail($('.form-for-key input').val())) {
                    var data = {
                        'email': $('.form-for-key input').val(),
                        'email_sender': CloudFaces.Language.translate('sender_email_reset'),
                        'email_body': CloudFaces.Language.translate('text_email_reset_password'),
                        'email_subject': CloudFaces.Language.translate('email_subject')
                    };

                    CloudFaces.API.User.getKey(data, function (userData) {
                        if (userData.message_long) {
                            $('.form-for-key .err-taken-mail').show().text(CloudFaces.Language.translate(userData.message_long))
                        } else {
                            $('.form-for-key').hide();
                            $('.ressetForm').show();
                        }
                    })
                } else {
                    $('.form-for-key .err-taken-mail').show().text(CloudFaces.Language.translate('not_valid_email'))
                }
            });

            $(document).on('click', '.saveprofile', function () {
                var userprofile = $('.profile').serializeArray();
                var userCache = CFPersistentStorage.readValue(CloudFaces.Config.app + '_userData');
                if (userCache) {
                    userCache = JSON.parse(userCache);
                } else {
                    CFMenuNavigation.navigate(CloudFaces.Login.Config.pages.login, '');
                    return;
                }

                CloudFaces.API.User.getUserData(userCache.token, function (userData) {
                    if (userData && Array.isArray(userData) && userData[0]) {
                        userData = userData[0];
                    }

                    var newArry = [];
                    for (var y = 0; y < userprofile.length; y++) {
                        addNew = true;
                        for (var i = 0; i < newArry.length; i++) {
                            if (newArry[i].name == userprofile[y].name) {
                                newArry[i].value += '| ' + userprofile[y].value;
                                addNew = false;
                            }
                        }

                        if (addNew) {
                            newArry.push(userprofile[y])
                        }

                        if (y == userprofile.length - 1) {
                            var addToCFPersistentStorage = userData;
                            $.each(newArry, function (ind, val) {
                                var name = val.name
                                addToCFPersistentStorage[name] = val.value;
                            });

                            newArry.push({
                                name: 'id',
                                value: userData.id
                            })

                            valueImg = $('.profile-iamge-inner').data('name');
                            imgSrc = $('.profile-iamge-inner').data('url')
                            if (imgSrc) {
                                CloudFaces.Login.saveImageprofile(imgSrc, valueImg, userData.id, userData.token)
                            }

                            CFPersistentStorage.writeValue(CloudFaces.Config.app + '_userData', JSON.stringify(addToCFPersistentStorage));
                            CloudFaces.API.User.update(newArry, userData.id, userData.token, function (addToCFPersistentStorage) {
                                CloudFaces.Login.popupStatus('success_save_profil', true);
                            })
                        }
                    }
                })
            })

            //add profile image
            $(document).on('click', '.change-iamge, .addImage', function () {
                key = $(this).data('name');
                CloudFaces.Login.getPermission('camera', function (callbackCamera) {
                    CloudFaces.Login.getPermission('storage', function (callbackStorage) {
                        if (callbackCamera && callbackStorage) {
                            CloudFaces.Helpers.showLoader();
                            var isLocal = location.href.charAt(0) == 'f';

                            if (isLocal) {
                                defaultResult = "URL";
                            } else {
                                defaultResult = "DATA";
                            }

                            CFPictureChooser.capturePicture('PHOTO', 'URL', function (callback) {
                                if (typeof callback != 'undefined' && callback != null) {
                                    var imageUri = callback;
                                    getDatafromCamera = true;
                                    base64 = false;
                                    if (isLocal) {
                                        CloudFaces.Login.Type.imageBttns(imageUri, key, '', getDatafromCamera, isLocal, base64, function (callback) {
                                            $('.top-section').prepend(callback)
                                        })

                                    } else {
                                        CFPictureChooser.getPictureData(imageUri, function (data) {
                                            base64 = data
                                            CloudFaces.Login.Type.imageBttns(imageUri, key, '', getDatafromCamera, isLocal, base64, function (callback) {
                                                $('.top-section').prepend(callback)
                                            })
                                        });
                                    }

                                    $('.addImage').waitForImages(function () {
                                        $('.addImage').outerHeight($('.addImage').outerWidth());
                                        $('.claims_photo').outerHeight($('.addImage').outerWidth());
                                        CloudFaces.Helpers.hideLoader();
                                    });
                                } else {
                                    CloudFaces.Helpers.hideLoader();
                                }
                            });
                        }
                    })
                })
            });

            $(document).on('click', '.remove-image', function () {
                var thisUrl = $(this).data('url');
                if (thisUrl != '') {
                    CFPictureChooser.deletePicture(thisUrl);
                }
                $('.profile-iamge').css('background', 'url(images/profile.png)');
                $('.profile-iamge-inner').css('background', 'url(images/profile.png)');
                $('.remove-image').hide();
                $('.change-iamge').css({
                    'float': 'none',
                    'margin': '2% auto'
                });

                // delete profile pic from CMS
                var userData = CloudFaces.Helpers.getUserData();
                userData.Image = '';
                CloudFaces.API.User.update(userData, userData.id, userData.token, function (addToCFPersistentStorage) {
                    CloudFaces.Login.popupStatus('success_save_profil', true);
                })
            });

            CloudFaces.Login.init = false;
        }
    },
});

$(document).on('cf-initialized', function () {
    CloudFaces.Login.events();
});
